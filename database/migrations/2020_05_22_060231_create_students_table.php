<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateStudentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('students', function (Blueprint $table) {
            $table->id();
            $table->foreignId('family_id')->constrained()->onDelete('cascade');
            $table->foreignId('profile_id')->constrained()->onDelete('cascade');
            
            $table->integer('shirt_size')->nullable();
            $table->integer('pant_size')->nullable();
            $table->integer('shoe_size')->nullable();
            $table->integer('weight')->nullable();
            $table->integer('height')->nullable();
            $table->string('blood_type')->nullable();
            $table->boolean('motor_deficiency')->default(0);
            $table->boolean('intellectual_deficiency')->default(0);
            $table->boolean('hearing_impairment')->default(0);
            $table->boolean('visual_deficiency')->default(0);
            $table->boolean('respiratory_deficiency')->default(0);
            $table->text('health_problem')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('students');
    }
}
