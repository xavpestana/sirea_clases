<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class ChangeTablePayments extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

        Schema::dropIfExists('payments');
        if (!Schema::hasTable('payments')) {
          Schema::create('payments', function (Blueprint $table) {
                $table->id();
                $table->foreignId('student_id')->constrained()->onDelete('cascade')
                      ->comment('relación con la tabla estudiantes');
                $table->foreignId('school_id')->constrained()->onDelete('cascade')
                      ->comment('relación con la tabla escuela');
                $table->bigInteger('number_invoice')->default(0)->comment('Control del número de factura');
                $table->string('identity', 100)->comment('CI o rif');
                $table->string('name', 400)->comment('Nombre o razon social');
                $table->string('address', 400)->comment('Direccion');
                $table->string('phone', 50)->comment('Telefono');
                $table->string('school_bank_account', 100)->comment('Cuenta del banco receptor');
                $table->foreignId('payment_method_id')->constrained()->onDelete('cascade')
                      ->comment('relación con la tabla métodos de pago');
                $table->float('amount', 50, 2)->comment('Monto del pago');
                $table->string('coin')->comment('Moneda');
                $table->float('amount_bsf', 50, 2)->comment('Monto del pago en bolivares');
                $table->float('change_amount_USD', 50, 2)->nullable()->comment('Monto del cambio en dolares');
                $table->float('change_amount_COP', 50, 2)->nullable()->comment('Monto del cambio en pesos');
                $table->float('credit', 50, 2)->nullable()->comment('Abono');
                $table->string('coin_credit')->nullable()->comment('Moneda del abono');
                $table->boolean('verified')->default(false)->comment('Indica si el pago fue verificado');
                $table->boolean('annulled')->default(false)->comment('Indica si el pago fue anulado');
                $table->string('transfer_code', 100)->nullable()->comment('Codigo de Transferencia');
                $table->timestamps();
            });
        }
            
        
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
         Schema::dropIfExists('payments');
    }
}
