<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

/**
 * @class CreateReceiptsTable
 * @brief Crear tabla de recibos de pago
 *
 * Gestiona la creación o eliminación de la recibos de pago
 *
 * @author William Páez <paez.william8@gmail.com>
 * @license <a href='​http://www.gnu.org/licenses/gpl-2.0.html'>GNU Public License versión 2 (GPLv2)</a>
 */
class CreateReceiptsTable extends Migration
{
    /**
     * Método que ejecuta las migraciones
     *
     * @author William Páez <paez.william8@gmail.com>
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable('receipts')) {
            Schema::create('receipts', function (Blueprint $table) {
                $table->id();
                $table->text('name')->comment('Descripción del recibo de pago');
                $table->decimal('amount', 20, 2)->comment('Monto del recibo de pago');
                $table->boolean('paid')->default(false)->comment('Indica si se hizo este pago');
                $table->foreignId('enrollment_id')->constrained()->onDelete('cascade')
                      ->comment('relación con la tabla inscripciones');
                $table->timestamps();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('receipts');
    }
}
