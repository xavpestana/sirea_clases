<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class RemoveFieldSeasonsIdToSectionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('sections', function (Blueprint $table) {
            if (Schema::hasColumn('sections', 'seasons_id')) {
                $table->dropForeign(['seasons_id']);
                $table->dropColumn('seasons_id');
            }
            if (!Schema::hasColumn('sections', 'season_id')) {
                $table->foreignId('season_id')->nullable()->constrained()->onDelete('cascade');
            }
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('sections', function (Blueprint $table) {
            if (!Schema::hasColumn('sections', 'seasons_id')) {
                $table->foreignId('seasons_id')->nullable()->constrained()->onDelete('cascade');
            }
            if (Schema::hasColumn('sections', 'season_id')) {
                $table->dropForeign(['season_id']);
                $table->dropColumn('season_id');
            }
        });
    }
}
