<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddFieldsToStudentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('students', function (Blueprint $table) {
            if (!Schema::hasColumn('students', 'chest_size')) {
                $table->string('chest_size')->nullable()->comment('ancho de pecho');
            }
            if (!Schema::hasColumn('students', 'arm_size')) {
                $table->string('arm_size')->nullable()->comment('ancho de pecho');
            }        
            if (!Schema::hasColumn('students', 'medical_report_special')) {
                $table->boolean('medical_report_special')->default(0)->comment('informe medico compromiso especial');
            }
            if (!Schema::hasColumn('students', 'practice_sports')) {
                $table->boolean('practice_sports')->default(0)->comment('impedimento para practicar deportes');
            } 
            if (!Schema::hasColumn('students', 'diabetes')) {
                $table->boolean('diabetes')->default(0)->comment('diabetes');
            } 
            if (!Schema::hasColumn('students', 'epilepsy')) {
                $table->boolean('epilepsy')->default(0)->comment('epilepsia');
            } 
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('students', function (Blueprint $table) {
            if (Schema::hasColumn('students', 'chest_size')) {
                $table->dropColumn('chest_size');
            }
            if (Schema::hasColumn('students', 'arm_size')) {
                $table->dropColumn('arm_size');
            }
            if (Schema::hasColumn('students', 'medical_report_special')) {
                $table->dropColumn('medical_report_special');
            }
            if (Schema::hasColumn('students', 'practice_sports')) {
                $table->dropColumn('practice_sports');
            }
            if (Schema::hasColumn('students', 'diabetes')) {
                $table->dropColumn('diabetes');
            }
            if (Schema::hasColumn('students', 'epilepsy')) {
                $table->dropColumn('epilepsy');
            }
        });
    }
}
