<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

/**
 * @class CreateEnrollmentsTable
 * @brief Crear tabla de inscripciones
 *
 * Gestiona la creación o eliminación de la tabla inscripciones
 *
 * @author William Páez <paez.william8@gmail.com>
 * @license <a href='​http://www.gnu.org/licenses/gpl-2.0.html'>GNU Public License versión 2 (GPLv2)</a>
 */
class CreateEnrollmentsTable extends Migration
{
    /**
     * Método que ejecuta las migraciones
     *
     * @author William Páez <paez.william8@gmail.com>
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable('enrollments')) {
            Schema::create('enrollments', function (Blueprint $table) {
                $table->id();
                $table->foreignId('student_id')->constrained()->onDelete('cascade')
                      ->comment('relación con la tabla usuarios estudiantes');
                $table->foreignId('section_id')->constrained()->onDelete('cascade')
                      ->comment('relación con la tabla secciones');
                $table->foreignId('plan_id')->constrained()->onDelete('cascade')
                      ->comment('relación con la tabla planes de pago');
                $table->timestamps();
            });
        }
    }

    /**
     * Método que elimina las migraciones
     *
     * @author William Páez <paez.william8@gmail.com>
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('enrollments');
    }
}
