<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class ChangeFieldsToStudentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('students', function (Blueprint $table) {
            if (Schema::hasColumn('students', 'shirt_size')) {
                $table->string('shirt_size')->change()->comment('talla de camisa');
            }
            if (Schema::hasColumn('students', 'pant_size')) {
                $table->string('pant_size')->change()->comment('talla de camisa');
            }
            if (Schema::hasColumn('students', 'shoe_size')) {
                $table->string('shoe_size')->change()->comment('talla de camisa');
            }
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('students', function (Blueprint $table) {
            if (Schema::hasColumn('students', 'shirt_size')) {
                $table->integer('shirt_size')->change()->comment('talla de camisa');
            }
            if (Schema::hasColumn('students', 'pant_size')) {
                $table->integer('pant_size')->change()->comment('talla de camisa');
            }
            if (Schema::hasColumn('students', 'shoe_size')) {
                $table->integer('shoe_size')->change()->comment('talla de camisa');
            }
        });
    }
}
