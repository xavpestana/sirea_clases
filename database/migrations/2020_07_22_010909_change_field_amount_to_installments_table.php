<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

/**
 * @class ChangeFieldAmountToInstallmentsTable
 * @brief Cambia el campo amount en la tabla cuotas
 *
 * Gestiona cambios en campos de la tabla cuotas
 *
 * @author William Páez <paez.william8@gmail.com>
 * @license <a href='​http://www.gnu.org/licenses/gpl-2.0.html'>GNU Public License versión 2 (GPLv2)</a>
 */
class ChangeFieldAmountToInstallmentsTable extends Migration
{
    /**
     * Método que cambia el campo amount de float a decimal
     *
     * @author William Páez <paez.william8@gmail.com>
     * @return void
     */
    public function up()
    {
        Schema::table('installments', function (Blueprint $table) {
            if (Schema::hasColumn('installments', 'amount')) {
                $table->decimal('amount', 20, 2)->change();
            }
        });
    }

    /**
     * Método que cambia el campo amount de decimal a float
     *
     * @author William Páez <paez.william8@gmail.com>
     * @return void
     */
    public function down()
    {
        Schema::table('installments', function (Blueprint $table) {
            if (Schema::hasColumn('installments', 'amount')) {
                $table->float('amount', 8, 2)->change();
            }
        });
    }
}
