<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class RemoveFieldsToSchoolsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('schools', function (Blueprint $table) {
            if (Schema::hasColumn('schools', 'principal_name')) $table->dropColumn('principal_name');
            if (Schema::hasColumn('schools', 'principal_document_type')) $table->dropColumn('principal_document_type');
            if (Schema::hasColumn('schools', 'principal_document_number')) $table->dropColumn('principal_document_number');
            if (Schema::hasColumn('schools', 'gender_id')){
                $table->dropForeign(['gender_id']);
                $table->dropColumn('gender_id');
            }
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('schools', function (Blueprint $table) {
            $table->string('principal_name',100)->nullable();
            $table->string('principal_document_type',1)->nullable();
            $table->string('principal_document_number',20)->nullable();
            $table->foreignId('gender_id')->constrained()->onDelete('cascade');
        });
    }
}
