<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

/**
 * @class CreateConceptEnrollmentTable
 * @brief Crear tabla intermedia entre conceptos e inscripciones
 *
 * Gestiona la creación o eliminación de la tabla concept_enrollment
 *
 * @author William Páez <paez.william8@gmail.com>
 * @license <a href='​http://www.gnu.org/licenses/gpl-2.0.html'>GNU Public License versión 2 (GPLv2)</a>
 */
class CreateConceptEnrollmentTable extends Migration
{
    /**
     * Método que ejecuta las migraciones
     *
     * @author William Páez <paez.william8@gmail.com>
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable('concept_enrollment')) {
            Schema::create('concept_enrollment', function (Blueprint $table) {
                $table->id();
                $table->foreignId('concept_id')->constrained()->onDelete('cascade')
                      ->comment('relación con la tabla conceptos');
                $table->foreignId('enrollment_id')->constrained()->onDelete('cascade')
                      ->comment('relación con la tabla inscripciones');
                $table->timestamps();
            });
        }
    }

    /**
     * Método que elimina las migraciones
     *
     * @author William Páez <paez.william8@gmail.com>
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('concept_enrollment');
    }
}
