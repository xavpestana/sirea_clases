<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSeasonsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('seasons', function (Blueprint $table) {
            $table->id();
            $table->foreignId('school_id')->constrained()->onDelete('cascade');

            $table->string('season',20);
            $table->integer('periods_qty');
            $table->integer('max_score');
            $table->boolean('sections_number')->default(0);
            $table->boolean('status')->default(0);
            $table->decimal('initial_pay', 20,2)->nullable();
            $table->string('name_range_1',50)->nullable();
            $table->string('name_range_2',50)->nullable();
            $table->string('name_range_3',50)->nullable();
            $table->string('name_range_4',50)->nullable();
            $table->string('range_1',20)->nullable();
            $table->string('range_2',20)->nullable();
            $table->string('range_3',20)->nullable();
            $table->string('range_4',20)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('seasons');
    }
}
