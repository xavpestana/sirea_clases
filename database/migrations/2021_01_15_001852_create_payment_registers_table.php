<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePaymentRegistersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('payment_registers', function (Blueprint $table) {
            $table->id();
            $table->foreignId('student_id')->onDelete('cascade')
                      ->comment('relación con la tabla estudiantes');
            $table->foreignId('school_id')->onDelete('cascade')
                      ->comment('relación con la tabla escuela');
            $table->foreignId('family_id')->onDelete('cascade')
                      ->comment('relación con la tabla escuela');
            $table->string('identity', 100)->comment('CI o rif');
            $table->string('name', 400)->comment('Nombre o razon social');
            $table->string('address', 400)->comment('Direccion');
            $table->string('phone', 50)->comment('Telefono');
            $table->string('school_bank_account', 100)->comment('Cuenta del banco receptor');
            $table->foreignId('payment_method_id')->onDelete('cascade')
                      ->comment('relación con la tabla métodos de pago');
            $table->float('amount', 50, 2)->comment('Monto del pago');
            $table->string('coin')->comment('Moneda');
            $table->string('transfer_code', 100)->nullable()->comment('Codigo de Transferencia');
            $table->string('observation')->nullable();
            $table->integer('status')->default(0)->comment('Status del pago');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('payment_registers');
    }
}
