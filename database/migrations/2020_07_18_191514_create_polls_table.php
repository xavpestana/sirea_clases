<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePollsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('polls', function (Blueprint $table) {
            $table->id();
            $table->foreignId('family_id')->constrained()->onDelete('cascade');
            $table->timestamps();

            $table->string('companion')->nullable()->comment('Con quien llega al colegio');
            $table->string('arrival')->nullable()->comment('Medio de llegada al colegio');
            $table->string('departure')->nullable()->comment('Medio de salida del colegio');
            $table->string('marital_status')->nullable()->comment('estado civil');
            $table->string('religion')->nullable();
            $table->string('property_type')->nullable()->comment('tipo de propiedad de la vivienda');
            $table->string('home_type')->nullable()->comment('tipo de vivienda');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('polls');
    }
}
