<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

/**
 * @class DropSectionsUsersTable
 * @brief Elimina la tabla intermedia secciones_usuarios
 *
 * Gestiona la creación o eliminación de la tabla Secciones Usuarios
 *
 * @author William Páez <paez.william8@gmail.com>
 * @license <a href='​http://www.gnu.org/licenses/gpl-2.0.html'>GNU Public License versión 2 (GPLv2)</a>
 */
class DropSectionsUsersTable extends Migration
{
    /**
     * Método que elimina la tabla sections_users
     *
     * @author William Páez <paez.william8@gmail.com>
     * @return void
     */
    public function up()
    {
        Schema::dropIfExists('sections_users');
    }

    /**
     * Método que crea la tabla sections_users
     *
     * @author William Páez <paez.william8@gmail.com>
     * @return void
     */
    public function down()
    {
        if (!Schema::hasTable('sections_users')) {
            Schema::create('sections_users', function (Blueprint $table) {
                $table->id();
                $table->foreignId('user_id')->constrained()->onDelete('cascade')
                      ->comment('relación con la tabla usuarios');
                $table->foreignId('section_id')->constrained()->onDelete('cascade')
                      ->comment('relación con la tabla secciones');
                $table->timestamps();
            });
        }
    }
}
