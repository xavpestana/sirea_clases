<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateQuotasCancelledsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('quotas_cancelleds', function (Blueprint $table) {
            $table->id();
            $table->foreignId('payment_id')->onDelete('cascade')
                      ->comment('relación con la tabla estudiantes');
            $table->foreignId('installment_id')->onDelete('cascade')
                      ->comment('relación con la tabla de cuotas');
            $table->foreignId('student_id')->onDelete('cascade')
                      ->comment('relación con la tabla de cuotas');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('quotas_cancelleds');
    }
}
