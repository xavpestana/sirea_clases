<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

/**
 * @class AddFieldMunicipalityIdToParishesTable
 * @brief Agrega el campo municipality_id a la tabla parroquia
 *
 * Gestiona la creación o eliminación de un campo en la tabla parroquia
 *
 * @author <paez.william8@gmail.com>
 * @license <a href='​http://www.gnu.org/licenses/gpl-2.0.html'>GNU Public License versión 2 (GPLv2)</a>
 */
class AddFieldMunicipalityIdToParishesTable extends Migration
{
    /**
     * Método que agrega el campo municipality_id
     *
     * @author William Páez <paez.william8@gmail.com>
     * @return void
     */
    public function up()
    {
        Schema::table('parishes', function (Blueprint $table) {
            if (!Schema::hasColumn('parishes', 'municipality_id')) {
                $table->foreignId('municipality_id')->nullable()->constrained()->onDelete('cascade')
                      ->comment('relación con la tabla municipio');
            }
        });
    }

    /**
     * Método que elimina el campo municipality_id
     *
     * @author William Páez <paez.william8@gmail.com>
     * @return void
     */
    public function down()
    {
        Schema::table('parishes', function (Blueprint $table) {
            if (Schema::hasColumn('parishes', 'municipality_id')) {
                $table->dropForeign(['municipality_id']);
                $table->dropColumn('municipality_id');
            }
        });
    }
}
