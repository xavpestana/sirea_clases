<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class RemoveSeasonsLevelTable extends Migration
{
    /**
     * Run the migrations.
     DELETE SEASONS_ID
     *
     * @return void
     */
    public function up()
    {
        Schema::table('levels', function ($table) {
            if (Schema::hasColumn('levels', 'season_id')) {
            $table->dropForeign(['season_id']);
            $table->dropColumn('season_id');
            }
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('levels', function (Blueprint $table) {
            if (!Schema::hasColumn('levels', 'season_id')) {
                $table->foreignId('season_id')->constrained()->onDelete('cascade')->after('name');
            }
        });
    }
}
