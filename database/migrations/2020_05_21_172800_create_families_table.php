<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateFamiliesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('families', function (Blueprint $table) {
            $table->id();
            $table->foreignId('user_id')->constrained()->onDelete('cascade');

            $table->string('last_name_1',50);
            $table->string('last_name_2',50);
            $table->string('phone',20)->nullable();
            //ingresos
            $table->decimal('monthly_income',20,2)->nullable();
            $table->integer('dependent_income')->nullable();
            $table->integer('contributors')->nullable();
            //ubicacion
            $table->foreignId('parish_id')->constrained()->onDelete('cascade');
            $table->text('address')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('families');
    }
}
