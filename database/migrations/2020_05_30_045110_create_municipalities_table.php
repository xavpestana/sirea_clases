<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

/**
 * @class CreateMunicipalitiesTable
 * @brief Crear tabla de municipios
 *
 * Gestiona la creación o eliminación de la tabla municipios
 *
 * @author William Páez <paez.william8@gmail.com>
 * @license <a href='​http://www.gnu.org/licenses/gpl-2.0.html'>GNU Public License versión 2 (GPLv2)</a>
 */
class CreateMunicipalitiesTable extends Migration
{
    /**
     * Método que ejecuta las migraciones
     *
     * @author William Páez <paez.william8@gmail.com>
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable('municipalities')) {
            Schema::create('municipalities', function (Blueprint $table) {
                $table->id();
                $table->string('name', 100)->comment('Nombre del municipio');
                $table->foreignId('state_id')->constrained()->onDelete('cascade')
                      ->comment('relación con la tabla estado');
                $table->timestamps();
            });
        }
    }

    /**
     * Método que elimina las migraciones
     *
     * @author William Páez <paez.william8@gmail.com>
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('municipalities');
    }
}
