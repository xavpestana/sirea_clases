<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddSeasonsIdInSectionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('sections', function (Blueprint $table) {
            if (!Schema::hasColumn('sections', 'seasons_id')) {
                $table->foreignId('seasons_id')->constrained()->onDelete('cascade')->after('grade_id');
            }
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('sections', function (Blueprint $table) {
            if (Schema::hasColumn('sections', 'seasons_id')) {
                $table->dropForeign(['seasons_id']);
                $table->dropColumn('seasons_id');
            }
        });
    }
}
