<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAlternatePaymentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('alternate_payments', function (Blueprint $table) {
            $table->id();
            $table->foreignId('school_id')->onDelete('cascade')->comment('relación con la tabla escuela');
            $table->string('email', 100)->comment('Correo para envio de email');
            $table->bigInteger('number_invoice')->default(0)->comment('Control del número de recibo');
            $table->string('name', 400)->comment('recibido de');
            $table->float('amount', 50, 2)->comment('Monto del pago');
            $table->string('coin')->comment('Moneda');
            $table->string('concept', 400)->comment('Concepto');
            $table->float('change_amount_USD', 50, 2)->nullable()->comment('Monto del cambio en dolares');
            $table->float('change_amount_COP', 50, 2)->nullable()->comment('Monto del cambio en pesos');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('alternate_payments');
    }
}
