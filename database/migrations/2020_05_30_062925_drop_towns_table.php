<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

/**
 * @class DropTownsTable
 * @brief Elimina la tabla pueblo
 *
 * Gestiona la creación o eliminación de la tabla Pueblo
 *
 * @author William Páez <paez.william8@gmail.com>
 * @license <a href='​http://www.gnu.org/licenses/gpl-2.0.html'>GNU Public License versión 2 (GPLv2)</a>
 */
class DropTownsTable extends Migration
{
    /**
     * Método que elimina la tabla towns
     *
     * @author William Páez <paez.william8@gmail.com>
     * @return void
     */
    public function up()
    {
        Schema::dropIfExists('towns');
    }

    /**
     * Método que crea la tabla towns
     *
     * @author William Páez <paez.william8@gmail.com>
     * @return void
     */
    public function down()
    {
        if (!Schema::hasTable('towns')) {
            Schema::create('towns', function (Blueprint $table) {
                $table->id();
                $table->foreignId('city_id')->constrained()->onDelete('cascade')
                      ->comment('Relación con la tabla ciudad');
                $table->string('name', 50)->comment('Nombre del pueblo');
                $table->timestamps();
            });
        }
    }
}
