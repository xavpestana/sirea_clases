<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

/**
 * @class CreateSectionUserTable
 * @brief Crear tabla intermedia seccion_usuario
 *
 * Gestiona la creación o eliminación de la tabla seccion_usuario
 *
 * @author William Páez <paez.william8@gmail.com>
 * @license <a href='​http://www.gnu.org/licenses/gpl-2.0.html'>GNU Public License versión 2 (GPLv2)</a>
 */
class CreateSectionUserTable extends Migration
{
    /**
     * Método que ejecuta las migraciones
     *
     * @author William Páez <paez.william8@gmail.com>
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable('section_user')) {
            Schema::create('section_user', function (Blueprint $table) {
                $table->id();
                $table->foreignId('user_id')->constrained()->onDelete('cascade')
                      ->comment('relación con la tabla usuarios');
                $table->foreignId('section_id')->constrained()->onDelete('cascade')
                      ->comment('relación con la tabla secciones');
                $table->timestamps();
            });
        }
    }

    /**
     * Método que elimina las migraciones
     *
     * @author William Páez <paez.william8@gmail.com>
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('section_user');
    }
}
