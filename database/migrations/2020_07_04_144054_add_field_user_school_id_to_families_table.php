<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddFieldUserSchoolIdToFamiliesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('families', function (Blueprint $table) {
            $table->string('last_name_1', 50)->nullable()->change();
            $table->string('last_name_2', 50)->nullable()->change();
            $table->foreignId('parish_id')->nullable()->change();
            $table->foreignId('user_school_id')->nullable()->constrained()->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('families', function (Blueprint $table) {
            $table->string('last_name_1', 50)->change();
            $table->string('last_name_2', 50)->change();
            $table->foreignId('parish_id')->nullable(false)->change();
            $table->dropForeign(['user_school_id']);
            $table->dropColumn('user_school_id');
        });
    }
}
