<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

/**
 * @class RemoveFieldSeasonIdToLevelsTable
 * @brief Elimina el campo season_id de la tabla niveles
 *
 * Gestiona la creación o eliminación de un campo en la tabla niveles
 *
 * @author <paez.william8@gmail.com>
 * @license <a href='​http://www.gnu.org/licenses/gpl-2.0.html'>GNU Public License versión 2 (GPLv2)</a>
 */
class RemoveFieldSeasonIdToLevelsTable extends Migration
{
    /**
     * Método que elimina el campo town_id
     *
     * @author William Páez <paez.william8@gmail.com>
     * @return void
     */
    public function up()
    {
        Schema::table('levels', function (Blueprint $table) {
            if (Schema::hasColumn('levels', 'season_id')) {
                $table->dropForeign(['season_id']);
                $table->dropColumn('season_id');
            }
        });
    }

    /**
     * Método que agrega el campo season_id
     *
     * @author William Páez <paez.william8@gmail.com>
     * @return void
     */
    public function down()
    {
        Schema::table('levels', function (Blueprint $table) {
            if (!Schema::hasColumn('levels', 'season_id')) {
                $table->foreignId('season_id')->nullable()->constrained()->onDelete('cascade')
                      ->comment('Relación con la tabla año escolar');
            }
        });
    }
}
