<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateRepresentativesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('representatives', function (Blueprint $table) {
            $table->id();
            $table->foreignId('family_id')->constrained()->onDelete('cascade');
            
            $table->string('document_type',2);
            $table->string('document_number',20);
            $table->string('first_name_1',50);
            $table->string('first_name_2',50)->nullable();
            $table->string('last_name_1',50);
            $table->string('last_name_2',50);
            $table->date('birthdate');
            $table->string('phone',20);
            $table->string('relationship', 50);
            $table->foreignId('country_id')->constrained()->onDelete('cascade');
            $table->foreignId('gender_id')->constrained()->onDelete('cascade');

            $table->string('avatar')->default('default.jpg');

            $table->boolean('principal')->default(1);

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('representatives');
    }
}
