<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

/**
 * @class AddFieldSchoolIdToPaymentMethodsTable
 * @brief Agrega el campo school_id a la tabla métodos de pago
 *
 * Gestiona la creación o eliminación de un campo en la tabla métodos de pago
 *
 * @author <paez.william8@gmail.com>
 * @license <a href='​http://www.gnu.org/licenses/gpl-2.0.html'>GNU Public License versión 2 (GPLv2)</a>
 */
class AddFieldSchoolIdToPaymentMethodsTable extends Migration
{
    /**
     * Método que agrega el campo school_id
     *
     * @author William Páez <paez.william8@gmail.com>
     * @return void
     */
    public function up()
    {
        Schema::table('payment_methods', function (Blueprint $table) {
            if (!Schema::hasColumn('payment_methods', 'school_id')) {
                $table->foreignId('school_id')->nullable()->constrained()->onDelete('cascade')
                      ->comment('relación con la tabla métodos de pago');
            }
        });
    }

    /**
     * Método que elimina el campo school_id
     *
     * @author William Páez <paez.william8@gmail.com>
     * @return void
     */
    public function down()
    {
        Schema::table('payment_methods', function (Blueprint $table) {
            if (Schema::hasColumn('payment_methods', 'school_id')) {
                $table->dropForeign(['school_id']);
                $table->dropColumn('school_id');
            }
        });
    }
}
