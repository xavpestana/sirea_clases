<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddFieldsToTeachersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('teachers', function (Blueprint $table) {
            if (!Schema::hasColumn('teachers', 'parish_id')) {
                $table->foreignId('parish_id')->nullable()->constrained()->onDelete('cascade')
                      ->comment('Relación con la tabla parroquia');
            }
            if (!Schema::hasColumn('teachers', 'adresss')) {
                $table->text('address')->nullable()->comment('direccion del docente');
            }
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('teachers', function (Blueprint $table) {
            if (Schema::hasColumn('teachers', 'parish_id')) {
                $table->dropForeign(['parish_id']);
                $table->dropColumn('parish_id');
            }
            if (Schema::hasColumn('teachers', 'address')) {
                $table->dropColumn('address');
            }

        });
    }
}
