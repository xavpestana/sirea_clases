<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

/**
 * @class RemoveFieldTownIdToParishesTable
 * @brief Elimina el campo town_id de la tabla parroquia
 *
 * Gestiona la creación o eliminación de un campo en la tabla parroquia
 *
 * @author <paez.william8@gmail.com>
 * @license <a href='​http://www.gnu.org/licenses/gpl-2.0.html'>GNU Public License versión 2 (GPLv2)</a>
 */
class RemoveFieldTownIdToParishesTable extends Migration
{
    /**
     * Método que elimina el campo town_id
     *
     * @author William Páez <paez.william8@gmail.com>
     * @return void
     */
    public function up()
    {
        Schema::table('parishes', function (Blueprint $table) {
            if (Schema::hasColumn('parishes', 'town_id')) {
                $table->dropForeign(['town_id']);
                $table->dropColumn('town_id');
            }
        });
    }

    /**
     * Método que agrega el campo town_id
     *
     * @author William Páez <paez.william8@gmail.com>
     * @return void
     */
    public function down()
    {
        Schema::table('parishes', function (Blueprint $table) {
            if (!Schema::hasColumn('parishes', 'town_id')) {
                $table->foreignId('town_id')->nullable()->constrained()->onDelete('cascade')
                      ->comment('Relación con la tabla pueblo');
            }
        });
    }
}
