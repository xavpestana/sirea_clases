<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddFieldsToStudentTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('students', function (Blueprint $table) {
            if (!Schema::hasColumn('students', 'waist_size')) {
                $table->string('waist_size')->nullable()->comment('talla de cintura');
            }
            if (!Schema::hasColumn('students', 'glasses')) {
                $table->boolean('glasses')->default(0)->comment('lentes');
            }
            if (!Schema::hasColumn('students', 'medical_report')) {
                $table->boolean('medical_report')->default(0)->comment('informe medico');
            } 
            if (!Schema::hasColumn('students', 'surgical_intervention')) {
                $table->boolean('surgical_intervention')->default(0)->comment('intervencion quirurgica');
            }
            if (!Schema::hasColumn('students', 'allergic')) {
                $table->string('allergic')->nullable()->comment('alergico a');
            }
            if (!Schema::hasColumn('students', 'medical_treatment')) {
                $table->boolean('medical_treatment')->default(0)->comment('tratamiento_medico');
            }
            if (!Schema::hasColumn('students', 'medicine')) {
                $table->string('medicine')->nullable()->comment('medicamento');
            }
            if (!Schema::hasColumn('students', 'health_insurance')) {
                $table->string('health_insurance')->nullable()->comment('seguro medico');
            }
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('students', function (Blueprint $table) {
            if (Schema::hasColumn('students', 'waist_size')) {
                $table->dropColumn('waist_size');
            }
            if (Schema::hasColumn('students', 'glasses')) {
                $table->dropColumn('glasses');
            }
            if (Schema::hasColumn('students', 'medical_report')) {
                $table->dropColumn('medical_report');
            }
            if (Schema::hasColumn('students', 'surgical_intervention')) {
                $table->dropColumn('surgical_intervention');
            }
            if (Schema::hasColumn('students', 'allergic')) {
                $table->dropColumn('allergic');
            }
            if (Schema::hasColumn('students', 'medical_treatment')) {
                $table->dropColumn('medical_treatment');
            }
            if (Schema::hasColumn('students', 'medicine')) {
                $table->dropColumn('medicine');
            }
            if (Schema::hasColumn('students', 'health_insurance')) {
                $table->dropColumn('health_insurance');
            }                   
        });
    }
}
