<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddCommentToTeacherNotifies extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('teacher_notifies', function (Blueprint $table) {
            if (!Schema::hasColumn('teacher_notifies', 'task_id')) {
                $table->integer('task_id')->unsigned()->nullable();
            }
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('teacher_notifies', function (Blueprint $table) {
            if (Schema::hasColumn('teacher_notifies', 'task_id')) {
                $table->dropColumn('task_id');
            }
        });
    }
}
