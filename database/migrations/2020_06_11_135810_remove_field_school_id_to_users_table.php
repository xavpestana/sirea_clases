<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

/**
 * @class RemoveFieldSchoolIdToUsersTable
 * @brief Elimina el campo school_id de la tabla usuario
 *
 * Gestiona la creación o eliminación de un campo en la tabla usuario
 *
 * @author <paez.william8@gmail.com>
 * @license <a href='​http://www.gnu.org/licenses/gpl-2.0.html'>GNU Public License versión 2 (GPLv2)</a>
 */
class RemoveFieldSchoolIdToUsersTable extends Migration
{
    /**
     * Método que elimina el campo school_id
     *
     * @author William Páez <paez.william8@gmail.com>
     * @return void
     */
    public function up()
    {
        Schema::table('users', function (Blueprint $table) {
            if (Schema::hasColumn('users', 'school_id')) {
                $table->dropForeign(['school_id']);
                $table->dropColumn('school_id');
            }
        });
    }

    /**
     * Método que agrega el campo school_id
     *
     * @author William Páez <paez.william8@gmail.com>
     * @return void
     */
    public function down()
    {
        Schema::table('users', function (Blueprint $table) {
            if (!Schema::hasColumn('users', 'school_id')) {
                $table->foreignId('school_id')->nullable()->constrained()->onDelete('cascade')
                      ->comment('Relación con la tabla usuario');
            }
        });
    }
}
