<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddFieldsToPollsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('polls', function (Blueprint $table) {
            if (!Schema::hasColumn('polls', 'emergency_phone')) {
                $table->string('emergency_phone')->nullable()->comment('Telefono en caso de emergencias');
            }
            if (!Schema::hasColumn('polls', 'policy_description')) {
                $table->text('policy_description')->nullable()->comment('descripcion de la poliza de seguros');
            } 
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('polls', function (Blueprint $table) {
            if (Schema::hasColumn('polls', 'emergency_phone')) {
                $table->dropColumn('emergency_phone');
            }
            if (Schema::hasColumn('polls', 'policy_description')) {
                $table->dropColumn('policy_description');
            }
        });
    }
}
