<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddLastToCreditTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('credits', function (Blueprint $table) {
            if (!Schema::hasColumn('credits', 'amount_last')) {
                $table->float('amount_last', 50, 2)->nullable()->comment('Monto anterior');
            }
            if (!Schema::hasColumn('credits', 'coin_last')) {
                $table->string('coin_last')->nullable()->comment('Moneda anterior');
            }
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('credits', function (Blueprint $table) {
            if (Schema::hasColumn('credits', 'amount_last')) {
                $table->dropColumn('amount_last');
            }
            if (Schema::hasColumn('credits', 'coin_last')) {
                $table->dropColumn('coin_last');
            }
        });
    }
}
