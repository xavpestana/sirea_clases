<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

/**
 * @class ChangeFieldsToUserSchoolsTable
 * @brief Cambia campos en la tabla usuario escolar
 *
 * Gestiona cambios en campos de la tabla usuario escolar
 *
 * @author William Páez <paez.william8@gmail.com>
 * @license <a href='​http://www.gnu.org/licenses/gpl-2.0.html'>GNU Public License versión 2 (GPLv2)</a>
 */
class ChangeFieldsToUserSchoolsTable extends Migration
{
    /**
     * Método que cambia a único los campos: user_id y school_id
     *
     * @author William Páez <paez.william8@gmail.com>
     * @return void
     */
    public function up()
    {
        Schema::table('user_schools', function (Blueprint $table) {
            $table->unique(['user_id', 'school_id',])
                  ->comment('Clave única para el registro');
        });
    }

    /**
     * Método que elimina la unicidad de los campos: user_id y school_id
     *
     * @author William Páez <paez.william8@gmail.com>
     * @return void
     */
    public function down()
    {
        Schema::table('user_schools', function (Blueprint $table) {
            $table->dropUnique(['user_id', 'school_id']);
        });
    }
}
