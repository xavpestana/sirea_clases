<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class ChangeFieldSchoolToSeasonInPlansTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('plans', function (Blueprint $table) {
            Schema::table('plans', function (Blueprint $table) {
            if (Schema::hasColumn('plans', 'school_id')) {
                $table->dropForeign(['school_id']);
                $table->dropColumn('school_id');
            }

            if (!Schema::hasColumn('plans', 'season_id')) {
                $table->foreignId('season_id')->nullable()->constrained()->onDelete('cascade')
                ->after('name')
                      ->comment('relación con la tabla seasons');
            }
        });
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('plans', function (Blueprint $table) {
            if (!Schema::hasColumn('plans', 'school_id')) {
                $table->foreignId('school_id')->nullable()->constrained()->onDelete('cascade')
                      ->comment('relación con la tabla colegios');
            }
            if (Schema::hasColumn('plans', 'season_id')) {
                $table->dropForeign(['season_id']);
                $table->dropColumn('season_id');
            }
        });
    }
}
