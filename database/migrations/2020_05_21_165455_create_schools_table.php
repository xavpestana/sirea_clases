<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSchoolsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('schools', function (Blueprint $table) {
            $table->id();
            $table->string('dea_code', 20)->nullable();
            $table->string('stadistic_code', 20)->nullable();
            $table->string('rif', 20)->nullable();
            $table->string('school_type', 20);
            $table->string('name', 20);
            $table->foreignId('parish_id')->constrained()->onDelete('cascade');
            $table->text('address');
            $table->string('phone', 20)->nullable();
            $table->string('fax', 20)->nullable();
            $table->string('email')->unique();
            $table->string('url',100)->nullable();
            $table->string('principal_name',100)->nullable();//nombre del director
            $table->string('principal_document_type',1)->nullable();//documento del director
            $table->string('principal_document_number',20)->nullable();//documento del director
            $table->foreignId('gender_id')->constrained()->onDelete('cascade');//genero del director
            $table->string('logo',50)->default('default.jpg');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('schools');
    }
}
