<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

/**
 * @class AddFieldSeasonIdToLevelsTable
 * @brief Agrega el campo season_id a la tabla nivel
 *
 * Gestiona la creación o eliminación de un campo en la tabla nivel
 *
 * @author <paez.william8@gmail.com>
 * @license <a href='​http://www.gnu.org/licenses/gpl-2.0.html'>GNU Public License versión 2 (GPLv2)</a>
 */
class AddFieldSeasonIdToLevelsTable extends Migration
{
    /**
     * Método que agrega el campo season_id
     *
     * @author William Páez <paez.william8@gmail.com>
     * @return void
     */
    public function up()
    {
        Schema::table('levels', function (Blueprint $table) {
            if (!Schema::hasColumn('levels', 'season_id')) {
                $table->foreignId('season_id')->nullable()->constrained()->onDelete('cascade')
                      ->comment('relación con la tabla niveles');
            }
        });
    }

    /**
     * Método que elimina el campo season_id
     *
     * @author William Páez <paez.william8@gmail.com>
     * @return void
     */
    public function down()
    {
        Schema::table('levels', function (Blueprint $table) {
            if (Schema::hasColumn('levels', 'season_id')) {
                $table->dropForeign(['season_id']);
                $table->dropColumn('season_id');
            }
        });
    }
}
