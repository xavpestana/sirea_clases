<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddFieldCodeToParishesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('parishes', function (Blueprint $table) {
            if (!Schema::hasColumn('parishes', 'code')) {
                $table->string('code', 10)->nullable()->comment('Código que identifica a la parroquia');
            }
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('parishes', function (Blueprint $table) {
            if (Schema::hasColumn('parishes', 'code')) {
                $table->dropColumn('code');
            }
        });
    }
}
