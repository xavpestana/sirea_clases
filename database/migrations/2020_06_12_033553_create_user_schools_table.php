<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

/**
 * @class CreateUserSchoolsTable
 * @brief Crear tabla de escuelas del usuario
 *
 * Gestiona la creación o eliminación de la tabla escuelas del usuario
 *
 * @author William Páez <paez.william8@gmail.com>
 * @license <a href='​http://www.gnu.org/licenses/gpl-2.0.html'>GNU Public License versión 2 (GPLv2)</a>
 */
class CreateUserSchoolsTable extends Migration
{
    /**
     * Método que ejecuta las migraciones
     *
     * @author William Páez <paez.william8@gmail.com>
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable('user_schools')) {
            Schema::create('user_schools', function (Blueprint $table) {
                $table->id();
                $table->foreignId('school_id')->constrained()->onDelete('cascade')
                      ->comment('relación con la tabla escuela del usuario');
                $table->foreignId('user_id')->constrained()->onDelete('cascade')
                      ->comment('relación con la tabla usuario');
                $table->foreignId('parent_id')->nullable()->constrained('user_schools')->onDelete('cascade')
                      ->comment('Relación con la tabla escuela del usuario para saber la dependencia');
                $table->timestamps();
            });
        }
    }

    /**
     * Método que elimina las migraciones
     *
     * @author William Páez <paez.william8@gmail.com>
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user_schools');
    }
}
