<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddFieldSchoolIdToFamiliesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('families', function (Blueprint $table) {
            if (Schema::hasColumn('families', 'user_school_id')) {

                $table->dropForeign(['user_school_id']);

                $table->dropColumn('user_school_id');
            }

            if (!Schema::hasColumn('families', 'school_id')) {

                $table->foreignId('school_id')->nullable()->constrained()->onDelete('cascade')
                      ->comment('relación con la tabla colegios');

            }
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('families', function (Blueprint $table) {
            
            if (!Schema::hasColumn('families', 'user_school_id')) {

                $table->foreignId('user_school_id')->nullable()->constrained()->onDelete('cascade')
                      ->comment('Relación con la tabla usuario_school');

            }

            if (Schema::hasColumn('families', 'school_id')) {
            
                $table->dropForeign(['school_id']);
            
                $table->dropColumn('school_id');
            }
        });
    }
}
