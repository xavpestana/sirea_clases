<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateStudentPlansTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('student_plans', function (Blueprint $table) {
            $table->id();
            $table->foreignId('season_id')->nullable()->constrained()->onDelete('cascade')
                ->comment('relación con la tabla seasons');
            $table->foreignId('student_id')->nullable()->constrained()->onDelete('cascade')
                ->comment('relación con la tabla student');
            $table->foreignId('plan_id')->nullable()->constrained()->onDelete('cascade')
                ->comment('relación con la tabla plan');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('student_plans');
    }
}
