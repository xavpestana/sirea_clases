<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddFieldCodeToMunicipalitiesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('municipalities', function (Blueprint $table) {
            if (!Schema::hasColumn('municipalities', 'code')) {
                $table->string('code', 10)->nullable()->comment('Código que identifica al Municipio');
            }
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('municipalities', function (Blueprint $table) {
            if (Schema::hasColumn('municipalities', 'code')) {
                $table->dropColumn('code');
            }
        });
    }
}
