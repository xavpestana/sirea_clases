<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

/**
 * @class CreateConceptsTable
 * @brief Crear tabla de conceptos
 *
 * Gestiona la creación o eliminación de la tabla conceptos
 *
 * @author William Páez <paez.william8@gmail.com>
 * @license <a href='​http://www.gnu.org/licenses/gpl-2.0.html'>GNU Public License versión 2 (GPLv2)</a>
 */
class CreateConceptsTable extends Migration
{
    /**
     * Método que ejecuta las migraciones
     *
     * @author William Páez <paez.william8@gmail.com>
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable('concepts')) {
            Schema::create('concepts', function (Blueprint $table) {
                $table->id();
                $table->string('name', 100)->comment('Nombre del concepto');
                $table->float('amount', 8, 2)->default(0)->comment('Monto del concepto');
                $table->timestamps();
            });
        }
    }

    /**
     * Método que elimina las migraciones
     *
     * @author William Páez <paez.william8@gmail.com>
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('concepts');
    }
}
