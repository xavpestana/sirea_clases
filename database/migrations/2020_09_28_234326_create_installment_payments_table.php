<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateInstallmentPaymentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('installment_payments', function (Blueprint $table) {
            $table->id();
            $table->foreignId('payment_id')->constrained()->onDelete('cascade')
                      ->comment('relación con la tabla estudiantes');
            $table->foreignId('installment_id')->constrained()->onDelete('cascade')
                      ->comment('relación con la tabla de cuotas');
            $table->foreignId('student_id')->constrained()->onDelete('cascade')
                      ->comment('relación con la tabla de cuotas');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('installment_payments');
    }
}
