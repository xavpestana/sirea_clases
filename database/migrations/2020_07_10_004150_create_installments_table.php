<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

/**
 * @class CreateInstallmentsTable
 * @brief Crear tabla de cuotas
 *
 * Gestiona la creación o eliminación de la tabla cuotas
 *
 * @author William Páez <paez.william8@gmail.com>
 * @license <a href='​http://www.gnu.org/licenses/gpl-2.0.html'>GNU Public License versión 2 (GPLv2)</a>
 */
class CreateInstallmentsTable extends Migration
{
    /**
     * Método que ejecuta las migraciones
     *
     * @author William Páez <paez.william8@gmail.com>
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable('installments')) {
            Schema::create('installments', function (Blueprint $table) {
                $table->id();
                $table->text('description')->comment('Descripción de la cuota');
                $table->float('amount', 8, 2)->comment('Monto de la cuota');
                $table->foreignId('plan_id')->constrained()->onDelete('cascade')
                      ->comment('relación con la tabla planes de pago');
                $table->timestamps();
            });
        }
    }

    /**
     * Método que elimina las migraciones
     *
     * @author William Páez <paez.william8@gmail.com>
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('installments');
    }
}
