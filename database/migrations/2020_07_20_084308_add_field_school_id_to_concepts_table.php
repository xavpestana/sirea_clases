<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

/**
 * @class AddFieldSchoolIdToConceptsTable
 * @brief Agrega el campo school_id a la tabla conceptos de inscripción
 *
 * Gestiona la creación o eliminación de un campo en la tabla conceptos de inscripción
 *
 * @author <paez.william8@gmail.com>
 * @license <a href='​http://www.gnu.org/licenses/gpl-2.0.html'>GNU Public License versión 2 (GPLv2)</a>
 */
class AddFieldSchoolIdToConceptsTable extends Migration
{
    /**
     * Método que agrega el campo school_id
     *
     * @author William Páez <paez.william8@gmail.com>
     * @return void
     */
    public function up()
    {
        Schema::table('concepts', function (Blueprint $table) {
            if (!Schema::hasColumn('concepts', 'school_id')) {
                $table->foreignId('school_id')->nullable()->constrained()->onDelete('cascade')
                      ->comment('relación con la tabla colegios');
            }
        });
    }

    /**
     * Método que elimina el campo school_id
     *
     * @author William Páez <paez.william8@gmail.com>
     * @return void
     */
    public function down()
    {
        Schema::table('concepts', function (Blueprint $table) {
            if (Schema::hasColumn('concepts', 'school_id')) {
                $table->dropForeign(['school_id']);
                $table->dropColumn('school_id');
            }
        });
    }
}
