<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class RemoveFieldsToProfilesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('profiles', function (Blueprint $table) {
            if (Schema::hasColumn('profiles', 'parish_id')) {
                $table->dropForeign(['parish_id']);
                $table->dropColumn('parish_id');
            }
            if (Schema::hasColumn('profiles', 'address')) {
                $table->dropColumn('address');
            }


        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('profiles', function (Blueprint $table) {
            if (!Schema::hasColumn('profiles', 'parish_id')) {
                $table->foreignId('parish_id')->nullable()->constrained()->onDelete('cascade')
                      ->comment('Relación con la tabla parroquia');
            }
            if (!Schema::hasColumn('profiles', 'adresss')) {
                $table->text('address')->nullable()->comment('direccion del perfil');
            }
        });
    }
}
