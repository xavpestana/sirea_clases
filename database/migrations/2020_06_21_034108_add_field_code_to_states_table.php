<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddFieldCodeToStatesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('states', function (Blueprint $table) {
            if (!Schema::hasColumn('states', 'code')) {
                $table->string('code', 10)->nullable()->comment('Código que identifica al Estado');
            }
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('states', function (Blueprint $table) {
            if (Schema::hasColumn('states', 'code')) {
                $table->dropColumn('code');
            }
        });
    }
}
