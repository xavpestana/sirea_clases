<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePaymentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable('payments')) {
            Schema::create('payments', function (Blueprint $table) {
                $table->id();
                $table->string('name', 100)->comment('Nombre del pago');
                $table->decimal('amount', 20, 2)->comment('Monto del pago');
                $table->boolean('verified')->default(false)->comment('Indica si el pago fue verificado');
                $table->boolean('annulled')->default(false)->comment('Indica si el pago fue anulado');
                $table->bigInteger('invoice_number')->default(0)->comment('Control del número de factura');
                $table->string('school_bank_account', 100)->comment('Cuenta del banco receptor');
                $table->string('family_bank_account', 100)->comment('Cuenta del banco emisor');
                $table->string('transfer_code', 100)->unique()->comment('Cuenta del banco emisor');
                $table->foreignId('payment_method_id')->constrained()->onDelete('cascade')
                      ->comment('relación con la tabla métodos de pago');
                $table->foreignId('receipt_id')->constrained()->onDelete('cascade')
                      ->comment('relación con la tabla recibos');
                $table->timestamps();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('payments');
    }
}
