<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\School;
use Faker\Generator as Faker;

$factory->define(School::class, function (Faker $faker) {
	$sexo = rand(1,2);

	if($sexo == 1)
	{
		$nombre = $faker->firstNameMale;
		$sexo = 1;
	}else{
		$nombre = $faker->firstNameFemale;
		$sexo = 2;
	}

    return [
		'dea_code'                  => $faker->numberBetween($min = 10000, $max = 20000),
		'stadistic_code'           => $faker->numberBetween($min = 10000, $max = 20000),
		'rif'                       => 'J'.$faker->numberBetween($min = 10000000, $max = 20000000),
		'school_type'               => 'Colegio',
		'name'                      => 'Demo Venezuela',
		'address'                   => 'Av 5. Centro',
		'phone'                     => '042412345678',
		'email'                     => 'colegio@clasesit.com',
		'url'                       => 'https://www.clasesit.com',
		'parish_id'                 => 1,
		'principal_name'            => $nombre.' '.$faker->lastname,
		'principal_document_type'   => 'V',
		'principal_document_number' => $faker->numberBetween($min = 10000000, $max = 20000000),
		'gender_id'                 => $sexo,
		'logo'                      => 'default.jpg'
    ];
});
