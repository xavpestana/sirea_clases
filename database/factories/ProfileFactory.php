<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Profile;
use Faker\Generator as Faker;

$factory->define(Profile::class, function (Faker $faker) {
	$sexo = rand(1,2);

	if($sexo == 1)
	{
		$nombre1 = $faker->firstNameMale;
		$nombre2 = $faker->firstNameMale;
		$sexo = 1;
	}else{
		$nombre1 = $faker->firstNameFemale;
		$nombre2 = $faker->firstNameFemale;
		$sexo = 2;
	}

    return [
		'document_type'   => 'CE',
		'document_number' => $faker->numberBetween($min = 20000000, $max = 40000000),
		'first_name_1'    => $nombre1,
		'first_name_2'    => $nombre2,
		'birthdate'       => $faker->dateTimeBetween($startDate = '-16 years', $endDate = '-5 years', $timezone = null),
		'phone'           => $faker->e164PhoneNumber,
		'country_id'      => 1,
		'gender_id'		  => $sexo,
		'avatar'  		  => 'default.jpg'
    ];
});
