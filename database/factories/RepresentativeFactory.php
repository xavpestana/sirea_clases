<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Representative;
use Faker\Generator as Faker;

$factory->define(Representative::class, function (Faker $faker) {
	$sexo = rand(1,2);
	if($sexo == 1)
	{
		$nombre1 = $faker->firstNameMale;
		$nombre2 = $faker->firstNameMale;
		$parentesco = 'Padre';
	}else{
		$nombre1 = $faker->firstNameFemale;
		$nombre2 = $faker->firstNameFemale;
		$parentesco = 'Madre';
	}

    return [
		'document_type' => 'V',
        'document_number' => $faker->numberBetween($min = 10000000, $max = 20000000),
        'first_name_1' => $nombre1,
        'first_name_2' => $nombre2,
        'last_name_1' => $faker->lastname,
        'last_name_2' => $faker->lastname,
        'gender_id' => $sexo,
		'birthdate' => $faker->dateTimeBetween($startDate = '-70 years', $endDate = '-30 years', $timezone = null),
		'phone' => $faker->e164PhoneNumber,
		'relationship' => $parentesco,
		'country_id' => 1,
		'avatar' => 'default.jpg',
        'principal' => 1
    ];
});
