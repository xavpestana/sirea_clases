<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Family;
use Faker\Generator as Faker;

$factory->define(Family::class, function (Faker $faker) {
    return [
        'last_name_1' => $faker->lastname,
        'last_name_2' => $faker->lastname,
        'phone' => $faker->e164PhoneNumber,
        'parish_id' => 1,
        'address' => $faker->address
    ];
});
