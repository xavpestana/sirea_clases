<?php

use Illuminate\Database\Seeder;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use App\Level;

class LevelsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();

        /** @var array Listado de niveles a registrar */
        $levels = [
            ['name' => 'Pre-Maternal'],
            ['name' => 'Maternal'],
            ['name' => 'Inicial'],
            ['name' => 'Primaria'],
            ['name' => 'Media General'],
            ['name' => 'Media Técnica']
        ];

        DB::transaction(function () use ($levels) {
            foreach ($levels as $level) {
                Level::updateOrCreate(
                    ['name' => $level['name']]
                );
            }
        });
    }
}



