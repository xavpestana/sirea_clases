<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;
use App\UserSchool;
use App\School;
use App\User;

/**
 * @class UserSchoolsTableSeeder
 * @brief información por defecto de usuarios escolares
 *
 * Gestiona la información por defecto a registrar inicialmente para los usuarios escolares
 *
 * @author William Páez <paez.william8@gmail.com>
 * @license <a href='​http://www.gnu.org/licenses/gpl-2.0.html'>GNU Public License versión 2 (GPLv2)</a>
 */
class UserSchoolsTableSeeder extends Seeder
{
    /**
     * Método que registra los valores iniciales de los usuarios escolares
     *
     * @author William Páez <paez.william8@gmail.com>
     * @return void
     */
    public function run()
    {
        Model::unguard();

        /** @var array Permisos disponibles para la gestión de usuarios escolares */
        $permissions = [
            [
                'name' => 'user.schools.list'
            ],
            [
                'name' => 'user.schools.create'
            ],
            [
                'name' => 'user.schools.show'
            ],
        ];

        DB::transaction(function () use ($permissions) {
            $school = School::where('email', 'demo@email.com')->first();

            $userAdmin = User::where('email', 'admin@admin.com')->first();

            $userSchool = User::where('email', 'school@email.com')->first();


            $userSchoolTable = UserSchool::updateOrCreate(
                [
                    'user_id' => $userSchool->id,
                    'school_id' => $school->id,
                    'parent_id' => null
                ]
            );

            if (!$userSchoolTable) {
                throw new Exception('Error creando usuario escolar por defecto');
            }

            $userAdminRole = Role::where('name', 'admin')->first();
            $userSchoolRole = Role::where('name', 'school')->first();
            foreach ($permissions as $permission) {
                $per = Permission::updateOrCreate(
                    ['name' => $permission['name']],
                );
                $userAdminRole->givePermissionTo($per);
                $userSchoolRole->givePermissionTo($per);
            }
        });
    }
}
