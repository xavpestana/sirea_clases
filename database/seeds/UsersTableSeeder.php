<?php

use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;
use App\User;
use Carbon\Carbon;

/**
 * @class UsersTableSeeder
 * @brief información por defecto de usuarios de prueba
 *
 * Gestiona la información por defecto a registrar inicialmente para los usuarios
 *
 * @author William Páez <paez.william8@gmail.com>
 * @license <a href='​http://www.gnu.org/licenses/gpl-2.0.html'>GNU Public License versión 2 (GPLv2)</a>
 */
class UsersTableSeeder extends Seeder
{
    /**
     * Método que registra los valores iniciales de los usuarios de prueba
     *
     * @author William Páez <paez.william8@gmail.com>
     * @return void
     */
    public function run()
    {
        /** @var array Permisos disponibles para la gestión de usuarios */
        $permissions = [
            [
                'name' => 'users.list'
            ],
            [
                'name' => 'users.create'
            ],
            [
                'name' => 'users.show'
            ],
            [
                'name' => 'users.edit'
            ],
            [
                'name' => 'users.delete'
            ],
        ];

        DB::transaction(function () use ($permissions) {

            /** @var object Crea el usuario ADMIN por defecto de la aplicación */
            $userAdmin = User::updateOrCreate(
                ['email' => 'admin@admin.com'],
                [
                    'name' => 'Admin',
                    'password' => bcrypt('usuario12345'),
                    'created_at' => Carbon::now()
                ]
            );

            if (!$userAdmin) {
                throw new Exception('Error creando el usuario administrador por defecto');
            }

            /** @var object Busca el rol de administrador del sistema */
            $adminRole = Role::where('name', 'admin')->first();

            /** Asigna el rol de administrador */
            if ($adminRole) {
                $userAdmin->assignRole($adminRole);
            }

            /** @var object Crea el usuario administrador de COLEGIO por defecto de la aplicación */
            $userSchool = User::updateOrCreate(
                ['email' => 'school@email.com'],
                [
                    'name' => 'School',
                    'password' => bcrypt('usuario12345'),
                    'created_at' => Carbon::now()
                ]
            );

            if (!$userSchool) {
                throw new Exception('Error creando el usuario school por defecto');
            }

            // @var object Busca el rol de school del sistema
            $schoolRole = Role::where('name', 'school')->first();

            /** Asigna el rol de administrador de colegio */
            if ($schoolRole) {
                $userSchool->assignRole($schoolRole);
                $userAdmin->assignRole($schoolRole);
            }

            /** @var object Crea el usuario de familia por defecto de la aplicación */
            /*$userFamily = User::updateOrCreate(
                ['email' => 'family@email.com'],
                [
                    'name' => 'Family',
                    'password' => bcrypt('usuario12345'),
                    'created_at' => Carbon::now()
                ]
            );*/

            /*if (!$userFamily) {
                throw new Exception('Error creando el usuario family por defecto');
            }*/

            /** @var object Busca el rol family del sistema */
            //$familyRole = Role::where('name', 'family')->first();

                /** Asigna el rol familia */
            /*if ($familyRole) {
                $userFamily->assignRole($familyRole);
                $userAdmin->assignRole($familyRole);
            }*/

            /** @var object Crea el usuario estudiante por defecto de la aplicación */
            /*$userStudent = User::updateOrCreate(
                ['email' => 'student@email.com'],
                [
                    'name' => 'Student',
                    'password' => bcrypt('usuario12345'),
                    'created_at' => Carbon::now()
                ]
            );*/

            /*if (!$userStudent) {
                throw new Exception('Error creando el user Student por defecto');
            }*/

            /** @var object Busca el rol student del sistema */
            //$studentRole = Role::where('name', 'student')->first();

                /** Asigna el rol student */
            /*if ($studentRole) {
                $userStudent->assignRole($studentRole);
                $userAdmin->assignRole($studentRole);
            }*/

            /** @var object Crea el usuario docente por defecto de la aplicación */
            /*$userTeacher = User::updateOrCreate(
                ['email' => 'teacher@email.com'],
                [
                    'name' => 'Teacher',
                    'password' => bcrypt('usuario12345'),
                    'created_at' => Carbon::now()
                ]
            );*/
            
            /*if (!$userTeacher) {
                throw new Exception('Error creando el usuario teacher por defecto');
            }*/

            /** @var object Busca el rol de administrador del sistema */
            //$teacherRole = Role::where('name', 'teacher')->first();

            /** Asigna el rol de teacher */
            /*if ($teacherRole) {
                $userTeacher->assignRole($teacherRole);
                $userAdmin->assignRole($teacherRole);
            }*/

            foreach ($permissions as $permission) {
                $per = Permission::updateOrCreate(
                    ['name' => $permission['name']],
                );
                $adminRole->givePermissionTo($per);
            }
        });
    }
}
