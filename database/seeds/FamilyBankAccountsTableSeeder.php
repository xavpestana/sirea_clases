<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;

/**
 * @class FamilyBankAccountsTableSeeder
 * @brief información por defecto de cuentas bancarias de las familias
 *
 * Gestiona la información por defecto a registrar inicialmente para las cuentas bancarias
 *
 * @author William Páez <paez.william8@gmail.com>
 * @license <a href='​http://www.gnu.org/licenses/gpl-2.0.html'>GNU Public License versión 2 (GPLv2)</a>
 */
class FamilyBankAccountsTableSeeder extends Seeder
{
    /**
     * Método que registra los valores iniciales de las cuentas bancarias
     *
     * @author William Páez <paez.william8@gmail.com>
     * @return void
     */
    public function run()
    {
        Model::unguard();

        /** @var array Permisos disponibles para la gestión de cuentas bancarias */
        $permissions = [
            [
                'name' => 'family.bank.accounts.list'
            ],
            [
                'name' => 'family.bank.accounts.create'
            ],
            [
                'name' => 'family.bank.accounts.show'
            ],
            [
                'name' => 'family.bank.accounts.edit'
            ],
            [
                'name' => 'family.bank.accounts.delete'
            ],
        ];

        DB::transaction(function () use ($permissions) {
            $adminRole = Role::where('name', 'admin')->first();
            $userFamilyRole = Role::where('name', 'family')->first();
            foreach ($permissions as $permission) {
                $per = Permission::updateOrCreate(
                    ['name' => $permission['name']],
                );
                $adminRole->givePermissionTo($per);
                $userFamilyRole->givePermissionTo($per);
            }
        });
    }
}
