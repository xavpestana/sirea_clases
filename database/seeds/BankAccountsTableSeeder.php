<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;

/**
 * @class BankAccountsTableSeeder
 * @brief información por defecto de cuentas bancarias
 *
 * Gestiona la información por defecto a registrar inicialmente para las cuentas bancarias
 *
 * @author William Páez <paez.william8@gmail.com>
 * @license <a href='​http://www.gnu.org/licenses/gpl-2.0.html'>GNU Public License versión 2 (GPLv2)</a>
 */
class BankAccountsTableSeeder extends Seeder
{
    /**
     * Método que registra los valores iniciales de las cuentas bancarias
     *
     * @author William Páez <paez.william8@gmail.com>
     * @return void
     */
    public function run()
    {
        Model::unguard();

        /** @var array Permisos disponibles para la gestión de cuentas bancarias */
        $permissions = [
            [
                'name' => 'bank.accounts.list'
            ],
            [
                'name' => 'bank.accounts.create'
            ],
            [
                'name' => 'bank.accounts.show'
            ],
            [
                'name' => 'bank.accounts.edit'
            ],
            [
                'name' => 'bank.accounts.delete'
            ],
        ];

        DB::transaction(function () use ($permissions) {
            $adminRole = Role::where('name', 'admin')->first();
            $userSchoolRole = Role::where('name', 'school')->first();
            foreach ($permissions as $permission) {
                $per = Permission::updateOrCreate(
                    ['name' => $permission['name']],
                );
                $adminRole->givePermissionTo($per);
                $userSchoolRole->givePermissionTo($per);
            }
        });
    }
}
