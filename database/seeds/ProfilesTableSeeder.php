<?php

use Illuminate\Database\Seeder;

use Carbon\Carbon;
use App\User;
use App\Profile;

class ProfilesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $userStudent = User::where('email', 'student@email.com')->first();
        $userTeacher = User::where('email', 'teacher@email.com')->first();

        DB::transaction(function () use($userStudent, $userTeacher){
	        $profileStudent = Profile::updateOrCreate(
                [   'user_id' => $userStudent->id ],
	            [
	                
	                'document_type' => 'CE',
	                'document_number' => '12345678',
	                'first_name_1' => 'Student',
	                'last_name_1' => 'Family',
	                'last_name_2' => 'Family',
	                'birthdate' => Carbon::now()->subYears(10),
	                'phone' => '04141234567',
	                'country_id' => 1,
	                'gender_id' => 1
	            ]
	        );

	        if(!$profileStudent) {
	            throw new Exception('Error creando el perfil Student por defecto');
	        }

            $profileTeacher = Profile::updateOrCreate(
                [   'user_id' => $userTeacher->id ],
                [
                    'document_type' => 'V',
                    'document_number' => '87654321',
                    'first_name_1' => 'Teacher',
                    'last_name_1' => 'Teacher',
                    'last_name_2' => 'Teacher',
                    'birthdate' => Carbon::now()->subYears(40),
                    'phone' => '04141234567',
                    'country_id' => 1,
                    'gender_id' => 1
                ]
            );

            if(!$profileTeacher) {
                throw new Exception('Error creando el perfil Teacher por defecto');
            }
        });
    }
}
