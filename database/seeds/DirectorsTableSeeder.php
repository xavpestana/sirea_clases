<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;
use App\Director;

class DirectorsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        /** @var array Permisos disponibles para la gestión del director del colegio */
        $permissions = [
            [
                'name' => 'directors.list'
            ],
            [
                'name' => 'directors.create'
            ],
            [
                'name' => 'directors.show'
            ],
            [
                'name' => 'directors.edit'
            ],
            [
                'name' => 'directors.delete'
            ],
        ];

        DB::transaction(function () use ($permissions) {
            $director = Director::updateOrCreate(
                [
                    'name' => 'Lic. José Angel',
                    'document_type' => 'V',
                    'document_number' => '12345678',
                    'gender_id' => 1,
                    'school_id' => 1
                ]
            );
            if (!$director) {
                throw new Exception('Error creando el director del colegio por defecto');
            }

            $adminRole = Role::where('name', 'admin')->first();
            $userSchoolRole = Role::where('name', 'school')->first();
            foreach ($permissions as $permission) {
                $per = Permission::updateOrCreate(
                    ['name' => $permission['name']],
                );
                $adminRole->givePermissionTo($per);
                $userSchoolRole->givePermissionTo($per);
            }
        });
    }
}
