<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Spatie\Permission\Models\Role;

/**
 * @class RolesTableSeeder
 * @brief información por defecto de roles
 *
 * Gestiona la información por defecto a registrar inicialmente para los roles
 *
 * @author William Páez <paez.william8@gmail.com>
 * @license <a href='​http://www.gnu.org/licenses/gpl-2.0.html'>GNU Public License versión 2 (GPLv2)</a>
 */
class RolesTableSeeder extends Seeder
{
    /**
     * Método que registra los valores iniciales de los roles
     *
     * @author William Páez <paez.william8@gmail.com>
     * @return void
     */
    public function run()
    {
        Model::unguard();

        /** @var array Listado de roles a registrar */
        $roles = [
            [
                'name' => 'admin'
            ],
            [
                'name' => 'school'
            ],
            [
                'name' => 'teacher'
            ],
            [
                'name' => 'family'
            ],
            [
                'name' => 'student'
            ],
        ];

        DB::transaction(function () use ($roles) {
            foreach ($roles as $role) {
                Role::updateOrCreate(
                    ['name' => $role['name']]
                );
            }
        });
    }
}
