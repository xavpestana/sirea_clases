<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;
use App\PaymentMethod;

/**
 * @class PaymentMethodsTableSeeder
 * @brief información por defecto de los métodos de pago
 *
 * Gestiona la información por defecto a registrar inicialmente para los métodos de pago
 *
 * @author William Páez <paez.william8@gmail.com>
 * @license <a href='​http://www.gnu.org/licenses/gpl-2.0.html'>GNU Public License versión 2 (GPLv2)</a>
 */
class PaymentMethodsTableSeeder extends Seeder
{
    /**
     * Método que registra los valores iniciales de los métodos de pago
     *
     * @author William Páez <paez.william8@gmail.com>
     * @return void
     */
    public function run()
    {
        Model::unguard();

        /** @var array Permisos disponibles para la gestión de métodos de pago */
        $permissions = [
            [
                'name' => 'payment.methods.list'
            ],
            [
                'name' => 'payment.methods.create'
            ],
            [
                'name' => 'payment.methods.show'
            ],
            [
                'name' => 'payment.methods.edit'
            ],
            [
                'name' => 'payment.methods.delete'
            ],
        ];

        $paymentMethods = [
            [
                'name' => 'Cheque',
            ],
            [
                'name' => 'Depósito en Efectivo',
            ],
            [
                'name' => 'Efectivo',
            ],
            [
                'name' => 'Tarjeta de Crédito',
            ],
            [
                'name' => 'Tarjeta de Débito',
            ],
            [
                'name' => 'Transferencias',
            ],
        ];

        DB::transaction(function () use ($permissions, $paymentMethods) {
            $adminRole = Role::where('name', 'admin')->first();
            $userSchoolRole = Role::where('name', 'school')->first();
            $userFamilyRole = Role::where('name', 'family')->first();
            foreach ($permissions as $permission) {
                $per = Permission::updateOrCreate(
                    ['name' => $permission['name']],
                );
                $adminRole->givePermissionTo($per);
                $userSchoolRole->givePermissionTo($per);
                $userFamilyRole->givePermissionTo($per);
            }

            foreach ($paymentMethods as $paymentMethod) {
                PaymentMethod::updateOrCreate(
                    ['name' => $paymentMethod['name']]
                );
            }
        });
    }
}
