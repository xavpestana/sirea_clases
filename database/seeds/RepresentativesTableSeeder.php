<?php

use Illuminate\Database\Seeder;

use Carbon\Carbon;
use App\Representative;
use App\Family;
use App\User;


class RepresentativesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	$userFamily = User::where('email', 'family@email.com')->first();

    	$family = Family::where('user_id', $userFamily->id)->first();

        DB::transaction(function () use($family) {

	        $representative = Representative::updateOrCreate(
	            [	'document_number' => '12345678'	],
	            [
	                'family_id' => $family->id,
	                'document_type' => 'CE',
	                'first_name_1' => 'Student',
	                'last_name_1' => 'Family',
	                'last_name_2' => 'Family',                            
	                'birthdate' => Carbon::now()->subYears(30),
	                'phone' => '04141234567',
	                'relationship_id' => 1,
	                'country_id' => 1,
	                'gender_id' => 1
	            ]
	        );

	        if(!$representative) {
	            throw new Exception('Error creando el representante por defecto');
	        }
	   	});
    }
}
