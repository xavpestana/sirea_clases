<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;
use App\User;
use App\Student;
use App\Profile;

/**
 * @class UsersTableSeeder
 * @brief información por defecto de usuarios de prueba
 *
 * Gestiona la información por defecto a registrar inicialmente para un estudainte por defecto
 *
 * @author Paúl Rojas <paul.rojase@gmail.com>
 * @license <a href='​http://www.gnu.org/licenses/gpl-2.0.html'>GNU Public License versión 2 (GPLv2)</a>
 */
class StudentsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();

        /** @var array Permisos disponibles para la gestión del perfil extendido de usuario estudiante */
        $permissions = [
            [
                'name' => 'students.list'
            ],
            [
                'name' => 'students.create'
            ],
            [
                'name' => 'students.show'
            ],
            [
                'name' => 'students.edit'
            ],
            [
                'name' => 'students.delete'
            ],
        ];

        $userFamily = User::where('email', 'family@email.com')->first();

        $userStudent = User::where('email', 'student@email.com')->first();

        $profileStudent = Profile::where('user_id', $userStudent->id)->first();

        DB::transaction(function () use ($userFamily, $profileStudent, $permissions) {
            $student = Student::updateOrCreate(
                [   'profile_id' => $profileStudent->id    ],
                [   'family_id' => $userFamily->family->id]
            );
            if (!$student) {
                throw new Exception('Error creando el estudiante por defecto');
            }

            $adminRole = Role::where('name', 'admin')->first();
            $userSchoolRole = Role::where('name', 'school')->first();
            foreach ($permissions as $permission) {
                $per = Permission::updateOrCreate(
                    ['name' => $permission['name']],
                );
                $adminRole->givePermissionTo($per);
                $userSchoolRole->givePermissionTo($per);
            }
        });
    }
}
