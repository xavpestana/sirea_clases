<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;

use App\User;
use App\Family;

/**
 * @class UsersTableSeeder
 * @brief información por defecto de usuarios de prueba
 *
 * Gestiona la información por defecto a registrar inicialmente para una familia por defecto
 *
 * @author Paúl Rojas <paul.rojase@gmail.com>
 * @license <a href='​http://www.gnu.org/licenses/gpl-2.0.html'>GNU Public License versión 2 (GPLv2)</a>
 */
class FamiliesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        /** @var array Permisos disponibles para la gestión del perfil extendido de usuario familia */
        $permissions = [
            [
                'name' => 'families.list'
            ],
            [
                'name' => 'families.create'
            ],
            [
                'name' => 'families.show'
            ],
            [
                'name' => 'families.edit'
            ],
            [
                'name' => 'families.delete'
            ],
        ];

        $userFamily = User::where('email', 'family@email.com')->first();

        DB::transaction(function () use ($userFamily, $permissions) {
            $family = Family::updateOrCreate(
                [
                    'user_id' => $userFamily->id,
                    'school_id' => 1,
                    'last_name_1' => 'Family',
                    'last_name_2' => 'Family',
                    'parish_id' => 1
                ]
            );
            if (!$family) {
                throw new Exception('Error creando familia por defecto');
            }

            $adminRole = Role::where('name', 'admin')->first();
            $userSchoolRole = Role::where('name', 'school')->first();
            foreach ($permissions as $permission) {
                $per = Permission::updateOrCreate(
                    ['name' => $permission['name']],
                );
                $adminRole->givePermissionTo($per);
                $userSchoolRole->givePermissionTo($per);
            }
        });
    }
}
