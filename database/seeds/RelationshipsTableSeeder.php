<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use App\Relationship;

class RelationshipsTableSeeder extends Seeder
{
    /**
     * Método que registra los valores iniciales de los parentescos
     *
     * @author Paúl Rojas <paul.rojase@gmail.com>
     * @return void
     */
    public function run()
    {
        Model::unguard();

        /** @var array Listado de parentescos a registrar */
        $relationships = [
            [
                'name' => 'Padre'
            ],
            [
                'name' => 'Madre'
            ],
            [
                'name' => 'Hermano'
            ],
            [
                'name' => 'Hermana'
            ],
            [
                'name' => 'Tío'
            ],
            [
                'name' => 'Tía'
            ],
            [
                'name' => 'Abuelo'
            ],
            [
                'name' => 'Abuela'
            ]
        ];

        DB::transaction(function () use ($relationships) {
            foreach ($relationships as $relationship) {
                Relationship::updateOrCreate(
                    ['name' => $relationship['name']]
                );
            }
        });
    }
}
