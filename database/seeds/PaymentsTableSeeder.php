<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;
use App\Payment;

/**
 * @class PaymentsTableSeeder
 * @brief información por defecto de los pagos
 *
 * Gestiona la información por defecto a registrar inicialmente para los pagos
 *
 * @author William Páez <paez.william8@gmail.com>
 * @license <a href='​http://www.gnu.org/licenses/gpl-2.0.html'>GNU Public License versión 2 (GPLv2)</a>
 */
class PaymentsTableSeeder extends Seeder
{
    /**
     * Método que registra los valores iniciales de los pagos
     *
     * @author William Páez <paez.william8@gmail.com>
     * @return void
     */
    public function run()
    {
        Model::unguard();

        /** @var array Permisos disponibles para la gestión de pagos */
        $permissions = [
            [
                'name' => 'payments.list'
            ],
            [
                'name' => 'payments.create'
            ],
            [
                'name' => 'payments.show'
            ],
            [
                'name' => 'payments.edit'
            ],
            [
                'name' => 'payments.delete'
            ],
        ];

        DB::transaction(function () use ($permissions) {
            $adminRole = Role::where('name', 'admin')->first();
            $userSchoolRole = Role::where('name', 'school')->first();
            foreach ($permissions as $permission) {
                $per = Permission::updateOrCreate(
                    ['name' => $permission['name']],
                );
                $adminRole->givePermissionTo($per);
                $userSchoolRole->givePermissionTo($per);
            }
        });
    }
}
