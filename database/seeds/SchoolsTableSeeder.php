<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;

use App\School;

/**
 * @class UsersTableSeeder
 * @brief información por defecto de usuarios de prueba
 *
 * Gestiona la información por defecto a registrar inicialmente para un colegio por defecto
 *
 * @author Paúl Rojas <paul.rojase@gmail.com>
 * @license <a href='​http://www.gnu.org/licenses/gpl-2.0.html'>GNU Public License versión 2 (GPLv2)</a>
 */
class SchoolsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        /** @var array Permisos disponibles para la gestión del colegio */
        $permissions = [
            [
                'name' => 'schools.list'
            ],
            [
                'name' => 'schools.create'
            ],
            [
                'name' => 'schools.show'
            ],
            [
                'name' => 'schools.edit'
            ],
            [
                'name' => 'schools.delete'
            ],
        ];

        DB::transaction(function () use ($permissions) {
            $school = School::updateOrCreate(
                [
                    'school_type' => 'Colegio',
                    'name' => 'Demo Venezuela',
                    'parish_id' => 1,
                    'address' => 'Venezuela',
                    'email' => 'demo@email.com'
                ]
            );
            if (!$school) {
                throw new Exception('Error creando el colegio por defecto');
            }

            $adminRole = Role::where('name', 'admin')->first();
            $userSchoolRole = Role::where('name', 'school')->first();
            foreach ($permissions as $permission) {
                $per = Permission::updateOrCreate(
                    ['name' => $permission['name']],
                );
                $adminRole->givePermissionTo($per);
                $userSchoolRole->givePermissionTo($per);
            }
        });
    }
}
