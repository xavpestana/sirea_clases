<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;
use App\Enrollment;

/**
 * @class EnrollmentTableSeeder
 * @brief información por defecto de las inscripciones
 *
 * Gestiona la información por defecto a registrar inicialmente para las inscripciones
 *
 * @author William Páez <paez.william8@gmail.com>
 * @license <a href='​http://www.gnu.org/licenses/gpl-2.0.html'>GNU Public License versión 2 (GPLv2)</a>
 */
class EnrollmentsTableSeeder extends Seeder
{
    /**
     * Método que registra los valores iniciales de las inscripciones
     *
     * @author William Páez <paez.william8@gmail.com>
     * @return void
     */
    public function run()
    {
        Model::unguard();

        /** @var array Permisos disponibles para la gestión de inscripciones */
        $permissions = [
            [
                'name' => 'enrollments.list'
            ],
            [
                'name' => 'enrollments.create'
            ],
            [
                'name' => 'enrollments.show'
            ],
            [
                'name' => 'enrollments.edit'
            ],
            [
                'name' => 'enrollments.delete'
            ],
        ];

        DB::transaction(function () use ($permissions) {
            $adminRole = Role::where('name', 'admin')->first();
            $userSchoolRole = Role::where('name', 'school')->first();
            foreach ($permissions as $permission) {
                $per = Permission::updateOrCreate(
                    ['name' => $permission['name']],
                );
                $adminRole->givePermissionTo($per);
                $userSchoolRole->givePermissionTo($per);
            }
        });
    }
}
