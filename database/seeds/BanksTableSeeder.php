<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;
use App\Bank;

/**
 * @class BanksTableSeeder
 * @brief información por defecto de bancos
 *
 * Gestiona la información por defecto a registrar inicialmente para los bancos
 *
 * @author William Páez <paez.william8@gmail.com>
 * @license <a href='​http://www.gnu.org/licenses/gpl-2.0.html'>GNU Public License versión 2 (GPLv2)</a>
 */
class BanksTableSeeder extends Seeder
{
    /**
     * Método que registra los valores iniciales de los bancos
     *
     * @author William Páez <paez.william8@gmail.com>
     * @return void
     */
    public function run()
    {
        Model::unguard();

        /** @var array Permisos disponibles para la gestión de bancos */
        $permissions = [
            [
                'name' => 'banks.list'
            ],
            [
                'name' => 'banks.create'
            ],
            [
                'name' => 'banks.show'
            ],
            [
                'name' => 'banks.edit'
            ],
            [
                'name' => 'banks.delete'
            ],
        ];

        /** @var array Listado de bancos a registrar */
        $banks = [
            [
                'code' => '0001',
                'short_name' => 'BCV',
                'name' => 'BANCO CENTRAL DE VENEZUELA',
            ],
            [
                'code' => '0003',
                'short_name' => 'BIV',
                'name' => 'BANCO INDUSTRIAL DE VENEZUELA, C.A. BANCO UNIVERSAL',
            ],
            [
                'code' => '0102',
                'short_name' => 'BDV',
                'name' => 'BANCO DE VENEZUELA S.A.C.A. BANCO UNIVERSAL',
            ],
            [
                'code' => '0104',
                'short_name' => 'BVC',
                'name' => 'VENEZOLANO DE CRÉDITO, S.A. BANCO UNIVERSAL',
            ],
            [
                'code' => '0105',
                'short_name' => 'MERCANTIL',
                'name' => 'BANCO MERCANTIL, C.A. S.A.C.A. BANCO UNIVERSAL',
            ],
            [
                'code' => '0108',
                'short_name' => 'BBVA',
                'name' => 'BANCO PROVINCIAL, S.A. BANCO UNIVERSAL',
            ],
            [
                'code' => '0114',
                'short_name' => 'BANCARIBE',
                'name' => 'BANCARIBE C.A. BANCO UNIVERSAL',
            ],
            [
                'code' => '0115',
                'short_name' => 'EXTERIOR',
                'name' => 'BANCO EXTERIOR C.A. BANCO UNIVERSAL',
            ],
            [
                'code' => '0116',
                'short_name' => 'BOD',
                'name' => 'BANCO OCCIDENTAL DE DESCUENTO, BANCO UNIVERSAL C.A.',
            ],
            [
                'code' => '0128',
                'short_name' => 'CARONI',
                'name' => 'BANCO CARONÍ C.A., BANCO UNIVERSAL',
            ],
            [
                'code' => '0134',
                'short_name' => 'BANESCO',
                'name' => 'BANESCO BANCO UNIVERSAL S.A.C.A.',
            ],
            [
                'code' => '0137',
                'short_name' => 'SOFITASA',
                'name' => 'BANCO SOFITASA BANCO UNIVERSAL',
            ],
            [
                'code' => '0138',
                'short_name' => 'PLAZA',
                'name' => 'BANCO PLAZA BANCO UNIVERSAL',
            ],
            [
                'code' => '0146',
                'short_name' => 'BANGENTE',
                'name' => 'BANCO DE LA GENTE EMPRENDEDORA C.A.',
            ],
            [
                'code' => '0149',
                'short_name' => '',
                'name' => 'BANCO DEL PUEBLO SOBERANO, C.A. BANCO DE DESARROLLO',
            ],
            [
                'code' => '0151',
                'short_name' => 'BFC',
                'name' => 'BANCO FONDO COMÚN C.A. BANCO UNIVERSAL',
            ],
            [
                'code' => '0156',
                'short_name' => '100%',
                'name' => '100% BANCO, BANCO UNIVERSAL C.A.',
            ],
            [
                'code' => '0157',
                'short_name' => 'DELSUR',
                'name' => 'DELSUR BANCO UNIVERSAL, C.A.',
            ],
            [
                'code' => '0163',
                'short_name' => 'TESORO',
                'name' => 'BANCO DEL TESORO, C.A. BANCO UNIVERSAL',
            ],
            [
                'code' => '0166',
                'short_name' => 'BAV',
                'name' => 'BANCO AGRÍCOLA DE VENEZUELA, C.A. BANCO UNIVERSAL',
            ],
            [
                'code' => '0168',
                'short_name' => 'BANCRECER',
                'name' => 'BANCRECER, S.A. BANCO MICROFINANCIERO',
            ],
            [
                'code' => '0169',
                'short_name' => 'MI BANCO',
                'name' => 'MI BANCO BANCO MICROFINANCIERO C.A.',
            ],
            [
                'code' => '0171',
                'short_name' => 'ACTIVO',
                'name' => 'BANCO ACTIVO, C.A. BANCO UNIVERSAL',
            ],
            [
                'code' => '0172',
                'short_name' => 'BANCAMIGA',
                'name' => 'BANCAMIGA BANCO MICROFINANCIERO C.A.',
            ],
            [
                'code' => '0173',
                'short_name' => 'BID',
                'name' => 'BANCO INTERNACIONAL DE DESARROLLO, C.A. BANCO UNIVERSAL',
            ],
            [
                'code' => '0174',
                'short_name' => 'BANPLUS',
                'name' => 'BANPLUS BANCO UNIVERSAL, C.A.',
            ],
            [
                'code' => '0175',
                'short_name' => 'BICENTENARIO',
                'name' => 'BANCO BICENTENARIO BANCO UNIVERSAL C.A.',
            ],
            [
                'code' => '0177',
                'short_name' => 'BANFANB',
                'name' => 'BANCO DE LA FUERZA ARMADA NACIONAL BOLIVARIANA, B.U.',
            ],
            [
                'code' => '0190',
                'short_name' => 'CITI',
                'name' => 'CITIBANK',
            ],
            [
                'code' => '0191',
                'short_name' => 'BNC',
                'name' => 'BANCO NACIONAL DE CRÉDITO, C.A. BANCO UNIVERSAL',
            ]
        ];

        DB::transaction(function () use ($permissions, $banks) {
            foreach ($banks as $bank) {
                Bank::updateOrCreate(
                    ['code' => $bank['code']],
                    [
                        'short_name' => $bank['short_name'],
                        'name' => $bank['name'],
                    ]
                );
            }

            $adminRole = Role::where('name', 'admin')->first();
            $userSchoolRole = Role::where('name', 'school')->first();
            foreach ($permissions as $permission) {
                $per = Permission::updateOrCreate(
                    ['name' => $permission['name']],
                );
                $adminRole->givePermissionTo($per);
                $userSchoolRole->givePermissionTo($per);
            }
        });
    }
}
