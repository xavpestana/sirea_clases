<?php

use Illuminate\Database\Seeder;

/**
 * @class DatabaseSeeder
 * @brief Gestiona los seeders del sistema
 *
 * @author William Páez <paez.william8@gmail.com>
 * @license <a href='​http://www.gnu.org/licenses/gpl-2.0.html'>GNU Public License versión 2 (GPLv2)</a>
 */
class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
    **/
    public function run()
    {
        /** Seeder para los roles disponibles */
        $this->call(RolesTableSeeder::class);
        /** Seeder para los permisos disponibles */
        $this->call(PermissionsTableSeeder::class);

        /** Seeder para paises, estados, ciudades, municipio y parroquias */
        $this->call(CountriesTableSeeder::class);
        $this->call(StatesTableSeeder::class);
        $this->call(CitiesTableSeeder::class);
        $this->call(MunicipalitiesTableSeeder::class);
        $this->call(ParishesTableSeeder::class);
        /** Seeder para los géneros disponibles */
        $this->call(GendersTableSeeder::class);
        /** Seeder para los parentescos disponibles */
        $this->call(RelationshipsTableSeeder::class);

        /** Seeder para el Colegio por defecto */
        $this->call(SchoolsTableSeeder::class);
        $this->call(DirectorsTableSeeder::class);
        $this->call(LevelsTableSeeder::class);
        $this->call(GradesTableSeeder::class);

        /** Seeder para los usuarios de prueba disponibles del sistema */
        $this->call(UsersTableSeeder::class);
        $this->call(UserSchoolsTableSeeder::class);
        //$this->call(FamiliesTableSeeder::class);
        //$this->call(ProfilesTableSeeder::class);
        //$this->call(RepresentativesTableSeeder::class);
        //$this->call(StudentsTableSeeder::class);

        /** Seeder para los conceptos de inscripción disponibles */
        $this->call(ConceptsTableSeeder::class);
        /** Seeder para los bancos disponibles */
        $this->call(BanksTableSeeder::class);
        /** Seeder para los métodos de pago disponibles */
        $this->call(PaymentMethodsTableSeeder::class);
        /** Seeder para los planes de pago disponibles */
        $this->call(PlansTableSeeder::class);
        /** Seeder para las inscripciones disponibles */
        $this->call(EnrollmentsTableSeeder::class);
        /** Seeder para las cuentas bancarias de colegios disponibles */
        $this->call(BankAccountsTableSeeder::class);
        /** Seeder para las cuentas bancarias de familias disponibles */
        $this->call(FamilyBankAccountsTableSeeder::class);
        /** Seeder para los pagos disponibles */
        $this->call(PaymentsTableSeeder::class);
    }
}
