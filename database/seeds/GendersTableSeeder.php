<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;
use App\Gender;

/**
 * @class GendersTableSeeder
 * @brief información por defecto de géneros
 *
 * Gestiona la información por defecto a registrar inicialmente para los géneros
 *
 * @author William Páez <paez.william8@gmail.com>
 * @license <a href='​http://www.gnu.org/licenses/gpl-2.0.html'>GNU Public License versión 2 (GPLv2)</a>
 */
class GendersTableSeeder extends Seeder
{
    /**
     * Método que registra los valores iniciales de los géneros
     *
     * @author William Páez <paez.william8@gmail.com>
     * @return void
     */
    public function run()
    {
        Model::unguard();

        $adminRole = Role::where('name', 'admin')->first();

        /** @var array Permisos disponibles para la gestión de géneros */
        $permissions = [
            [
                'name' => 'genders.list'
            ],
            [
                'name' => 'genders.create'
            ],
            [
                'name' => 'genders.edit'
            ],
            [
                'name' => 'genders.delete'
            ],
        ];

        /** @var array Listado de géneros a registrar */
        $genders = [
            [
                'name' => 'Masculino'
            ],
            [
                'name' => 'Femenino'
            ]
        ];

        DB::transaction(function () use ($genders, $permissions, $adminRole) {
            foreach ($genders as $gender) {
                Gender::updateOrCreate(
                    ['name' => $gender['name']]
                );
            }

            foreach ($permissions as $permission) {
                $per = Permission::updateOrCreate(
                    ['name' => $permission['name']],
                );
                $adminRole->givePermissionTo($per);
            }
        });
    }
}
