<?php

use Illuminate\Database\Seeder;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use App\Level;
use App\Grade;


class GradesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();

        /** @var array Listado de grados a registrar */

        $levels_grades = [
            "Pre-Maternal" => [
                "Pre-Maternal"
            ],
            "Maternal" => [
                "Maternal"
            ],
            "Inicial" => [
                "I Nivel",
                "II Nivel",
                "III Nivel"
            ],            
            "Primaria" => [
                "1er Grado",
                "2do Grado",
                "3er Grado",
                "4to Grado",
                "5to Grado",
                "6to Grado"
            ],  
            "Media General" => [
                "1er Año",
                "2do Año",
                "3er Año",
                "4to Año",
                "5to Año"
            ],
            "Media Técnica" => [
                "6to Año"
            ],              
        ];

        DB::transaction(function () use ($levels_grades) {
            foreach ($levels_grades as $level_name =>  $grades) {
                /** @var object Almacena información del nivel */
                $level = Level::where('name', $level_name)->first();
                foreach ($grades as $grade) {
                    if ($grade) {
                        Grade::updateOrCreate(
                            ['name' => $grade, 'level_id' => $level->id]
                        );
                    }
                }
            }
        });
    }
}
