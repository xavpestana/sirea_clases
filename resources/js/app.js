/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

window.Vue = require('vue');

import Print from 'vue-print-nb'
Vue.use(Print);

import {ServerTable, ClientTable, Event} from 'vue-tables-2';
Vue.use(ClientTable);

/**
 * The following block of code may be used to automatically register your
 * Vue components. It will recursively scan this directory for the Vue
 * components and automatically register them with their "basename".
 *
 * Eg. ./components/ExampleComponent.vue -> <example-component></example-component>
 */

// const files = require.context('./', true, /\.vue$/i)
// files.keys().map(key => Vue.component(key.split('/').pop().split('.')[0], files(key).default))

Vue.component('example-component', require('./components/ExampleComponent.vue').default);

/**
 * Componente genérico para el uso de listas desplegables con select2 y selects dependientes
 *
 * @author William Páez <paez.william8@gmail.com>
 * @author  Ing. Roldan Vargas <roldandvg@gmail.com>
 */
Vue.component('select2', require('./components/settings/SelectsComponent.vue').default);

/**
 * Componente genérico para el uso de listas desplegables de selección multiple
 *
 * @author  William Páez <paez.william8@gmail.com>
 * @author  Ing. Roldan Vargas <roldandvg@gmail.com>
 *
 * @param array     options         Arreglo de objetos con las opciones a cargar.
 *                                  Ej.: [{clave: clave, valor: valor}, ...]
 * @param string    track_by        Define el nombre de la clave por la cual se va a gestionar la información.
 * @param boolean   taggable        Define si se muestra la selección mediante tags o no (opcional).
 *                                  El valor por defecto es true.
 * @param string    id              Define el identificador del objeto (opcional).
 * @param boolean   preselect_first Define si se preselecciona el primero objeto del arreglo (opcional).
 *                                  El valor por defecto es false
 * @param boolean   preserve_search Define si se preserva el campo de búsqueda (opcional).
 *                                  El valor por defecto es true
 * @param boolean   hide_selected   Define si se ocultan los elementos seleccionados (opcional).
 *                                  El valor por defecto es true
 * @param boolean   clear_on_select Define si se limpia el texto al seleccionar un elemento (opcional).
 *                                  El valor por defecto es true
 * @param boolean   close_on_select Define si se cierra la lista de elementos al seleccionar uno de ellos (opcional).
 *                                  El valor por defecto es true
 *
 * @note
 * Ejemplo de uso:
 * <v-multiselect :options="[{key: 1, name: 'one'},{key: 2, name: 'two'},{key: 3, name: 'three'}]" track_by="key"
 * :hide_selected="false"></v-multiselect>
 */
Vue.component('v-multiselect', require('./components/settings/MultiSelectsComponent.vue').default);

/**
 * Componente para el listado de usuarios
 *
 * @author William Páez <paez.william8@gmail.com>
 */
Vue.component('user-list', require('./components/settings/UserListComponent.vue').default);

/**
 * Componente para crear y editar usuarios
 *
 * @author William Páez <paez.william8@gmail.com>
 */
Vue.component('user', require('./components/settings/UserComponent.vue').default);

/**
 * Componente para el listado de usuarios escolares
 *
 * @author William Páez <paez.william8@gmail.com>
 */
Vue.component('user-school-list', require('./components/school/UserSchoolListComponent.vue').default);

/**
 * Componente para crear y editar usuarios escolares
 *
 * @author William Páez <paez.william8@gmail.com>
 */
Vue.component('user-school', require('./components/school/UserSchoolComponent.vue').default);

/**
 * Componente para el listado de conceptos de inscripción
 *
 * @author William Páez <paez.william8@gmail.com>
 */
Vue.component('concept-list', require('./components/payment-settings/ConceptListComponent.vue').default);

/**
 * Componente para crear y editar conceptos de inscripción
 *
 * @author William Páez <paez.william8@gmail.com>
 */
Vue.component('concept', require('./components/payment-settings/ConceptComponent.vue').default);

/**
 * Componente para el listado de bancos
 *
 * @author William Páez <paez.william8@gmail.com>
 */
Vue.component('bank-list', require('./components/payment-settings/BankListComponent.vue').default);

/**
 * Componente para crear y editar bancos
 *
 * @author William Páez <paez.william8@gmail.com>
 */
Vue.component('bank', require('./components/payment-settings/BankComponent.vue').default);

/**
 * Componente para el listado de métodos de pago
 *
 * @author William Páez <paez.william8@gmail.com>
 */
Vue.component('payment-method-list', require('./components/payment-settings/PaymentMethodListComponent.vue').default);

/**
 * Componente para crear y editar métodos de pago
 *
 * @author William Páez <paez.william8@gmail.com>
 */
Vue.component('payment-method', require('./components/payment-settings/PaymentMethodComponent.vue').default);

/**
 * Componente para el listado de planes de pago
 *
 * @author William Páez <paez.william8@gmail.com>
 */
Vue.component('plan-list', require('./components/payment-settings/PlanListComponent.vue').default);

/**
 * Componente para crear y editar planes de pago
 *
 * @author William Páez <paez.william8@gmail.com>
 */
Vue.component('plan', require('./components/payment-settings/PlanComponent.vue').default);

/**
 * Componente para el listado de inscripciones
 *
 * @author William Páez <paez.william8@gmail.com>
 */
Vue.component('enrollment-list', require('./components/payment-settings/EnrollmentListComponent.vue').default);

/**
 * Componente para crear y editar inscripciones
 *
 * @author William Páez <paez.william8@gmail.com>
 */
Vue.component('enrollment', require('./components/payment-settings/EnrollmentComponent.vue').default);

/**
 * Componente para el listado de cuentas bancarias del colegio
 *
 * @author William Páez <paez.william8@gmail.com>
 */
Vue.component('bank-account-list', require('./components/payment-settings/BankAccountListComponent.vue').default);

/**
 * Componente para crear y editar cuentas bancarias de colegios
 *
 * @author William Páez <paez.william8@gmail.com>
 */
Vue.component('bank-account', require('./components/payment-settings/BankAccountComponent.vue').default);

/**
 * Componente para crear y editar pagos
 *
 * @author William Páez <paez.william8@gmail.com>
 */
Vue.component('payment', require('./components/payment-settings/PaymentComponent.vue').default);

/**
 * Componente para el listado de pagos de los familiares
 *
 * @author William Páez <paez.william8@gmail.com>
 */
Vue.component('family-payment-list', require('./components/family/FamilyPaymentListComponent.vue').default);

/**
 * Componente para crear pagos de familia
 *
 * @author William Páez <paez.william8@gmail.com>
 */
Vue.component('family-payment', require('./components/family/FamilyPaymentComponent.vue').default);

/**
 * Componente para el listado de pagos de los familiares
 *
 * @author William Páez <paez.william8@gmail.com>
 */
Vue.component('payment-list', require('./components/payment-settings/PaymentListComponent.vue').default);

/**
 * Opciones de configuración global para utilizar en todos los componentes vuejs de la aplicación
 *
 * @author  William Páez <paez.william8@gmail.com>
 * @param  {object} methods Métodos generales a implementar en CRUDS
 */
Vue.mixin({
    data() {
		return {
			/**
			 * Opciones generales a implementar en tablas
			 * @type {JSON}
			 */
			table_options: {
				pagination: { edge: true },
				//filterByColumn: true,
				highlightMatches: true,
				texts: {
                    filter: "Buscar:",
                    filterBy: 'Buscar por {column}',
                    //count:'Página {page}',
                    count: ' ',
                    first: 'PRIMERO',
                    last: 'ÚLTIMO',
                    limit: 'Registros',
                    //page: 'Página:',
                    noResults: 'No existen registros',
                    filterPlaceholder: '...',
				},
				sortIcon: {
					is: 'fa-sort cursor-pointer',
					base: 'fa',
					up: 'fa-sort-up cursor-pointer',
					down: 'fa-sort-down cursor-pointer'
				},
			},
		}
	},
    props: {
		route_list: {
			type: String,
			required: false,
			default: ''
		},
		route_create: {
			type: String,
			required: false,
			default: ''
		},
		route_edit: {
			type: String,
			required: false,
			default: ''
		},
		route_update: {
			type: String,
			required: false,
			default: ''
		},
		route_delete: {
			type: String,
			required: false,
			default: ''
		},
		route_show: {
			type: String,
			required: false,
			default: ''
		}
	},
    methods: {
        /**
         * Inicializa los registros base del formulario
         *
         * @author  William Páez <paez.william8@gmail.com>
         * @author Ing. Roldan Vargas <roldandvg@gmail.com>
         * @param {string} url      Ruta que obtiene los datos a ser mostrado en listados
         * @param {string} modal_id Identificador del modal a mostrar con la información solicitada
         */
        initRecords(url, modal_id) {
            this.errors = [];
            this.reset();
            const vm = this;

            axios.get(url).then(response => {
                if (typeof(response.data.records) !== "undefined") {
                    vm.records = response.data.records;
                }
                if ($("#" + modal_id).length) {
                    $("#" + modal_id).modal('show');
                }
            }).catch(error => {
                if (typeof(error.response) !== "undefined") {
                    if (error.response.status == 403) {
                        vm.showMessage(
                            'custom', 'Acceso Denegado', 'danger', 'screen-error', error.response.data.message
                        );
                    }
                    else {
                        vm.logs('resources/js/all.js', 343, error, 'initRecords');
                    }
                }
            });
        },
        /**
		 * Método que obtiene los registros a mostrar
		 *
		 * @author  William Páez <paez.william8@gmail.com>
         * @author  Ing. Roldan Vargas <roldandvg@gmail.com>
		 * @param  {string} url Ruta que obtiene todos los registros solicitados
		 */
		readRecords(url) {
			const vm = this;
			axios.get('/' + url).then(response => {
				if (typeof(response.data.records) !== "undefined") {
					vm.records = response.data.records;
				}
			});
		},
        /**
		 * Método que permite crear o actualizar un registro
		 *
		 * @author  William Páez <paez.william8@gmail.com>
         * @author  Ing. Roldan Vargas <roldandvg@gmail.com>
		 * @param  {string} url Ruta de la acción a ejecutar para la creación o actualización de datos
		 */
		createRecord(url) {
			const vm = this;
			if (this.record.id) {
				this.updateRecord(url);
			}
			else {
				var fields = {};
				for (var index in this.record) {
					fields[index] = this.record[index];
				}
				axios.post('/' + url, fields).then(response => {
					if (typeof(response.data.redirect) !== "undefined") {
						location.href = response.data.redirect;
					}
					else {
						vm.reset();
						vm.readRecords(url);
						//vm.showMessage('store');
					}
				}).catch(error => {
					vm.errors = [];

					if (typeof(error.response) !="undefined") {
						for (var index in error.response.data.errors) {
							if (error.response.data.errors[index]) {
								vm.errors.push(error.response.data.errors[index][0]);
							}
						}
					}
				});
			}
		},
        /**
         * Redirecciona al formulario de actualización de datos
         *
         * @author  William Páez <paez.william8@gmail.com>
         * @author Ing. Roldan Vargas <roldandvg@gmail.com>
         * @param  {integer} id Identificador del registro a actualizar
         */
        editForm(id) {
            location.href = (this.route_edit.indexOf("{id}") >= 0)
                            ? this.route_edit.replace("{id}", id)
                            : this.route_edit + '/' + id;
        },
        /**
		 * Método que permite actualizar información
		 *
         * @author  William Páez <paez.william8@gmail.com>
         * @author  Ing. Roldan Vargas <roldandvg@gmail.com>
		 * @param  {string} url Ruta de la acci´on que modificará los datos
		 */
		updateRecord(url) {
			const vm = this;
			var fields = {};
			for (var index in this.record) {
				fields[index] = this.record[index];
			}
			axios.patch('/' + url + '/' + this.record.id, fields).then(response => {
				if (typeof(response.data.redirect) !== "undefined") {
					location.href = response.data.redirect;
				}
				else {
					vm.readRecords(url);
					vm.reset();
					vm.showMessage('update');
				}
			}).catch(error => {
				vm.errors = [];

				if (typeof(error.response) !="undefined") {
					for (var index in error.response.data.errors) {
						if (error.response.data.errors[index]) {
							vm.errors.push(error.response.data.errors[index][0]);
						}
					}
				}
			});
	    },
        /**
         * Método que muestra datos de un registro seleccionado
         *
         * @author  William Páez <paez.william8@gmail.com>
         * @author  Ing. Roldan Vargas <roldandvg@gmail.com>
         * @param  {integer} id Identificador del registro a mostrar
         */
        showRecord(id) {
            if (typeof(this.route_show) !== "undefined" && this.route_show) {
                if (this.route_show.indexOf("{id}") >= 0) {
                    location.href = this.route_show.replace("{id}", id);
                }
                else {
                    location.href = this.route_show + '/' + id;
                }
            }
        },
        /**
         * Método para la eliminación de registros
         *
         * @author  William Páez <paez.william8@gmail.com>
         * @author  Ing. Roldan Vargas <roldandvg@gmail.com>
         * @param  {integer} index Elemento seleccionado para su eliminación
         * @param  {string}  url   Ruta que ejecuta la acción para eliminar un registro
         */
        deleteRecord(index, url) {
            var url = (url)?url:this.route_delete;
            var records = this.records;
            var confirmated = false;
            var index = index - 1;
            const vm = this;

            bootbox.confirm({
                title: "Eliminar registro?",
                message: "Esta seguro de eliminar este registro?",
                buttons: {
                    cancel: {
                        label: '<i class="fa fa-times"></i> Cancelar'
                    },
                    confirm: {
                        label: '<i class="fa fa-check"></i> Confirmar'
                    }
                },
                callback: function (result) {
                    if (result) {
                        confirmated = true;
                        axios.delete(url + '/' + records[index].id).then(response => {
                            if (typeof(response.data.error) !== "undefined") {
                                /** Muestra un mensaje de error si sucede algún evento en la eliminación */
                                vm.showMessage('custom', 'Alerta!', 'warning', 'screen-error', response.data.message);
                                return false;
                            }
                            records.splice(index, 1);
                            vm.showMessage('destroy');
                        }).catch(error => {
                            //vm.logs('mixins.js', 498, error, 'deleteRecord');
                        });
                    }
                }
            });

            if (confirmated) {
                this.records = records;
                this.showMessage('destroy');
            }
        },
        /**
         * Elimina la fila del elemento indicado
         *
         * @author William Páez <paez.william8@gmail.com>
         * @author Ing. Roldan Vargas <roldandvg@gmail.com>
         *
         * @param  {integer}      index Indice del elemento a eliminar
         * @param  {object|array} el    Elemento del cual se va a eliminar un elemento
         */
        removeRow: function(index, el) {
            el.splice(index, 1);
        },
        /**
         * Obtiene un arreglo con los roles
         *
         * @author William Páez <paez.william8@gmail.com>
         */
        getRoles() {
            const vm = this;
            vm.roles = [];
            axios.get('/get-roles').then(response => {
                vm.roles = response.data;
            });
        },
        /**
		 * Obtiene los datos de los roles
		 *
		 * @author William Páez <paez.william8@gmail.com>
		 */
		getJsonRoles() {
			this.json_roles = [];
			axios.get('/get-json-roles').then(response => {
				this.json_roles = response.data.jsonRoles;
			});
		},
        /**
         * Obtiene los estados
         *
         * @author William Páez <paez.william8@gmail.com>
         */
        listStates() {
            this.states = [];
            axios.get('/list-states').then(response => {
                this.states = response.data;
            });
        },
        /**
         * Obtiene los municipios del estado seleccionado
         *
         * @author William Páez <paez.william8@gmail.com>
         */
        listMunicipalities() {
            this.municipalities = [];
            if (this.record.state_id) {
                axios.get('/list-municipalities/' + this.record.state_id).then(response => {
                    this.municipalities = response.data;
                });
            }
        },
        /**
         * Obtiene las ciudades del estado seleccionado
         *
         * @author William Páez <paez.william8@gmail.com>
         */
        listCities() {
            this.cities = [];
            if (this.record.estate_id) {
                axios.get('/list-cities/' + this.record.state_id).then(response => {
                    this.cities = response.data;
                });
            }
        },
        /**
         * Obtiene las parroquias del municipio seleccionado
         *
         * @author William Páez <paez.william8@gmail.com>
         */
        listParishes() {
            this.parishes = [];
            if (this.record.municipality_id) {
                axios.get('/list-parishes/' + this.record.municipality_id).then(response => {
                    this.parishes = response.data;
                });
            }
        },
        /**
         * Obtiene los colegios de la parroquia seleccionada
         *
         * @author William Páez <paez.william8@gmail.com>
         */
        listSchools() {
            this.schools = [];
            if (this.record.parish_id) {
                axios.get('/list-schools/' + this.record.parish_id).then(response => {
                    this.schools = response.data;
                });
            }
        },
        /**
         * Obtiene los estudiantes
         *
         * @author William Páez <paez.william8@gmail.com>
         */
        listStudents() {
            this.students = [];
            axios.get('/colegio/list-students').then(response => {
                this.students = response.data;
            });
        },
        /**
         * Obtiene los años escolares activos
         *
         * @author William Páez <paez.william8@gmail.com>
         */
        listSeasons() {
            this.seasons = [];
            axios.get('/colegio/list-seasons').then(response => {
                this.seasons = response.data;
            });
        },
        /**
         * Obtiene los niveles escolares del año escolar activo
         *
         * @author William Páez <paez.william8@gmail.com>
         */
        listLevels() {
            this.levels = [];
            axios.get('/colegio/list-levels').then(response => {
                this.levels = response.data;
            });
        },
        /**
         * Obtiene los grados escolares del nivel escolar
         *
         * @author William Páez <paez.william8@gmail.com>
         */
        listGrades() {
            this.grades = [];
            if (this.record.level_id) {
                axios.get('/colegio/list-grades/' + this.record.level_id).then(response => {
                    this.grades = response.data;
                });
            }
        },
        /**
         * Obtiene las secciones que pertenecen a un año escolar
         *
         * @author William Páez <paez.william8@gmail.com>
         */
        listSections() {
            this.sections = [];
            if (this.record.grade_id) {
                axios.get('/colegio/list-sections/' + this.record.grade_id).then(response => {
                    this.sections = response.data;
                });
            }
        },
        /**
         * Obtiene los planes de pago
         *
         * @author William Páez <paez.william8@gmail.com>
         */
        listPlans() {
            this.plans = [];
            axios.get('/pagos/list-plans').then(response => {
                this.plans = response.data;
            });
        },
        /**
         * Obtiene las cuotas que pertenecen a un plan de pago
         *
         * @author William Páez <paez.william8@gmail.com>
         */
        listInstallments() {
            this.installments = [];
            if (this.record.plan_id) {
                axios.get('/pagos/list-installments/' + this.record.plan_id).then(response => {
                    this.installments = response.data;
                });
            }
        },
        /**
         * Obtiene los conceptos de inscripción
         *
         * @author William Páez <paez.william8@gmail.com>
         */
        listConcepts() {
            this.concepts = [];
            axios.get('/pagos/list-concepts').then(response => {
                this.concepts = response.data;
            });
        },
        /**
		 * Obtiene los datos de los conceptos
		 *
		 * @author William Páez <paez.william8@gmail.com>
		 */
		listJsonConcepts() {
			this.json_concepts = [];
			axios.get('/pagos/list-json-concepts').then(response => {
				this.json_concepts = response.data.jsonConcepts;
			});
		},
        /**
         * Obtiene los bancos
         *
         * @author William Páez <paez.william8@gmail.com>
         */
        listBanks() {
            this.banks = [];
            axios.get('/pagos/list-banks').then(response => {
                this.banks = response.data;
            });
        },
        /**
         * Obtiene las inscripciones
         *
         * @author William Páez <paez.william8@gmail.com>
         */
        listEnrollments() {
            this.enrollments = [];
            axios.get('/pagos/list-enrollments').then(response => {
                this.enrollments = response.data;
            });
        },
        /**
         * Obtiene los métodos de pago de un colegio
         *
         * @author William Páez <paez.william8@gmail.com>
         */
        listPaymentMethods() {
            this.payment_methods = [];
            axios.get('/pagos/list-payment-methods').then(response => {
                this.payment_methods = response.data;
            });
        },
        /**
         * Obtiene los métodos de pago de un colegio
         *
         * @author William Páez <paez.william8@gmail.com>
         */
        listReceipts() {
            this.receipts = [];
            axios.get('/list-receipts').then(response => {
                this.receipts = response.data;
            });
        },
        /**
         * Obtiene las cuentas bancarias del colegio
         *
         * @author William Páez <paez.william8@gmail.com>
         */
        listBankAccounts() {
            this.bank_accounts = [];
            axios.get('/list-bank-accounts').then(response => {
                this.bank_accounts = response.data;
            });
        },
        /**
         * Obtiene las cuentas bancarias del familiar
         *
         * @author William Páez <paez.william8@gmail.com>
         */
        listFamilyBankAccounts() {
            this.family_bank_accounts = [];
            axios.get('/familia/list-family-bank-accounts').then(response => {
                this.family_bank_accounts = response.data;
            });
        },
    }
});

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

const app = new Vue({
    el: '#app',
});
