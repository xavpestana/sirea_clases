<aside class="left-sidebar">
    <!-- Sidebar scroll-->
    <div class="scroll-sidebar">
        <!-- User profile -->
        @auth
            <div class="user-profile">
                @php($user = auth()->user())
                @if($user->hasRole('admin'))
                    @php($school = null)
                @elseif($user->hasRole('school'))
                    @php($school = $user->userSchool->school)
                @elseif($user->hasRole('family'))
                    @php($school = $user->family->school)
                @elseif($user->hasRole('teacher'))
                    @php($school = $user->profile->teacher->school)
                @else
                    @php($school = $user->profile->student->family->school)
                @endif

                <div class="profile-img square-img">
                    @isset($school)
                    <img src="{{ asset('storage/pictures/logos/'.$school->logo, Request::secure()) }}" alt="user" />
                    @else
                    <img src="{{ asset('storage/pictures/avatars/default.jpg') }}" alt="user" />
                    @endisset
                    <!-- this is blinking heartbit-->
                    {{--<div class="notify setpos"> <span class="heartbit"></span> <span class="point"></span> </div>--}}
                </div>
                <!-- User profile text-->
                <div class="profile-text">
                    @isset($school)
                    <h5>{{ $school->school_type }}</h5>
                    <h5>{{ $school->name }}</h5>
                    @else
                    <h5>Administrador</h5>
                    @endisset
                    {{-- <a href="#" class="dropdown-toggle u-dropdown" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="true"><i class="mdi mdi-settings"></i></a>
                    <a href="app-email.html" class="" data-toggle="tooltip" title="Email"><i class="mdi mdi-gmail"></i></a>
                    <a href="{{ route('logout') }}" onclick="event.preventDefault();document.getElementById('logout-form1').submit();" class="" data-toggle="tooltip" title="Logout"><i class="mdi mdi-power"></i></a>
                    <form id="logout-form1" action="{{ route('logout') }}" method="POST"
                          style="display: none;">
                          @csrf
                    </form>
                    <div class="dropdown-menu animated flipInY">
                        <a href="#" class="dropdown-item"><i class="ti-user"></i> My Profile</a>
                        <a href="#" class="dropdown-item"><i class="ti-wallet"></i> My Balance</a>
                        <a href="#" class="dropdown-item"><i class="ti-email"></i> Inbox</a>
                        <div class="dropdown-divider"></div>
                        <a href="#" class="dropdown-item"><i class="ti-settings"></i> Account Setting</a>
                        <div class="dropdown-divider"></div>
                        <a href="{{ route('logout') }}" onclick="event.preventDefault();document.getElementById('logout-form2').submit();" class="dropdown-item"><i class="fa fa-power-off"></i> Logout</a>
                        <form id="logout-form2" action="{{ route('logout') }}" method="POST"
                              style="display: none;">
                              @csrf
                        </form>
                    </div> --}}
                </div>
            </div>
        @endauth
        <!-- End User profile text-->
        <!-- Sidebar navigation-->
        <nav class="sidebar-nav">
            <ul id="sidebarnav">
                <li class="nav-devider"></li>
                @role('school')
                <li class="{{ request()->route()->named('home') ? 'active' : '' }}"> <a class="waves-effect waves-dark" href="{{ route('home') }}" aria-expanded="false"><i class="mdi mdi-home"></i><span class="hide-menu">Inicio</span></a>
                <li class="nav-small-cap">CONTROL ACADÉMICO</li>
                <li class="{{ request()->route()->named('families.index')|| request()->route()->named('school.representatives.index') || request()->route()->named('school.students.index') || request()->route()->named('teachers.index') ? 'active' : '' }}">
                    <a class="has-arrow waves-effect waves-dark" href="#" aria-expanded="false"><i class="mdi mdi-book-open-variant"></i><span class="hide-menu">Registro General</span></a>
                    <ul aria-expanded="false" class="collapse">
                        <li><a class="{{ request()->route()->named('families.index') ? 'active' : '' }}" href="{{ route('families.index') }}">Familias</a></li>
                        <li><a class="{{ request()->route()->named('school.representatives.index') ? 'active' : '' }}" href="{{ route('school.representatives.index') }}">Representantes</a></li>
                        <li><a class="{{ request()->route()->named('school.students.index') ? 'active' : '' }}" href="{{ route('school.students.index') }}">Estudiantes</a></li>
                        <li><a class="{{ request()->route()->named('teachers.index') ? 'active' : '' }}" href="{{ route('teachers.index') }}">Docentes</a></li>
                    </ul>
                </li>
                <li class="{{ request()->route()->named('enrollments.index')||request()->route()->named('no-enrollments') ? 'active' : '' }}">
                    <a class="has-arrow waves-effect waves-dark" href="#" aria-expanded="false"><i class="mdi mdi-book-plus"></i><span class="hide-menu">Inscripciones</span></a>
                    <ul aria-expanded="false" class="collapse">
                        <li><a class="{{ request()->route()->named('enrollments.index') ? 'active' : '' }}" href="{{ route('enrollments.index') }}">Inscritos</a></li>
                        <li><a class="{{ request()->route()->named('no-enrollments') ? 'active' : '' }}" href="{{ route('no-enrollments') }}">Pendientes por inscribir</a></li>
                        {{-- <li><a class="{{ request()->route()->named('enrollments.index') ? 'active' : '' }}" href="{{ route('enrollments.index') }}">Inscripciones</a></li> --}}

                        {{-- <li><a class="" href="#">Pendientes</a></li> --}}
                        {{-- <li><a href="{{ route('payments.index') }}">Pagos</a></li> --}}
                    </ul>
                </li>
                <li class="{{ request()->route()->named('payment-settings.index')||request()->route()->named('payments.index') ||request()->route()->named('defaulter.index') ||request()->route()->named('reports.index')||request()->route()->named('ap.index') ||request()->route()->named('reports.report_paid') ? 'active' : '' }}">
                    <a class="has-arrow waves-effect waves-dark" href="#" aria-expanded="false"><i class="mdi mdi-currency-usd"></i><span class="hide-menu">Pagos</span></a>
                    <ul aria-expanded="false" class="collapse">
                        {{-- <li><a class="{{ request()->route()->named('enrollments.index') ? 'active' : '' }}" href="{{ route('enrollments.index') }}">Inscripciones</a></li> --}}

                        {{-- <li><a class="" href="#">Pendientes</a></li> --}}
                        {{-- <li><a href="{{ route('payments.index') }}">Pagos</a></li> --}}
                        <li><a class="{{ request()->route()->named('reports.index') ? 'active' : '' }}" href="{{ route('reports.index') }}">Reportes de facturas y en linea</a></li>
                        <li><a class="{{ request()->route()->named('reports.report_paid') ? 'active' : '' }}" href="{{ route('reports.report_paid') }}">Conteo de pagos</a></li>
                        <li><a class="{{ request()->route()->named('defaulter.index') ? 'active' : '' }}" href="{{ route('defaulter.index') }}">Registro de morosos</a></li>
                        <li><a class="{{ request()->route()->named('payments.index') ? 'active' : '' }}" href="{{ route('payments.index') }}">Registrar pago</a></li>
                        <li><a class="{{ request()->route()->named('ap.index') ? 'active' : '' }}" href="{{ route('ap.index') }}">Pagos sin facturas</a></li>
                        <li><a class="{{ request()->route()->named('payment-settings.index') ? 'active' : '' }}" href="{{ route('payment-settings.index') }}">Configuración</a></li>
                    </ul>
                </li>
                <li class="{{ request()->route()->named('courses.index') ? 'active' : '' }}">
                    <a class="has-arrow waves-effect waves-dark" href="#" aria-expanded="false"><i class="mdi mdi-book-open-page-variant"></i><span class="hide-menu">Académico</span></a>
                     <ul aria-expanded="false" class="collapse">
                        <li><a class="{{ request()->route()->named('courses.index') ? 'active' : '' }}" href="{{ route('courses.index') }}">Materias</a></li>
                    </ul>
                </li>
                <li class="{{ request()->route()->named('library.index') ? 'active' : '' }}">
                    <a class="waves-effect waves-dark" href="{{ route('library.index') }}" aria-expanded="false"><i class="mdi mdi-book"></i><span class="hide-menu">Biblioteca <br><small>El mejor negocio de tu vida</small></span></a>
                </li>
                <li class="{{ request()->route()->named('school.settings') ? 'active' : '' }}">
                    <a class="waves-effect waves-dark" href="{{ route('school.settings') }}" aria-expanded="false"><i class="mdi mdi-settings"></i><span class="hide-menu">Configuración</span></a>
                </li>
                @if(auth()->user()->userSchool->parent_id == null)
                <li class="{{ request()->route()->named('user-schools.index') ? 'active' : '' }}">
                    <a class="waves-effect waves-dark" href="{{ route('user-schools.index') }}" aria-expanded="false"><i class="mdi mdi-account-key"></i><span class="hide-menu">Usuarios Escolares</span></a>
                </li>
                @endif
                @endrole
                @role('family')
                    @if(auth()->user()->disabled == 0)
                    <li class="nav-small-cap">PANEL DE FAMILIA</li>
                    <li class="{{ request()->route()->named('families.edit', auth()->user()->family->id) ? 'active' : '' }}"> 
                        <a class="waves-effect waves-dark"
                        @if(auth()->user()->family)
                        @php($family = auth()->user()->family)
                        href="{{ route('families.edit',  auth()->user()->family->id) }}"
                        @endif
                        aria-expanded="false"><i class="mdi mdi-book-open-variant"></i><span class="hide-menu">Datos Básicos</span></a>
                    </li>
                    {{--<li class="{{ request()->route()->named('family-bank-accounts.index') ? 'active' : '' }}">
                        <a class="waves-effect waves-dark" href="{{ route('family-bank-accounts.index') }}" aria-expanded="false"><i class="mdi mdi-bank"></i><span class="hide-menu">Cuentas Bancarias</span></a>
                    </li>
                    <li class="{{ request()->route()->named('family-payments.index') ? 'active' : '' }}">
                        <a class="waves-effect waves-dark" href="{{ route('family-payments.index') }}" aria-expanded="false"><i class="mdi mdi-cash-multiple"></i><span class="hide-menu">Pagos de la Familia</span></a>
                    </li> --}}
                    <li class="{{ request()->route()->named('representatives.index') ? 'active' : '' }}">
                        <a class="waves-effect waves-dark" href="{{ route('representatives.index') }}" aria-expanded="false"><i class="mdi mdi-account"></i><span class="hide-menu">Representantes</span></a>
                    </li>
                    <li class="{{ request()->route()->named('students.index') ? 'active' : '' }}">
                        <a class="waves-effect waves-dark" href="{{ route('students.index') }}" aria-expanded="false"><i class="mdi mdi-face"></i><span class="hide-menu">Estudiantes</span></a>
                    </li>
                    @else
                    <li class="nav-small-cap">Usuario Bloqueado</li>
                    @endif
                @endrole
                @role('admin')
                    <li class="nav-small-cap">PANEL ADMINISTRATIVO</li>






                <li class="{{ 
                    request()->route()->named('admin.all-users')||
                    request()->route()->named('admin.operator-users')||
                    request()->route()->named('admin.school-users')||
                    request()->route()->named('admin.family-users')||
                    request()->route()->named('admin.student-users')||
                    request()->route()->named('admin.teacher-users') ? 'active' : '' }}">
                    <a class="has-arrow waves-effect waves-dark" href="#" aria-expanded="false"><i class="mdi mdi-account-multiple-plus"></i><span class="hide-menu">Gestión de usuarios</span></a>
                    <ul aria-expanded="false" class="collapse">
                        <li><a class="{{ request()->route()->named('admin.all-users') ? 'active' : '' }}" href="{{ route('admin.all-users') }}">Todos los usuarios</a>
                        </li>
                        <li><a class="{{ request()->route()->named('admin.operator-users') ? 'active' : '' }}" href="{{ route('admin.operator-users') }}">Operadores</a>
                        </li>
                        <li><a class="{{ request()->route()->named('admin.school-users') ? 'active' : '' }}" href="{{ route('admin.school-users') }}">Escolares</a>
                        </li>
                        <li><a class="{{ request()->route()->named('admin.family-users') ? 'active' : '' }}" href="{{ route('admin.family-users') }}">Familias</a>
                        </li>
                        <li><a class="{{ request()->route()->named('admin.student-users') ? 'active' : '' }}" href="{{ route('admin.student-users') }}">Estudiantes</a>
                        </li>
                        <li><a class="{{ request()->route()->named('admin.teacher-users') ? 'active' : '' }}" href="{{ route('admin.teacher-users') }}">Docentes</a>
                        </li>

                    </ul>
                </li>
                <li class="{{ request()->route()->named('schools.index')? 'active' : '' }}">
                    <a class="waves-effect waves-dark" href="{{ route('schools.index') }}" aria-expanded="false">
                        <i class="ti ti-home"></i>
                        <span class="hide-menu">
                            Colegios
                        </span>
                    </a>
                </li>















                    <li> <a class="waves-effect waves-dark" href="{{ route('settings.index') }}" aria-expanded="false"><i class="mdi mdi-settings"></i><span class="hide-menu">Configuración</span></a>
                    </li>
                @endrole
                @role('teacher')
                    <li class="{{ request()->route()->named('home') ? 'active' : '' }}"> <a class="waves-effect waves-dark" href="{{ route('home') }}" aria-expanded="false"><i class="mdi mdi-home"></i><span class="hide-menu">Inicio</span></a>
                    <li class="nav-small-cap">PANEL DOCENTE</li>
                    <li>
                        <a class="waves-effect waves-dark" href="{{ route('chat.index') }}" aria-expanded="false"><i class="mdi mdi-message-bulleted"></i><span class="hide-menu">Mensajería
                        <span class="label label-rouded label-danger pull-right count">0</span>
                        </span></a>
                    </li>
                    <li>
                        <a class="waves-effect waves-dark" href="{{ route('tasks.index') }}" aria-expanded="false"><i class="mdi mdi-grease-pencil"></i><span class="hide-menu">Gestión de Notas y Tareas</span></a>
                    </li>
                    <li class="{{ request()->route()->named('library.teacher') ? 'active' : '' }}">
                    <a class="waves-effect waves-dark" href="{{ route('library.teacher') }}" aria-expanded="false"><i class="mdi mdi-book"></i><span class="hide-menu">Biblioteca <br><small>El mejor negocio de tu vida</small></span></a>
                </li>
                    <li> 
                        <a class="waves-effect waves-dark" href="{{ route('materias.profile') }}" aria-expanded="false"><i class="mdi mdi-face"></i><span class="hide-menu">Mi Perfil</span></a>
                    </li>
                    <li hidden=""> 
                        <a class="has-arrow waves-effect waves-dark" href="#" aria-expanded="false"><i class="mdi mdi-book-open-variant"></i><span class="hide-menu">Descargas</span></a>
                        <ul aria-expanded="false" class="collapse">
                            <li><a href="#">Descargar carnet</a></li>
                        </ul>
                </li>
                @endrole
                @role('student')
                <li class="{{ request()->route()->named('home') ? 'active' : '' }}"> <a class="waves-effect waves-dark" href="{{ route('home') }}" aria-expanded="false"><i class="mdi mdi-home"></i><span class="hide-menu">Inicio</span></a>
                    <li class="nav-small-cap">PANEL ESTUDIANTE</li>

                    <li>
                        <a class="waves-effect waves-dark" href="{{ route('chat.index') }}" aria-expanded="false"><i class="mdi mdi-message-bulleted"></i><span class="hide-menu">Mensajería

                        <span class="label label-rouded label-danger pull-right count">0</span>
                        </span></a>
                    </li>
                    <li> 
                        <a class="has-arrow waves-effect waves-dark" href="javascript:void(0);" aria-expanded="false"><i class="mdi mdi-book-open-variant"></i><span class="hide-menu">Actividades</span></a>
                        <ul id="activities" aria-expanded="false" class="collapse">
                            
                        </ul>
                </li>
                <li class="{{ request()->route()->named('library.student') ? 'active' : '' }}">
                    <a class="waves-effect waves-dark" href="{{ route('library.student') }}" aria-expanded="false"><i class="mdi mdi-book"></i><span class="hide-menu">Biblioteca <br><small>El mejor negocio de tu vida</small></span></a>
                </li>
                @endrole
            </ul>
        </nav>
        <!-- End Sidebar navigation -->
    </div>
    <!-- End Sidebar scroll-->
</aside>
