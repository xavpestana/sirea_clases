@extends('layouts.app')

@section('title-view', 'Docentes - Evaluaciones')

@section('extra-css')

@endsection
@section('content')
<div class="row">
                    <div class="col-md-8">
                        <div class="card m-b-0">
                            <!-- .chat-row -->
                           <div class="card">
                            <div class="card-header">
                                <h4 class="card-title">Materia: {{ $course->name }}</h4>
                                <h4 class="card-title">
                                    Grado: {{ $course->grade->name }}<br>
                                    Seccion: {{ $course->courses_sections_user[0]->section->name }}<br>
                                    Periodo: {{ $season->season }}
                                </h4>
                            </div>
                            <div class="card-body">
                                <span id="student">Cargando estudiantes espere porfavor...</span>
                            </div>
                        </div>
                            <!-- /.chat-row -->
                        </div>

                    </div>
                    <div class="col-md-4">
                        <div class="card">
                            <div class="card-header">
                                <h4 class="card-title">Plan de Evaluación <span id="total"></span></h4>
                            </div>
                            <div class="card-body">
                                    <div class="row">
                                        <form id="form_plan" class="needs-validation">
                                    @csrf
                                    <div class="row">
                                        
                                        <div class="col-md-12 nopadding">
                                            <input type="hidden" name="section" value="{{ $course->courses_sections_user->last()->section->id }}">
                                            <input type="hidden" name="period" value="{{ $period->id }}">
                                            <label for="description">Descripcion</label>
                                                <input type="text" class="form-control" id="description" name="descriptions" value="" placeholder="Descripcion" required="">
                                            </div>
                                        
                                        <div class="col-md-8 nopadding">
                                            <div class="form-group">
                                                <label for="date">Fecha</label>
                                            
                                                <input type="date" class="form-control" id="date" name="dates" placeholder="Fecha" required="">
                                            </div>
                                        </div>
                                        <div class="col-sm-3 nopadding">
                                            <div class="form-group">
                                                <div class="input-group">
                                                    <div class="input-group-append">
                                                        <button class="btn btn-success mt-4 save" type="submit" title="Agregar plan"><i class="fa fa-plus"></i></button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </form>
                                    <div id="education_fields"></div>
                                </div>
                                
                            
                            </div>
                            <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel1">
                                <div class="modal-dialog" role="document">
                                        <div id="modal_nota" class="modal-content">
                                        </div>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
@endsection

@section('extra-js')
<script src="https://cdn.jsdelivr.net/npm/sweetalert2@9"></script>
<script type="text/javascript" charset="utf-8">

var section_id = {{ $course->courses_sections_user->last()->section->id }};

    (function() {
   'use strict';
        window.addEventListener('load', function() {
        // Fetch all the forms we want to apply custom Bootstrap validation styles to
        var forms = document.getElementsByClassName('needs-validation');
        // Loop over them and prevent submission
        var validation = Array.prototype.filter.call(forms, function(form) {
            form.addEventListener('submit', function(event) {
                if (form.checkValidity() === false) {
                    event.preventDefault();
                    event.stopPropagation();
                }
            form.classList.add('was-validated');
        }, false);
        });
        }, false);
})();

function remove_education_fields(id) {
    $.ajax({
                    type : "DELETE",
                    data : {"_token": "{{ csrf_token() }}"},
                    url : "{{ url('docente/evaluationPlan/') }}/" + id,
                    success : function(data){
                        $.ajax({
                    type : "GET",
                    url : "{{ url('docente/evaluacionPlan/') }}/" + section_id,
                    success : function(data){
                    $('#education_fields').html(data);
                    }
                });
                $.ajax({
                    type : "GET",
                    data : {"section_id" : {{ $course->courses_sections_user->last()->section->id }}, "_token": "{{ csrf_token() }}", "course_id": {{ $course->id }} },
                    url : "{{ route('evaluacion.create') }}",
                    success : function(data){
                        $('#student').html(data);
                    }
                });
                        Swal.fire({
                            position: 'center-center',
                            icon: 'success',
                            title: 'Eliminado exitosamente',
                            showConfirmButton: false,
                            timer: 1500
                        })
                    }
                });
    
}

/*******  Guardar plan  ********/
$( "#form_plan" ).submit(function(e) {
e.preventDefault();

  $(".save").prop('disabled', true);
  datos = $("#form_plan").serialize();
          
$.ajax({
        type:'POST',
        data:datos,
        url:"{{ route('evaluacionPlan.store') }}",
        })
        .done(function(response) {
                
                $(".save").prop('disabled', false);
                $('#form_plan')[0].reset();
                $.ajax({
                    type : "GET",
                    data : {"_token": "{{ csrf_token() }}"},
                    url : "{{ url('docente/evaluacionPlan/') }}/" + section_id,
                    success : function(data){
                    $('#education_fields').html(data);
                    }
                });
                $.ajax({
                    type : "GET",
                    data : {"section_id" : {{ $course->courses_sections_user->last()->section->id }}, "_token": "{{ csrf_token() }}", "course_id": {{ $course->id }} },
                    url : "{{ route('evaluacion.create') }}",
                    success : function(data){
                        $('#student').html(data);
                    }
                });
            Swal.fire({
                position: 'center-center',
                icon: 'success',
                title: 'Guardado exitosamente',
                showConfirmButton: false,
                timer: 1500
            })
        })
        .fail(function(response) {
            $(".save").prop('disabled', false);
            Swal.fire({
                icon: 'error',
                title: 'Oops...',
                text: 'Error, intente denuevo.',
                })
        }) 
});

$.ajax({
         type : "GET",
         data : {"_token": "{{ csrf_token() }}"},
         url : "{{ url('docente/evaluacionPlan/') }}/" + section_id,
         success : function(data){
            $('#education_fields').html(data);
         }
      });


  $.ajax({
         type : "GET",
         data : {"section_id" : {{ $course->courses_sections_user->last()->section->id }}, "_token": "{{ csrf_token() }}", "course_id": {{ $course->id }} },
         url : "{{ route('evaluacion.create') }}",
         success : function(data){
            $('#student').html(data);
         }
      });

  function notas(user_id, course_id){
    $.ajax({
         type : "POST",
         data : {"user_id" : user_id, "_token": "{{ csrf_token() }}", "course_id": course_id },
         url : "{{ route('evaluacion.store') }}",
         success : function(data){
            $('#modal_nota').html(data);
         }
      });
  }

</script>
@endsection

