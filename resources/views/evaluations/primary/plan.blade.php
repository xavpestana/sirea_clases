                                @if(!$plan->isEmpty())
                                @foreach($plan as $activity)
                                    <div class="row">
                                    
                                    <div class="col-md-12 nopadding">
                                        <div class="form-group">
                                        <label for="description">Descripcion</label>
                                            <input type="text" class="form-control" id="description" value="{{ $activity->description }}" name="descriptions[]" value="" placeholder="Descripcion" required="" disabled="">
                                        
                                        </div>
                                    </div>
                                    <div class="col-md-8 nopadding">
                                        <div class="form-group">
                                            <label for="date">Fecha</label>
                                            <input type="date" class="form-control" id="date" name="dates[]" placeholder="Fecha" value="{{ $activity->fecha }}" required="" disabled="">
                                        </div>
                                    </div>
                                    
                                    <div class="col-sm-3 nopadding"> 
                                        <div class="form-group"> 
                                            <div class="input-group"> 
                                                <div class="input-group-append"> 
                                                    <button class="btn btn-danger mt-4" type="button" onclick="remove_education_fields('{{ $activity->id }}')"><i class="fa fa-minus"></i></button> 
                                                </div> 
                                            </div> 
                                        </div> 
                                    </div>
                                    
                                    </div>
                                    @endforeach
                                    @endif