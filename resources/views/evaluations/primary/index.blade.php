<style>
    .table td, .table th {
    padding: 0.25rem;
}
</style>

<div style="overflow-x: scroll" class="table-responsive">
                                    <table class="table full-color-table full-muted-table hover-table">
                                        <tbody>
                                            @foreach($students as $student)
                                            <tr>
                                                <td style="font-size: 13px">{{ $student->last_name_1 }} {{ $student->last_name_2 }} {{ $student->first_name_1 }} {{ $student->first_name_2 }}</td> 
                                                <td><button type="button" class="btn btn-success" title="Agregar Nota" data-toggle="modal" data-target="#exampleModal" data-whatever="@getbootstrap" onclick="notas('{{ $student->user_id }}','{{ $course_id }}')"><i class="fa fa-plus"></i> Agregar Evaluación</button></td>
                                            </tr>
                                            @endforeach
                                        </tbody>
                                    </table>
                                </div>
