           @if(!$plan->isEmpty())
           @php
           $i=0;
           @endphp
            <form id="form_ev" accept-charset="utf-8">
                @csrf
            <div class="modal-header">
                <h4 class="modal-title" id="exampleModalLabel1">Alumno: {{ $student->profile->last_name_2 }} {{ $student->profile->last_name_1 }} {{ $student->profile->first_name_2 }} {{ $student->profile->first_name_1 }}</h4>

                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            </div>
            <div class="modal-body">
                <div class="row">
                @foreach($plan as $activity)
                @php
                    $ev = App\evaluation::where('user_id',$student->profile->user_id)->where('evaluation_plan_id',$activity->id)->first();
                    
                    if(!is_null($ev)){
                        $eval = $ev->description;
                    }else{
                        $eval = null;
                    }
                    @endphp
                    <div class="col-md-12">
                        <div id="accordian-3">
                            <div class="card m-b-0">
                                <a class="card-header text-decoration-none" id="heading11">
                                    <button type="button" class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapse{{ $i }}" aria-expanded="false" aria-controls="collapse{{ $i }}">
                                    <h5 class="m-b-0">{{ $activity->description }}</h5>
                                    </button>
                                </a>
                                <div id="collapse{{ $i }}" class="collapse" aria-labelledby="heading11" data-parent="#accordian-3" style="">
                                    <div class="card-body">
                                        <textarea name="nota[]" class="form-control" rows="5" placeholder="Evaluacion del Alumno (1000 palabras)" maxlength="1000">{{ $eval }}</textarea>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    @php
                    $i++;
                    @endphp
                    <input type="hidden" id="plan_id" name="plan_id[]" value="{{ $activity->id }}" >
                @endforeach
                
                <input type="hidden" id="user_id" name="user_id" value="{{ $student->profile->user_id }}" >
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                <button id="save" type="submit" class="btn btn-primary">Guardar</button>
            </div>
            </form>

            <script>
                /*******  Guardar nota  ********/
$( "#form_ev" ).submit(function(e) {
e.preventDefault();
var plan = $("#plan_id").val();
  $(".save").prop('disabled', true);
  datos = $("#form_ev").serialize();
          
$.ajax({
        type:'PUT',
        data:datos,
        url:"{{ url('docente/evaluacion/') }}/" + plan,
        })
        .done(function(response) {
            $(".save").prop('disabled', false);
            Swal.fire({
                position: 'center-center',
                icon: 'success',
                title: 'Guardado exitosamente',
                showConfirmButton: false,
                timer: 1500
            }) 
            $.ajax({
         type : "GET",
         data : {"section_id" : {{ $course->courses_sections_user->last()->section->id }}, "_token": "{{ csrf_token() }}", "course_id": {{ $course->id }} },
         url : "{{ route('evaluacion.create') }}",
         success : function(data){
            $('#student').html(data);
         }
            });
            $('#exampleModal').modal('hide');
        })
        .fail(function(response) {
            $(".save").prop('disabled', false);
            Swal.fire({
                icon: 'error',
                title: 'Oops...',
                text: 'Error, intente denuevo.',
                })
        }) 
});
            </script>
            @else
            <div class="modal-header">
                <h4 class="modal-title" id="exampleModalLabel1">Debe Agregar plan de Evaluación</h4>

                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            </div>
            <div class="modal-body">
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
            </div>
            @endif


