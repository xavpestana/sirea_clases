@if(!$plan->isEmpty())
<style>
    .table td, .table th {
    padding: 0.25rem;
}
</style>
@php
                    $total = 0;
                    @endphp
<div style="overflow-x: scroll" class="table-responsive">
                                    <table class="table full-color-table full-muted-table hover-table">
                                        <thead>
                                            <tr>
                                                <th>Nombre Completo</th>
                                                @foreach($plan as $evaluation)
                                                <th>{{ $evaluation->description }}
                                                    <br>
                                                    
                                                    {{ $evaluation->percents }} %
                                                    
                                                </th>
                                                @endforeach
                                                <th>Prom. Final</th>
                                                <th>Acciones</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @foreach($students as $student)
                                            <tr>
                                                <td style="font-size: 13px">{{ $student->last_name_1 }} {{ $student->last_name_2 }} {{ $student->first_name_1 }} {{ $student->first_name_2 }}</td> 
                                                
                                                @foreach($plan as $evaluation)
                                                @php
                                            $ev = App\evaluation::where('user_id',$student->user_id)->where('evaluation_plan_id',$evaluation->id)->first();

                                            if(!is_null($ev)){
                                               $nota = $ev->description;
                                                $percent = number_format($nota*$evaluation->percents/100 ,2);
                                                $total = $total + $percent;
                        
                                            }else{
                                                $nota = 0;
                                                $total = 0;
                                            }
                                                @endphp
                                                @if(is_null($ev))
                                                <td>0</td>
                                                @else
                                                <td>{{ $nota }}</td>
                                                @endif
                                                @endforeach
                                                <td>{{ $total }}</td>
                                                <td><button type="button" class="btn btn-success" title="Agregar Nota" data-toggle="modal" data-target="#exampleModal" data-whatever="@getbootstrap" onclick="notas('{{ $student->user_id }}','{{ $course_id }}')"><i class="fa fa-plus"></i></button></td>
                                            </tr>
                                            @endforeach
                                        </tbody>
                                    </table>
                                </div>
                                @else
                                   <h4 class="card-title">Debe agregar un plan de evaluación</h4>
                                @endif