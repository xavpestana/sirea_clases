            <form id="form_ev" accept-charset="utf-8">
                @csrf
            <div class="modal-header">
                <h4 class="modal-title" id="exampleModalLabel1">Alumno: {{ $student->profile->last_name_2 }} {{ $student->profile->last_name_1 }} {{ $student->profile->first_name_2 }} {{ $student->profile->first_name_1 }}</h4>

                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            </div>
            <div class="modal-body">
                <div class="row">
                    @php
                    $total = 0;
                    @endphp
                    @foreach($plan as $evaluation)
                    @php
                    $ev = App\evaluation::where('user_id',$student->profile->user_id)->where('evaluation_plan_id',$evaluation->id)->first();
                    
                    if(!is_null($ev)){
                        $nota = $ev->description;
                        $percent = number_format($nota*$evaluation->percents/100 ,2);
                        $total = $total + $percent;
                        
                    }else{
                        $nota = 0;
                        $total = 0;
                    }
                    @endphp
                    <div class="col-md-3 mb-2">
                        
                        <label> {{ $evaluation->description }}</label>
                        
                        <input type="number" class="form-control eval" id="nota" name="nota[]" placeholder="Nota" value="{{ $nota }}" required="" min="0" max="20">
                        
                        <input type="hidden" id="plan_id" name="plan_id[]" value="{{ $evaluation->id }}" >
                    </div>
                    <div class="col-md-3">
                        <input type="hidden" name="percents[]" value="{{ $evaluation->percents }}">
                        <h4>{{ $evaluation->percents }} %</h4>
                    </div>
                    
                    @endforeach
                    <div class="col-md-3 mb-2">
                        
                        <label> Promedio Final</label>
                        
                        <input type="text" class="form-control eval" id="nota" placeholder="Promedio final" value="{{ $total }}" required="" min="1" max="20">
                        
                        <input type="hidden" id="plan_id" name="plan_id[]" value="{{ $evaluation->id }}" >
                    </div>
                    <input type="hidden" id="user_id" name="user_id" value="{{ $student->profile->user_id }}" >
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                <button id="save" type="submit" class="btn btn-primary">Guardar</button>
            </div>
            </form>

            <script>
                /*******  Guardar nota  ********/
$( "#form_ev" ).submit(function(e) {
e.preventDefault();
var plan = $("#plan_id").val();
  $(".save").prop('disabled', true);
  datos = $("#form_ev").serialize();
          
$.ajax({
        type:'PUT',
        data:datos,
        url:"{{ url('docente/evaluations/') }}/" + plan,
        })
        .done(function(response) {
            $(".save").prop('disabled', false);
            Swal.fire({
                position: 'center-center',
                icon: 'success',
                title: 'Guardado exitosamente',
                showConfirmButton: false,
                timer: 1500
            }) 
            //location.reload();
            $.ajax({
         type : "GET",
         data : {"section_id" : {{ $course->courses_sections_user->last()->section->id }}, "_token": "{{ csrf_token() }}", "course_id": {{ $course->id }} },
         url : "{{ route('evaluations.create') }}",
         success : function(data){
            $('#student').html(data);
         }
            });
            $('#exampleModal').modal('hide');
        })
        .fail(function(response) {
            $(".save").prop('disabled', false);
            Swal.fire({
                icon: 'error',
                title: 'Oops...',
                text: 'Error, intente denuevo.',
                })
        }) 
});
            </script>