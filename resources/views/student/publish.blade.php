@foreach($courses as $course)
<li class="mb-3"><a href="{{ route('studentActivity.show', ['student' => $course->Course->id ]) }}">{{ $course->Course->name }}</a></li>
@php
$notifies = App\teacherNotify::where('course_id',$course->Course->id)
                        ->where('user_id','!=', auth()->user()->id)
                        ->where('method', 'activity')
                        ->where(function ($query) {
                			$query->where('user_id_r', 0)
                      			  ->orWhere('user_id_r', auth()->user()->id);
            			})
                        ->orderBy('created_at', 'desc')
                        ->limit(5)
                        ->get();
                        //dd($notifies);
@endphp
@foreach($notifies as $notify)
<li>
    <div class="bg-light-info"><i class="fa fa-bell-o"></i></div> <strong>{{ $notify->user->profile->first_name_1 }}</strong> <a href="{{ route('studentActivity.show', ['student' => $course->Course->id ]) }}" title=""> {{ $notify->message }}</a> <span class="text-muted">{{ $notify->created_at->format('d/m/Y') }}</span></li>
@endforeach

@endforeach

