@extends('layouts.app')

@section('title-view', 'Actividades')

@section('extra-css')

@endsection

@section('content')
<div class="row justify-content-center">
                    <div class="col-md-8">
                        <div class="card m-b-0">
                            <!-- .chat-row -->
                            <div class="chat-main-box">
                                <!-- .chat-right-panel -->
                                <div class="chat-right-asid">
                                    <div class="chat-main-header">
                                        <div class="p-20 b-b">

                                            <h2 class="box-title">Materia: {{ $course->name }}</h2>
                                            <h3 class="box-title">Grado: {{ $course->grade->name }}<br>
                                                Seccion: {{ $course->courses_sections_user[0]->section->name }}<br>
                                                Periodo: {{ $season->season }}
                                            </h3>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            
                        </div>
                    </div>
                    <div style=" height: 200px; overflow-y: scroll;" class="col-md-4">
                        <div class="card">
                            <div class="card-body">
                                <h4 class="card-title">Ultimas Noticias</h4>
                                <ul class="feeds">
                                    @foreach($notifies as $notify)
                                    <li>
                                        <div class="dw-user-box">
                                        <div class="u-img"><img style="border-radius: 50%" src="
                            @if(!$notify->user->profile)
                            {{ asset('storage/pictures/avatars/default.jpg') }}
                            @else
                            {{ asset('storage/pictures/avatars/'.$notify->user->profile->avatar )}}
                            @endif" alt="user" data-toggle="tooltip" data-placement="top" title="" data-original-title="{{ $notify->user->profile->first_name_1 }}" width="30"></div></div> <strong>{{ $notify->user->profile->first_name_1 }} <a href="#task-{{ $notify->task_id }}" title="">{{ $notify->message }}</a></strong></li>
                                    @endforeach
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-10">
                        <span id="message"></span>
                    </div>
                </div>

                <div id="responsive-modal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
                                    <div class="modal-dialog">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                                <h4 class="modal-title">Materia: {{ $course->name }}</h4>
                                            </div>
                                            <div class="modal-body">
                                                
                    <form style="cursor: pointer !important" method="post" action="{{route('tasks.store')}}" enctype="multipart/form-data" class="dropzone" id="dropzone">
                                                        <div style="padding: 0px 15px" class="dz-message">
                                                        <h3>Cargar Archivo</h3>
                            <em>Arrastra aquí el archivo o haz clic para cargarla</em>
                            <p style="font-size: 20px"><span class="feather-video"></span> 
                            Tamaño maximo de archivo 40mb.</p>
                                                            <div class="drag-icon-cph">
                                                            </div>
                                                        </div>
                                                        <div class="fallback">
                                                            <input id="fileAttachmentBtn" id="image" name="image" type="file" class="d-none">
                                                            
                                                        </div>
                                                        <input type="hidden" id="task_id" name="task_id">
                                                        <input type="hidden" id="curso_id" name="course_id">
                                                        @csrf
                                                    </form>
                                            </div>
                                            <div class="modal-footer">
                                                <button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Cerrar</button>
                                                <button id="up_activity" type="button" class="btn btn-danger waves-effect waves-light"><i class="fa fa-file-picture-o"></i> Enviar</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>

@endsection

@section('extra-js')
<script src="{{ asset('themeforest/Dropzonejs/js/dropzone.js', Request::secure()) }}"></script>
<script src="https://cdn.jsdelivr.net/npm/sweetalert2@9"></script>
<script>
    $('#message').load('{{ route('tasks.messages', ['id' => $id]) }}');
$.ajax({
         type : "GET",
         data : {"id" : {{ auth()->user()->profile->student->id }} },
         url : "{{ route('studentActivity.activities') }}",
         success : function(data){
            $('#activities').html(data);
         }
      });
    
    $.ajax({
         type : "GET",
         data : {"id" : {{ auth()->user()->id }} },
         url : "{{ route('chat.count') }}",
         success : function(data){
            $('.count').html(data);
         }
      });
    
    function see_more(id) {
      $('#shorttext-'+id).toggle();
      $('#longtext-'+id).removeAttr("hidden");
   }

   function comment(id) {
    $('.comment').html(' ');
    $('#comment'+id).load("{{route('showcomment')}}?id="+id);
    }
function save_comment(id_task, id_course) {

      var comment = $('#comment_new'+id_task).val();
      
    $.ajax({
            type : "POST",
            data :  {"comment" : comment,"task_id" : id_task,"course_id" : id_course, "_token": "{{ csrf_token() }}"},
            url : "{{ route('comment') }}",
        })
        .done(function(response) {
            $('#comment_new'+id_task).val('');
            $('#comment'+id_task).load("{{route('showcomment')}}?id="+id_task);
        })
        .fail(function(response) {
            Swal.fire({
                icon: 'error',
                title: 'Oops...',
                text: 'Intente nuevamente.',
                })
        }) 
    }
    function delete_comment(id,id_task){
      $.ajax({
            type : "DELETE",
            data : {"id" : id, "_token": "{{ csrf_token() }}"},
            url : "{{ route('delete.comment') }}",
            success : function(r){
    if (r==0){
        $('#comment'+id_task).load("{{route('showcomment')}}?id="+id_task);
    }else{
        Swal.fire({
            icon: 'error',
            title: 'Oops...',
            text: 'Error, intente denuevo!',
        })
    }
           }
        });
            
        }
        function response_task(task_id, course_id){
        $('#task_id').val(task_id);
        $('#curso_id').val(course_id) ;
        }

        function delete_response(response_id){
        $.ajax({
            type : "GET",
            data : {"id" : response_id, "_token": "{{ csrf_token() }}"},
            url : "{{ route('studentActivity.delete') }}",
            success : function(r){
    if (r==0){
        Swal.fire({
                    icon: 'success',
                    title: 'Exito!!',
                    text: 'Actividad eliminada con exito',
                  })
        location.reload();
    }else{
        Swal.fire({
            icon: 'error',
            title: 'Oops...',
            text: 'Error, intente denuevo!',
        })
    }
           }
        });
        }

Dropzone.options.dropzone = {
            autoProcessQueue: false,
            paramName: "file",
            addRemoveLinks: true,
            dictRemoveFile : "×",
            uploadMultiple: false,
            maxFiles: 1,
            createImageThumbnails: false,
            maxFilesize:20,
            timeout: 1200000,
            dictCancelUpload: "Cancelar subida",
            dictCancelUploadConfirmation: "¿Seguro que desea cancelar esta subida?",
            dictMaxFilesExceeded: "Se ha excedido el numero de archivos permitidos.",

           init: function() {
                var submitBtn = document.querySelector("#up_activity");
                myDropzone = this;
                submitBtn.addEventListener("click", function(e){
                    e.preventDefault();
                    e.stopPropagation();
                    myDropzone.processQueue();
                });
                this.on("addedfile", function(file) {
                    $(".dz-success-mark").hide();
                    $(".dz-error-mark").hide();
                    $(".dz-details").css('background','#3a3535');
                });
                this.on("canceled", function(file) {
                  Swal.fire({
                    icon: 'error',
                    text: 'Cancelado por el usuario',
                  })
                });
                this.on("removedfile", function(file) {
                myDropzone.removeFile(file);
                });

                this.on("error", function(file, response) {
                  Swal.fire({
                    icon: 'error',
                    title: 'Oops...',
                    html: response + ' Intente denuevo!',
                  })
                });
                this.on("success", function(success) {
                Swal.fire({
                    icon: 'success',
                    title: 'Exito!!',
                    text: 'Actividad enviada con exito',
                  })
                location.reload();
                });
                this.on("complete", function(file) {
                  myDropzone.removeFile(file);
                });
            }
        };     

</script>
@endsection
