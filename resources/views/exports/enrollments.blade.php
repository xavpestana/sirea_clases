<table>
    <thead>
    <tr>
        <th colspan="3"><strong>Apellidos Completos</strong></th>
        <th colspan="3"><strong>Nombres Completos</strong></th>
        <th colspan="3"><strong>Documento</strong></th>
        <th colspan="3"><strong>Correo del Estudiante</strong></th>
        <th colspan="3"><strong>Fecha de Nacimiento</strong></th>
        <th colspan="3"><strong>Telefono</strong></th>
        <th colspan="3"><strong>Pais de Nacimiento</strong></th>
        <th colspan="3"><strong>Genero</strong></th>
        <th colspan="3"><strong>Apellidos de la familia</strong></th>
        <th colspan="3"><strong>Telefono</strong></th>
        <th colspan="3"><strong>Ingreso mensual</strong></th>
        <th colspan="3"><strong>Direccion</strong></th>
        <th colspan="3"><strong>Parroquia</strong></th>
        <th colspan="3"><strong>Ciudad</strong></th>
        <th colspan="3"><strong>Municipio</strong></th>
        <th colspan="3"><strong>Estado</strong></th>
        <th colspan="3"><strong>Nombres de Representantes</strong></th>
        <th colspan="3"><strong>Telefonos</strong></th>
        <th colspan="3"><strong>Parentescos</strong></th>
        <th colspan="3"><strong>Correos</strong></th>
    </tr>
    </thead>
    <tbody>
        <!--manejo de grados-->
    @foreach($grades as $grade)
        @php
            $sections = $grade->sections->where('season_id', $actual_season->id);
        @endphp
        
        @if(!$sections->isEmpty())
        <tr><td colspan="60"> </td></tr>
        <tr>
            <td colspan="60"><strong>{{ $grade->name }}</strong></td>
        </tr>
        <tr><td colspan="60"> </td></tr>
            <!--manejo de secciones de cada grado-->
            @foreach($sections as $section)
                @php
                    $enrollments = $section->enrollments;
                @endphp
                <!--estudiantes decada seccion-->
                @foreach($enrollments as $enrollment)
                    @php
                        $student = $enrollment->student;
                        $profile = $student->profile;
                        $user_student = $profile->user;
                        $representatives = $student->family->representatives;
                        $representatives_name ="";
                        $representatives_phone ="";
                        $representatives_parents = "";
                        $representatives_emails = "";
                        $family = $student->family;
                        if(!is_null($representatives)){
                            foreach ($representatives as $representative) {
                                $representatives_name .= $representative->first_name_1." ".$representative->last_name_1." / ";
                                $representatives_phone .= $representative->phone." / ";
                                $representatives_parents .= $representative->relationship->name." / ";
                                $representatives_emails .= $representative->correo." / ";
                            }  
                        }
                    @endphp
                    <tr>
                        <td colspan="3">{{ $profile->last_name_1 }} {{ $profile->last_name_2 }}</td>
                        <td colspan="3">{{ $profile->first_name_1 }} {{ $profile->first_name_2 }}</td>
                       <td colspan="3">{{ $profile->document_type }} - {{ $profile->document_number }}</td>
                       <td colspan="3">{{ $user_student->email }}</td>
                       <td colspan="3">{{ $profile->birthdate }}</td>
                       <td colspan="3">{{ $profile->phone }}</td>
                       <td colspan="3">{{ $profile->country->name }}</td>
                       <td colspan="3">{{ $profile->gender->name }}</td>
                       <td colspan="3">{{ $family->last_name_1 }} {{ $family->last_name_2 }}</td>
                       <td colspan="3">{{ $family->phone }}</td>
                       <td colspan="3">{{ $family->monthly_income }}</td>
                       <td colspan="3">{{ $family->address }}</td>
                       <td colspan="3">{{ $family->parish->name }}</td>
                       <td colspan="3">{{ $family->city->name }}</td>
                       <td colspan="3">{{ $family->parish->municipality->name }}</td>
                       <td colspan="3">{{ $family->parish->municipality->state->name }}</td>
                       <td colspan="3">{{ $representatives_name }}</td>
                       <td colspan="3">{{ $representatives_phone }}</td>
                       <td colspan="3">{{ $representatives_parents }}</td>
                       <td colspan="3">{{ $representatives_emails }}</td>
                    </tr>
                @endforeach
            @endforeach
        @endif
    @endforeach
    </tbody>
</table>