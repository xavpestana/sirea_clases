@extends('layouts.app')

@section('content')
	<!-- ============================================================== -->
	<!-- Main wrapper - style you can find in pages.scss -->
	<!-- ============================================================== -->
	<section id="wrapper" class="login-register login-sidebar" style="background-image:url({{ asset('themeforest/assets/images/background/login-register.jpg', Request::secure()) }} );">
		<div class="login-box card">
			<div class="card-body">
				<form class="form-horizontal form-material" id="loginform" method="POST" action="{{ route('login') }}" autocomplete="off">
					@csrf
					<a href="{{ route('login') }}" class="text-center db"><h4>Plataforma Educativa<br>MLK</h4></a>
					<div class="form-group m-t-40">
						<div class="col-xs-12">
							<input class="form-control @error('email') is-invalid @enderror" id="email" type="email" name="email" value="{{ old('email') }}" required="" placeholder="Correo Electrónico" autofocus>
							@error('email')
								<span class="invalid-feedback" role="alert">
									<strong>{{ $message }}</strong>
								</span>
							@enderror
						</div>
					</div>
					<div class="form-group">
						<div class="col-xs-12">
							<input class="form-control @error('password') is-invalid @enderror" id="password" type="password" name="password" required="" placeholder="Contraseña">
							@error('password')
								<span class="invalid-feedback" role="alert">
									<strong>{{ $message }}</strong>
								</span>
							@enderror
						</div>
					</div>
					<div class="form-group">
						<div class="col-md-12">
							<div class="checkbox checkbox-primary pull-left p-t-0">
								<input id="remember" type="checkbox" name="remember" {{ old('remember') ? 'checked' : '' }}>
								<label for="remember"> Recuérdame </label>
							</div>
							@if (Route::has('password.request'))
								<a class="text-dark pull-right" href="{{ route('password.request') }}">
									<i class="fa fa-lock m-r-5"></i> Olvidé mi contraseña
								</a>
							@endif
						</div>
					</div>
					<div class="form-group text-center m-t-20">
						<div class="col-xs-12">
							<button class="btn btn-info btn-lg btn-block text-uppercase waves-effect waves-light" type="submit">Iniciar sesión</button>
						</div>
					</div>
{{--					<div class="row">
						<div class="col-xs-12 col-sm-12 col-md-12 m-t-10 text-center">
							<div class="social"><a href="javascript:void(0)" class="btn  btn-facebook" data-toggle="tooltip" title="Login with Facebook">
								<i aria-hidden="true" class="fa fa-facebook"></i> </a> <a href="javascript:void(0)" class="btn btn-googleplus" data-toggle="tooltip" title="Login with Google"> <i aria-hidden="true" class="fa fa-google-plus"></i> </a> </div>
						</div>
					</div>
					<div class="form-group m-b-0">
						<div class="col-sm-12 text-center">
							<p>¿No tienes una cuenta? <a href="#" class="text-primary m-l-5"><b>Regístrate</b></a></p>
						</div>
					</div>--}}
				</form>
			</div>
		</div>
	</section>
@endsection
