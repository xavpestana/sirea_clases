@extends('layouts.app')

@section('content')
    <div class="row">
        <div class="col-12">
            <user :user_id="{!! (isset($user)) ? $user->id : "null" !!}"
                route_list='{{ url('usuarios') }}'>
            </user>
        </div>
    </div>
@endsection
