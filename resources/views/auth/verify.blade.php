@extends('layouts.login')

@section('title', 'Correo de verificación enviado')

@section('content')
	<section id="wrapper" class="login-register">
		@if (session('resent'))
		<div class="alert alert-success" role="alert">
			{{ __('A fresh verification link has been sent to your email address.') }}
		</div>
		@endif


		<div class="login-box">
			<div class="white-box">
				<form class="form-horizontal d-inline"  action="{{ route('verification.resend') }}">
					@csrf
					<div class="form-group ">
						<div class="col-xs-12">
							<h3>Recover Password</h3>
							<p class="text-muted">{{ __('Before proceeding, please check your email for a verification link.') }}</p>
							<p class="text-muted">{{ __('If you did not receive the email') }}</p>
						</div>
					</div>
					<div class="form-group text-center m-t-20">
						<div class="col-xs-12">
							<button class="btn btn-primary btn-lg btn-block text-uppercase waves-effect waves-light" type="submit">{{ __('click here to request another') }}</button>
						</div>
					</div>
				</form>
			</div>
		</div>
	</section>
@endsection