@extends('layouts.app')

@section('content')
    <section id="wrapper">
        <div class="login-register" style="background-image:url({{ asset('themeforest/assets/images/background/login-register.jpg', Request::secure()) }} );">
            <div class="login-box card">
                <div class="card-body">
                    <form class="form-horizontal form-material" id="loginform" method="POST" action="{{ route('password.email') }}" autocomplete="off">
                        @csrf
                        @if (session('status'))
                            <div class="alert alert-success" role="alert">
                                {{ session('status') }}
                            </div>
                        @endif

                        <h3 class="box-title m-b-20">Restablecer Contraseña</h3>
                        <p class="m-b-20">Ingrese correo electrónico del usuario para enviarle un correo con el enlace a la pantalla de cambio de contraseña</p>
                        <div class="form-group ">
                          <div class="col-xs-12">
                            <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autofocus>
                            @error('email')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                          </div>
                        </div>
                        <div class="form-group text-center m-t-20">
                          <div class="col-xs-12">
                            <button class="btn btn-info btn-lg btn-block text-uppercase waves-effect waves-light" type="submit">Enviar</button>
                          </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </section>
@endsection
