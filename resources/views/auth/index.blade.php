@extends('layouts.app')

@section('content')
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-header border-0">
                    Lista de Usuarios<a class="btn btn-primary btn-xs float-right" href="{{ route('users.create') }}"><i class="fa fa-plus-circle"></i></a>
                </div>
                <div class="card-body">
                    <user-list route_list="{{ url('usuarios/show/vue-list') }}"
						route_delete="{{ url('usuarios') }}"
						route_edit="{{ url('usuarios/{id}/editar') }}"
						route_show="{{ url('usuarios/{id}') }}">
					</user-list>
                </div>
            </div>
        </div>
    </div>
@endsection
