@extends('layouts.app')

@section('title-view', 'Biblioteca - Leer')

@section('extra-css')
<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-select@1.13.14/dist/css/bootstrap-select.min.css">
<link href="{{ asset('themeforest/multiselect/css/multi-select.css', Request::secure()) }}" rel="stylesheet" type="text/css" />
<link href="https://cdn.datatables.net/1.10.21/css/jquery.dataTables.min.css" rel="stylesheet">
@endsection

@section('content')
    <div class="row">
        <div class="col-12">
            <div class="card">
            	<div class="card-header">
                    <div class="row">
                    <div class="col-lg-6 col-sm-4 col-md-4 col-xs-12">
                        <h4>LEER: <small>El mejor negocio de tu vida</small></h4>
                    </div>
                    <div class="col-lg-3 col-sm-4 col-md-4 col-xs-12 ml-auto">
                        <button class="btn btn-block btn-success" data-toggle="modal" data-target="#exampleModal" data-whatever="@getbootstrap">Agregar Libro</button>
                    </div>
                </div>
            	</div>
                <div class="card-body">
                	<div id="students" class="table-responsive">
                	</div>
                </div>
            </div>
            <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel1">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="exampleModalLabel1">Nuevo libro</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            </div>
            <div class="col-12 row text-white mt-4">
                                                <div class="col-md-6">
                                                    <form style="border: 0 !important" method="post" action="{{route('library.store')}}" enctype="multipart/form-data" class="dropzone" id="dropzone">
                                                        <div style="padding: 0px 15px" class="dz-message">
                                                            <label type="text" id="file_up" style="color:#fff"  class="btn btn-info btn-circle btn-lg mb-2">
                                                        <i class="fa fa-file-picture-o"></i>
                                                        </label>
                                                            <div class="drag-icon-cph">
                                                            </div>
                                                        </div>
                                                        <div class="fallback">
                                                            <input id="fileAttachmentBtn" id="image" name="image" type="file" class="d-none">
                                                        </div>
                                                        @csrf
                                                    </form>
                                                </div>
                                                
                                                <div class="col-md-6 text-center">
                                                    
                                                        <form id="form_activity" class="row">
                                                    
                                                    <div class="col-md-6">
                                                        <label for="levels" class="text-dark">Seleccione Nivel</label>
                                                        <select id="levels" name="levels[]" class="form-control" multiple  required="">
                                                            <option value="kids">Kids</option>
                                                            <option value="reformers">Reformers</option>
                                                            <option value="millenium">Millenium</option>
                                                            <option value="influencers">Influencers</option>
                                                        </select>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <label for="levels" class="text-dark">Ingrese monto en SLB</label>
                                                        <input type="number" step="any" name="amount" class="form-control" required="">
                                                    </div>
                                                    
                                                    </form>
                                                        
                                                        
                                                    
                                                    <button id="up_activity" style="color:#fff" type="button" class="btn btn-info btn-rounded btn-lg mb-2 mt-4" title="Enviar Actividad"><i class="fa fa-paper-plane-o"></i> Guardar</button>    
                                                </div>
                                                
                                <div id="responsive-modal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
                                    <div id="dialog" class="modal-dialog">
                                        
                                    </div>
                                </div>
                                            </div>
        </div>
    </div>
</div>
        </div>
    </div>
@endsection
@section('extra-js')
<script src="{{ asset('themeforest/Dropzonejs/js/dropzone.js', Request::secure()) }}"></script>
<script src="https://cdn.jsdelivr.net/npm/sweetalert2@9"></script>
<script src="https://cdn.jsdelivr.net/jquery.validation/1.16.0/jquery.validate.min.js"></script>
<script src="https://cdn.datatables.net/1.10.21/js/jquery.dataTables.min.js"></script>

<script>
    function salvar(){
        
            datos = $("#form_activity").serialize();
          
                $.ajax({
                    type:'GET',
                    data:datos,
                    url:"{{ route('library.create') }}",
                    success:function(r)
                    {
                        if (r==0){                        
                            
                            $('#students').html(' ');
                            Swal.fire({
                              position: 'center-center',
                              icon: 'success',
                              title: 'Guardado con exito',
                              showConfirmButton: false,
                              timer: 1000,
                           })
                           flag = 0;
                        }else{
                           Swal.fire({
                                 icon: 'error',
                                 title: 'Oops...',
                                 text: 'Algo ha ocurrido, Intente denuevo!',
                           })
                        }
                    }    
    })
    };

    Dropzone.options.dropzone = {
            autoProcessQueue: false,
            paramName: "file",
            addRemoveLinks: true,
            dictRemoveFile : "×",
            uploadMultiple: false,
            maxFiles: 1,
            createImageThumbnails: false,
            timeout: 1200000,
            dictCancelUpload: "Cancelar subida",
            dictCancelUploadConfirmation: "¿Seguro que desea cancelar esta subida?",
            dictMaxFilesExceeded: "Se ha excedido el numero de archivos permitidos.",

           init: function() {
                var submitBtn = document.querySelector("#up_activity");
                myDropzone = this;
                submitBtn.addEventListener("click", function(e){
                    e.preventDefault();
                    e.stopPropagation();
                    myDropzone.processQueue();
                });
                this.on("addedfile", function(file) {
                    $('#file_up').hide();
                    $(".dz-success-mark").hide();
                    $(".dz-error-mark").hide();
                    $(".dz-details").css('background','#3a3535');
                    $("#idfile").val('1');
                    $('#write_activity').removeAttr("required");
                });
                this.on("canceled", function(file) {
                  Swal.fire({
                    icon: 'error',
                    text: 'Cancelado por el usuario',
                  })
                });
                this.on("removedfile", function(file) {
                $('#file_up').show();
                $("#idfile").val('0');
                $('#write_activity').prop("required", true);
                myDropzone.removeFile(file);
                });

                this.on("error", function(file, response) {
                  Swal.fire({
                    icon: 'error',
                    title: 'Oops...',
                    html: response + ' Intente denuevo!',
                  })
                });
                this.on("success", function(success) {
                salvar();
                });
                this.on("complete", function(file) {
                  $('#file_up').show();
                  myDropzone.removeFile(file);
                  $("#idfile").val('0');
                  $('#write_activity').prop("required", true);
                });
            }
        }; 
</script>
@endsection
