<p>Enviar a:</p>
<select id="student" name="student[]" class="form-control custom-select" multiple="true" required>
    @foreach ($users as $user)
        <option value="{{ $user->user_id }}">{{ $user->last_name_1 }} {{ $user->first_name_1 }}</option>
    @endforeach
</select>
<script>
	$('#student').multiSelect();
</script>