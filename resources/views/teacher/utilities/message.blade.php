				@if(!$tasks->isEmpty())
                @php
                    function myTruncate($string, $limit, $break='.', $pad='...') {
                        // return with no change if string is shorter than $limit
                        if(strlen($string) <= $limit)
                            return $string;
                            // is $break present between $limit and the end of the string?
                        if(false !== ($breakpoint = strpos($string, $break, $limit))) {
                        if($breakpoint < strlen($string) - 1) {
                            $string = substr($string, 0, $breakpoint) . $pad;
                        }
                        }
                        return $string;
                    }
                @endphp
                @foreach ($tasks as $task)
                @if($task->receiver=='1' && auth()->user()->hasRole('student'))
                    @php
                    $exist = DB::table('student_task')
                            ->where('task_id', $task->id)
                            ->where('user_id',auth()->user()->id)
                            ->first();

                    @endphp
                @else
                    @php
                    $exist=true;
                    @endphp
                    
                @endif
                @if($exist)
                <div id="task-{{ $task->id }}" class="row">
                    <div class="col-md-12">
				<div class="card mt-4 mb-0">
                    <div class="card-header">
                        Materia: {{ $task->course->name }}<br>
                        Fecha y Hora: {{ $task->course->created_at }} 
                        <div class="card-actions">
                        @role('teacher')
                            @if($task->request=='1')
                                <button type="button" class="btn waves-effect waves-light btn-outline-success mr-2" onclick="response_task('{{ $task->id }}')" data-toggle="modal" data-target="#responsive-modal" class="model_img img-responsive"> Ver respuestas <i class="mdi mdi-message"></i></button>
                            @endif
                            @if($task->course->Courses_sections_user()->where('user_id',auth()->user()->id)->first())
                            
                                <a class="btn-close" data-action="close" onclick="delete_task('{{ $task->id }}')"><i class="ti-close"></i></a>
                            @endif
                        @endrole
                        @role('student')
                            @if($task->request=='1')

                                @php
                                $response = App\responseActivity::where('user_id',auth()->user()->id)
                                            ->where('task_id', $task->id)
                                            ->first();
                                @endphp
                                @if($response)
                                <h3 class="text-danger">Actividad entregada</h3>
                                <a class="btn-close text-danger" onclick="delete_response('{{ $response->id }}')"><i class="ti-close"></i> Borrar Archivo</a>
                                @else
                                <button type="button" class="btn waves-effect waves-light btn-outline-success mr-2" onclick="response_task('{{ $task->id }}', '{{ $task->course->id }}')" data-toggle="modal" data-target="#responsive-modal" class="model_img img-responsive"> Enviar Respuesta <i class="mdi mdi-message"></i></button>
                                @endif
                            @endif
                        @endrole
                        </div>
                    </div>
                    <div class="card-body collapse show">
                            @php
                            $seemore='... <a href="javascript:void(0);" title="" onclick="see_more('.$task->id.')"> See more</a>';
                            $msg=nl2br($task->message);
                            $msg=str_replace("<br />","<br>",$msg);
                            $cadena_resultante= preg_replace('/((http|https|www)[^\s]+)/', '<a target=\"_blank\" href="$1">$0</a>', $msg);

                           // echo $cadena_resultante;
                            @endphp
                            <div id="shorttext-{{ $task->id }}" class="text"> 
                           <p>
                           @php
                           echo myTruncate($cadena_resultante, 150, ' ', $seemore);
                           @endphp</p>
                        </div>
                            @if(strlen($cadena_resultante) >= 148)
                        <div id="longtext-{{ $task->id }}" hidden>
                           <p>
                          @php
                           echo $cadena_resultante;
                           @endphp
                           </p>
                        </div>
                        @endif
                        
                        @if ($task->file == 1)
                        @php
                            $files = App\FileUpdate::where('task_id', $task->id)->get();
                        @endphp
                            <div class="downloads">
                                @foreach ($files as $file)
                                @if($file->extension == 'mp4' || $file->extension == 'MP4' || $file->extension == 'WebM')
                                <video src="{{ asset($file->url) }}" width="100%" height="300" preload="metadata" controls="controls">
                                </video>
                                @else
                                <a href="{{ asset($file->url) }}" title="Descargar Archivo" target="_blank" download="file-{{ $task->id }}-{{ $file->created_at }}">Descargar Archivo</a>
                                @endif
                                @endforeach
                            </div>
                        @endif
                        
                    </div>
                    <div class="card-footer text-muted text-center">
                       <a href="javascript:void()" title="" onclick="comment({{ $task->id }})">Comentarios ( {{ App\Comments::where('task_id', $task->id)->count() }} )</a>
                    </div>
                </div>
                </div>
                <div class="col-md-12">
                    <span id="comment{{ $task->id }}" class="comment">
                    </span>
                    
                </div>
                </div>
                @endif
				@endforeach
                @else
                <div class="row">
                    <div class="col-md-12">
                        <div class="card mt-4 mb-0">
                            <div class="card-header">
                                Nada que mostrar
                            </div>
                        </div>
                    </div>
                </div>
				@endif