<div class="card mt-0 mb-0">
	<div id="people_messages" class="chat-right-aside">
	<div style="max-height: 300px; overflow-y: scroll;" class="chat-rbox">
        @if(!$comments->isEmpty())
        
        <ul class="chat-list p-20">
                                        @foreach($comments as $comment)

                                            <li>
                                                <div class="chat-img"><img src="{{ asset('storage/pictures/avatars/'.$comment->user->profile->avatar, Request::secure()) }}" alt="user" /></div>
                                                <div class="chat-content">
                                                    <h5>{{ $comment->user->profile->last_name_1 }} {{ $comment->user->profile->first_name_1 }}</h5>
                                                    <div class="box bg-light-info">
                                                                     @php
                                                        $msg=nl2br($comment->comment);
                                                        $msg=str_replace("<br />","<br>",$msg);
                                                        $cadena_resultante= preg_replace('/((http|https|www)[^\s]+)/', '<a target=\"_blank\" href="$1">$0</a>', $msg);
                                                        echo $cadena_resultante;
                                                    @endphp</div>
                                                </div>
                                                @if($comment->user->id == auth()->user()->id)
                                                <div class="chat-time"> <a class="text-danger" href="javascript:void(0);"  onclick="delete_comment('{{ $comment->id }}','{{ $comment->task_id }}')">Borrar</a></div>
                                                @endif
                                            </li>
                                            
                                            @endforeach
                                        </ul>
        
        @endif                        
    </div>
        <div class="card-body b-t">
            <div class="row">
                <div class="col-8">
                    <textarea id="comment_new{{ $task->id }}" placeholder="Escribir Comentario" name="comment" class="form-control b-0" required=""></textarea>
                    <input type="hidden" name="course_id" value="{{ $task->course_id }}">
                </div>
                <div class="col-4 text-right">
    	            <button id="save_comment" type="button" title="Enviar Comentario" class="btn btn-info btn-circle btn-lg" onclick="save_comment('{{ $task->id }}', '{{ $task->course_id }}')"><i class="fa fa-paper-plane-o"></i> </button>
                </div>
            </div>
        </div>  
    </div>        
</div>