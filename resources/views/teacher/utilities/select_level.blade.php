<select name="sec" class="form-control custom-select">
    @foreach ($sections as $section)
        <option value="{{ $section->id }}">{{ $section->name }}</option>
    @endforeach
</select>