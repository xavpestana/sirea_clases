@extends('layouts.app')

@section('title-view', 'Biblioteca - Alumnos')

@section('extra-css')
<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-select@1.13.14/dist/css/bootstrap-select.min.css">
<link href="{{ asset('themeforest/multiselect/css/multi-select.css', Request::secure()) }}" rel="stylesheet" type="text/css" />
<link href="https://cdn.datatables.net/1.10.21/css/jquery.dataTables.min.css" rel="stylesheet">
@endsection

@section('content')
@php
    $header = [];
    $curso = [];
@endphp
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-header">
                    <h4>LEER: <small>El mejor negocio de tu vida</small></h4>
                </div>
                <div class="card-body">
                    <div class="card-header">
                        <ul style="list-style: none" class="row">
                            @foreach ($courses as $course)
                                @if(!in_array($course->Section->grade->name, $header))
                                @php
                                    $header = Arr::prepend($header, $course->Section->grade->name);
                                @endphp
                                <li class="col-md-3 mb-3">
                                    <a href="#library-{{ $course->Section->grade->name }}">Lectura: {{ $course->Section->grade->name }}</a>
                                </li>
                                @endif
                            @endforeach
                        </ul>
                    
                    </div>
                        
                                    @foreach ($courses as $course)

                                    @if(!in_array($course->Section->grade->name, $curso))
                                    @php

                                        $curso = Arr::prepend($curso, $course->Section->grade->name);
                                        $grade = $course->Section->grade->name;
                                        if ($grade == '1er Grado' || $grade == '2do Grado') {
                                            $level = 'kids';
                                        }
                                        if ($grade == '3er Grado' || $grade == '4to Grado' || $grade == '5to Grado') {
                                            $level = 'reformers';
                                        }
                                        if ($grade == '6to Grado' || $grade == '1er Año' || $grade == '2do Año') {
                                            $level = 'millenium';
                                        }
                                        if ($grade == '3er Año' || $grade == '4to Año' || $grade == '5to Año') {
                                            $level = 'influencers';
                                        }
                                        
                                        $libraries = \App\Library::where('level', $level)->get();
                                    @endphp

                                    <div id="library-{{ $grade }}" class="card-header">
                                        <h4>Lecturas: {{ $grade }}</h4>
                                    </div>
                                    <div id="students-{{ $grade }}" class="table-responsive">
                                    <table class="table table-bordered table-striped table-active">
                                        <tbody>
                                @foreach ($libraries as $library)
                                    <tr>
                                        <td colspan="2">{{ $library->name }}</td>
                                        <td>
                                            <div class="d-flex flex-row">
                                                <div class="p-10 bg-warning">
                                                    <h3 class="text-white box m-b-0"><i class="ti-wallet"></i></h3></div>
                                                    <div class="align-self-center m-l-20">
                                                        <h3 class="m-b-0">Costo {{ $library->amount }} SLB</h3>
                                                    </div>
                                            </div>
                                        </td>
                                        <td>
                                        <a href="{{ asset($library->url) }}" title="Descargar" download>
                                        <button class="btn btn-block btn-circle btn-success">
                                            <i class="ti-arrow-down"></i>
                                        </button>
                                        </a>
                                    </td>
                                    </tr>
                                    @endforeach
                                    </tbody>
                        </table>
                        </div>
                        @endif
                                    @endforeach
                            
                    
                </div>
            </div>
        </div>
    </div>
@endsection
@section('extra-js')
<script src="{{ asset('themeforest/Dropzonejs/js/dropzone.js', Request::secure()) }}"></script>
<script src="https://cdn.jsdelivr.net/npm/sweetalert2@9"></script>
<script src="https://cdn.jsdelivr.net/jquery.validation/1.16.0/jquery.validate.min.js"></script>
<script src="https://cdn.datatables.net/1.10.21/js/jquery.dataTables.min.js"></script>

@endsection
