<div class="table-responsive">
                <table id="docente" class="display nowrap" cellspacing="0" width="100%">
                    <thead>
                        <tr>
                            <th>Nombre Completo</th>
                            <th>Correo</th>
                            <th>Teléfono</th>
                            <th></th>
                        </tr>
                    </thead>
                     @foreach ($teachers as $teacher)
                        <tr>
                            <td>
                                <span class="mytooltip tooltip-effect-1">
                                        {{ $teacher->profile->first_name_1 }} {{ $teacher->profile->last_name_1 }}
                                </span>                                
                            </td>
                            <td>{{ $teacher->profile->user->email }}</td>
                            <td>{{ $teacher->profile->phone }}</td>
                            <td>
                                <button type="button" class="btn btn-info btn-circle" data-toggle="modal" data-target="#editModal" data-whatever="@getbootstrap" onclick="edit_teacher('{{ $teacher->profile_id }}')"><i class="fa fa-pencil"></i> </button>
                                <button type="button" class="btn btn-success btn-circle" data-toggle="modal" data-target="#pswModal" data-whatever="@getbootstrap"  onclick="updt_psw('{{ $teacher->profile->user_id }}')"><i class="fa fa-lock"></i> </button>
                                <button type="button" class="btn btn-danger btn-circle" onclick="delete_teacher('{{ $teacher->profile->user_id }}')"><i class="fa fa-times"></i> </button>
                            </td>                               
                        </tr>
                        @endforeach
                    <tfoot>
                        <tr>
                            <th>Apellidos</th>
                            <th>Correo</th>
                            <th>Teléfono</th>
                            <th></th>
                        </tr>
                    </tfoot>
                    <tbody>
                       
                    </tbody>
                </table>
            </div>
            <script>
            $(document).ready(function() {
                $('#docente').DataTable( {
                    dom: 'Bfrtip',
                    buttons: [
                    'csv', 'excel', 'pdf', 'print'
                    ]
                });
            });
            </script>