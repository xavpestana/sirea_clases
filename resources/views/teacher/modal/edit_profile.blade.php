            <div class="row">
                    <div class="form-group col-md-3">
                        <label for="tipo">Tipo <span style="color: red">*</span></label>
                        <select id="tipo" name="type_document" class="form-control" required="">
                            <option value="V" @if($teacher->profile->document_type =='V') selected="" @endif>V</option>
                            <option value="E" @if($teacher->profile->document_type =='E') selected="" @endif>E</option>
                        </select>
                    </div>
                    <div class="form-group col-md-9">
                        <label for="document">Nro de documento <span style="color: red">*</span></label>
                        <input type="text" class="form-control" id="document" name="document" value="{{ $teacher->profile->document_number }}" required="">
                        <input type="text" id="id_user" name="id_user" value="{{ $teacher->profile->user_id }}" required="" hidden="">
                        <input type="text" id="profile_id" name="profile_id" value="{{ $teacher->profile_id }}" required="" hidden="">
                    </div>
                    <div class="form-group col-md-6">
                        <label for="name_teacher">Nombres <span style="color: red">*</span></label>
                        <input type="text" class="form-control" id="name_teacher" name="name_teacher" value="{{ $teacher->profile->first_name_1 }}" required="">
                    </div>
                    <div class="form-group col-md-6">
                        <label for="last_name_teacher">Apellidos <span style="color: red">*</span></label>
                        <input type="text" class="form-control" id="last_name_teacher" name="last_name_teacher" value="{{ $teacher->profile->last_name_1 }}" required="">
                    </div>
                    <div class="form-group col-md-5">
                        <label for="gender">Sexo <span style="color: red">*</span></label>
                        <select id="gender" name="gender" class="form-control" required="">
                            <option value="1" @if($teacher->profile->gender_id == 1) selected="" @endif>Masculino</option>
                            <option value="2" @if($teacher->profile->gender_id == 2) selected="" @endif>Femenino</option>
                        </select>
                    </div>
                    <div class="form-group col-md-7">
                        <label for="email">Corre electronico <span style="color: red">*</span></label>
                        <input type="text" class="form-control" id="email" value="{{ $teacher->profile->user->email }}" name="email" required="">
                    </div>
                </div>