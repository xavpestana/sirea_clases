<div class="modal-content">
                                            <div class="modal-header">
                                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                                <h4 class="modal-title">Respuestas de Activiad</h4>
                                            </div>
                                            <div class="modal-body">
                                                <div class="table-responsive">
                                                    @if(!$responses->isEmpty())
                <table id="docente" class="display nowrap" cellspacing="0" width="100%">
                    <thead>
                        <tr>
                            <th class="text-info">Nombre Completo</th>
                            <th class="text-info">Fecha y hora de entrega</th>
                            <th class="text-info">Acciones</th>
                            
                        </tr>
                    </thead>
                     @foreach ($responses as $response)
                     
                        <tr>
                            <td class="text-info">
                                <span class="mytooltip tooltip-effect-1">
                                        {{ $response->user->profile->first_name_1 }} {{ $response->user->profile->last_name_1 }}
                                </span>                                
                            </td>
                            <td class="text-info">{{ $response->created_at }}</td>
                            <td>
                                <a href="{{ asset($response->fileResponse->url) }}" title="Descargar Actividad" download="Actividad-{{ $response->user->profile->first_name_1 }}-{{ $response->user->profile->last_name_1 }}"><button type="button" class="btn btn-info btn-circle"><i class="mdi mdi-briefcase-download"></i> </button></a>
                                <button type="button" class="btn btn-danger btn-circle"  title="Eliminar Actividad" onclick="delete_response('{{ $response->id }}')"><i class="fa fa-times"></i> </button>
                            </td>                               
                        </tr>
                        @endforeach
                    <tfoot>
                        <tr>
                            <th>Nombre Completo</th>
                            <th>Fecha y hora de entrega</th>
                            <th>Acciones</th>
                            
                        </tr>
                    </tfoot>
                    <tbody>
                       
                    </tbody>
                </table>
                @else
                <h3>Nada que mostrar</h3>
                @endif
            </div>
            <script>
            $(document).ready(function() {
                $('#docente').DataTable();
            });
            </script>
                                            </div>
                                            <div class="modal-footer">
                                                <button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Cerrar</button>
                                            </div>
                                        </div>