@extends('layouts.app')

@section('title-view', 'Docentes - Actividades')

@section('extra-css')
<!-- Latest compiled and minified CSS -->
<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-select@1.13.14/dist/css/bootstrap-select.min.css">
<link href="{{ asset('themeforest/multiselect/css/multi-select.css', Request::secure()) }}" rel="stylesheet" type="text/css" />
<link href="https://cdn.datatables.net/1.10.21/css/jquery.dataTables.min.css" rel="stylesheet">
@endsection
@section('content')
<div class="row">
                    <div class="col-md-8">
                        <div class="card m-b-0">
                            <!-- .chat-row -->
                            <div class="chat-main-box">
                                <!-- .chat-right-panel -->
                                <div class="chat-right-asid">
                                    <div class="chat-main-header">
                                        <div class="p-20 b-b">

                                            <h2 class="box-title">Materia: {{ $course->name }}</h2>
                                            <h3 class="box-title">Grado: {{ $course->grade->name }}<br>
                                                Seccion: {{ $course->courses_sections_user[0]->section->name }}<br>
                                                Periodo: {{ $season->season }}
                                            </h3>
                                        </div>
                                    </div>
                                    <div class="card-body b-t">
                                        <div class="row">
                                            <div class="col-12">
                                                <form id="form_activity" method="post" accept-charset="utf-8">
                                                    @csrf
                                                    <div class="row">
                                                      <div class="col-md-12">
                                                        <div class="demo-checkbox">
                                                          <input type="checkbox" id="request" name="response" class="filled-in" checked="">
                                                          <label for="request">Entrega de trabajo <small>(Seleccione si desea que el alumno entregue la actividad por este medio)</small></label>
                                                        </div>
                                                      </div>
                                                      <div class="col-md-5">
                                                        <div class="demo-checkbox">
                                                          <input type="checkbox" id="todos" name="all_section" class="filled-in" checked="">
                                                          <label for="todos">Seleccione si envía a todos los alumnos</label>
                                                        </div>
                                                      </div>
                                                      <div class="col-md-7">
                                                        <span id="students"></span>
                                                      </div>
                                                    </div>
                                                <textarea id="write_activity" placeholder="Escribir actividad" name="message" cols="20" rows="5" class="form-control b-0 border-bottom" required=""></textarea>
                                                <input type="hidden" name="id_course" value="{{ $course->id }}">
                                                <input type="hidden" id="idfile" name="file" value="0" />
                                                </form>
                                            </div>
                                            <div class="col-12 row text-white mt-4">
                                                <div class="col-md-6">
                                                    <form style="border: 0 !important" method="post" action="{{route('tasks.store')}}" enctype="multipart/form-data" class="dropzone" id="dropzone">
                                                        <div style="padding: 0px 15px" class="dz-message">
                                                            <label type="text" id="file_up" style="color:#fff"  class="btn btn-info btn-circle btn-lg mb-2">
                                                        <i class="fa fa-file-picture-o"></i>
                                                        </label>
                                                            <div class="drag-icon-cph">
                                                            </div>
                                                        </div>
                                                        <div class="fallback">
                                                            <input id="fileAttachmentBtn" id="image" name="image" type="file" class="d-none">
                                                        </div>
                                                        @csrf
                                                    </form>
                                                </div>
                                                <div class="col-md-6 text-right">
                                                    <button id="up_activity" style="color:#fff" type="button" class="btn btn-info btn-circle btn-lg mb-2" title="Enviar Actividad"><i class="fa fa-paper-plane-o"></i></button>    
                                                </div>
                                <div id="responsive-modal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
                                    <div id="dialog" class="modal-dialog">
                                        
                                    </div>
                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!-- .chat-right-panel -->
                            </div>
                            <!-- /.chat-row -->
                        </div>
                        <span id="message"></span>
                    </div>
                    <div style=" height: 200px; overflow-y: scroll;" class="col-md-4">
                        <div class="card">
                            <div class="card-body">
                                <h4 class="card-title">Ultimas Noticias</h4>
                                <ul class="feeds">
                                    @if(!$notifies->isEmpty())
                                    @foreach($notifies as $notify)
                                    <li>
                                        <div class="dw-user-box">
                                        <div class="u-img"><img style="border-radius: 50%" src="
                            @if(!$notify->user->profile)
                            {{ asset('storage/pictures/avatars/default.jpg') }}
                            @else
                            {{ asset('storage/pictures/avatars/'.$notify->user->profile->avatar )}}
                            @endif" alt="user" data-toggle="tooltip" data-placement="top" title="" data-original-title="{{ $notify->user->profile->first_name_1 }}" width="30"></div></div> <strong>{{ $notify->user->profile->first_name_1 }} <a href="#task-{{ $notify->task_id }}" title="">{{ $notify->message }}</a></strong></li>
                                    @endforeach
                                    @else
                                    <li>Nada que mostrar</li>
                                    @endif
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
@endsection

@section('extra-js')
<script src="{{ asset('themeforest/Dropzonejs/js/dropzone.js', Request::secure()) }}"></script>
<script src="https://cdn.jsdelivr.net/npm/sweetalert2@9"></script>
<script src="https://cdn.jsdelivr.net/jquery.validation/1.16.0/jquery.validate.min.js"></script>
<script src="https://cdn.datatables.net/1.10.21/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.6.2/js/dataTables.buttons.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.6.2/js/buttons.flash.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/pdfmake.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/vfs_fonts.js"></script>
<script src="https://cdn.datatables.net/buttons/1.6.2/js/buttons.html5.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.6.2/js/buttons.print.min.js"></script
<script type="text/javascript" src="{{ asset('themeforest/multiselect/js/jquery.multi-select.js', Request::secure()) }}"></script>
<script type="text/javascript" charset="utf-8">
$('#message').load('{{ route('tasks.messages', ['id' => $course->id]) }}');
    // seleccion de alumnos
    $( '#todos' ).on( 'click', function() {
        if( $(this).is(':checked') ){
           // Hacer algo si el checkbox ha sido seleccionado
            $('#students').html(' ');
        } else {
            // Hacer algo si el checkbox ha sido deseleccionado
            $('#students').load('{{ route('tasks.students', ['id' => $course->courses_sections_user[0]->section->id]) }}');
        }
    });
    $.ajax({
         type : "GET",
         data : {"id" : {{ auth()->user()->id }} },
         url : "{{ route('chat.count') }}",
         success : function(data){
            $('.count').html(data);
         }
      });
    function see_more(id) {
      $('#shorttext-'+id).toggle();
      $('#longtext-'+id).removeAttr("hidden");
   }

   function comment(id) {
    $('.comment').html(' ');
    $('#comment'+id).load("{{route('showcomment')}}?id="+id);
    }
function save_comment(id_task, id_course) {

      var comment = $('#comment_new'+id_task).val();
      
    $.ajax({
            type : "POST",
            data :  {"comment" : comment,"task_id" : id_task,"course_id" : id_course, "_token": "{{ csrf_token() }}"},
            url : "{{ route('comment') }}",
        })
        .done(function(response) {
            $('#comment_new'+id_task).val('');
            $('#comment'+id_task).load("{{route('showcomment')}}?id="+id_task);
        })
        .fail(function(response) {
            Swal.fire({
                icon: 'error',
                title: 'Oops...',
                text: 'Intente nuevamente.',
                })
        }) 
    }
    function delete_comment(id,id_task){
      $.ajax({
            type : "DELETE",
            data : {"id" : id, "_token": "{{ csrf_token() }}"},
            url : "{{ route('delete.comment') }}",
            success : function(r){
    if (r==0){
        $('#comment'+id_task).load("{{route('showcomment')}}?id="+id_task);
    }else{
        Swal.fire({
            icon: 'error',
            title: 'Oops...',
            text: 'Error, intente denuevo!',
        })
    }
           }
        });
            
        }
    //guardar tarea

    $( "#up_activity" ).click(function() {
    var file = $('#idfile').val();
    if (file=='0'){
      if ($("#form_activity").valid()){
    salvar();
      }
    }
    })
function salvar(){
        
            datos = $("#form_activity").serialize();
                $.ajax({
                    type:'POST',
                    data:datos,
                    url:"{{ route('tasks.save') }}",
                    success:function(r)
                    {
                        if (r==0){                        
                            $('#form_activity')[0].reset();
                            $('#students').html(' ');
                            Swal.fire({
                              position: 'center-center',
                              icon: 'success',
                              title: 'La actividad se guardó con éxito',
                              showConfirmButton: false,
                              timer: 1000,
                           })
                           flag = 0;
                        $('#message').load('{{ route('tasks.messages', ['id' => $course->id]) }}');
                        }else{
                           Swal.fire({
                                 icon: 'error',
                                 title: 'Oops...',
                                 text: 'Algo ha ocurrido, Intente denuevo!',
                           })
                        }
                    }    
    })
    };

    Dropzone.options.dropzone = {
            autoProcessQueue: false,
            paramName: "file",
            addRemoveLinks: true,
            dictRemoveFile : "×",
            uploadMultiple: false,
            maxFiles: 1,
            createImageThumbnails: false,
            maxFilesize:20,
            timeout: 1200000,
            dictCancelUpload: "Cancelar subida",
            dictCancelUploadConfirmation: "¿Seguro que desea cancelar esta subida?",
            dictMaxFilesExceeded: "Se ha excedido el numero de archivos permitidos.",

           init: function() {
                var submitBtn = document.querySelector("#up_activity");
                myDropzone = this;
                submitBtn.addEventListener("click", function(e){
                    e.preventDefault();
                    e.stopPropagation();
                    myDropzone.processQueue();
                });
                this.on("addedfile", function(file) {
                    $('#file_up').hide();
                    $(".dz-success-mark").hide();
                    $(".dz-error-mark").hide();
                    $(".dz-details").css('background','#3a3535');
                    $("#idfile").val('1');
                    $('#write_activity').removeAttr("required");
                });
                this.on("canceled", function(file) {
                  Swal.fire({
                    icon: 'error',
                    text: 'Cancelado por el usuario',
                  })
                });
                this.on("removedfile", function(file) {
                $('#file_up').show();
                $("#idfile").val('0');
                $('#write_activity').prop("required", true);
                myDropzone.removeFile(file);
                });

                this.on("error", function(file, response) {
                  Swal.fire({
                    icon: 'error',
                    title: 'Oops...',
                    html: response + ' Intente denuevo!',
                  })
                });
                this.on("success", function(success) {
                salvar();
                });
                this.on("complete", function(file) {
                  $('#file_up').show();
                  myDropzone.removeFile(file);
                  $("#idfile").val('0');
                  $('#write_activity').prop("required", true);
                });
            }
        }; 
        function delete_task(id){
            Swal.fire({
  title: '¿Está seguro?',
  text: "No podrá revertir esto!",
  icon: 'warning',
  showCancelButton: true,
  confirmButtonColor: '#3085d6',
  cancelButtonColor: '#d33',
  confirmButtonText: 'Yes, delete it!'
}).then((result) => {
  if (result.value) {
   
   $.ajax({
            type : "DELETE",
            data : {"id" : id, "_token": "{{ csrf_token() }}"},
            url : "{{ url('docente/tasks/') }}/"+id,
            success : function(r){
    if (r==0){
        $('#message').load('{{ route('tasks.messages', ['id' => $course->id]) }}');
        Swal.fire(
        'Eliminado!',
        'La actividad ha sido eliminado con exito.',
        'success'
    )
    }else{
        Swal.fire({
            icon: 'error',
            title: 'Oops...',
            text: 'Error, intente denuevo!',
        })
    }
           }
        });
    
  }
})
        }
function response_task(task_id){
$('#dialog').html("Cargando....");
$('#dialog').load("{{route('showresponse')}}?id="+task_id);
}

function delete_response(response_id){
        $.ajax({
            type : "GET",
            data : {"id" : response_id, "_token": "{{ csrf_token() }}"},
            url : "{{ route('studentActivity.delete') }}",
            success : function(r){
    if (r==0){
        Swal.fire({
                    icon: 'success',
                    title: 'Exito!!',
                    text: 'Actividad eliminada con exito',
                  })
        location.reload();
    }else{
        Swal.fire({
            icon: 'error',
            title: 'Oops...',
            text: 'Error, intente denuevo!',
        })
    }
           }
        });
        }
</script>
@endsection

