@extends('layouts.app')

@section('title-view', 'Docentes - Materias asignadas')

@section('content')
<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-body">
                <h4 class="card-title">Listado de materias asignadas</h4>
                <div id="table" class="table-responsive m-t-40">
                    <div class="table-responsive">
                <table id="docente" class="table table-striped" cellspacing="0" width="100%">
                    <thead class="thead-dark">
                        <tr>
                            <th>NOMBRE DE LA MATERIA</th>
                            <th>GRADO</th>
                            <th>SECCIÓN</th>
                            <th>ACCIÓN</th>
                        </tr>
                    </thead>
                     @foreach ($courses as $course)
                        <tr>
                            <td>
                                {{ $course->Course->name }}                               
                            </td>
                            <td>{{ $course->Course->Grade->name }}</td>
                            <td>{{ $course->Section->name }}</td>
                            <td>
                                <a href="{{ route('tasks.show', ['task' => $course->course->id]) }}" title="Gestión de Tareas">
                                <button type="button" class="btn btn-info btn-circle ml-2" title="Gestión de Tareas"><i class="fa fa-book"></i></button>
                                </a>
                                
                                <a href="{{ route('evaluations.show', ['evaluation' => $course->course->id]) }}" title="Gestión de Notas">
                                <button type="button" class="btn btn-success btn-circle" title="Gestión de Notas"><i class="fa fa-check-circle-o"></i></button>
                                </a>
                                
                            </td>                               
                        </tr>
                        @endforeach
                    <tbody>
                       
                    </tbody>
                </table>
            </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@section('extra-js')
<script>
    $.ajax({
         type : "GET",
         data : {"id" : {{ auth()->user()->id }} },
         url : "{{ route('chat.count') }}",
         success : function(data){
            $('.count').html(data);
         }
      });
</script>
@endsection