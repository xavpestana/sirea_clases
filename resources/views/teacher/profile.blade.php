@extends('layouts.app')

@section('title-view', 'Docentes - Mi Perfil')

@section('extra-css')
<!-- Latest compiled and minified CSS -->
<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-select@1.13.14/dist/css/bootstrap-select.min.css">
<link href="{{ asset('themeforest/multiselect/css/multi-select.css', Request::secure()) }}" rel="stylesheet" type="text/css" />
@endsection
@section('content')
      
                
                <div class="row">
                    <div class="col-lg-12">
                        <div class="card card-outline-info">
                            <div class="card-header">
                                <h4 class="m-b-0 text-white">Mi Perfil</h4>
                            </div>
                            <div class="card-body">
                                <div class="col-md-12 mb-4">
                                               
            <div class="user-profile">
                <!-- User profile image -->
                <div class="profile-img"> 
                    <img src="{{ asset('storage/pictures/avatars/'.auth()->user()->profile->avatar, Request::secure()) }}" alt="user" />
                    <form style="border: 0 !important" method="POST" action="{{url('docente/image/avatar')}}" enctype="multipart/form-data" class="dropzone" id="dropzone">
                        <div style="cursor: pointer" align="center" class="dz-message">
                            <label data-toggle="tooltip" data-placement="top" data-original-title="Cambiar imagen de perfil" class="btn btn-info m-0" for="fileAttachmentBtn">
                           <i class="mdi mdi-account-circle"></i>
                           </label>
                        <div class="drag-icon-cph">
                        </div>
                        </div>
                        <div class="fallback">
                            <input id="fileAttachmentBtn" id="avatar" name="avatar" type="file" class="d-none">
                        </div>
                        @csrf
                    </form>
                    
                </div>
                <!-- User profile text-->
                <div class="profile-text">
                    <h5>{{ auth()->user()->profile->first_name_1 }} {{ auth()->user()->profile->last_name_1 }}</h5>
                </div>
            </div>
       
                                            </div>
                                <form id="form_update_teacher" class="form-horizontal">
                                    @csrf
                                    <div class="form-body">
                                        <div class="row">
                                            
                                            <div class="col-md-6">
                                                <div class="form-group row">
                                                    <label class="control-label text-right col-md-3">Tipo de Documento</label>
                                                    <div class="col-md-9">
                                                        <select class="form-control custom-select" name="document_type"  required="">
                                                            <option @if(auth()->user()->profile->document_type =='V') selected="" @endif value="V">V</option>
                                                            <option @if(auth()->user()->profile->document_type == 'E') selected="" @endif value="E">E</option>
                                                        </select>
                                                        </div>
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group row">
                                                    <label class="control-label text-right col-md-3">Nro de documento</label>
                                                    <div class="col-md-9">
                                                        <input type="text" class="form-control" placeholder="Numero de documento" name="document" value="{{ auth()->user()->profile->document_number }}"  required="">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group row">
                                                    <label class="control-label text-right col-md-3">Nombre</label>
                                                    <div class="col-md-9">
                                                        <input type="text" class="form-control" placeholder="Nombre" name="first_name" value="{{ auth()->user()->profile->first_name_1 }}"  required="">
                                                    </div>
                                                </div>
                                            </div>
                                            <!--/span-->
                                            <div class="col-md-6">
                                                <div class="form-group row">
                                                    <label class="control-label text-right col-md-3">Apellido</label>
                                                    <div class="col-md-9">
                                                        <input type="text" class="form-control" name="last_name" placeholder="Apellido" value="{{ auth()->user()->profile->last_name_1 }}"  required="">
                                                        </div>
                                                </div>
                                            </div>
                                            <!--/span-->
                                        </div>
                                        <!--/row-->
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="form-group row">
                                                    <label class="control-label text-right col-md-3">Genero</label>
                                                    <div class="col-md-9">
                                                        <select class="form-control custom-select" name="gender"  required="">
                                                            <option @if(auth()->user()->profile->gender_id == 1) selected="" @endif value="1">Masculino</option>
                                                            <option @if(auth()->user()->profile->gender_id == 2) selected="" @endif value="2">Femenino</option>
                                                        </select>
                                                        </div>
                                                </div>
                                            </div>
                                            <!--/span-->
                                            <div class="col-md-6">
                                                <div class="form-group row">
                                                    <label class="control-label text-right col-md-3">Fecha de Nacimiento</label>
                                                    <div class="col-md-9">
                                                        <input type="date" class="form-control" value="{{ auth()->user()->profile->birthdate }}" placeholder="yyyy/mm/dd"  required="">
                                                    </div>
                                                </div>
                                            </div>
                                            <!--/span-->
                                        </div>
                                        <!--/row-->
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="form-group row">
                                                    <label class="control-label text-right col-md-3">Telefono</label>
                                                    <div class="col-md-9">
                                                        <input type="phone" class="form-control" name="phone" placeholder="Telefono" value="{{ auth()->user()->profile->phone }}"  required="">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group row">
                                                    <label class="control-label text-right col-md-3">Contraseña</label>
                                                    <div class="col-md-9">
                                                        <input type="password" class="form-control" name="password" placeholder="Cambiar contraseña actual (opcional)">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="form-group row">
                                                    <label class="control-label text-center col-md-2">Direccion</label>
                                                    <div class="col-md-10">
                                                        <input type="text" class="form-control" name="adress" placeholder="Dirección" value="{{ auth()->user()->profile->teacher->address }}" required="">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <hr>
                                    <div class="form-actions">
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="row">
                                                    <div class="col-md-offset-3 col-md-9">
                                                        <button type="submit" id="update" class="btn btn-success">Actualizar Perfil</button>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-6"> </div>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
    
@endsection

@section('extra-js')
<script src="{{ asset('themeforest/Dropzonejs/js/dropzone.js', Request::secure()) }}"></script>
<script src="https://cdn.jsdelivr.net/npm/sweetalert2@9"></script>
<script src="https://cdn.jsdelivr.net/jquery.validation/1.16.0/jquery.validate.min.js"></script>
<script type="text/javascript" src="{{ asset('themeforest/multiselect/js/jquery.multi-select.js', Request::secure()) }}"></script>
<script>
    /*******  Validate forms  ********/
(function() {
   'use strict';
        window.addEventListener('load', function() {
        // Fetch all the forms we want to apply custom Bootstrap validation styles to
        var forms = document.getElementsByClassName('needs-validation');
        // Loop over them and prevent submission
        var validation = Array.prototype.filter.call(forms, function(form) {
            form.addEventListener('submit', function(event) {
                if (form.checkValidity() === false) {
                    event.preventDefault();
                    event.stopPropagation();
                }
            form.classList.add('was-validated');
        }, false);
        });
        }, false);
})();

$.ajax({
         type : "GET",
         data : {"id" : {{ auth()->user()->id }} },
         url : "{{ route('chat.count') }}",
         success : function(data){
            $('.count').html(data);
         }
      });

/********  Imagen de Perfil  *********/
Dropzone.options.dropzone = {
            autoProcessQueue: true,
            paramName: "avatar",
            acceptedFiles: ".jpeg,.jpg,.png",
            withCredentials: false,
            uploadMultiple: false,
            resizeWidth:400,
            resizeHeight:400,
            resizeMethod:'crop',
            maxFiles: 1,
            createImageThumbnails: false,
            dictFallbackMessage: "Su navegador no soporta arrastrar y soltar para subir archivos.",
            dictInvalidFileType: "No se puede subir este tipo de archivos.",
            dictCancelUpload: "Cancelar subida",
            dictCancelUploadConfirmation: "¿Seguro que desea cancelar esta subida?",
            dictMaxFilesExceeded: "Se ha excedido el numero de archivos permitidos.",
            
           init: function() {

               myDropzone = this;

                this.on("addedfile", function(file) {
                    $(".dz-success-mark").hide();
                  $(".dz-error-mark").hide();
                });
                this.on("error", function(file, response) {
                  Swal.fire({
                                 icon: 'error',
                                 title: 'Oops...',
                                 html: response,
                           })

                  });
                this.on("success", function(success) {
                 Swal.fire({
                              position: 'top-end',
                              icon: 'success',
                              title: 'La imagen ha sido actualizado',
                              showConfirmButton: false,
                              timer: 1000,
                           })
                    location. reload();
                myDropzone.removeFile(file);
                  });
            }
        }; 
/********  Update docente  *********/

$( "#form_update_teacher" ).submit(function(e) {
e.preventDefault();

  $("#update").prop('disabled', true);
  datos = $("#form_update_teacher").serialize();
$.ajax({
        type:'POST',
        data:datos,
        url:"{{ route('profile') }}",
        success:function(r)
        {
          if (r==0) {         
            $("#update").prop('disabled', false);
            Swal.fire({
                position: 'center-center',
                icon: 'success',
                title: 'El perfil se ha actualizado exitosamente',
                showConfirmButton: false,
                timer: 1500
            }) 
          }else{
            $("#update").prop('disabled', false);
            Swal.fire({
                icon: 'error',
                title: 'Oops...',
                text: 'No se guardo correctamente, intente denuevo!',
                })
          }
        }
        })
});
</script>
@endsection