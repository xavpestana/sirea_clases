@extends('layouts.app')

@section('title-view', 'Reportes de pagos - Conteo de pagos')

@section('extra-css')
<link href="https://cdn.datatables.net/1.10.21/css/jquery.dataTables.min.css" rel="stylesheet">
@endsection

@section('content')

                <!-- Row -->
                <div class="row">
                    <div class="col-lg-12">
                        @foreach ($plans as $plan)
                            <div class="card">
                            <div class="card-body">
                                <div class="d-flex no-block">
                                    <h4 class="card-title">{{ $plan->name }}</h4>
                                </div>
                                <div class="table-responsive m-t-20">
                                    <table class="table table-hover table-striped">
                                        <thead>
                                            <tr>
                                                <th>Nombre de la Cuota</th>
                                                <th>Numero de pagos</th>
                                                <th></th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @foreach ($plan->installments as $installment)
                                            <tr>
                                                <td>{{ $installment->description }}</td>
                                                <td>{{ $installment->installmentPayment->count() }} Pagos realizados</td>
                                                <td>
                                                    <button type="button" class="btn btn-outline-success" data-toggle="modal" data-target=".bs-example-modal-lg" onclick="conteo('{{ $installment->id }}')"><i class="fa fa-check"></i> Ver detalle de la cuota</button>
                                                </td>
                                            </tr>
                                            @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                        @endforeach
                    </div>
                </div>
                <!-- Row -->
                <div class="modal fade bs-example-modal-lg " tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none;">
                                    <div class="modal-dialog">
                                        <div id="modal_all" class="modal-content">
                                            
                                        </div>
                                        <!-- /.modal-content -->
                                    </div>
                                    <!-- /.modal-dialog -->
                                </div>
@endsection
@section('extra-js')
<script src="https://cdn.jsdelivr.net/npm/sweetalert2@9"></script>
<script src="https://cdn.datatables.net/1.10.21/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.6.2/js/dataTables.buttons.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.6.2/js/buttons.flash.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/pdfmake.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/vfs_fonts.js"></script>
<script src="https://cdn.datatables.net/buttons/1.6.2/js/buttons.html5.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.6.2/js/buttons.print.min.js"></script>

<script type="text/javascript">

    $('.table').DataTable( {
                    "language": {
                "url": "//cdn.datatables.net/plug-ins/1.10.11/i18n/Spanish.json"
            },
                    dom: 'Bfrtip',
                    "pageLength": 5,
                    "aaSorting": [[ 0, "asc" ]],
                     buttons: [
                'copyHtml5',
                'excelHtml5',
                'csvHtml5',
            ]
                });

    function conteo(id){
        $('#modal_all').html(" ");
        $('#modal_all').load("{{url('conteo/pagos/')}}/"+id);
    }
</script>
@endsection
