@extends('layouts.app')

@section('title-view', 'Reportes de pagos - Pagos')

@section('extra-css')
<link href="https://cdn.datatables.net/1.10.21/css/jquery.dataTables.min.css" rel="stylesheet">
@endsection

@section('content')
<!-- Row -->
                <div class="row">
                    <div class="col-lg-12">
                        <div class="card">
                            <div class="card-body">
                                <div class="row">
                                    <div class="col-md-6">
                                        <h4 class="card-title">Reportes de pagos</h4>
                                    </div>
                                    <div class="col-md-6">
                                        <form id="form_date">
                                        <div class="form-group row ml-auto">
                                        <label for="example-date-input" class="col-3 col-form-label">Desde</label>
                                        <div class="col-9">
                                            <input class="form-control" id="desde" type="date" max="{{ $today->format('Y-m-d') }}" id="example-date-input" value="{{ $today->format('Y-m-d') }}">
                                        </div>
                                    </div><div class="form-group row">
                                        <label for="example-date-input" class="col-3 col-form-label">Hasta</label>
                                        <div class="col-9">
                                            <input class="form-control" id="hasta" type="date" id="example-date-input" max="{{ $today->format('Y-m-d') }}" value="{{ $today->format('Y-m-d') }}">
                                        </div>
                                    </div><div class="form-group row justify-content-end">
                                        <div class="col-md-5">
                                            <button type="button" class="btn btn-success" data-toggle="modal" data-target=".bs-example-modal-lg" onclick="report('2')"><i class="ti-marker-alt"></i> Buscar</button>
                                        </div>
                                    </div>
                                    </form>
                                    </div>
                                </div>
                                <div id="reports" class="table-responsive m-t-20">
                                    
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- Row -->
                <!-- Row -->
                <div class="row">
                    <div class="col-lg-12">
                        <div class="card">
                            <div class="card-body">
                                <div class="d-flex no-block">
                                    <h4 class="card-title">Status de reportes en Linea</h4>
                                </div>
                                <div id="report_online" class="table-responsive m-t-20">
                                    
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- Row -->

<!-- Row -->
                <div class="row">
                    <div class="col-lg-12">
                        <div class="card">
                            <div class="card-body">
                                <div class="d-flex no-block">
                                    <h4 class="card-title">Reportes de pagos anulados</h4>
                                </div>
                                <div id="annulled" class="table-responsive m-t-20">
                                    
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- Row -->
@endsection
@section('extra-js')
<script src="https://cdn.jsdelivr.net/npm/sweetalert2@9"></script>
<script src="https://cdn.datatables.net/1.10.21/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.6.2/js/dataTables.buttons.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.6.2/js/buttons.flash.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/pdfmake.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/vfs_fonts.js"></script>
<script src="https://cdn.datatables.net/buttons/1.6.2/js/buttons.html5.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.6.2/js/buttons.print.min.js"></script>

<script type="text/javascript">

$('#reports').load('{{ url('Reportes/table') }}/?desde={{ $today->format('Y-m-d') }}&hasta={{ $today->format('Y-m-d') }}');
function report(){
    
    var desde = document.getElementById("desde").value;
    var hasta = document.getElementById("hasta").value;
    //alert(desde);
    $('#reports').load('{{ url('Reportes/table') }}/?desde=' + desde + '&hasta=' + hasta);
}
$('#annulled').load('{{ route('reports.nullable') }}');
$('#report_online').load('{{ route('reports.online') }}');

    
</script>
@endsection
