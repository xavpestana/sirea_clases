@php
        $suma_dolares = 0;
         $suma_pesos = 0;
         $suma_bsf = 0;
@endphp
<table id="table_reports" class="table_reports display nowrap table table-hover table-striped table-bordered" cellspacing="0" width="100%">
    
	 <thead>
        <tr>
            <th>Acciones</th>
            <th>Status</th>
        	<th>Nro de factura</th>
        	<th>Fecha de pago</th>
            <th>Nombre y Apellido del alumno</th>
            <th>Grado</th>
            <th>Plan actual</th>
            <th>Metodo de Pago</th>
            <th>Monto Registrado</th>
            <th>Cuota paga</th>
            <th>Cambio en dolares</th>
            <th>Cambio en pesos colombianos</th>
            <th>Monto en Bolivares</th>
            
        </tr>
    </thead>
    <tbody>
         @foreach ($reports as $report)
         @php
         $installments = $report->installmentPayment;
         $student = $report->Student->profile;
         $plan = $report->Student->studentPlan->where('season_id', $actual_season->id)->first();
         @endphp
        	<tr>
                <td>
                    <div class="btn-group">
                    @if($report->annulled == false)
                    <button type="button" id="send" class="btn waves-effect waves-light btn-rounded btn-warning"  title="Enviar factura" onclick="send('{{ $report->id }}')">
                        <i class="mdi mdi-file-send"></i>
                    </button>
                    <button type="button" id="send" class="btn waves-effect waves-light btn-rounded btn-danger"  title="Anular factura" onclick="annull('{{ $report->id }}')">
                        <i class="mdi mdi-close-box"></i>
                    </button>
                    <button type="button" class="btn waves-effect waves-light btn-rounded btn-info dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" title="Descargar factura">
                        <i class="mdi mdi-folder-download"></i>
                    </button>
                    <div class="dropdown-menu">
                        <a class="dropdown-item" href="{{ url('print/factura') }}/1/{{ $report->id }}" target="_blank">Pagina Completa</a>
                        <div class="dropdown-divider"></div>
                        <a class="dropdown-item" href="{{ url('print/factura') }}/2/{{ $report->id }}" target="_blank">Media Pagina</a>
                        <!--<div class="dropdown-divider"></div>
                        <a class="dropdown-item" href="{{ url('print/factura') }}/3/{{ $report->id }}" target="_blank">Cuarto de Página</a>-->
                    </div>
                    @endif
                </div>
                </td>
                <td>
                    @if($report->annulled == true)
                <span class="text-danger">Anulado</span>
                @else
                <span class="text-success">Activo</span>
                @endif
                </td>
        		<td>{{ $report->number_invoice }}</td>
        		<td>{{ $report->created_at->format('d-M-Y') }}</td>
         		<td>{{ $student->last_name_1 }} {{ $student->last_name_2 }} {{ $student->first_name_1 }} {{ $student->first_name_2 }}</td>
         		<td>{{ $report->Student->grade }}</td>
         		<td>{{ $plan->plan->name }}</td>
         		<td>{{ $report->paymentMethod->name }}</td>
         		<td>{{ $report->amount }} {{ $report->coin }}
                @php
                        if($report->coin == 'USD'){
                            $suma_dolares+= $report->amount;
                        }
                            if($report->coin == 'COP'){
                                $suma_pesos+= $report->amount;
                            }
                                if($report->coin == 'Bsf'){
                                    $suma_bsf+= $report->amount;
                                }
                        @endphp
                </td>
         		<td>
         			<ul>
         			@foreach ($installments as $installment)
         				<li>{{ $installment->Installment->description }} ({{ $installment->Installment->amount }} {{ $installment->Installment->coin }})</li>
                        
         			@endforeach
         			</ul>
         		</td>
         		<td>{{ $report->change_amount_USD }}</td>
         		<td>{{ $report->change_amount_COP }}</td>
         		<td>{{ $report->amount_bsf }}</td>
         	</tr>
         @endforeach  
    </tbody>
    <caption>
        <h5> Fecha actual  {{ $hoy }}</h5>
        <h5> Total de entradas en dolares: {{ $suma_dolares }} </h5><br>
        <h5> Total de entradas en pesos colombianos: {{ $suma_pesos }} </h5><br>
        <h5> Total de entradas en bolivares: {{ $suma_bsf }} </h5><br></caption>
    
</table>
			<script>
                $('#table_reports').DataTable( {
                	"language": {
                "url": "//cdn.datatables.net/plug-ins/1.10.11/i18n/Spanish.json"
            },
                    dom: 'Bfrtip',
                    "aaSorting": [[ 3, "asc" ]],
                     buttons: [
                'copyHtml5',
                'excelHtml5',
                'csvHtml5',
            ]
                });
      /**Mostrar las cuotas**/
 function send(id){
    
    $('.btn').attr('disabled', true);
$.ajax({
        type:'GET',
        url:"{{route('invoice.send')}}/?invoice="+id,
        })
        .done(function(response) {
            
            $('.btn').removeAttr('disabled');
            Swal.fire({
                position: 'center',
                icon: 'success',
                title: 'Factura Enviada Exitosamente',
                showConfirmButton: false,
                timer: 1500
            }) 
        })
        .fail(function(xhr, status, error) {
            $('.btn').removeAttr('disabled');
            Swal.fire({
                icon: 'error',
                title: 'Oops...',
                text: 'Algo a sucedido, intente denuevo!',
                })
        }) 
    }
    function annull(id){
        Swal.fire({
            title: 'Desea anular la factura?',
            text: "Estos cambios no se pueden revertir!",
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Si, anular!'
                }).then((result) => {
                    if (result.isConfirmed) {
                        $('.btn').attr('disabled', true);
                        $.ajax({
                            type:'DELETE',
                            data : {"id" : id, "_token": "{{ csrf_token() }}"},
                            url:"{{url('pagos/facturar/')}}/"+id,
                        })
                        .done(function(response) {
                            $('.btn').attr('disabled', false);
                            var x = document.getElementById("reports_select").value;
                            $('#reports').load('{{ url('Reportes') }}/'+ x);
                            Swal.fire(
                                'Factura anulada!',
                                'La factura ha sido anulada con exito.',
                                'success'
                            )
                        })
                    }
                })
    }
            </script>