<div class="modal-content">
<div class="modal-body">
    <h4>Estudiantes que han cancelado la cuota {{ $installments->description }}</h4>
        <h4>Costo de la cuota {{ number_format($installments->amount, 2, '.', ',') }} {{ $installments->coin }}</h4>
    <div class="table-responsive pt-4">
        <table id="student" class="table table-striped table-hover" cellspacing="0" width="100%">
            <thead>
                <tr>
                    <th>Nombre del estudiante</th>
                    <th>Grado</th>
                    <th>Fecha de pago</th>
                    <th>Numero de factura</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($installments->installmentPayment as $installment)
                    <tr>
                        <td>
                            @php
                                $nombre = $installment->Student->profile;
                            @endphp
                            {{ $nombre->last_name_1 }} {{ $nombre->last_name_2 }} {{ $nombre->first_name_1 }} {{ $nombre->first_name_2 }}
                        </td>
                        <td>
                            {{ $installment->Student->grade }}
                        </td>
                        <td class="text-center">
                            {{ $installment->created_at->format('d/m/Y') }}
                        </td>
                        <td class="text-center">
                            {{ $installment->Payment->number_invoice }}
                        </td>
                    </tr>
                @endforeach
            </tbody>
        </table>
    </div>
</div>
<div class="modal-footer">
    <button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Cerrar</button>
</div>
</div>
<script>
    $('#student').DataTable( {
                    "language": {
                "url": "//cdn.datatables.net/plug-ins/1.10.11/i18n/Spanish.json"
            },
                    dom: 'Bfrtip',
                    "aaSorting": [[ 0, "asc" ]],
                     buttons: [
                'copyHtml5',
                'excelHtml5',
                'csvHtml5',
            ]
                });
</script>