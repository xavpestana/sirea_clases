
<table id="nullable" class="table_reports display nowrap table table-hover table-striped table-bordered" cellspacing="0" width="100%">
    
	 <thead>
        <tr>
        	<th>Nro de factura</th>
            <th>Estado</th>
        	<th>Fecha de pago</th>
            <th>Nombre y Apellido del alumno</th>
            <th>Grado</th>
            <th>Plan actual</th>
            <th>Metodo de Pago</th>
            <th>Monto Registrado</th>
            <th>Cuota paga</th>
            <th>Cambio en dolares</th>
            <th>Cambio en pesos colombianos</th>
            <th>Monto en Bolivares</th>
            
        </tr>
    </thead>
    <tbody>
         @foreach ($reports as $report)
         @php
         $installments = $report->quotasCancelled;
         $student = $report->Student->profile;
         $plan = $report->Student->studentPlan->where('season_id', $actual_season->id)->first();
         @endphp
        	<tr>
        		<td><span class="text-danger">{{ $report->number_invoice }}</span></td>
                <td><span class="text-danger">Anulado</span></td>
        		<td>{{ $report->created_at->format('d-M-Y') }}</td>
         		<td>{{ $student->last_name_1 }} {{ $student->last_name_2 }} {{ $student->first_name_1 }} {{ $student->first_name_2 }}</td>
         		<td>{{ $report->Student->grade }}</td>
         		<td>{{ $plan->plan->name }}</td>
         		<td>{{ $report->paymentMethod->name }}</td>
         		<td>{{ $report->amount }} {{ $report->coin }}
               
                </td>
         		<td>
         			<ul>
         			@foreach ($installments as $installment)
         				<li>{{ $installment->Installment->description }} ({{ $installment->Installment->amount }} {{ $installment->Installment->coin }}) <span class="text-danger">(anulado)</span></li>
                        
         			@endforeach
         			</ul>
         		</td>
         		<td>{{ $report->change_amount_USD }}</td>
         		<td>{{ $report->change_amount_COP }}</td>
         		<td>{{ $report->amount_bsf }}</td>
         	</tr>
         @endforeach  
    </tbody>
   
    
</table>
			<script>
                $('#nullable').DataTable( {
                	"language": {
                "url": "//cdn.datatables.net/plug-ins/1.10.11/i18n/Spanish.json"
            },
                    dom: 'Bfrtip',
                    "aaSorting": [[ 3, "asc" ]],
                     buttons: [
                'copyHtml5',
                'excelHtml5',
                'csvHtml5',
            ]
                });
     
            </script>