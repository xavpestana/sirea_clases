@extends('layouts.app')

@section('title-view', 'Inicio')

@section('extra-css')
<link href="{{ asset('themeforest/main/css/jquery.toast.css', Request::secure()) }}" id="theme" rel="stylesheet">
@endsection

@section('content')
{{--<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Dashboard</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif
                    You are logged in!
                </div>
            </div>
        </div>
    </div>
</div>--}}
<!-- ============================================================== -->
<!-- Inicio Role School -->
<!-- ============================================================== -->
@role('school')


@isset($school)
@php($actual = $school->seasons->where('status', 1)->last())
@php($totalEstudiantes = $school->students->count())
<div class="row">
    <div class="col-lg-12 col-md-12">
    <h3>Información General del Registro</h3>
    </div>
    <div class="col-lg-3 col-md-3">
        <div class="card card-secondary">
            <div class="card-body">
                <div class="d-flex">
                    <div class="m-r-20 align-self-center">
                        <h1 ><i class="mdi mdi-face"></i></h1></div>
                    <div>
                        <h3 class="card-title">{{ $totalEstudiantes }}</h3>
                        <h6 class="card-subtitle">Estudiantes</h6>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-lg-3 col-md-3">
        <div class="card card-inverse card-success">
            <div class="card-body">
                <div class="d-flex">
                    <div class="m-r-20 align-self-center">
                        <h1 class="text-white"><i class="ti ti-user"></i></h1></div>
                    <div>
                        <h3 class="card-title">{{ $school->teachers->count() }}</h3>
                        <h6 class="card-subtitle">Docentes</h6>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-lg-3 col-md-3">
        <div class="card card-inverse card-info">
            <div class="card-body">
                <div class="d-flex">
                    <div class="m-r-20 align-self-center">
                        <h1 class="text-white"><i class="icon-people"></i></h1></div>
                    <div>
                        <h3 class="card-title">{{ $school->families->count() }}</h3>
                        <h6 class="card-subtitle">Familias</h6>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-lg-3 col-md-3">
        <div class="card card-inverse card-primary">
            <div class="card-body">
                <div class="d-flex">
                    <div class="m-r-20 align-self-center">
                        <h1 class="text-white"><i class="ti ti-user"></i></h1></div>
                    <div>
                        <h3 class="card-title">{{ $school->representatives->count() }}</h3>
                        <h6 class="card-subtitle">Representantes</h6>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endisset


<!-- Row -->
<div class="row">

    <div class="col-lg-8">
    @isset($actual)
    @php($inscritos = $actual->enrollments->count())
    @php($ultimos = $actual->enrollments->sortBy('desc')->take(10))
    @php($pendientes = $totalEstudiantes - $inscritos)
    
        <div class="row">
            <div class="col-lg-12">
                <h3>Año Escolar {{ $actual->season }}</h3>
            </div>
            <div class="col-lg-4">
                <div class="card">
                    <div class="card-body">
                        <h4 class="card-title">Matriculados:</h4>
                        <h5>{{ $inscritos }} de  {{ $totalEstudiantes }}</h5>
                        <div>
                            <canvas id="grafico-inscritos" height="150"></canvas>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-8">
                <div class="card">
                    <div class="card-body">
                        <div class="table-responsive">
                            <table class="table table-hover earning-box">
                                <thead>
                                    <tr>
                                        <h4>Ultimos Matriculados</h4>
                                        <th colspan="2">Nombre</th>
                                        <th>Grado</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach ($ultimos->sortByDesc('created_at') as $ultimo)
                                    <tr>
                                        <td style="width:50px;"><span class="round"><img src="{{ asset('storage/pictures/avatars/'.$ultimo->student->profile->avatar) }}" alt="user" width="50"></span></td>
                                        <td>
                                            <h6>{{ $ultimo->student->profile->first_name_1 }} {{ $ultimo->student->profile->last_name_1 }}</h6><small class="text-muted">fecha: {{ $ultimo->created_at->format('d/m/Y') }}</small></td>
                                        <td><span class="label label-success">{{ $ultimo->section->grade->name }} {{ $ultimo->section->name }}</span></td>
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    @else
        <h3 class="card-title">No existe un año escolar activo</h3>
        <p class="card-text">Necesita agregar un año escolar activo para realizar procesos de inscripción, pagos y crear periodos para evaluaciones, puede crear un nuevo año escolar haciendo clic en el siguiente botón</p>
        <a href="{{ route('season.create') }}" class="btn btn-inverse">Crear Año Escolar</a>
    @endisset
    </div>

    <div class="col-lg-4">
        <div class="card">
            <div class="card-body">
                <div class="d-flex no-block">
                    <h4 class="card-title">Tutoriales<br/><small class="text-muted">Indicacion de las funciones basicas de SIREA</small></h4>
                </div>

                <div id="accordionexample" class="accordion" role="tablist" aria-multiselectable="true">
                    <div class="card">
                        <div class="card-header" role="tab" id="headingOne">
                            <h5 class="mb-0">
                                <a data-toggle="collapse" data-parent="#accordionexample" href="#c1" aria-expanded="true" aria-controls="c1">
                                    Creación de Usuarios Familia
                                </a>
                            </h5>
                        </div>

                        <div id="c1" class="collapse show" role="tabpanel" aria-labelledby="headingOne">
                            <div class="card-body">
                                <div class="col-md-12">
                                    <iframe width="100%" src="https://www.youtube.com/embed/N1ujqCwWstY" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@endrole
<!-- ============================================================== -->
<!-- Inicio Role Family -->
<!-- ============================================================== -->
@role('family')
@if(auth()->user()->disabled == 0)
<!-- Row -->
<div class="row">
    <div class="col-lg-6">
        <div class="card">
            <div class="card-body">
                <div class="d-flex no-block">
                    <h4 class="card-title">Guia del sistema<br/><small class="text-muted">Tuturial para cargar información importante, agregar Representantes y Estudiantes</small></h4>
                </div>

                <div id="accordionexample" class="accordion" role="tablist" aria-multiselectable="true">
                    <div class="card">
                        <div class="card-header" role="tab" id="headingOne">
                            <h5 class="mb-0">
                                <a data-toggle="collapse" data-parent="#accordionexample" href="#f1" aria-expanded="true" aria-controls="f1">
                                    1. Llenar datos de la Familia
                                </a>
                            </h5>
                        </div>

                        <div id="f1" class="collapse show" role="tabpanel" aria-labelledby="headingOne">
                            <div class="card-body">
                                <div class="col-md-12">
                                    <iframe width="100%" src="https://www.youtube.com/embed/rA9AXdRotE8" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div id="accordionexample" class="accordion" role="tablist" aria-multiselectable="true">
                    <div class="card">
                        <div class="card-header" role="tab" id="headingOne">
                            <h5 class="mb-0">
                                <a data-toggle="collapse" data-parent="#accordionexample" href="#f2" aria-expanded="true" aria-controls="f2">
                                    2. Agregando Representantes
                                </a>
                            </h5>
                        </div>

                        <div id="f2" class="collapse show" role="tabpanel" aria-labelledby="headingOne">
                            <div class="card-body">
                                <div class="col-md-12">
                                    <iframe width="100%" src="https://www.youtube.com/embed/bUopm6n-2uk" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div id="accordionexample" class="accordion" role="tablist" aria-multiselectable="true">
                    <div class="card">
                        <div class="card-header" role="tab" id="headingOne">
                            <h5 class="mb-0">
                                <a data-toggle="collapse" data-parent="#accordionexample" href="#f3" aria-expanded="true" aria-controls="f3">
                                    3. Agregando Estudiantes
                                </a>
                            </h5>
                        </div>

                        <div id="f3" class="collapse show" role="tabpanel" aria-labelledby="headingOne">
                            <div class="card-body">
                                <div class="col-md-12">
                                    <iframe width="100%" src="https://www.youtube.com/embed/nEOTHg0i3A0" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>                

            </div>
        </div>
    </div>
</div>
@else
<div class="row">
    <div class="col-lg-6">
        <div class="card">
            <div class="card-body">
                <div class="d-flex no-block">
                    <h4 class="card-title">Usuario Bloqueado, consulte con la administración del colegio para solventar el problema</h4>
                </div>
            </div>
        </div>
    </div>
</div>
@endif
@endrole
<!-- ============================================================== -->
<!-- Inicio Role Student -->
<!-- ============================================================== -->
@role('student')

<div class="row">
    <div class="col-lg-8">
                        <div class="card">
                            <div style="height: 500px; overflow-y: scroll;" class="card-body">
                                <h4 class="card-title">Actividades Publicadas</h4>
                                <ul id="feed" class="feeds">
                                    
                                </ul>
                            </div>
                        </div>
                    </div>
</div>
@endrole
<!-- ============================================================== -->
<!-- Inicio Role Teacher -->
<!-- ============================================================== -->
@role('teacher')

<div class="row">
                    <div class="col-lg-6">
                        
                        <div class="card">
                            <div class="card-body">
                                <div class="d-flex no-block">
                                    <h4 class="card-title">Programas</h4>
                                </div>
                            </div>
                            <div class="card-body">
                                <div class="table-responsive">
                                    <table class="table table-hover earning-box">
                                        <thead>
                                            <tr>
                                                <th colspan="2">Nombre</th>
                                                <th></th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td>Areas de formacion en educacion media general</td>
                                                <td>
                                                </td>
                                                <td>
                                                <a href="{{ asset('documents/teachers/area_formacion.docx', Request::secure()) }}" type="button" class="btn waves-effect waves-light btn-rounded btn-success" download="Areas de formacion en educacion media general">Descargar</a>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>Primer Grado</td>
                                                <td>
                                                </td>
                                                <td>
                                                <a href="{{ asset('documents/teachers/primer_grado.docx', Request::secure()) }}" type="button" class="btn waves-effect waves-light btn-rounded btn-success" download="Primer Grado">Descargar</a>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>Segundo Grado</td>
                                                <td>
                                                </td>
                                                <td>
                                                <a href="{{ asset('documents/teachers/segundo_grado.docx', Request::secure()) }}" type="button" class="btn waves-effect waves-light btn-rounded btn-success" download="Segundo Grado">Descargar</a>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>Tercer Grado</td>
                                                <td>
                                                </td>
                                                <td>
                                                <a href="{{ asset('documents/teachers/tercer_grado.docx', Request::secure()) }}" type="button" class="btn waves-effect waves-light btn-rounded btn-success" download="Tercer Grado">Descargar</a>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>Cuarto Grado</td>
                                                <td>
                                                </td>
                                                <td>
                                                <a href="{{ asset('documents/teachers/cuarto_grado.docx', Request::secure()) }}" type="button" class="btn waves-effect waves-light btn-rounded btn-success" download="Cuarto Grado">Descargar</a>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>Quinto Grado</td>
                                                <td>
                                                </td>
                                                <td>
                                                <a href="{{ asset('documents/teachers/quinto_grado.docx', Request::secure()) }}" type="button" class="btn waves-effect waves-light btn-rounded btn-success" download="Quinto Grado">Descargar</a>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>Sexto Grado</td>
                                                <td>
                                                </td>
                                                <td>
                                                <a href="{{ asset('documents/teachers/sexto_grado.docx', Request::secure()) }}" type="button" class="btn waves-effect waves-light btn-rounded btn-success" download="Sexto Grado">Descargar</a>
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                    </div>
                </div>

@endrole





@endsection


@section('extra-js')
<script src="{{ asset('themeforest/main/js/jquery.charts-sparkline.js') }}"></script>
<script src="{{ asset('themeforest/main/js/jquery.sparkline.min.js') }}"></script>
<script src="{{ asset('themeforest/main/js/jquery.toast.js') }}"></script>


<!-- Chart JS -->
<script src="{{ asset('themeforest/main/js/Chart.min.js') }}"></script>
<script>
    $(function(){
        @if (\Session::has('error'))
        $.toast({
            heading: 'Error',
            text: '{{ Session::get('error') }}',
            position: 'top-right',
            loaderBg:'#ff6849',
            icon: 'error',
            hideAfter: 3500
        });
        @endif

        @if (\Session::has('success'))
        $.toast({
            heading: '!Operación exitosa!',
            text: '{{ Session::get('success') }}',
            position: 'top-right',
            loaderBg:'#ff6849',
            icon: 'success',
            hideAfter: 3500, 
            stack: 6
        });
        @endif  

        @role('school')
        var data3 = [
            {
                value: @isset($inscritos){{ $inscritos }}@else 0 @endisset,
                color:"#009efb",
                highlight: "#009efb",
                label: "Matriculados"
            },
            {
                value: @isset($pendientes){{ $pendientes }}@else 0 @endisset,
                color:"#edf1f5",
                highlight: "#edf1f5",
                label: "Pendientes"
            },
        ];
        var ctx3 = document.getElementById("grafico-inscritos").getContext("2d");
        
        var myPieChart = new Chart(ctx3).Pie(data3,{
            segmentShowStroke : true,
            segmentStrokeColor : "#fff",
            segmentStrokeWidth : 0,
            animationSteps : 100,
            tooltipCornerRadius: 0,
            animationEasing : "easeOutBounce",
            animateRotate : true,
            animateScale : false,
            legendTemplate : "<ul class=\"<%=name.toLowerCase()%>-legend\"><% for (var i=0; i<segments.length; i++){%><li><span style=\"background-color:<%=segments[i].fillColor%>\"></span><%if(segments[i].label){%><%=segments[i].label%><%}%></li><%}%></ul>",
            responsive: true
        });
        @endrole
        @role('teacher')
        $.ajax({
         type : "GET",
         data : {"id" : {{ auth()->user()->id }} },
         url : "{{ route('chat.count') }}",
         success : function(data){
            $('.count').html(data);
         }
      });
        @endrole
        @role('student')
         $.ajax({
         type : "GET",
         data : {"id" : {{ auth()->user()->id }} },
         url : "{{ route('chat.count') }}",
         success : function(data){
            $('.count').html(data);
         }
      });

        $.ajax({
         type : "GET",
         data : {"id" : {{ auth()->user()->profile->student->id }} },
         url : "{{ route('studentActivity.activities') }}",
         success : function(data){
            $('#activities').html(data);
         }
      });


        $.ajax({
         type : "GET",
         data : {"id" : {{ auth()->user()->profile->student->id }} },
         url : "{{ route('studentActivity.published') }}",
         success : function(data){
            $('#feed').html(data);
         }
      });
        @endrole
    })
</script>
@endsection
