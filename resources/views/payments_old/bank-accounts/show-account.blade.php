<table class="table table-hover earning-box">
	<thead>
		<tr>
			<th>Codigo</th>
			<th>Cuenta Bancaria</th>
			<th></th>
		</tr>
	</thead>
	<tbody>
		
			@foreach($bankAccounts as $bankAccount)
			<tr>
			<td>{{ $bankAccount->bank->code }}</td>
			<td>{{ $bankAccount->bank->name }}</td>
			<td>
				<div class="btn-group" role="group" aria-label="First group">
					<button type="button" data-toggle="modal" data-target=".bs-example-modal-lg" class="btn waves-effect waves-light btn-rounded btn-sm btn-success" title="Ver o editar"><i class="mdi mdi-eye" onclick="edit('{{ $bankAccount->id }}')"></i></button>
					<button type="button" class="btn waves-effect waves-light btn-rounded btn-sm btn-danger" title="Eliminar cuenta"><i class="mdi mdi-delete" onclick="eliminar('{{ $bankAccount->id }}')"></i></button>
				</div>
			</td>
			</tr>
			@endforeach
		
	</tbody>
</table>