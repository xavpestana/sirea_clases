    <form id="form_updt_installments">
    @csrf
    <div class="row">
        <div class="col-sm-3 col-xs-3">
            <div class="form-group">
            	<label id="name">Nombre de la cuota <span style="color:red">*</span></label>
            	
                <input type="text" class="form-control" id="name" name="name" placeholder="Nombre de la cuota" value="{{ $installment->description }}" required>
                <input type="hidden" name="plan_id" value="{{ $installment->plan_id }}">
            </div>
        </div>
        <div class="col-sm-3 col-xs-3">
            <div class="form-group">
            	<label id="date">Fecha de expiracion</label>
                <input type="date" class="form-control" id="date" name="date" placeholder="Fecha de expiracion" @if(!is_null($installment->expire_at)) value="{{ $installment->expire_at->format('Y-m-d') }}" @endif>
            </div>
        </div>
        <div class="col-sm-3 col-xs-3">
            <div class="form-group">
            	<label id="date">Monto de la cuota <span style="color:red">*</span></label>
                <input type="number" class="form-control decimal-inputmask" id="amount" name="amount" placeholder="Monto de la cuota" step="0.01" title="El monto decimal debe ser un punto" pattern="[0-9]+([\.,][0-9]+)?" value="{{ $installment->amount }}" required>
            </div>
        </div>
        <div class="col-sm-3 col-xs-3">
            <div class="form-group">
            	<label for="monto">Monto expresado en: <span style="color:red">*</span></label>
            	<select id="monto" name="coin" class="form-control">
            		<option value="Bsf" @if($installment->coin == 'Bsf') selected @endif>Bolivares</option>
            		<option value="USD" @if($installment->coin == 'USD') selected @endif>Dolares</option>
            		<option value="COP" @if($installment->coin == 'COP') selected @endif>Pesos Colombianos</option>
            	</select>
            </div>
        </div>
        <div align="justify" class="col-sm-7 col-xs-7">
        	
        </div>
        <div align="right" class="col-sm-4 col-xs-4 ml-auto">
            <button type="submit" class="btn btn-outline-success save"><i class="fa fa-check"></i> Actualizar Cuota</button>
        </div>
    </div>
    </form>
<script type="text/javascript">
        /********  Nueva cuenta de colegio  *********/
$( "#form_updt_installments" ).submit(function(e) {
e.preventDefault();

  $(".save").prop('disabled', true);
  datos = $("#form_updt_installments").serialize();
  
$.ajax({
        type:'PUT',
        data:datos,
        url:"{{ route('installments.update', ['installment' => $installment->id]) }}",
        })
        .done(function(response) {
            $(".save").prop('disabled', false);
            $('#updt').html(" ");
            $('#installments').load('{{ route('installments.index', ['plan_id' => $installment->plan_id]) }}');
            Swal.fire({
                position: 'center-center',
                icon: 'success',
                title: 'Cuota agregada exitosamente',
                showConfirmButton: false,
                timer: 1500
            }) 
        })
        .fail(function(response) {
            $(".save").prop('disabled', false);
            Swal.fire({
                icon: 'error',
                title: 'Oops...',
                text: 'Hubo un problema, intente denuevo!',
                })
        }) 
}); 
</script>