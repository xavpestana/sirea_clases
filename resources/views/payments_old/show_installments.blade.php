@if(!$installments->isEmpty())
<div class="row mb-4">
	<div class="col-md-4">
		Saldo Anterior: <span>
			@if(!is_null($credit))
			{{ $credit->amount }} {{ $credit->coin }}
			@else
			0
			@endif
		</span>
	</div>
	<div class="col-md-4">
		Monto en {{ $coin }}: <span id="amount_remaining_coin"> {{ $amount }} </span>{{ $coin }}
		<input type="hidden" id="credit" name="credit" value="{{ $amount }}">
	</div>
	<div class="col-md-4">
		Total en Bsf: <span id="amount_remaining">{{ $bsf }}</span>
		 <input type="hidden" id="deposit" name="deposit" value="{{ $bsf }}">
		 <input type="hidden" id="deposit" name="amountbsf" value="{{ $bsf }}">
	</div>
</div>
 

 <h4 class="card-title">Cuotas Pendientes</h4>
<h6 class="card-subtitle">Seleccione las cuotas correspondientes del pago</h6>
<div class="table table-responsive">
	<table class="table table-hover earning-box">
	<thead>
		<tr>
			<th>Cuota</th>
			<th>Vencimiento</th>
			<th>Pago</th>
			<th></th>
		</tr>
	</thead>
	<tbody>
		
			@foreach($installments as $installment)
			@php
				$ip = App\installmentPayment::where('installment_id', $installment->id)->where('student_id', $student_id)->first();
				//dd($ip);
			@endphp
			@if(is_null($ip))
			<tr>
			<td>
				<div class="checkbox checkbox-info">
                	<input type="checkbox" id="pay_{{ $installment->id }}" name="installments[]" onchange="count_installments('{{ $installment->id }}','{{ $installment->coin }}','{{ $installment->amount }}','{{ $coin_amount }}')" value="{{ $installment->id }}">
                	<label for="pay_{{ $installment->id }}" class=""> <span>
                		<small>	{{ $installment->description }} </small></span> </label>
                	@if($installment->coin == 'USD' && is_null(session('USD_CLASES'))||$installment->coin == 'COP' && is_null(session('COP_CLASES')))
                		<span class="label label-danger">Debe configurar el precio de {{ $installment->coin }} para registrar el pago</span>
                	@endif
            	</div>
            	@php
            		$season_id = $installment->plan->season_id;
            	@endphp
            	
        	</td>
			<td>
				<div class="item-date">
					<small>	
            		@if(!is_null($installment->expire_at))
            			{{ $installment->expire_at->format('d-M-Y') }}
            		@else
            			No especifico
            		@endif
            		</small>
            	</div>
        	</td>
			<td>
				<span><small>
					{{ $installment->amount }} {{ $installment->coin }} /
					@if($installment->coin == 'USD')
					{{ $installment->amount*session('USD_CLASES') }} 
					@elseif($installment->coin == 'COP')
					{{ $installment->amount*session('COP_CLASES') }}
					@elseif($installment->coin == 'Bsf')
					{{ $installment->amount }}
					@endif
					Bsf
				</small></span> 
			</td>
			<td>
			</td>
			</tr>
			@endif
			@endforeach
		
	</tbody>
</table>
<input type="hidden" name="season_id" value="{{ $season_id }}">
    </div>
@else
<h4 class="card-title">No hay cuotas disponibles para este plan</h4>

@endif
<script>
	$(".save").prop('disabled', false);

 function count_installments(id, coin, amount,coin_amount){

 	var element = document.getElementById("pay_"+id);
 	var paid = $("#deposit").val();
 	if (coin == 'Bsf') { var resultado = parseFloat(amount * 1); }

    @if(!is_null(session('USD_CLASES')))
        if (coin == 'USD') { var resultado = parseFloat(amount)*parseFloat({{ session('USD_CLASES') }});
        }
    @else
    	if (coin == 'USD') { alert('Error debe configurar el valor del dolar para realizar esta operacion'); element.checked = 0; }
    @endif

    @if(!is_null(session('COP_CLASES')))
        if (coin == 'COP') { var resultado = (parseFloat(amount)*parseFloat({{ session('COP_CLASES') }}));
        }
    @else
    	if (coin == 'COP') { alert('Error debe configurar el valor del peso colombiano para realizar esta operacion'); element.checked = 0;	}
    @endif	

 	if(element.checked){ if (parseFloat(paid) >= parseFloat(resultado)) {
            	resultado = parseFloat(paid) - parseFloat(resultado);
            	$('#amount_remaining').html(resultado);
            	$("#deposit").val(resultado);

            	/*calculo de la moneda de pago*/
            	if (coin_amount == 'Bsf') { paid_coin = 1; }
            	if (coin_amount == 'USD') { paid_coin = parseFloat({{ session('USD_CLASES') }}); }
            	if (coin_amount == 'COP') { paid_coin = parseFloat({{ session('COP_CLASES') }}); }

            	var paid_coin = parseFloat(resultado)/parseFloat(paid_coin);
            	$('#amount_remaining_coin').html(paid_coin);
            	$("#credit").val(paid_coin);
            } else {
            	alert('El monto es inferior al de la cuota, si desea dejarlo como abono, no realice ninguna accion, solo registre el pago');
            	element.checked = 0;
            }
 	}else{
			resultado = parseFloat(paid) + parseFloat(resultado);
            $('#amount_remaining').html(resultado);
            $("#deposit").val(resultado);

            /*calculo de la moneda de pago*/
            if (coin_amount == 'Bsf') { paid_coin = 1; }
            if (coin_amount == 'USD') { paid_coin = parseFloat({{ session('USD_CLASES') }}); }
            if (coin_amount == 'COP') { paid_coin = parseFloat({{ session('COP_CLASES') }}); }

            var paid_coin = parseFloat(resultado)/parseFloat(paid_coin);
            $('#amount_remaining_coin').html(paid_coin);
            $("#credit").val(paid_coin);
            
 	}
  }

</script>