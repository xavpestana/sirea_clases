@extends('layouts.app')

@section('title-view', 'Registro de morosos - Pagos')

@section('extra-css')
<link href="https://cdn.datatables.net/1.10.21/css/jquery.dataTables.min.css" rel="stylesheet">
@endsection

@section('content')
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-header">
                    <form id="form_coin">
                        @csrf()
                    <div class="row justify-content-center">
                        <div class="col-md-3">
                            <label for="USD">1 USD es igual a: (Bsf)</label>
                            <input id="USD" type="number" class="form-control" name="usd" value="{{ $USD ?? '' }}" step="any">
                        </div>
                        <div class="col-md-3">
                            <label for="USD">1 COP es igual a: (Bsf)</label>
                            <input id="COP" type="number" class="form-control" name="cop" value="{{ $COP ?? '' }}" step="any">
                        </div>
                        <div align="center" class="col-md-3">
                            <button type="submit" class="btn waves-effect waves-light btn-rounded btn-success mt-4">Actualizar</button>
                        </div>
                        <div align="justify" class="col-md-7">
                            <small>Estos valores son volatiles y se guardan en las cookies del ordenador por seguridad, por lo que se debe configurar en cada ordenador y cuando inicie sesion</small>
                        </div>
                    </div>
                    </form>
                </div>
                <div class="card-body">
                    <div class="row">
                        <div align="justify" class="col-md-6">
                            Los alumnos que no tienen un plan registrado no apareceran en el registro de morosos
                        </div>
                        <div align="center" class="col-md-4 ml-auto">
                            <button id="send_mail" type="button" class="btn btn-warning" onclick="send()"><i class="mdi mdi-send"></i> Enviar notificación de cobro</button>
                        </div>
                    </div>
                	<div id="students" class="table-responsive">
                	</div>
                    <div class="row mt-4">
                        
                    </div>
                </div>
            </div>
        </div>

        <div class="modal fade bs-example-modal-lg " tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none;">
            <div class="modal-dialog  modal-xl">
                <div id="modal_all" class="modal-content">
                </div>
                <!-- /.modal-content -->
            </div>
            <!-- /.modal-dialog -->
        </div>
    </div>
@endsection
@section('extra-js')
<script src="https://cdn.jsdelivr.net/npm/sweetalert2@9"></script>
<script src="https://cdn.datatables.net/1.10.21/js/jquery.dataTables.min.js"></script>

<script type="text/javascript">
	(function() {
   'use strict';
        window.addEventListener('load', function() {
        // Fetch all the forms we want to apply custom Bootstrap validation styles to
        var forms = document.getElementsByClassName('needs-validation');
        // Loop over them and prevent submission
        var validation = Array.prototype.filter.call(forms, function(form) {
            form.addEventListener('submit', function(event) {
                if (form.checkValidity() === false) {
                    event.preventDefault();
                    event.stopPropagation();
                }
            form.classList.add('was-validated');
        }, false);
        });
        }, false);
})();

$('#students').load('{{ route('defaulters.list') }}');
$( "#form_coin" ).submit(function(e) {
e.preventDefault();

  $(".save").prop('disabled', true);
  datos = $("#form_coin").serialize();
$.ajax({
        type:'POST',
        data:datos,
        url:"{{ route('coin.save') }}",
        })
        .done(function(response) {
            $(".save").prop('disabled', false);
            Swal.fire({
                position: 'center-center',
                icon: 'success',
                title: 'Actualizado exitosamente',
                showConfirmButton: false,
                timer: 1500
            }) 
            location.reload();
        })
        .fail(function(response) {

            $(".save").prop('disabled', false);
            Swal.fire({
                icon: 'error',
                title: 'Oops...',
                text: 'Algo ha sucedido, intente denuevo!',
                })
        }) 
});

/*Nueva cuenta*/
    function pago(id,ins){
        $('#modal_all').html(" ");
        $('#modal_all').load("{{route('payments.create')}}/?id_student="+id);
    }

    function send(){

$("#send_mail").prop('disabled', true);
  datos = $("#form_student_email").serialize();

  $.ajax({
        type:'POST',
        data:datos,
        url:"{{ route('defaulters.send') }}",
        })
        .done(function(response) {
            $("#send_mail").prop('disabled', false);
            Swal.fire({
                position: 'center',
                icon: 'success',
                title: 'Actualizado exitosamente',
                showConfirmButton: false,
                timer: 1500
            }) 
        })
        .fail(function(response) {

            $("#send_mail").prop('disabled', false);
            Swal.fire({
                icon: 'error',
                title: 'Oops...',
                text: 'Algo ha sucedido, intente denuevo!',
                })
        }) 
    }
</script>
@endsection
