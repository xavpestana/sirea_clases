@extends('layouts.app')

@section('content')
    <div class="row">
        <div class="col-12">
            <payment-method :payment_method_id="{!! (isset($paymentMethod)) ? $paymentMethod->id : "null" !!}"
                route_list='{{ url('pagos/metodo-pagos') }}'>
            </payment-method>
        </div>
    </div>
@endsection
