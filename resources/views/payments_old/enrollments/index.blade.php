@extends('layouts.app')

@section('content')
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-header border-0">
                    Lista de Inscripciones<a class="btn btn-primary btn-xs float-right" href="{{ route('enrollments.create') }}"><i class="fa fa-plus-circle"></i></a>
                </div>
                <div class="card-body">
                    <enrollment-list route_list="{{ url('pagos/inscripciones/show/vue-list') }}"
						route_delete="{{ url('pagos/inscripciones') }}"
						route_edit="{{ url('pagos/inscripciones/{id}/editar') }}"
						route_show="{{ url('pagos/inscripciones/{id}') }}">
					</enrollment-list>
                </div>
            </div>
        </div>
    </div>
@endsection
