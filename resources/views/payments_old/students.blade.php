<table id="table_student" class="display nowrap table table-hover table-striped table-bordered" cellspacing="0" width="100%">
	 <thead>
                            <tr>
                                <th>Familia</th>
                                <th>Documento</th>
                                <th>Nombre y Apellido del alumno</th>
                                <th>Grado</th>
                                <th>Seccion</th>
                                <th class="no-sort">Acciones</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($students as $student)
                            <tr>
                                <td>{{ $student->family->last_name_1 }} {{ $student->family->last_name_2 }}</td>
                                <td>{{ $student->profile->document_type }}-{{ $student->profile->document_number }}</td>
                                <td>{{ $student->profile->first_name_1 }} {{ $student->profile->first_name_2 }} {{ $student->profile->last_name_1 }} {{ $student->profile->last_name_2 }}</td>
                                <td>{{ $student->grade }}</td>
                                <td>
                                	@php
                                	$student_enrollments = DB::table('enrollments')
            							->join('sections', 'sections.id', '=', 'enrollments.section_id')
            							->join('seasons', 'seasons.id', '=', 'sections.season_id')
            							->select('sections.name')
            							->where('enrollments.student_id', $student->id)
            							->where('seasons.school_id', auth()->user()->userSchool->school_id)
            							->where('status','1')
            							->first();
                                	@endphp
                                	@if(!is_null($student_enrollments))
                                	{{ $student_enrollments->name }}
                                	@else
                                	<span>No inscrito</span>
                                	@endif
                                </td>
                                <td>
                                    <div class="btn-group" role="group" aria-label="Basic example">
                                        <button type="button" class="btn btn-success" data-toggle="modal" data-target=".bs-example-modal-lg" onclick="pago('{{ $student->id }}')"><i class="ti-marker-alt"></i> Pago</button>
                                        <button type="button" class="btn btn-warning" data-toggle="modal" data-target=".bs-example-modal-lg" onclick="registro('{{ $student->id }}')"><i class="ti-eye"></i> Registros</button>
                                    </div>
                                </td>
                            </tr>
                            @endforeach
	</tbody>
</table>
			<script>
                $('#table_student').DataTable( {
                    dom: 'Bfrtip',
                });
     
            </script>