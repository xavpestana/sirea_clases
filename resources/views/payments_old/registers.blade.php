<div class="modal-header">
    <h5 class="modal-title" id="myLargeModalLabel">Registros de pago del alumno: {{ $student->profile->first_name_1 }} {{ $student->profile->first_name_2 }} {{ $student->profile->last_name_1 }} {{ $student->profile->last_name_2 }}</h5>
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
</div>
<div class="modal-body">
    <div class="table-responsive">
        
    
<table id="table_register" class="display nowrap table table-hover table-striped table-bordered" cellspacing="0">
	 <thead>
        <tr>
            <th>Nro de Factura</th>
            <th>Pagos de:</th>
            <th>Metodo de Pago</th>
            <th>Fecha</th>
            <th>Monto de pago</th>
            <th class="no-sort">Acciones
            <span class="preloader_mail">
            <svg class="circular" viewBox="25 25 50 50">
                <circle class="path" cx="50" cy="50" r="20" fill="none" stroke-width="2" stroke-miterlimit="10"></circle> </svg>
        </span></th>
        </tr>
    </thead>
    <tbody>
    @foreach ($payments as $payment)
        <tr>
            <td>{{ $payment->number_invoice }}</td>
            <td>
                @foreach($payment->installmentPayment as $installment)
                <ul>
                    <li>{{ $installment->Installment->description }} ({{ $installment->Installment->amount }} {{ $installment->Installment->coin }})</li>
                </ul>
                @endforeach
            </td>
            <td>{{ $payment->paymentMethod->name }}</td>
            <td>{{ $payment->created_at->format('d-M-Y') }}</td>
            <td>{{ $payment->amount_bsf }} Bsf</td>
            <td>

                <div class="btn-group">
                    <button type="button" id="send" class="btn waves-effect waves-light btn-rounded btn-warning"  title="Enviar factura" onclick="send('{{ $payment->id }}')">
                        <i class="mdi mdi-file-send"></i>
                    </button>
                    <button type="button" class="btn waves-effect waves-light btn-rounded btn-info dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" title="Descargar factura">
                        <i class="mdi mdi-folder-download"></i>
                    </button>
                    <div class="dropdown-menu">
                        <a class="dropdown-item" href="{{ url('print/factura') }}/1/{{ $payment->id }}" target="_blank">Pagina Completa</a>
                        <div class="dropdown-divider"></div>
                        <a class="dropdown-item" href="{{ url('print/factura') }}/2/{{ $payment->id }}" target="_blank">Media Pagina</a>
                        <div class="dropdown-divider"></div>
                        <a class="dropdown-item" href="{{ url('print/factura') }}/3/{{ $payment->id }}" target="_blank">Cuarto de Página</a>
                        
                    </div>
                </div>
                
            </td>
        </tr>      
    @endforeach                  
	</tbody>
</table>
</div>
</div>
			<script>
                $('.preloader_mail').hide();
                $('#table_register').DataTable( {
                    dom: 'Bfrtip',
                    "aaSorting": [[ 0, "desc" ]],
                });
     /**Mostrar las cuotas**/
 function send(id){
    $('.preloader_mail').show();
    $('#send').attr('disabled', true);
$.ajax({
        type:'GET',
        url:"{{route('invoice.send')}}/?invoice="+id,
        })
        .done(function(response) {
            $('.preloader_mail').hide();
            $('#send').removeAttr('disabled');
            Swal.fire({
                position: 'center',
                icon: 'success',
                title: 'Factura Enviada Exitosamente',
                showConfirmButton: false,
                timer: 1500
            }) 
        })
        .fail(function(xhr, status, error) {
            $('#send').removeAttr('disabled');
            $('.preloader_mail').hide();
            Swal.fire({
                icon: 'error',
                title: 'Oops...',
                text: 'Algo a sucedido, intente denuevo!',
                })
        }) 
    }
            </script>