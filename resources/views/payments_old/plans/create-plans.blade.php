<form id="form_plan">
    @csrf
<div class="modal-header">
    <h4 class="modal-title" id="myLargeModalLabel">Agregar Plan Nuevo</h4>
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
</div>
<div class="modal-body">
    
    <div class="row">
        <div class="col-sm-6 col-xs-6">
            <div class="form-group">
                <input type="text" class="form-control" id="name" name="name" placeholder="Nombre del Plan" required>
            </div>
        </div>
        <div class="col-sm-4 col-xs-4">
            <button type="submit" class="btn btn-outline-success save"><i class="fa fa-check"></i> Guardar Plan</button>
        </div>
    </div>
</div>                                            
</form>

<script type="text/javascript">
        /********  Nueva cuenta de colegio  *********/

$( "#form_plan" ).submit(function(e) {
e.preventDefault();

  $(".save").prop('disabled', true);
  datos = $("#form_plan").serialize();
$.ajax({
        type:'POST',
        data:datos,
        url:"{{ route('plans.store') }}",
        })
        .done(function(response) {
            $(".save").prop('disabled', false);
            $('#plan').load('{{ route('plans.index') }}');
            $('#modal_all').load('{{ url('pagos/planes/') }}/'+response);
            Swal.fire({
                position: 'center-center',
                icon: 'success',
                title: 'Plan agregado exitosamente',
                showConfirmButton: false,
                timer: 1500
            }) 
        })
        .fail(function(response) {
            $(".save").prop('disabled', false);
            Swal.fire({
                icon: 'error',
                title: 'Oops...',
                text: 'Hubo un problema, intente denuevo!',
                })
        }) 
});
</script>