@if(!$students->isEmpty())
@if(auth()->user()->hasRole('teacher'))
<label for="student">Seleccione un Alumno</label>
@else
<label for="student">Seleccione Docente</label>
@endif

<select id="student" name="sec" class="form-control custom-select" onchange="location = this.value">
	<option>--   Seleccione   --</option>
    @foreach ($students as $student)
        <option value="{{ route('chat.index').'?type='.$student->user_id }}">{{ $student->last_name_1 }} {{ $student->last_name_2 }} {{ $student->first_name_1 }} {{ $student->first_name_2 }}</option>
    @endforeach
</select>
@else
<h3>No hay alumnos asignados en la sección</h3>
@endif