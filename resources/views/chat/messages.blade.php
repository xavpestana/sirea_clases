                            @if(!is_null($users))
                            @php
                                $fecha = 0;
                                $bandera=0;
                            @endphp
                                <ul class="chat-list p-20">
                                        @foreach($users as $user)
                                            @php
                                            //dd($user);
                                        if ($user->new == '1' && $user->user_id != auth()->user()->id) {
                                          DB::table('messages')
                                                ->where('user_id', $user->user_id)
                                                ->update(['new' => 0]);
                                        }
                                            @endphp
                                            @if(auth()->user()->id == $user->user_id)
                                            <!--chat Row -->
                                            <li class="reverse">
                                                <div class="chat-content">
                                                    <h5>{{ $user->last_name_1 }} {{ $user->first_name_1 }}</h5>
                                                    <div class="box bg-light-inverse">
                                                    @php
                                                    $msg=nl2br($user->message);
                                                    $msg=str_replace("<br />","<br>",$msg);
                                                    $cadena_resultante= preg_replace('/((http|https|www)[^\s]+)/', '<a target=\"_blank\" href="$1">$0</a>', $msg);
                                                    echo $cadena_resultante;
                                                    @endphp</div>
                                                </div>
                                                

                                                <div class="chat-img"><img src="{{ asset('storage/pictures/avatars/'.$user->avatar, Request::secure()) }}" alt="user" /></div>
                                                <div class="chat-time"> <a class="text-danger" href="javascript:void(0);"  onclick="delete_msg('{{ $user->id }}')">Borrar</a></div>
                                            </li>
                                            @else
                                            <li>
                                                <div class="chat-img"><img src="{{ asset('storage/pictures/avatars/'.$user->avatar, Request::secure()) }}" alt="user" /></div>
                                                <div class="chat-content">
                                                    <h5>{{ $user->last_name_1 }} {{ $user->first_name_1 }}</h5>
                                                    <div class="box bg-light-info">
                                                                     @php
                                                        $msg=nl2br($user->message);
                                                        $msg=str_replace("<br />","<br>",$msg);
                                                        $cadena_resultante= preg_replace('/((http|https|www)[^\s]+)/', '<a target=\"_blank\" href="$1">$0</a>', $msg);
                                                        echo $cadena_resultante;
                                                    @endphp</div>
                                                </div>
                                            </li>
                                            @endif
                                            @endforeach
                                        </ul>
                                @endif
