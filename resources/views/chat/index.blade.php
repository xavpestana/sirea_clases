@extends('layouts.app')

@section('title-view', 'Docentes - Mensajería')

@section('extra-css')

@endsection
@section('content')

                
                <!-- ============================================================== -->
                <!-- Start Page Content -->
                <!-- ============================================================== -->
                <div class="row">
                    <div class="col-12">
                        <div class="card m-b-0">
                            <div class="row mt-3">
                                <div class="col-md-4 ml-auto">
                                    <label for="course">
                                        Seleccione un curso
                                    </label>
                                    <select id="course" name="course" class="form-control" required=""  onchange="Student()">
                                        <option>--   Seleccione   --</option>
                                        option
                                    @foreach ($courses as $course)
                                        <option value="{{ $course->section_id }}">{{ $course->Course->name }} {{ $course->Course->Grade->name }} Sección {{ $course->Section->name }}</option>
                                    @endforeach
                                    </select>
                                </div>
                                <div class="col-md-1">
                                    
                                </div>
                                <div class="col-md-4 mr-3">
                                    <span id="select_level">
                                    </span>
                                </div>
                            </div>
                            <!-- .chat-row -->
                            <div class="chat-main-box mt-3 border-top">
                                <!-- .chat-left-panel -->
                                <div class="chat-left-aside">
                                    <div class="open-panel"><i class="ti-angle-right"></i></div>
                                    <div class="chat-left-inner">
                                        <div class="form-material">
                                            
                                        </div>
                                        <ul id="side_msg" class="chatonline style-none ">
                                            
                                            
                                            
                                        </ul>
                                    </div>
                                </div>
                                <!-- .chat-left-panel -->
                                <!-- .chat-right-panel -->
                                <div id="people_messages" class="chat-right-aside">
                                    
                                </div>
                                <!-- .chat-right-panel -->
                            </div>
                            <!-- /.chat-row -->
                        </div>
                    </div>
                </div>
         

@endsection

@section('extra-js')
<script src="{{ asset('js/chat.js', Request::secure()) }}"></script>
<script src="https://cdn.jsdelivr.net/npm/sweetalert2@9"></script>
<script>
    
/*******  Validate forms  ********/
(function() {
   'use strict';
        window.addEventListener('load', function() {
        // Fetch all the forms we want to apply custom Bootstrap validation styles to
        var forms = document.getElementsByClassName('needs-validation');
        // Loop over them and prevent submission
        var validation = Array.prototype.filter.call(forms, function(form) {
            form.addEventListener('submit', function(event) {
                if (form.checkValidity() === false) {
                    event.preventDefault();
                    event.stopPropagation();
                }
            form.classList.add('was-validated');
        }, false);
        });
        }, false);
})();  

$('#side_msg').load("{{ route('chat.sidebar') }}");
/*******  Guardar mensaje  ********/
$( "#form_msg" ).submit(function(e) {
e.preventDefault();

  $("#save").prop('disabled', true);
  datos = $("#form_msg").serialize();
          
$.ajax({
        type:'GET',
        data:datos,
        url:"{{ route('chat.store') }}",
        })
        .done(function(response) {
            $('#form_msg')[0].reset();
            
            $("#save").prop('disabled', false);
            Swal.fire({
                position: 'center-center',
                icon: 'success',
                title: 'Mensaje Enviado',
                showConfirmButton: false,
                timer: 1500
            }) 
        })
        .fail(function(response) {
            $("#save").prop('disabled', false);
            Swal.fire({
                icon: 'error',
                title: 'Oops...',
                text: 'Intente nuevamente.',
                })
        }) 
});
    $('#search_desktop').hide();
$("#search_student").keyup(function(){
    var text = encodeURIComponent($("#search_student").val());
    if(text != '' && $("#search_student").val().length>3){
      $('#search_desktop').show();
       $('#search_desktop').load('{{ route('chat.search') }}?text='+text);
    }
    if(text == '' || $("#search_student").val().length<3){
      
      $('#search_desktop').hide();
    }
});
function Student() {
  var id = $("#course").val();
    $("#select_level").html('<svg class="circular" viewBox="25 25 50 50"> <circle class="path" cx="50" cy="50" r="20" fill="none" stroke-width="2" stroke-miterlimit="10" /> </svg>');
    $.ajax({
        type:'GET',
        data: {"course" : id, "_token": "{{ csrf_token() }}"},
        url:"{{ route('chat.students') }}",
        success:function(data)
        {
            $("#select_level").html(data);
        }
        })
}

function delete_msg(id){
      $.ajax({
            type : "DELETE",
            data : {"id" : id, "_token": "{{ csrf_token() }}"},
            url : "{{ url('docente/chat/') }}/"+id,
            success : function(r){
    if (r==0){

    }else{
        Swal.fire({
            icon: 'error',
            title: 'Oops...',
            text: 'Error, intente denuevo!',
        })
    }
           }
        });
            
        }

@isset($id)

$.ajax({
         type : "GET",
         data : {"id" : {{ $id }}},
         url : "{{ route('chat.create') }}",
         success : function(data){
            $('#people_messages').html(data);
         }
      })
@endisset
@role('teacher')
        $.ajax({
         type : "GET",
         data : {"id" : {{ auth()->user()->id }} },
         url : "{{ route('chat.count') }}",
         success : function(data){
            $('.count').html(data);
         }
      });
        @endrole
@role('student')
$.ajax({
         type : "GET",
         data : {"id" : {{ auth()->user()->id }} },
         url : "{{ route('chat.count') }}",
         success : function(data){
            $('.count').html(data);
         }
      });
$.ajax({
         type : "GET",
         data : {"id" : {{ auth()->user()->profile->student->id }} },
         url : "{{ route('studentActivity.activities') }}",
         success : function(data){
            $('#activities').html(data);
         }
      });
@endrole

</script>
@endsection

