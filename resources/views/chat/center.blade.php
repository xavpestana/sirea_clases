<div class="chat-main-header">
                                        <div class="p-20 b-b">
                                            <h3 class="box-title">{{ $student->profile->last_name_1 }} {{ $student->profile->last_name_2 }} {{ $student->profile->first_name_1 }}  {{ $student->profile->first_name_2 }}</h3>
                                        </div>
                                    </div>
                                    <div style=" height: 400px; overflow-y: scroll;" id="center_messages" class="chat-rbox">
                                        
                                    </div>
                                    <div class="card-body b-t">
                                        <form id="form_msg" accept-charset="utf-8">
                                            @csrf
                                        <div class="row">
                                            <div class="col-8">
                                                <textarea placeholder="Escribir Mensaje" name="message" class="form-control b-0" required=""></textarea>
                                                <input type="text" name="user_id" value="{{ $student->id }}" hidden="">
                                            </div>
                                            <div class="col-4 text-right">
                                                <button id="save" type="submit" title="Enviar Mensaje" class="btn btn-info btn-circle btn-lg"><i class="fa fa-paper-plane-o"></i> </button>
                                            </div>
                                        </div>
                                        </form>
                                    </div>
                                    <script>
function showmessage() {
$('#center_messages').load("{{ route('chat.show', ['chat' => $student->id]) }}");
 }
/*******  Guardar mensaje  ********/
$( "#form_msg" ).submit(function(e) {
e.preventDefault();

  $("#save").prop('disabled', true);
  datos = $("#form_msg").serialize();
          
$.ajax({
        type:'POST',
        data:datos,
        url:"{{ route('chat.store') }}",
        })
        .done(function(response) {
            $('#form_msg')[0].reset();
            $('#center_messages').load("{{ route('chat.show', ['chat' => $student->id]) }}");
            $("#save").prop('disabled', false);
            Swal.fire({
                position: 'center-center',
                icon: 'success',
                title: 'Mensaje Enviado',
                showConfirmButton: false,
                timer: 1500
            }) 
        })
        .fail(function(response) {
            $("#save").prop('disabled', false);
            Swal.fire({
                icon: 'error',
                title: 'Oops...',
                text: 'Intente nuevamente.',
                })
        }) 
});
    
 setInterval(showmessage,2000);
                                    </script>