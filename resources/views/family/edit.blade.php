@extends('layouts.app')

@section('extra-css')
    <link rel="stylesheet" href="{{asset('themeforest/main/css/steps.css')}}">
@endsection

@section('content')
@inject('states', 'App\Services\States')
<div class="row">
    <div class="col-sm-12">
        <div class="card card-body">
            <h3 class="card-title">Datos de la familia</h3>
            <h6 class="card-subtitle"> Formulario para suministrar la información solicitada de su grupo familiar </h6>
            
            <form class="form-horizontal m-t-40" method="POST" action="@role('family'){{ route('families.update', $family) }}@else{{ route('school.families.update', $family) }}@endrole">
          
                @method('patch')
                @csrf
                @if(auth()->user()->hasRole('family') && !auth()->user()->family)
                    <input type="hidden" name="user_id" id="user_id" value="{{ auth()->user()->id }}">
                @endif

                <div class="form-group">
                    <label for="email"><strong>Usuario / Correo electrónico @isset($family)(nota: si cambia este dato se enviará un mensaje al nuevo correo con nueva contraseña)@endisset</strong><span class="danger"> *</span></label>
                    <input type="text" class="form-control @error('email') is-invalid @enderror" id="email" name="email" value="@isset($family){{$family->user->email}}@else{{ old('email') }}@endisset">
                    @error('email')
                    <div class="invalid-feedback">
                      {{ $message }}
                    </div>
                    @enderror
                </div>

                <p><strong>Nota: </strong>Los apellidos de la familia son un dato guía para identificar al grupo familiar.</p>
                <div class="form-group">
                    <label for="last_name_1">Primer Apellido de la familia<span class="danger">*</span></label>
                    <input type="text" class="form-control @error('last_name_1') is-invalid @enderror" id="last_name_1" name="last_name_1" value="@isset($family){{$family->last_name_1}}@else{{ old('last_name_1') }}@endisset">
                    @error('last_name_1')
                    <div class="invalid-feedback">
                      {{ $message }}
                    </div>
                    @enderror
                </div>
                <div class="form-group">
                    <label for="last_name_2">Segundo Apellido de la familia<span class="danger">*</span></label>
                    <input type="text" class="form-control @error('last_name_2') is-invalid @enderror" id="last_name_2" name="last_name_2" value="@isset($family){{$family->last_name_2}}@else{{ old('last_name_2') }}@endisset">
                    @error('last_name_2')
                    <div class="invalid-feedback">
                      {{ $message }}
                    </div>
                    @enderror
                </div>

                <div class="form-group">
                    <label for="phone">Teléfono <span class="danger">*</span></label>
                    <input type="phone" id="phone" name="phone" class="form-control @error('phone') is-invalid @enderror" value="@isset($family){{ $family->phone }}@else{{ old('phone') }}@endisset"
                    placeholder="ejemplo: 04141234567">
                    @error('phone')
                    <div class="invalid-feedback">
                      {{ $message }}
                    </div>
                    @enderror
                </div>

                <h4 class="card-subtitle"> Información sobre la ubicación de su grupo familiar </h4>                

                <div class="row">
                    <div class="form-group col-md-3">
                        <label for="state">Estado <span class="danger">*</span></label>
                        <select class="form-control @error('state') is-invalid @enderror" id="state" name="state">
                            @foreach ($states->get() as $index => $state)
                            <option value="{{ $index }}" @if((isset($family->parish->municipality->state->id) && $family->parish->municipality->state->id==$index) or (old('state')==$index)) selected @endif>{{ $state }}</option>
                            @endforeach
                        </select>
                        @error('state')
                        <div class="invalid-feedback">
                          {{ $message }}
                        </div>
                        @enderror                        
                    </div>

                    <div class="form-group col-md-3">
                        <label for="city">City <span class="danger">*</span></label>
                        <select class="form-control" id="city_id" name="city_id" data-value="@isset($family){{ $family->city_id }}@endisset">
                        </select>
                    </div>

                    <div class="form-group col-md-3">
                        <label for="municipality">Municipio <span class="danger">*</span></label>
                        <select class="form-control @error('municipality') is-invalid @enderror" id="municipality" name="municipality" data-value="@isset($family->parish->municipality->id){{ $family->parish->municipality->id }}@endisset">
                        </select>
                        @error('municipality')
                        <div class="invalid-feedback">
                          {{ $message }}
                        </div>
                        @enderror   
                    </div>

                    <div class="form-group col-md-3">
                        <label for="parish_id">Parroquia <span class="danger">*</span></label>
                        <select class="form-control @error('parish_id') is-invalid @enderror" id="parish_id" name="parish_id" data-value="@isset($family){{ $family->parish_id }}@endisset">
                        </select>
                        @error('parish_id')
                        <div class="invalid-feedback">
                            {{ $message }}
                        </div>
                        @enderror 
                    </div>
                </div>

                <div class="form-group">
                    <label for="address">Dirección</label>
                    <textarea class="form-control @error('address') is-invalid @enderror" rows="5" id="address" name="address">@isset($family){{ $family->address }}@else{{ old('address') }}@endisset</textarea>
                    @error('address')
                    <div class="invalid-feedback">
                      {{ $message }}
                    </div>
                    @enderror                   
                </div>

                @if($family->school->additional_data)
                
                <h3>Información Adicional:</h3>

                <p class="mb-5">Los siguientes campos pueden ser llenados opcionalmente de acuerdo a los requerimientos de su colegio</p>


                <div id="familiar-income">
                <h4 class="card-subtitle"> Ingreso familiar </h4>
                <div class="form-group">
                    <label for="monthly_income">Ingreso mensual</label>
                    <input type="number" class="form-control @error('monthly_income') is-invalid @enderror" id="monthly_income" name="monthly_income" min="0" value="@isset($family){{ $family->monthly_income }}@else{{ old('monthly_income') }}@endisset">
                    @error('monthly_income')
                    <div class="invalid-feedback">
                      {{ $message }}
                    </div>
                    @enderror
                </div>
                
                <div class="row">                    
                <div class="form-group col-md-6">
                    <label for="dependent_income">Cantidad de personas depedientes del ingreso</label>
                    <input type="number" class="form-control @error('dependent_income') is-invalid @enderror" id="dependent_income" name="dependent_income" min="0" value="@isset($family){{ $family->dependent_income }}@else{{ old('dependent_income') }}@endisset">
                    @error('dependent_income')
                    <div class="invalid-feedback">
                      {{ $message }}
                    </div>
                    @enderror
                </div>

                <div class="form-group col-md-6">
                    <label for="contributors">Cantidad de personas que aportan al ingreso</label>
                    <input type="number" class="form-control @error('contributors') is-invalid @enderror" id="contributors" name="contributors" min="0" value="@isset($family){{ $family->contributors }}@else{{ old('contributors') }}@endisset">
                    @error('contributors')
                    <div class="invalid-feedback">
                      {{ $message }}
                    </div>
                    @enderror
                </div>
                </div>
                </div>

                <h4 class="card-subtitle">Transporte</h4>
                <div class="row" id="transport">


                <div class="form-group col-md-12">
                    <label for="companion">¿Con quién llegan y se retiran los estudiantes del colegio? (nombre, documento y número de contacto)</label>
                    <input type="text" class="form-control " id="companion" name="companion" value="@isset($family->poll){{$family->poll->companion}}@else{{ old('companion') }}@endisset">
                </div>


                @inject('transportations', 'App\Services\Transportations')
                <div class="form-group col-md-6">
                    <label for="arrival">Medio utilizado para llegar al colegio</label>
                    <select class="form-control" id="arrival" name="arrival" data-value="@isset($family->poll){{ $family->poll->arrival }}@endisset">
                        @foreach ($transportations->get() as $arrival)
                            <option value="{{ $arrival }}" 
                            @if((isset($family->poll->arrival) && $family->poll->arrival==$arrival) or (old('arrival')==$arrival)) selected 
                            @endif
                            >{{ $arrival }}</option>
                        @endforeach 
                    </select>
                </div>

                <div class="form-group col-md-6">
                    <label for="departure">Medio utilizado para salir del colegio</label>
                    <select class="form-control" id="departure" name="departure" data-value="@isset($family->poll){{ $family->poll->departure }}@endisset">
                        @foreach ($transportations->get() as $departure)
                            <option value="{{ $departure }}" 
                            @if((isset($family->poll->departure) && $family->poll->departure==$departure) or (old('departure')==$departure)) selected 
                            @endif
                            >{{ $departure }}</option>
                        @endforeach 
                    </select>
                </div>
                </div>

                <h4 class="card-subtitle">Aspectos sociales</h4>
                <div class="row">
                    @inject('maritalStatuses', 'App\Services\MaritalStatuses')
                    <div class="form-group col-md-6">
                        <label for="marital_status">Estado Civil de los padres</label>
                        <select class="form-control" id="marital_status" name="marital_status" data-value="@isset($family->poll){{ $family->poll->marital_status }}@endisset">
                            @foreach ($maritalStatuses->get() as $maritalStatus)
                                <option value="{{ $maritalStatus }}" 
                                @if((isset($family->poll->marital_status) && $family->poll->marital_status==$maritalStatus) or (old('marital_status')==$maritalStatus)) selected 
                                @endif
                                >{{ $maritalStatus }}</option>
                            @endforeach 
                        </select>
                    </div>
                    
                    @inject('religions', 'App\Services\Religions')
                    <div class="form-group col-md-6">
                        <label for="reliegion">Religión</label>
                        <select class="form-control" id="religion" name="religion" data-value="@isset($family->poll){{ $family->poll->religion }}@endisset">
                            <option value="">Selecciona</option>
                            @foreach ($religions->get() as $religion)
                                <option value="{{ $religion }}" 
                                @if((isset($family->poll->religion) && $family->poll->religion==$religion) or (old('religion')==$religion)) selected 
                                @endif
                                >{{ $religion }}</option>
                            @endforeach 
                        </select>
                    </div>



                    @inject('propertyTypes', 'App\Services\PropertyTypes')
                    <div class="form-group col-md-6">
                        <label for="property_type">Propiedad de la vivienda familiar</label>
                        <select class="form-control" id="property_type" name="property_type" data-value="@isset($family->poll){{ $family->poll->property_type }}@endisset">
                            @foreach ($propertyTypes->get() as $property_type)
                                <option value="{{ $property_type }}" 
                                @if((isset($family->poll->property_type) && $family->poll->property_type==$property_type) or (old('property_type')==$property_type)) selected 
                                @endif
                                >{{ $property_type }}</option>
                            @endforeach 
                        </select>
                    </div>
                    @inject('homeTypes', 'App\Services\HomeTypes')
                    <div class="form-group col-md-6">
                        <label for="home_type">Tipo de vivienda familiar</label>
                        <select class="form-control" id="home_type" name="home_type" data-value="@isset($family->poll){{ $family->poll->home_type }}@endisset">
                            @foreach ($homeTypes->get() as $home_type)
                                <option value="{{ $home_type }}" 
                                @if((isset($family->poll->home_type) && $family->poll->home_type==$home_type) or (old('home_type')==$home_type)) selected 
                                @endif
                                >{{ $home_type }}</option>
                            @endforeach 
                        </select>
                    </div>
                </div>
                <div class="form-group">
                    <label for="emergency_phone">Teléfono en caso de emergencia</label>
                    <input type="emergency_phone" id="emergency_phone" name="emergency_phone" class="form-control @error('emergency_phone') is-invalid @enderror" value="@isset($family->poll){{ $family->poll->emergency_phone }}@else{{ old('emergency_phone') }}@endisset" placeholder="ejemplo: 04141234567">
                    @error('emergency_phone')
                    <div class="invalid-feedback">
                      {{ $message }}
                    </div>
                    @enderror
                </div>
                @endif

                <button type="submit" class="btn btn-success waves-effect waves-light m-r-10">Guardar</button>

                <a href="{{ url()->previous() }}" class="btn btn-inverse waves-effect waves-light">Cancelar</a>
                
            </form>
        </div>
    </div>
</div>

@endsection

@section('extra-js')

<script src="{{asset('themeforest/main/js/wizard/jquery.steps.min.js')}}"></script>
<script src="{{asset('themeforest/main/js/wizard/jquery.validate.min.js')}}"></script>
<script src="{{asset('themeforest/main/js/wizard/steps.js')}}"></script>
<script>
    loadCity();
    $('#state').on('change', loadCity);
    $('#municipality').on('change', loadParish);

    function loadParish() {
        var municipality_id = $('#municipality').val();
        if($.trim(municipality_id) != ''){
            $.get('/parishes', {municipality_id: municipality_id}, function(parishes){
                $('#parish_id').empty();
                $('#parish_id').append("<option value=''>Selecciona</option>");
                $.each(parishes, function(index, value){
                    $('#parish_id').append("<option value='"+index+"'>"+value+"</option>");
                });
            });
        }
    }    

    function loadCity() {
        var state_id = $('#state').val();

        if ($.trim(state_id) != '') {
            $.get('/cities', {state_id: state_id}, function (cities) {
                var city = $('#city_id').data('value') != '' ? $('#city_id').data('value') : '';
                var oldCity = $('#city_id').data('old') != '' ? $('#city_id').data('old') : '';
                $('#city_id').empty();
                $('#city_id').append("<option value=''>Selecciona</option>");
                $.each(cities, function (index, value) {
                    $('#city_id').append("<option value='" + index + "'" + (city == index ? 'selected' : oldCity == index ? 'selected' : '') + ">" + value +"</option>");
                })
            });

            $.get('/municipalities', {state_id: state_id}, function(municipalities){
                var municipality = $('#municipality').data('value') != '' ? $('#municipality').data('value') : '';
                var oldMunicipality = $('#municipality').data('old') != '' ? $('#municipality').data('old') : '';
                $('#municipality').empty();
                $('#municipality').append("<option value=''>Selecciona</option>");
                $.each(municipalities, function(index, value){
                    $('#municipality').append("<option value='" + index + "'" + (municipality == index ? 'selected' : oldMunicipality == index ? 'selected' : '') + ">" + value +"</option>");
                });

                var municipality_id = $('#municipality').val();

                $.get('/parishes', {municipality_id: municipality_id}, function(parishes){
                    var parish = $('#parish_id').data('value') != '' ? $('#parish_id').data('value') : '';
                    var oldParish = $('#parish_id').data('old') != '' ? $('#parish_id').data('old') : '';
                    $('#parish_id').empty();
                    $('#parish_id').append("<option value=''>Selecciona</option>");
                    $.each(parishes, function(index, value){
                        $('#parish_id').append("<option value='"+ index + "'" + (parish == index ? 'selected': oldParish == index ? 'selected' : '') + ">" + value +"</option>");
                    });
                });
            });
        }
    }
</script>
@endsection
