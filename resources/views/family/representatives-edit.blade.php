@extends('layouts.app')

@section('extra-css')
    <link rel="stylesheet" href="{{asset('themeforest/main/css/dropify.min.css')}}">
@endsection

@section('content')
@inject('genders', 'App\Services\Genders')
@inject('countries', 'App\Services\Countries')
@inject('relationships', 'App\Services\Relationships')
<div class="row">
    @isset($representative)
    <form class="form-horizontal m-t-40" method="POST" action="@role('family'){{ route('representatives.update', $representative->id) }}@else{{ route('school.representatives.update', $representative->id) }}@endrole" enctype="multipart/form-data">
        @method('patch')
    @else
    <form class="form-horizontal m-t-40" method="POST" action="{{ route('representatives.store') }}" enctype="multipart/form-data">
    @endisset
        <div class="col-lg-12 col-md-12">
            <div class="card">
                <div class="card-body @error('avatar') is-invalid @enderror">
                    <h4 class="card-title">Fotografía del representante</h4>
                    <label for="input-file-now-custom-1">La imagen debe ser una fotografía tipo carnet con fondo blanco, los formatos deben ser .jpg, .jpeg. o .png, tamaño máximo 3Mb</label>
                    @isset($representative) @php($avatar = $representative->avatar)@else @php($avatar="default.jpg") @endisset
                    <input type="file" id="avatar" name="avatar" class="dropify form-control " data-default-file="{{ asset('storage/pictures/avatars/'.$avatar) }}" data-allowed-file-extensions="jpg jpeg png gif" data-max-file-size="3M"/>
                </div>
                @error('avatar')
                <div class="invalid-feedback text-center">
                  {{ $message }}
                </div>
                @enderror
            </div>
        </div>

        <div class="col-sm-12">
            <div class="card card-body">
                <h4 class="card-title">Datos del Representante  @isset($representative)(familia: {{ $representative->family->last_name_1 }} {{ $representative->family->last_name_2 }}) @endisset</h4>
                <h6 class="card-subtitle"> Formulario para suministrar la información solicitada para el representante</h6>
                    @csrf
                    <div class="form-group">
                <label for="email"><strong>Usuario / Correo electrónico del Representante</strong></label>
                <input type="text" class="form-control @error('email') is-invalid @enderror" id="email" name="email" value="@isset($representative){{$representative->correo}}@else{{ old('email') }}@endisset">
                @error('email')
                <div class="invalid-feedback">
                  {{ $message }}
                </div>
                @enderror
            </div>
                <div class="row">
                    <div class="form-group col-md-6">
                        <label for="document_type">Tipo de documento</label>
                        <select class="form-control @error('document_type') is-invalid @enderror" id="document_type" name="document_type" >
                            <option value="">Seleccione</option>
                            <option @if((isset($representative) && $representative->document_type=='V' ) or (old('document_type')=='V' )) selected @endif value="V" >V </option>
                            <option @if((isset($representative) && $representative->document_type=='E' ) or (old('document_type')=='E' )) selected @endif value="E" >E </option>
                        </select>
                        @error('document_type')
                        <div class="invalid-feedback">
                          {{ $message }}
                        </div>
                        @enderror
                    </div>

                    <div class="form-group col-md-6">
                        <label for="document_number">Documento</label>
                        <input type="text" class="form-control @error('document_number') is-invalid @enderror" id="document_number" name="document_number" value="@isset($representative){{$representative->document_number}}@else{{ old('document_number') }}@endisset" placeholder="ej: 12345678" maxlength="30">
                        @error('document_number')
                        <div class="invalid-feedback">
                          {{ $message }}
                        </div>
                        @enderror
                    </div>                    
                </div>


                <div class="form-group">
                    <label for="first_name_1">Primer nombre</label>
                    <input type="text" class="form-control @error('first_name_1') is-invalid @enderror" id="first_name_1" name="first_name_1" value="@isset($representative){{$representative->first_name_1}}@else{{ old('first_name_1') }}@endisset">
                    @error('first_name_1')
                    <div class="invalid-feedback">
                      {{ $message }}
                    </div>
                    @enderror
                </div>

                <div class="form-group">
                    <label for="first_name_2">Segundo nombre <i>(opcional)</i></label>
                    <input type="text" class="form-control @error('first_name_2') is-invalid @enderror" id="first_name_2" name="first_name_2" value="@isset($representative){{$representative->first_name_2}}@else{{ old('first_name_2') }}@endisset">
                </div>

                <div class="form-group">
                    <label for="last_name_1">Primer apellido</label>
                    <input type="text" class="form-control @error('last_name_1') is-invalid @enderror" id="last_name_1" name="last_name_1" value="@isset($representative){{$representative->last_name_1}}@else{{ old('last_name_1') }}@endisset">
                    @error('last_name_1')
                    <div class="invalid-feedback">
                      {{ $message }}
                    </div>
                    @enderror
                </div>

                <div class="form-group">
                    <label for="last_name_2">Segundo apellido</label>
                    <input type="text" class="form-control @error('last_name_2') is-invalid @enderror" id="last_name_2" name="last_name_2" value="@isset($representative){{$representative->last_name_2}}@else{{ old('first_name_2') }}@endisset">
                    @error('last_name_2')
                    <div class="invalid-feedback">
                      {{ $message }}
                    </div>
                    @enderror
                </div>

                <div class="form-group">
                    <label for="birthdate">Fecha de nacimiento</label>
                    <input class="form-control @error('birthdate') is-invalid @enderror" type="date" value="@isset($representative){{$representative->birthdate}}@else{{ old('birthdate') }}@endisset" id="birthdate" name="birthdate">
                    @error('birthdate')
                    <div class="invalid-feedback">
                      {{ $message }}
                    </div>
                    @enderror
                </div>

                <div class="form-group">
                    <label for="country_id">País de nacimiento</label>
                    <select class="form-control @error('country_id') is-invalid @enderror" id="country_id" name="country_id" value="@isset($representative){{$representative->birthdate}}@endisset" >
                        @foreach ($countries->get() as $index => $country)
                        <option value="{{ $index }}" @if((isset($representative) && $representative->country_id == $index) or (old('country_id')==$index)) selected @endif  >{{ $country }}</option>
                        @endforeach
                    </select>
                    @error('country_id')
                    <div class="invalid-feedback">
                      {{ $message }}
                    </div>
                    @enderror
                </div>

                <div class="form-group">
                    <label for="phone">Teléfono</label>
                    <input class="form-control @error('phone') is-invalid @enderror" type="tel" value="@isset($representative){{$representative->phone}}@else{{ old('phone') }}@endisset" id="phone" name="phone">
                    @error('phone')
                    <div class="invalid-feedback">
                      {{ $message }}
                    </div>
                    @enderror
                </div>

                <div class="form-group">
                    <label for="relationship_id">Parentesco con los estudiantes</label>
                    <select class="form-control @error('relationship_id') is-invalid @enderror" id="relationship_id" name="relationship_id" >
                        @foreach ($relationships->get() as $index => $relationship)
                        <option value="{{ $index }}" @if((isset($representative) && $representative->relationship_id == $index) or (old('relationship_id')==$index)) selected @endif>{{ $relationship }}</option>
                        @endforeach
                    </select>
                    @error('relationship_id')
                    <div class="invalid-feedback">
                      {{ $message }}
                    </div>
                    @enderror                    
                </div>

                <div class="row pt-4 pb-4">

                    <div class="form-group col-md-6">
                        <label for="gender">Género</label>
                        <div class="demo-radio-button @error('gender_id') is-invalid @enderror">
                            @foreach ($genders->get() as $index => $gender)
                                <input name="gender_id" type="radio" id="{{ $gender }}" class="radio-col-green" value="{{ $index }}"
                                @if((isset($representative) && $representative->gender_id == $index) or (old('gender')==$index))
                                    checked 
                                @endif
                                />
                                <label for="{{ $gender }}">{{ $gender }}</label>
                            @endforeach
                        </div>
                        @error('gender_id')
                        <div class="invalid-feedback">
                          {{ $message }}
                        </div>
                        @enderror
                    </div>

                    <div class="col-sm-6">
                        <div class="demo-switch-title">Representante Principal</div>
                        <div class="switch">
                            <label>
                                <input type="checkbox" value="1"
                                @if((isset($representative) && $representative->principal == '1') or (old('principal')=='1'))
                                    checked 
                                @endif
                                name="principal" id="principal"
                                >
                                <span class="lever switch-col-green"></span>
                            </label>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-6">
                        <button type="submit" class="btn btn-success waves-effect waves-light m-r-10">Guardar</button>
                    </div>
                    <div class="col-md-6">
                        <a href="{{ url()->previous() }}" class="btn btn-inverse waves-effect waves-light">Cancelar</a>
                    </div>
                </div>
            </div>
        </div>
    </form>
</div>
@endsection

@section('extra-js')
<script src="{{asset('themeforest/main/js/dropify.min.js')}}"></script>
<script>
    $(function() {
        $('.dropify').dropify({
            messages: {
                default: 'Arrastra aquí un archivo o haz clic para subirlo',
                replace: 'Arrastra aquí un archivo o haz clic para reemplazarlo',
                remove: 'Eliminar',
                error: 'Lo siento, el archivo es demasiado grande'
            },
            tpl: {
                wrap:            '<div class="dropify-wrapper"></div>',
                loader:          '<div class="dropify-loader"></div>',
                message:         '<div class="dropify-message"><span class="file-icon" /> <h4>Arrastra aquí un archivo o haz clic para subirlo</h4></div>',
                preview:         '<div class="dropify-preview"><span class="dropify-render"></span><div class="dropify-infos"><div class="dropify-infos-inner"><p class="dropify-infos-message">Puede cambiar esta imagen haciendo click aquí o arrastrando la nueva imagen</p></div></div></div>',
                filename:        '<p class="dropify-filename"><span class="file-icon">Imagen Actual</span></p>',
                clearButton:     '<button type="button" class="dropify-clear">Eliminar</button>',
                errorLine:       '<h4 class="dropify-error"></h4>',
                errorsContainer: '<div class="dropify-errors-container"><ul></ul></div>'
            },
            error: {
                'fileSize': 'El tamaño del archivo es muy grande (3mb max).',
                'minWidth': 'el ancho de la imagen es muy pequeño (200px min).',
                'maxWidth': 'el ancho de la imagen es muy grande (200px max).',
                'minHeight': 'el alto de la imagen es muy pequeño (1000px min).',
                'maxHeight': 'el alto de la imagen es muy grande (1000px max).',
                'imageFormat': 'El formato de imagen no está permitido, solo (jpg, jpeg, png).'
            }
        });

        // Used events
        var drEvent = $('#input-file-events').dropify();

        drEvent.on('dropify.beforeClear', function(event, element) {
            return confirm("Realmente desea borrar \"" + element.file.name + "\" ?");
        });

        drEvent.on('dropify.afterClear', function(event, element) {
            alert('Archivo borrado');
        });

        drEvent.on('dropify.errors', function(event, element) {
            console.log('Tiene errores');
        });

        var drDestroy = $('#input-file-to-destroy').dropify();
        drDestroy = drDestroy.data('dropify')
        $('#toggleDropify').on('click', function(e) {
            e.preventDefault();
            if (drDestroy.isDropified()) {
                drDestroy.destroy();
            } else {
                drDestroy.init();
            }
        })
    });
</script>


@endsection
