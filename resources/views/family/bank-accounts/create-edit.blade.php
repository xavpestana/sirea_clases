@extends('layouts.app')

@section('content')
    <div class="row">
        <div class="col-12">
            <bank-account :bank_account="2" :bank_account_id="{!! (isset($bankAccount)) ? $bankAccount->id : "null" !!}"
                route_list='{{ url('familia/cuentas-bancarias') }}'>
            </bank-account>
        </div>
    </div>
@endsection
