@extends('layouts.app')

@section('content')
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-header border-0">
                    Lista de las Cuentas Bancarias de la Familia<a class="btn btn-primary btn-xs float-right" href="{{ route('family-bank-accounts.create') }}"><i class="fa fa-plus-circle"></i></a>
                </div>
                <div class="card-body">
                    <bank-account-list route_list="{{ url('familia/cuentas-bancarias/show/vue-list') }}"
						route_delete="{{ url('familia/cuentas-bancarias') }}"
						route_edit="{{ url('familia/cuentas-bancarias/{id}/editar') }}"
						route_show="{{ url('familia/cuentas-bancarias/{id}') }}" :bank_account="2">
					</bank-account-list>
                </div>
            </div>
        </div>
    </div>
@endsection
