@extends('layouts.app')

@section('title-view', 'Listado de Estudiantes')

@section('extra-css')
    <!--alerts CSS -->
    <link href="{{ asset('themeforest/main/css/sweetalert.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ asset('themeforest/main/css/jquery.toast.css', Request::secure()) }}" id="theme" rel="stylesheet">
@endsection

@section('content')
<div class="row">
    <div class="col-md-12 col-lg-12 col-xlg-12">
	<div class="card">
	    <div class="card-body">
	        <h3 class="card-title">Listado de Estudiantes</h3>
	        <p class="card-text">En este espacio se visualizan los estudiantes agregados en esta cuenta de familia, el estatus de cada uno (inscrito o no inscrito) y los botones para editar o eliminar estudiantes no inscritos. Puede agregar un nuevo estudiante para la familia en el siguiente boton:</p>
	        <a class="btn btn-info  waves-effect waves-light" href="{{ route('students.create') }}"><span class="btn-label"><i class="mdi mdi-account-star"></i></span>Agregar nuevo estudiante</a>
	    </div>
	</div>
    </div>
	@foreach ($students as $student)
    @isset($actual)
        @php($enrollment = $student->enrollments->where('section.season_id', $actual->id)->first())
    @endisset
    <div class="col-md-6 col-lg-6 col-xlg-4">
        <div class="card card-body">
            <div class="row">
                <div class="col-md-4 col-lg-3 text-center">
                    <a><img src="{{ asset('storage/pictures/avatars/'.$student->profile->avatar) }}" alt="user" class="img-circle img-responsive"></a>
                </div>
                <div class="col-md-8 col-lg-9">
                    <h3 class="box-title m-b-0">{{ $student->profile->first_name_1 }} {{ $student->profile->first_name_2 }} {{ $student->profile->last_name_1 }} {{ $student->profile->last_name_2 }}</h3> <small>Estudiante</small>
                    <address>
                        @isset($actual)
                            @isset($enrollment)
                    	       <span class="label label-info">Inscrito en {{ $enrollment->section->grade->name  }} "{{ $enrollment->section->name  }}" ({{ $actual->season }})</span>
                            @else
                               <span class="label label-warning">no Inscrito ({{ $actual->season }})</span>
                            @endisset
                        @else
                               <span class="label label-danger">Año escolar no activo</span>
                        @endisset
                        <br/>
                        <br/>
                        <a class="btn btn-sm btn-warning  waves-effect waves-light" href="{{ route('students.edit', $student->id) }}"><span class="btn-label"><i class="fa fa-pencil"></i></span>Editar</a>


                        <button class="btn btn-sm btn-danger  waves-effect waves-light @isset($enrollment)disabled @else student-delete @endisset" data-name="{{ $student->profile->first_name_1 }} {{ $student->profile->first_name_2 }} {{ $student->profile->last_name_1 }} {{ $student->profile->last_name_2 }}"  data-id="{{ $student->id }}" type="button"><span class="btn-label"><i class="fa fa-trash"></i></span>Eliminar</button>


                        <button class="btn btn-sm btn-primary  waves-effect waves-light dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" data-name="{{ $student->profile->first_name_1 }} {{ $student->profile->first_name_2 }} {{ $student->profile->last_name_1 }} {{ $student->profile->last_name_2 }}"  data-id="{{ $student->id }}" type="button"><span class="btn-label"><i class="fa fa-file-pdf-o"></i></span>Descargar</button>
                        <div class="dropdown-menu" aria-labelledby="btn-download">
                          <a class="dropdown-item" href="{{ route('pdf.planilla', $student->id) }}">Planilla Matriculación</a>
                           @isset($actual)
                                @isset($enrollment)
                                  {{-- <a class="dropdown-item" href="{{ route('pdf.student-carnet', '1') }}">Carnet</a> --}}
                                  <a class="dropdown-item" href="{{ route('pdf.student-constancia', $enrollment->id) }}">Constancia de estudio</a>
                                  {{-- <a class="dropdown-item" href="{{ route('bc') }}">bc</a> --}}
                                @else
                                    <a href="javascript:void(0)" class="dropdown-item disabled">Carnet (En espera)</a>
                                    <a href="javascript:void(0)" class="dropdown-item disabled">Constancia (En espera)</a>
                                @endisset
                            @else
                                <a href="javascript:void(0)" class="dropdown-item disabled">Carnet (En espera)</a>
                                <a href="javascript:void(0)" class="dropdown-item disabled">Constancia (En espera)</a>
                            @endisset
                        </div>

                    </address>
                </div>
            </div>
        </div>
    </div>
	@endforeach
</div>

@endsection


@section('extra-js')
<!-- Sweet-Alert  -->
<script src="{{ asset('themeforest/main/js/jquery.toast.js') }}"></script>
<script src="{{ asset('themeforest/main/js/sweetalert.min.js') }}"></script>
<script src="{{ asset('themeforest/main/js/jquery.toast.js') }}"></script>
<script>
$(function(){
    @if (\Session::has('error'))
           $.toast({
            heading: 'Error',
            text: '{{ Session::get('error') }}',
            position: 'top-right',
            loaderBg:'#ff6849',
            icon: 'error',
            hideAfter: 3500
          });
    @endif

    @if (\Session::has('success'))
           $.toast({
            heading: '!Operación exitosa!',
            text: '{{ Session::get('success') }}',
            position: 'top-right',
            loaderBg:'#ff6849',
            icon: 'success',
            hideAfter: 3500, 
            stack: 6
          });
    @endif  

})

$('.student-delete').click(function(){
    var studentName = $(this).data('name')
    var studentId = $(this).data('id')
    swal({   
        title: "¿Eliminar estudiante?",   
        text: studentName+" sera eliminado de su cuenta familia",   
        type: "warning",   
        showCancelButton: true,   
        confirmButtonColor: "#DD6B55",   
        confirmButtonText: "Si, eliminarlo",   
        closeOnConfirm: false 
    }, function(){
        $.ajax({
            headers: {'X-CSRF-Token': $('meta[name="_token"]').attr('content')},
            url: '/familia/estudiantes/'+studentId,
            type: 'delete',
            dataType: 'json',
            data:{id:studentId}
        })
        .done(function(data) {
            $('#app').empty().append($(data));
            swal("Eliminado", studentName+" ha sido eliminado de su cuenta.", "success"); 
        }); 
    });
});
</script>
@endsection
