@extends('layouts.app')

@section('content')
    <div class="row">
        <div class="col-12">
            <family-payment route_list='{{ url('familia/pagos') }}'>
            </family-payment>
        </div>
    </div>
@endsection
