@extends('layouts.app')

@section('content')
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-header border-0">
                    Lista de Pagos del Familiar<a class="btn btn-primary btn-xs float-right" href="{{ route('family-payments.create') }}"><i class="fa fa-plus-circle"></i></a>
                </div>
                <div class="card-body">
                    <family-payment-list route_list="{{ url('familia/pagos/show/vue-list') }}"
						route_delete=""
						route_edit=""
						route_show="{{ url('familia/pagos/{id}') }}">
					</family-payment-list>
                </div>
            </div>
        </div>
    </div>
@endsection
