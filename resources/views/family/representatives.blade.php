@extends('layouts.app')

@section('extra-css')
    <!--alerts CSS -->
    <link href="{{ asset('themeforest/main/css/sweetalert.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ asset('themeforest/main/css/jquery.toast.css', Request::secure()) }}" id="theme" rel="stylesheet">
@endsection

@section('content')
<div class="row">
    <div class="col-md-12 col-lg-12 col-xlg-12">
	<div class="card">
	    <div class="card-body">
	        <h3 class="card-title">Listado de Representantes</h3>
	        <p class="card-text">En este espacio se visualizan los representantes agregados en esta cuenta de familia además los botones para editar o eliminarlos. Puede agregar un nuevo estudiante para la familia en el siguiente boton:</p>
	        <a class="btn btn-info  waves-effect waves-light" href="{{ route('representatives.create') }}"><span class="btn-label"><i class="mdi mdi-account-star"></i></span>Agregar nuevo representante</a>
	    </div>
	</div>
    </div>
	@foreach ($representatives as $representative)
    <div class="col-md-6 col-lg-6 col-xlg-4">
        <div class="card card-body">
            <div class="row">
                <div class="col-md-4 col-lg-3 text-center">
                    <img src="{{ asset('storage/pictures/avatars/'.$representative->avatar) }}" alt="user" class="img-circle img-responsive">
                </div>
                <div class="col-md-8 col-lg-9">
                    <h3 class="box-title m-b-0">{{ $representative->first_name_1 }} {{ $representative->first_name_2 }} {{ $representative->last_name_1 }} {{ $representative->last_name_2 }}</h3> <small>Representante ({{ $representative->relationship->name }})</small>
                    <address>
                    	@if($representative->principal)
                    	<span class="label label-info">Principal</span>
                    	@else
                    	<span class="label label-success">Secundario</span>
                    	@endif
                        <br/>
                        <br/>
                        <a class="btn btn-sm btn-warning  waves-effect waves-light" href="{{ route('representatives.edit', $representative->id) }}"><span class="btn-label"><i class="fa fa-pencil"></i></span>Editar</a>
                        <button class="btn btn-sm btn-danger  waves-effect waves-light representative-delete" data-name="{{ $representative->first_name_1 }} {{ $representative->first_name_2 }} {{ $representative->last_name_1 }} {{ $representative->last_name_2 }}"  data-id="{{ $representative->id }}" type="button"><span class="btn-label"><i class="fa fa-trash"></i></span>Eliminar</button>
                    </address>
                </div>
            </div>
        </div>
    </div>
	@endforeach
</div>
@endsection

@section('extra-js')
<!-- Sweet-Alert  -->
<script src="{{ asset('themeforest/main/js/sweetalert.min.js') }}"></script>
<script src="{{ asset('themeforest/main/js/jquery.toast.js') }}"></script>

<script>
    $(function(){
        @if (\Session::has('error'))
           $.toast({
            heading: 'Error',
            text: '{{ Session::get('error') }}',
            position: 'top-right',
            loaderBg:'#ff6849',
            icon: 'error',
            hideAfter: 3500
          });
        @endif

        @if (\Session::has('success'))
           $.toast({
            heading: '!Operación exitosa!',
            text: '{{ Session::get('success') }}',
            position: 'top-right',
            loaderBg:'#ff6849',
            icon: 'success',
            hideAfter: 3500, 
            stack: 6
          });
        @endif
    })

    $('.representative-delete').click(function(){
        var representativeName = $(this).data('name')
        var representativeId = $(this).data('id')
        swal({   
            title: "¿Eliminar representante?",   
            text: representativeName+" sera eliminado de su cuenta familia",   
            type: "warning",   
            showCancelButton: true,   
            confirmButtonColor: "#DD6B55",   
            confirmButtonText: "Si, eliminarlo",   
            closeOnConfirm: false 
        }, function(){
            $.ajax({
                headers: {'X-CSRF-Token': $('meta[name="_token"]').attr('content')},
                url: '/familia/representantes/'+representativeId,
                type: 'delete',
                dataType: 'json',
                data:{id:representativeId}
            })
            .done(function(data) {
                $('#app').empty().append($(data));
                swal("Eliminado", representativeName+" ha sido eliminado de su cuenta.", "success"); 
            }); 
        });
    });

</script>
@endsection
