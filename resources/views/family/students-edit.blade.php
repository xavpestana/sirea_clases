@extends('layouts.app')


@section('extra-css')
    <link rel="stylesheet" href="{{asset('themeforest/main/css/dropify.min.css')}}">
@endsection

@section('content')
@inject('genders', 'App\Services\Genders')
@inject('countries', 'App\Services\Countries')
@inject('bloodTypes', 'App\Services\BloodTypes')
<div class="row">
    @isset($student)
    <form class="form-horizontal m-t-40" method="POST" action="@role('family'){{ route('students.update', $student) }}@else{{ route('school.students.update', $student) }}@endrole" enctype="multipart/form-data">
        @method('PATCH')
    @else
    <form class="form-horizontal m-t-40" method="POST" action="{{ route('students.store') }}" enctype="multipart/form-data">
    @endisset
    <div class="col-lg-12 col-md-12">
        <div class="card">
            <div class="card-body @error('avatar') is-invalid @enderror">
                <h4 class="card-title">Fotografía del estudiante</h4>
                <label for="input-file-now-custom-1">La imagen debe ser una fotografía tipo carnet con fondo blanco, los formatos deben ser .jpg, .jpeg. o .png, tamaño máximo 3Mb</label>
                @isset($student) @php($avatar = $student->profile->avatar)@else @php($avatar="default.jpg") @endisset
                <input type="file" id="avatar" name="avatar" class="dropify form-control " data-default-file="{{ asset('storage/pictures/avatars/'.$avatar) }}" data-allowed-file-extensions="jpg jpeg png" data-max-file-size="3M"/>
            </div>
            @error('avatar')
            <div class="invalid-feedback text-center">
              {{ $message }}
            </div>
            @enderror
        </div>
    </div>

    <div class="col-sm-12">
        <div class="card card-body">
            <h4 class="card-title">Datos del Estudiante @isset($student)(familia: {{ $student->family->last_name_1 }} {{ $student->family->last_name_2 }}) @endisset</h4>
            <h6 class="card-subtitle"> Formulario para suministrar la información solicitada para el estudiante</h6>

            {{-- aqui estaba e form --}}

            @csrf
            <div class="form-group">
                <label for="email"><strong>Usuario / Correo electrónico del estudiante @isset($student)(nota: si cambia este dato se enviará un mensaje al nuevo correo con nueva contraseña)@endisset</strong></label>
                <input type="text" class="form-control @error('email') is-invalid @enderror" id="email" name="email" value="@isset($student){{$student->profile->user->email}}@else{{ old('email') }}@endisset">
                @error('email')
                <div class="invalid-feedback">
                  {{ $message }}
                </div>
                @enderror
            </div>

            <p>Indique la cedula de indentidad o estudiantil del estudiante, en caso de no tener puede ingresar la cedula del representante</p>

            <div class="row">
                <div class="form-group col-md-6">
                    <label for="document_type">Tipo de documento</label>
                    <select class="form-control @error('document_type') is-invalid @enderror" id="document_type" name="document_type" >
                        <option value="">Seleccione</option>
                        <option @if((isset($student) && $student->profile->document_type=='CE') or (old('document_type')=='CE')) selected @endif value="CE">CE</option>
                        <option @if((isset($student) && $student->profile->document_type=='V' ) or (old('document_type')=='V' )) selected @endif value="V" >V </option>
                        <option @if((isset($student) && $student->profile->document_type=='E' ) or (old('document_type')=='E' )) selected @endif value="E" >E </option>
                    </select>
                    @error('document_type')
                    <div class="invalid-feedback">
                      {{ $message }}
                    </div>
                    @enderror
                </div>

                <div class="form-group col-md-6">
                    <label for="document_number">Documento</label>
                    <input type="text" class="form-control @error('document_number') is-invalid @enderror" id="document_number" name="document_number" value="@isset($student){{$student->profile->document_number}}@else{{ old('document_number') }}@endisset" placeholder="ej: 12345678" maxlength="30">
                    @error('document_number')
                    <div class="invalid-feedback">
                      {{ $message }}
                    </div>
                    @enderror
                </div>                    
            </div>


            <div class="form-group">
                <label for="first_name_1">Primer nombre</label>
                <input type="text" class="form-control @error('first_name_1') is-invalid @enderror" id="first_name_1" name="first_name_1" value="@isset($student){{$student->profile->first_name_1}}@else{{ old('first_name_1') }}@endisset">
                @error('first_name_1')
                <div class="invalid-feedback">
                  {{ $message }}
                </div>
                @enderror
            </div>

            <div class="form-group">
                <label for="first_name_2">Segundo nombre</label>
                <input type="text" class="form-control @error('first_name_2') is-invalid @enderror" id="first_name_2" name="first_name_2" value="@isset($student){{$student->profile->first_name_2}}@else{{ old('first_name_2') }}@endisset">
            </div>

            <div class="form-group">
                <label for="last_name_1">Primer apellido</label>
                <input type="text" class="form-control @error('last_name_1') is-invalid @enderror" id="last_name_1" name="last_name_1" value="@isset($student){{$student->profile->last_name_1}}@else{{ old('last_name_1') }}@endisset">
                @error('last_name_1')
                <div class="invalid-feedback">
                  {{ $message }}
                </div>
                @enderror
            </div>

            <div class="form-group">
                <label for="last_name_2">Segundo apellido</label>
                <input type="text" class="form-control @error('last_name_2') is-invalid @enderror" id="last_name_2" name="last_name_2" value="@isset($student){{$student->profile->last_name_2}}@else{{ old('last_name_2') }}@endisset">
                @error('last_name_2')
                <div class="invalid-feedback">
                  {{ $message }}
                </div>
                @enderror
            </div>

            <div class="form-group">
                <label for="birthdate">Fecha de nacimiento</label>
                <input class="form-control @error('birthdate') is-invalid @enderror" type="date" value="@isset($student){{$student->profile->birthdate}}@else{{ old('birthdate') }}@endisset" id="birthdate" name="birthdate">
                @error('birthdate')
                <div class="invalid-feedback">
                  {{ $message }}
                </div>
                @enderror
            </div>

            <div class="form-group">
                <label for="country_id">País de nacimiento</label>
                <select class="form-control @error('country_id') is-invalid @enderror" id="country_id" name="country_id" value="@isset($student){{$student->profile->birthdate}}@endisset" >
                    @foreach ($countries->get() as $index => $country)
                    <option value="{{ $index }}" @if((isset($student) && $student->profile->country_id == $index) or (old('country_id')==$index)) selected @endif  >{{ $country }}</option>
                    @endforeach
                </select>
                @error('country_id')
                <div class="invalid-feedback">
                  {{ $message }}
                </div>
                @enderror
            </div>

            <div class="form-group">
                <label for="phone">Teléfono</label>
                <input class="form-control @error('phone') is-invalid @enderror" type="tel" value="@isset($student){{$student->profile->phone}}@else{{ old('phone') }}@endisset" id="phone" name="phone">
                @error('phone')
                <div class="invalid-feedback">
                  {{ $message }}
                </div>
                @enderror
            </div>

            <div class="form-group col-md-6">
                <label for="gender">Género</label>
                <div class="demo-radio-button @error('gender_id') is-invalid @enderror">
                    @foreach ($genders->get() as $index => $gender)
                        <input name="gender_id" type="radio" id="{{ $gender }}" class="radio-col-green" value="{{ $index }}"
                        @if((isset($student) && $student->profile->gender_id == $index) or (old('gender')==$index))
                            checked 
                        @endif
                        />
                        <label for="{{ $gender }}">{{ $gender }}</label>
                    @endforeach
                </div>
                @error('gender_id')
                <div class="invalid-feedback">
                  {{ $message }}
                </div>
                @enderror
            </div>

            <h4>Datos físicos del estudiante</h4>
            <div class="row">
                <div class="form-group col-md-4">
                    <label for="shirt_size">Talla de camisa</label>
                    <input type="text" class="form-control @error('shirt_size') is-invalid @enderror" id="shirt_size" name="shirt_size" value="@isset($student){{$student->shirt_size}}@else{{ old('shirt_size') }}@endisset">
                    @error('shirt_size')
                    <div class="invalid-feedback">
                      {{ $message }}
                    </div>
                    @enderror
                </div>
                <div class="form-group col-md-4">
                    <label for="pant_size">Talla de pantalón</label>
                    <input type="text" class="form-control @error('pant_size') is-invalid @enderror" id="pant_size" name="pant_size" value="@isset($student){{$student->pant_size}}@else{{ old('pant_size') }}@endisset">
                    @error('pant_size')
                    <div class="invalid-feedback">
                      {{ $message }}
                    </div>
                    @enderror
                </div>
                <div class="form-group col-md-4">
                    <label for="shoe_size">Talla de calzado</label>
                    <input type="text" class="form-control @error('shoe_size') is-invalid @enderror" id="shoe_size" name="shoe_size" value="@isset($student){{$student->shoe_size}}@else{{ old('shoe_size') }}@endisset">
                    @error('shoe_size')
                    <div class="invalid-feedback">
                      {{ $message }}
                    </div>
                    @enderror
                </div>                    
            </div>

            <div class="row">
                <div class="form-group col-md-6">
                    <label for="height">Altura (cm)</label>
                    <input type="number" class="form-control @error('height') is-invalid @enderror" id="height" name="height" value="@isset($student){{$student->height}}@else{{ old('height') }}@endisset">
                    @error('height')
                    <div class="invalid-feedback">
                      {{ $message }}
                    </div>
                    @enderror
                </div>
                <div class="form-group col-md-6">
                    <label for="weight">Peso (kg)</label>
                    <input type="number" class="form-control @error('weight') is-invalid @enderror" id="weight" name="weight" value="@isset($student){{$student->weight}}@else{{ old('weight') }}@endisset">
                    @error('weight')
                    <div class="invalid-feedback">
                      {{ $message }}
                    </div>
                    @enderror
                </div>                   
            </div>

            <div class="row">
                <div class="form-group col-md-6">
                    <label for="waist_size">ancho de pecho (cm)</label>
                    <input type="number" class="form-control @error('chest_size') is-invalid @enderror" id="chest_size" name="chest_size" value="@isset($student){{$student->chest_size}}@else{{ old('chest_size') }}@endisset">
                    @error('chest_size')
                    <div class="invalid-feedback">
                      {{ $message }}
                    </div>
                    @enderror
                </div>
                <div class="form-group col-md-6">
                    <label for="waist_size">Ancho de brazo (cm)</label>
                    <input type="number" class="form-control @error('arm_size') is-invalid @enderror" id="arm_size" name="arm_size" value="@isset($student){{$student->arm_size}}@else{{ old('arm_size') }}@endisset">
                    @error('arm_size')
                    <div class="invalid-feedback">
                      {{ $message }}
                    </div>
                    @enderror
                </div>
            </div>

            <div class="row">
                <div class="form-group col-md-6">
                    <label for="waist_size">Ancho de cintura (cm)</label>
                    <input type="number" class="form-control @error('waist_size') is-invalid @enderror" id="waist_size" name="waist_size" value="@isset($student){{$student->waist_size}}@else{{ old('waist_size') }}@endisset">
                    @error('waist_size')
                    <div class="invalid-feedback">
                      {{ $message }}
                    </div>
                    @enderror
                </div>
                <div class="col-sm-4">
                    <div class="demo-switch-title">¿Usa lentes?</div>
                    <div class="switch">
                        <label>
                            <input type="checkbox" value="1"
                            @if((isset($student) && $student->glasses == '1') or (old('glasses')=='1'))
                                checked 
                            @endif
                            name="glasses" id="glasses"
                            >
                            <span class="lever switch-col-green"></span>
                        </label>
                    </div>
                </div>
            </div>

            <div class="form-group">
                <label for="blood_type">Tipo de sangre</label>
                <select class="form-control @error('blood_type') is-invalid @enderror" id="blood_type" name="blood_type" >
                    @foreach ($bloodTypes->get() as $index => $bloodType)
                    <option value="{{ $bloodType }}" @if((isset($student) && $student->blood_type == $bloodType) or (old('blood_type')==$index)) selected @endif  >{{ $bloodType }}</option>
                    @endforeach                 
                </select>
                @error('blood_type')
                <div class="invalid-feedback">
                  {{ $message }}
                </div>
                @enderror
            </div>
                
            <h3>Información de la salud del estudiante</h3>
            <h4>Indique si el estudiante padece alguno de los siguientes compromisos:</h4>
            <div class="row pt-4 pb-4">
                <div class="col-sm-3">
                    <div class="demo-switch-title">Motor</div>
                    <div class="switch">
                        <label>
                            <input type="checkbox" value="1"
                            @if((isset($student) && $student->motor_deficiency == '1') or (old('motor_deficiency')=='1'))
                                checked 
                            @endif
                            name="motor_deficiency" id="motor_deficiency"
                            >
                            <span class="lever switch-col-green"></span>
                        </label>
                    </div>
                </div>
                <div class="col-sm-3">
                    <div class="demo-switch-title">Cognitivo</div>
                    <div class="switch">
                        <label>
                            <input type="checkbox" value="1" 
                            @if((isset($student) && $student->intellectual_deficiency == '1') or (old('intellectual_deficiency')=='1'))
                                checked 
                            @endif
                            name="intellectual_deficiency" id="intellectual_deficiency"
                            >
                            <span class="lever switch-col-green"></span>
                        </label>
                    </div>
                </div>
                <div class="col-sm-3">
                    <div class="demo-switch-title">Auditivo</div>
                    <div class="switch">
                        <label>
                            <input type="checkbox" value="1" 
                            @if((isset($student) && $student->hearing_impairment == '1') or (old('hearing_impairment')=='1'))
                                checked 
                            @endif
                            name="hearing_impairment" id="hearing_impairment"
                            >
                            <span class="lever switch-col-green"></span>
                        </label>
                    </div>
                </div>
                <div class="col-sm-3">
                    <div class="demo-switch-title">Visual</div>
                    <div class="switch">
                        <label>
                            <input type="checkbox" value="1" 
                            @if((isset($student) && $student->visual_deficiency == '1') or (old('visual_deficiency')=='1'))
                                checked 
                            @endif
                            name="visual_deficiency" id="visual_deficiency"
                            >
                            <span class="lever switch-col-green"></span>
                        </label>
                    </div>
                </div>                                               
            </div>

            <div class="row pt-4 pb-4">
                <div class="col-sm-12">
                    <div class="demo-switch-title">Marque si posee informe médico de los compromisos mencionados</div>
                    <div class="switch">
                        <label>
                            <input type="checkbox" value="1"
                            @if((isset($student) && $student->medical_report_special == '1') or (old('medical_report_special')=='1'))
                                checked 
                            @endif
                            name="medical_report_special" id="medical_report_special"
                            >
                            <span class="lever switch-col-green"></span>
                        </label>
                    </div>
                </div>
            </div>


            <h3>Otras condiciones de salud:</h3>    

            <div class="row pb-4">
                <div class="col-sm-6">
                    <div class="demo-switch-title">¿Ha sido intervenido quirurgicamente?</div>
                    <div class="switch">
                        <label>
                            <input type="checkbox" value="1"
                            @if((isset($student) && $student->surgical_intervention == '1') or (old('surgical_intervention')=='1'))
                                checked 
                            @endif
                            name="surgical_intervention" id="surgical_intervention"
                            >
                            <span class="lever switch-col-green"></span>
                        </label>
                    </div>
                </div>
                <div class="col-sm-6">
                    <div class="demo-switch-title">¿Tiene impedimento para practicar Educación Física?</div>
                    <div class="switch">
                        <label>
                            <input type="checkbox" value="1"
                            @if((isset($student) && $student->practice_sports == '1') or (old('practice_sports')=='1'))
                                checked 
                            @endif
                            name="practice_sports" id="practice_sports"
                            >
                            <span class="lever switch-col-green"></span>
                        </label>
                    </div>
                </div>
            </div>

            <div class="row pb-4">
                <div class="col-sm-3">
                    <div class="demo-switch-title">Problemas respiratorios</div>
                    <div class="switch">
                        <label>
                            <input type="checkbox" value="1" 
                            @if((isset($student) && $student->respiratory_deficiency == '1') or (old('respiratory_deficiency')=='1'))
                                checked 
                            @endif
                            name="respiratory_deficiency" id="respiratory_deficiency"
                            >
                            <span class="lever switch-col-green"></span>
                        </label>
                    </div>
                </div>
                <div class="col-sm-3">
                    <div class="demo-switch-title">Diabetes</div>
                    <div class="switch">
                        <label>
                            <input type="checkbox" value="1" 
                            @if((isset($student) && $student->diabetes == '1') or (old('diabetes')=='1'))
                                checked 
                            @endif
                            name="diabetes" id="diabetes"
                            >
                            <span class="lever switch-col-green"></span>
                        </label>
                    </div>
                </div>
                <div class="col-sm-3">
                    <div class="demo-switch-title">Epilepsia</div>
                    <div class="switch">
                        <label>
                            <input type="checkbox" value="1" 
                            @if((isset($student) && $student->epilepsy == '1') or (old('epilepsy')=='1'))
                                checked 
                            @endif
                            name="epilepsy" id="epilepsy"
                            >
                            <span class="lever switch-col-green"></span>
                        </label>
                    </div>
                </div>                            
            </div>

            <div class="form-group">
                <label for="allergic">Si el estudiante es alérgico por favor describa:</label>
                <input type="text" class="form-control @error('allergic') is-invalid @enderror" id="allergic" name="allergic" value="@isset($student){{$student->allergic}}@else{{ old('allergic') }}@endisset">
                @error('allergic')
                <div class="invalid-feedback">
                  {{ $message }}
                </div>
                @enderror
            </div>

            <div class="form-group">
                <label for="health_problem">Describa si el estudiante padece o ha padecido alguna otra enfermedad</label>
                <input type="text" class="form-control @error('health_problem') is-invalid @enderror" id="health_problem" name="health_problem" value="@isset($student){{$student->health_problem}}@else{{ old('health_problem') }}@endisset">
                @error('health_problem')
                <div class="invalid-feedback">
                  {{ $message }}
                </div>
                @enderror
            </div>

            <div class="row pb-4">
                <div class="col-sm-6">
                    <div class="demo-switch-title">Marque si posee informe médico de alguna enfermdad</div>
                    <div class="switch">
                        <label>
                            <input type="checkbox" value="1"
                            @if((isset($student) && $student->medical_report == '1') or (old('medical_report')=='1'))
                                checked 
                            @endif
                            name="medical_report" id="medical_report"
                            >
                            <span class="lever switch-col-green"></span>
                        </label>
                    </div>
                </div>
                <div class="col-sm-6">
                    <div class="demo-switch-title">¿Recibe tratamiento médico en su horario de clases?</div>
                    <div class="switch">
                        <label>
                            <input type="checkbox" value="1"
                            @if((isset($student) && $student->medical_treatment == '1') or (old('medical_treatment')=='1'))
                                checked 
                            @endif
                            name="medical_treatment" id="medical_treatment"
                            >
                            <span class="lever switch-col-green"></span>
                        </label>
                    </div>
                </div>
            </div>

            <div class="form-group">
                <label for="medicine">En caso de requerir algún medicamento por favor indique:</label>
                <input type="text" class="form-control @error('medicine') is-invalid @enderror" id="medicine" name="medicine" value="@isset($student){{$student->medicine}}@else{{ old('medicine') }}@endisset">
                @error('medicine')
                <div class="invalid-feedback">
                  {{ $message }}
                </div>
                @enderror
            </div>   

            <div class="form-group">
                <label for="health_insurance">Si el estudiante posee seguro médico por favor describa:</label>
                <input type="text" class="form-control @error('health_insurance') is-invalid @enderror" id="health_insurance" name="health_insurance" value="@isset($student){{$student->health_insurance}}@else{{ old('health_insurance') }}@endisset">
                @error('health_insurance')
                <div class="invalid-feedback">
                  {{ $message }}
                </div>
                @enderror
            </div>           

            <h3>Grado o Año que cursará el estudiante</h3>    

            <div class="row">
                <div class="form-group col-md-6">
                    <label for="grade">Nivel / Grado</label>
                    <select class="form-control @error('grade') is-invalid @enderror" id="grade" name="grade" >
                        <option value="">Seleccione</option>
                        @foreach ($levels as $level)
                            <optgroup label="{{ $level->name }}">
                                @foreach ($level->grades as $grade)
                                    <option @if((isset($student) && $student->grade == $grade->name) or (old('grade')== $grade->name )) selected @endif value="{{ $grade->name }}">{{ $grade->name }}</option>
                                @endforeach
                            </optgroup>
                        @endforeach
                    </select>
                    @error('grade')
                    <div class="invalid-feedback">
                      {{ $message }}
                    </div>
                    @enderror
                </div>
            </div>
            
            <div class="row">
                <div class="col-md-6">
                    <button type="submit" class="btn btn-success waves-effect waves-light m-r-10">Guardar</button>
                </div>
                <div class="col-md-6">
                    <a href="{{ url()->previous() }}" class="btn btn-inverse waves-effect waves-light">Cancelar</a>                
                </div>
            </div>
        </div>
    </div>
            </form>
</div>
@endsection

@section('extra-js')
    <script src="{{asset('themeforest/main/js/dropify.min.js')}}"></script>
    <script>
    $(document).ready(function() {
        // Translated
        $('.dropify').dropify({
            messages: {
                default: 'Arrastra aquí un archivo o haz clic para subirlo',
                replace: 'Arrastra aquí un archivo o haz clic para reemplazarlo',
                remove: 'Eliminar',
                error: 'Lo siento, el archivo es demasiado grande'
            },
            tpl: {
                wrap:            '<div class="dropify-wrapper"></div>',
                loader:          '<div class="dropify-loader"></div>',
                message:         '<div class="dropify-message"><span class="file-icon" /> <h4>Arrastra aquí un archivo o haz clic para subirlo</h4></div>',
                preview:         '<div class="dropify-preview"><span class="dropify-render"></span><div class="dropify-infos"><div class="dropify-infos-inner"><p class="dropify-infos-message">Puede cambiar esta imagen haciendo click aquí o arrastrando la nueva imagen</p></div></div></div>',
                filename:        '<p class="dropify-filename"><span class="file-icon">Imagen Actual</span></p>',
                clearButton:     '<button type="button" class="dropify-clear">Eliminar</button>',
                errorLine:       '<h4 class="dropify-error"></h4>',
                errorsContainer: '<div class="dropify-errors-container"><ul></ul></div>'
            },
            error: {
                'fileSize': 'El tamaño del archivo es muy grande (3mb max).',
                'minWidth': 'el ancho de la imagen es muy pequeño (200px min).',
                'maxWidth': 'el ancho de la imagen es muy grande (200px max).',
                'minHeight': 'el alto de la imagen es muy pequeño (1000px min).',
                'maxHeight': 'el alto de la imagen es muy grande (1000px max).',
                'imageFormat': 'El formato de imagen no está permitido, solo (jpg, jpeg, png).'
            }
        });

        // Used events
        var drEvent = $('#input-file-events').dropify();

        drEvent.on('dropify.beforeClear', function(event, element) {
            return confirm("Realmente desea borrar \"" + element.file.name + "\" ?");
        });

        drEvent.on('dropify.afterClear', function(event, element) {
            alert('Archivo borrado');
        });

        drEvent.on('dropify.errors', function(event, element) {
            console.log('Tiene errores');
        });

        var drDestroy = $('#input-file-to-destroy').dropify();
        drDestroy = drDestroy.data('dropify')
        $('#toggleDropify').on('click', function(e) {
            e.preventDefault();
            if (drDestroy.isDropified()) {
                drDestroy.destroy();
            } else {
                drDestroy.init();
            }
        })
    });
    </script>
@endsection
