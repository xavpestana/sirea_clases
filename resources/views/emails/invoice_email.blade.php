@extends('layouts.invoice_mail')

@section('content')
@php
    $change_usd = $payment->change_amount_USD;
    $change_cop = $payment->change_amount_COP;
    $monto_total = $payment->amount_bsf;
    if($invoiceSetting->iva == true){
       $iva = $monto_total*0.16;
       $sub = $monto_total - $iva;
    }
@endphp
<table width="500" border="0" align="center" cellpadding="0" cellspacing="0" class="small-tweet-template" style="font-family: 'Helvetica Neue', Helvetica, Arial, sans-serif; background-color: white; table-layout: fixed;">
    <tbody>
        <tr>
            <td colspan="2" class="small-tweet-template-text" style="word-wrap: break-word; font-size: 12px; line-height: 18px; color: #333;">
                <h3>Datos del estudiante</h3>
                <h4>Identificacion: <span>{{ $user['document_type'] }} - {{ $user['document_number'] }}</span></h4>
                <h4>Nombre y Apellido: <span>{{ $user['first_name_1'] }} {{ $user['first_name_2'] }} {{ $user['last_name_1'] }} {{ $user['last_name_2'] }}</span></h4>
                <h4>Nacimiento: <span>{{ $user['birthdate'] }}</span> / Teléfono: <span>{{ $user['phone'] }}</span></h4>
                <h4>Grado: <span>{{ $user['grade'] }}</span></h4>

            </td>
        </tr>
        <tr>
            <td colspan="2" class="small-tweet-template-text" style="word-wrap: break-word; font-size: 12px; line-height: 18px; color: #333;">
                <p>Nombre o razón social: {{ $payment->name }} </p>
                <p>Direccón: {{ $payment->address }} </p>
                <p>RIF O C.I: {{ $payment->identity }} / Teléfono: {{ $payment->phone }} / Correo: {{ $user['email'] }}</p>
            </td>
            <td class="small-tweet-template-text" style="word-wrap: break-word; font-size: 12px; line-height: 18px; color: #333;">
                 <h4 style="color: red">N° de recibo <span>{{ $payment->number_invoice }}</span></h4>
                 <h4>Fecha <span>{{ $payment->created_at->format('d-M-Y') }}</span></h4>
                 <h4>Status <strong>Cancelado</strong></h4>
            </td>
        </tr>
        <tr>
            <td>
                <table width="500" border="0" align="center" cellpadding="0" cellspacing="0" class="small-tweet-template" style="font-family: 'Helvetica Neue', Helvetica, Arial, sans-serif; background-color: white; table-layout: fixed;">
                    <tbody>
                        <thead>
                            <td colspan="2" style="word-wrap: break-word; font-size: 12px; line-height: 18px; color: #333; border: 1px solid #000; padding: 2%;"><strong>Concepto</strong></td>
                            <td style="word-wrap: break-word; font-size: 12px; line-height: 18px; color: #333; border: 1px solid #000; padding: 2%;"><strong>Vencimiento</strong></td>
                            <td style="word-wrap: break-word; font-size: 12px; line-height: 18px; color: #333; border: 1px solid #000; padding: 2%;"><strong>Monto Unitario</strong></td>
                            <td style="word-wrap: break-word; font-size: 12px; line-height: 18px; color: #333; border: 1px solid #000; padding: 2%;"><strong>Cantidad</strong></td>
                            <td style="word-wrap: break-word; font-size: 12px; line-height: 18px; color: #333; border: 1px solid #000; padding: 2%;"><strong>Total</strong></td>
                        </thead>
        @foreach ($installments as $installment)
        @php
        $amount = $installment->Installment->amount;
        
            if($installment->Installment->coin == 'Bsf'){
                $total = $amount * 1;
            }
            if($installment->Installment->coin == 'USD'){
                $total = $amount * $change_usd;
            }
            if($installment->Installment->coin == 'COP'){
                $total = $amount * $change_cop;
            }
        @endphp
            <tr>
            <td colspan="2" class="small-tweet-template-text" style="word-wrap: break-word; font-size: 12px; line-height: 18px; color: #333; border: 1px solid #000; padding: 2%;">
                <p>{{ $installment->Installment->description }} </p>
            </td>
            <td class="small-tweet-template-text" style="word-wrap: break-word; font-size: 12px; line-height: 18px; color: #333; border: 1px solid #000; padding: 2%;">
                 <p>
                    @if(!is_null($installment->Installment->expire_at))
                    {{ $installment->Installment->expire_at->format('d-M-Y') }}
                 @else
                    No Aplica
                @endif</p>
            </td>
            <td class="small-tweet-template-text" style="word-wrap: break-word; font-size: 12px; line-height: 18px; color: #333; border: 1px solid #000; padding: 2%;">
                 <p>{{ $total }}</p>
            </td>
            <td class="small-tweet-template-text" style="word-wrap: break-word; font-size: 12px; line-height: 18px; color: #333; border: 1px solid #000; padding: 2%;">
                 <p>1</p>
            </td>
            <td class="small-tweet-template-text" style="word-wrap: break-word; font-size: 12px; line-height: 18px; color: #333; border: 1px solid #000; padding: 2%;">
                 <p>{{ $total }}</p>
            </td>
        </tr>
        @endforeach
        <tr>
            <td colspan="2" class="small-tweet-template-text" style="word-wrap: break-word; font-size: 12px; line-height: 18px; color: #333; border: 1px solid #000; padding: 2%;">
                <p>Abono en la cuenta de:  {{ $user['first_name_1'] }} {{ $user['first_name_2'] }} {{ $user['last_name_1'] }} {{ $user['last_name_2'] }}</p>
            </td>
            <td class="small-tweet-template-text" style="word-wrap: break-word; font-size: 12px; line-height: 18px; color: #333; border: 1px solid #000; padding: 2%;">
                 No aplica
            </td>
            <td class="small-tweet-template-text" style="word-wrap: break-word; font-size: 12px; line-height: 18px; color: #333; border: 1px solid #000; padding: 2%;">
                @php
                $abono = $payment->credit;
            if($payment->coin_credit == 'Bsf'){
                $abono = $abono * 1;
            }
            if($payment->coin_credit == 'USD'){
                $abono = $abono * $change_usd;
            }
            if($payment->coin_credit == 'COP'){
                $abono = $abono * $change_cop;
            }
                @endphp
                 <p>{{ $abono }} Bsf</p>
            </td>
            <td class="small-tweet-template-text" style="word-wrap: break-word; font-size: 12px; line-height: 18px; color: #333; border: 1px solid #000; padding: 2%;">
                 <p>1</p>
            </td>
            <td class="small-tweet-template-text" style="word-wrap: break-word; font-size: 12px; line-height: 18px; color: #333; border: 1px solid #000; padding: 2%;">
                 <p>{{ $abono }}</p>
            </td>
        </tr>
        <tr>
            <td colspan="4" class="small-tweet-template-text" style="word-wrap: break-word; font-size: 12px; line-height: 18px; color: #333; border: 1px solid #000; padding: 2%;">
                <h4>Metodo de pago: {{ $payment->paymentMethod->name }}</h4>
                @if(!is_null($payment->observation))
                <h4>Observacion: </h4><p>{{ $payment->observation }}</p>
                @endif
            </td>
            <td class="small-tweet-template-text" style="word-wrap: break-word; font-size: 12px; line-height: 18px; color: #333; border: 1px solid #000; padding: 2%;">
                @if($invoiceSetting->iva == true)
                 <p>IVA 16%</p>
                @endif
            </td>
            <td class="small-tweet-template-text" style="word-wrap: break-word; font-size: 12px; line-height: 18px; color: #333; border: 1px solid #000; padding: 2%;">
                @if($invoiceSetting->iva == true)
                 <p>{{ $iva }} Bsf</p>
                @endif
            </td>
        </tr>
        @if($invoiceSetting->iva == true)
        <tr>
            <td colspan="4" class="small-tweet-template-text" style="word-wrap: break-word; font-size: 12px; line-height: 18px; color: #333; padding: 2%;">
                
            </td>
            <td class="small-tweet-template-text" style="word-wrap: break-word; font-size: 12px; line-height: 18px; color: #333; border: 1px solid #000; padding: 2%;">
                <p>Sub-Total</p>
            </td>
            <td class="small-tweet-template-text" style="word-wrap: break-word; font-size: 12px; line-height: 18px; color: #333; border: 1px solid #000; padding: 2%;">
                <p>{{ $sub }} Bsf</p>
            </td>
        </tr>
        @endif
        <tr>
            <td colspan="4" class="small-tweet-template-text" style="word-wrap: break-word; font-size: 12px; line-height: 18px; color: #333; padding: 2%;">
                
            </td>
            <td class="small-tweet-template-text" style="word-wrap: break-word; font-size: 12px; line-height: 18px; color: #333; border: 1px solid #000; padding: 2%;">
                <p>Total</p>
            </td>
            <td class="small-tweet-template-text" style="word-wrap: break-word; font-size: 12px; line-height: 18px; color: #333; border: 1px solid #000; padding: 2%;">
                <p>{{ $monto_total }} Bsf</p>
            </td>
        </tr>
        <tr>
            <td colspan="4" class="small-tweet-template-text" style="word-wrap: break-word; font-size: 12px; line-height: 18px; color: #333;">
                <p>Puede descargar su factura en PDF desde la aplicacion: {{ $appUrl }} </p>
            </td>
        </tr>
                    </tbody>
                </table>
            </td>
        </tr>
    </tbody>
</table>
@endsection
