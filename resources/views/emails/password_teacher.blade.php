@extends('layouts.mail')

@section('content')
<h1>Bienvenido a Sirea de Clases It</h1>

<p>Su Contraseña como docente ha sido cambiado:</p>

<ul>
    <li>Contraseña: {{ $datos['password'] }}</li>
</ul>
</p>

<p>Este correo es enviado de manera automática por la aplicación Sirea de Clases It.</p>
@endsection
