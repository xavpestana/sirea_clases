@extends('layouts.mail')

@section('content')

<h1>Bienvenido a {{ $appName }}</h1>

<p>Se ha registrado un usuario en la plataforma con las siguientes credenciales de acceso:</p>

<ul>
    <li>Usuario: {{ $user->email }}</li>
    <li>Contraseña: {{ $password }}</li>
</ul>

<p>Es necesario que los estudiantes posean cuenta de correo electrónico, si no la tienen es necesario crearla. Al acceder como cuenta familia al sistema Sirea de su colegio debe llenar o actualizar toda los datos correspondintes a su familia y sus representados, con la finalidad de poder generar las planillas y documentos respectivos.</p>

<p>Para acceder visite la URL {{ $appUrl }} e indique sus credenciales de acceso.</p>

<p>
    <strong>NOTA:</strong> Una vez acceda al sistema, se recomienda modificar la contraseña generada.
</p>

<p>Este correo es enviado de manera automática por la aplicación {{ $appName }}.</p>

@endsection
