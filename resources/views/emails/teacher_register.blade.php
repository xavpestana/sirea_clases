@extends('layouts.mail')

@section('content')

<h1>Bienvenido a Sirea de Clases It</h1>

<p>Se ha registrado como docente en la plataforma con las siguientes credenciales de acceso:</p>

<ul>
    <li>Usuario: {{ $datos['email'] }}</li>
    <li>Contraseña: {{ $datos['password'] }}</li>
</ul>

    <strong>NOTA:</strong> Una vez acceda al sistema, se recomienda modificar la contraseña generada.
</p>

<p>Este correo es enviado de manera automática por la aplicación Sirea de Clases It.</p>

@endsection
