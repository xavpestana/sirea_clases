@extends('layouts.mail')

@section('content')

<h2 style="line-height: 32px;" align="center">Pasos para formalizar la inscripción al<br>
Año escolar 2020-2021.
</h2>

<p>Bendecido día, es un placer para todo el equipo del Colegio <strong>“Martin Luther King”</strong> darle la bienvenida al programa educativo Escuela del Futuro, donde juntos <strong>Familia y Colegio</strong> nos unimos en compromiso para <strong>Edificar vidas con propósito</strong>, desde el recate de la práctica de los valores.</p>

<p>Hemos recibo con gran agradado y para la gloria de Dios, su preinscripción donde se le remitió a su correo electrónico la información de los programas de pago  que les ofrecemos para su comodidad, junto a  las coordenadas o información para ingresar a la Plataforma online Colegio MLK,  allí registró tanto los datos de su núcleo familiar como los del estudiante, el siguiente paso es <strong>formalizar su inscripción</strong> para ello se le invita a realizar este procedimiento:</p>

<ul>
    <li>Leer detenidamente el <strong>Código de ética</strong> adjunto a este correo.</li>
    <li>Imprimir la última hoja del Código de ética, firmar y escanear.</li>
    <li>Escanear en archivos por separado los requisitos que a continuación se enuncia:</li>
    <ul>
    	<li>Cedula de identidad de los representantes.</li>
    	<li>Cedula de identidad del estudiante.</li>
    	<li>Partida de nacimiento del estudiante.</li>
    	<li>Solvencia económica.</li>
    	<li>Informe de promoción de grado o Boletín de notas.</li>
    </ul>
    <li>Comprobante de pago de la inscripción del año <strong>2020-2021</strong>.</li>
</ul>



<p>
    Recuerde por favor que los documentos deben estar legibles y si en caso de no contar con un scanner, pueden hacer uso de la siguiente aplicación gratuita para celulares Android, es decir, que la puede instalar en su celular inteligente.
</p>
<p>
<a href="https://play.google.com/store/apps/details?id=pdf.tap.scanner" title="app scanner">Scanner App to PDF: TapScanner</a>
</p>

<p>Esos documentos deben ser enviados al correo colegiomlkmerida@gmail.com, de igual manera se le recuerda completar todos los datos de la plataforma Colegio MLK.</p>
<p>Ante cualquier duda pueden comunicarse al 
0426-3716161.
</p>
<p>Este correo es enviado de manera automática por la aplicación {{ $appName }}.</p>

@endsection
