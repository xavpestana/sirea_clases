@component('mail::message')
<table>
  <tr>
    <td><img src="{{ asset('storage/pictures/logos/'.$school->logo) }}" width="100px" alt=""></td>
    <td>{{ $school->school_type }} {{ $school->name }}</td>
  </tr>
</table>
<h1>{{ $subject }}</h1>
<body>
{!! $body !!}
</body>
{{ config('app.name') }}
@endcomponent
