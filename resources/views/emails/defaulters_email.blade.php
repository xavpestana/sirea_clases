@extends('layouts.invoice_mail')

@section('content')

<table width="500" border="0" align="center" cellpadding="0" cellspacing="0" class="small-tweet-template" style="font-family: 'Helvetica Neue', Helvetica, Arial, sans-serif; background-color: white; table-layout: fixed;">
    <tbody>
    	<tr>
    		<td colspan="2" class="small-tweet-template-text" style="word-wrap: break-word; font-size: 12px; line-height: 18px; color: #333;">
    			<h3>Datos del estudiante</h3>
    			<h4>Identificacion: <span>{{ $student->profile->document_type }} - {{ $student->profile->document_number }}</span></h4>
    			<h4>Nombre y Apellido: <span>{{ $student->profile->first_name_1 }} {{ $student->profile->first_name_2 }} {{ $student->profile->last_name_1 }} {{ $student->profile->last_name_2 }}</span></h4>
    			<h4>Nacimiento: <span>{{ $student->profile->birthdate }}</span> / Teléfono: <span>{{ $student->profile->phone }}</span></h4>
                <h4>Grado: <span>{{ $student->grade }}</span></h4>

    		</td>
    	</tr>
    	<tr>
    		<td colspan="2" class="small-tweet-template-text" style="word-wrap: break-word; font-size: 12px; line-height: 18px; color: #333;">
    			<p>Familia: {{ $student->family->last_name_1 }} {{ $student->family->last_name_2 }} </p>
    			<p>Enviamos este mensaje para notificarle el atraso de pago en las cuotas que se describen a continuacion, le pedimos se pueda colocar al dia con los pagos correspondiente</p>
    		</td>
        	<td class="small-tweet-template-text" style="word-wrap: break-word; font-size: 12px; line-height: 18px; color: #333;">
            	 <h4 style="color: red">N° de recibo <span class="text-danger">No pagada</span></h4>
            	 <h4>Status <strong><span class="text-danger">Factura vencida</span></strong></h4>
        	</td>
    	</tr>
    	<tr>
    		<td>
    			<table width="500" border="0" align="center" cellpadding="0" cellspacing="0" class="small-tweet-template" style="font-family: 'Helvetica Neue', Helvetica, Arial, sans-serif; background-color: white; table-layout: fixed;">
    				<tbody>
    					<thead>
    						<td colspan="2" style="word-wrap: break-word; font-size: 12px; line-height: 18px; color: #333; border: 1px solid #000; padding: 2%;"><strong>Concepto</strong></td>
    						<td style="word-wrap: break-word; font-size: 12px; line-height: 18px; color: #333; border: 1px solid #000; padding: 2%;"><strong>Vencimiento</strong></td>
    						<td style="word-wrap: break-word; font-size: 12px; line-height: 18px; color: #333; border: 1px solid #000; padding: 2%;"><strong>Monto Unitario</strong></td>
    						<td style="word-wrap: break-word; font-size: 12px; line-height: 18px; color: #333; border: 1px solid #000; padding: 2%;"><strong>Cantidad</strong></td>
    						<td style="word-wrap: break-word; font-size: 12px; line-height: 18px; color: #333; border: 1px solid #000; padding: 2%;"><strong>Total</strong></td>
    					</thead>
    	@foreach ($defaulters as $defaulter)
    		<tr>
    		<td colspan="2" class="small-tweet-template-text" style="word-wrap: break-word; font-size: 12px; line-height: 18px; color: #333; border: 1px solid #000; padding: 2%;">
    			<p>{{ $defaulter->description }} </p>
    		</td>
        	<td class="small-tweet-template-text" style="word-wrap: break-word; font-size: 12px; line-height: 18px; color: #333; border: 1px solid #000; padding: 2%;">
            	 <p>
            	 	@if(!is_null($defaulter->expire_at))
            	 	{{ $defaulter->expire_at->format('d-M-Y') }}
            	 @else
            		No Aplica
            	@endif</p>
        	</td>
        	<td class="small-tweet-template-text" style="word-wrap: break-word; font-size: 12px; line-height: 18px; color: #333; border: 1px solid #000; padding: 2%;">
            	 <p>{{ $defaulter->amount }} {{ $defaulter->coin }}</p>
        	</td>
        	<td class="small-tweet-template-text" style="word-wrap: break-word; font-size: 12px; line-height: 18px; color: #333; border: 1px solid #000; padding: 2%;">
            	 <p>1</p>
        	</td>
        	<td class="small-tweet-template-text" style="word-wrap: break-word; font-size: 12px; line-height: 18px; color: #333; border: 1px solid #000; padding: 2%;">
            	 <p>{{ $defaulter->amount }} {{ $defaulter->coin }}</p>
        	</td>
    	</tr>
    	@endforeach
    	<tr>
    		<td colspan="4" class="small-tweet-template-text" style="word-wrap: break-word; font-size: 12px; line-height: 18px; color: #333;">
    			<p>Si hay algun error en el mensaje anterior, comuniquese con el colegio</p>
    		</td>
    	</tr>
    				</tbody>
    			</table>
    		</td>
    	</tr>
    </tbody>
</table>
@endsection
