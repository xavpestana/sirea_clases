            <div class="modal-body">
                <div class="row">
                    <div class="form-group col-md-12">
                        <label for="document">Nombre de Asignatura o curso <span style="color: red">*</span></label>
                        <input type="text" class="form-control" id="name" name="name" value="{{ $course->name  }}" required="">
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label>Grado</label>
                            <input type="text" class="form-control" id="grade" value="{{ $course->grade->name  }}" required="" readonly="">
                            <input type="text" value="{{ $course->id }}" id="id_course" name="id_course" hidden="">
                            <input type="text" value="{{ $course->courses_sections_user[0]->section->id }}" id="id_section" name="id_section" hidden="">
                            <input type="text" value="{{ $course->grade_id }}" id="id_grade" name="id_grade" hidden="">
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label>Sección</label>
                            <input type="text" class="form-control" id="section" value="{{ $course->courses_sections_user[0]->section->name }}" required="" readonly="">
                        </div>
                    </div>
                    <div class="col-md-10 mx-auto">
                            <label>Docentes a cargo  <span style="color: red">*</span></label>
                            <select id='updt-options' multiple='multiple' name="teacher[]" required="">
                                @foreach($teachers as $teacher)
                                <option value='{{ $teacher->profile->user_id }}'
                                	@foreach($course->courses_sections_user as $teacher_selected)
                                    	@if ($teacher->profile->user_id == $teacher_selected->user->id)
                                    		selected=""
                                    		@break
                                    	@endif
                                	@endforeach
                                	>{{ $teacher->profile->first_name_1 }} {{ $teacher->profile->last_name_1 }}</option>
                                @endforeach
                            </select>
                            <div class="button-box m-t-20"> <a id="select-all-updt" class="btn btn-danger" href="#">Seleccionar todos</a> <a id="deselect-all-updt" class="btn btn-success" href="#">Deseleccionar todos</a> </div>
                    </div>
                </div>
            </div>
            <script>
            	$('#updt-options').multiSelect();
            	$('#select-all-updt').click(function() {
            $('#updt-options').multiSelect('select_all');
            return false;
        });
        $('#deselect-all-updt').click(function() {
            $('#updt-options').multiSelect('deselect_all');
            return false;
        });
            </script>