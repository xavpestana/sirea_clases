<div class="table-responsive">
                <table id="docente" class="display nowrap" cellspacing="0" width="100%">
                    <thead>
                        <tr>
                            <th>Nombre</th>
                            <th>Grado</th>
                            <th>Seccion</th>
                            <th>Profesores a cargo:</th>
                            <th>Acciones</th>
                        </tr>
                    </thead>
                     @foreach ($courses as $course)
                         
                            @if($course->Courses_sections_user->first()->Section->season_id == $seasons->id)
                        <tr>
                            <td>
                                <span class="mytooltip tooltip-effect-1">
                                        {{ $course->name  }}
                                </span>                                
                            </td>
                            <td>{{ $course->grade->name }}</td>
                            <td>{{ $course->courses_sections_user[0]->section->name }}</td>
                            <td>
                                <ul>
                                @foreach($course->courses_sections_user as $teacher)
                                    <li>{{ $teacher->user->name }}</li>
                                @endforeach
                                </ul>
                            </td>
                            <td>
                                <button type="button" class="btn btn-info btn-circle" data-toggle="modal" data-target="#editModal" data-whatever="@getbootstrap" onclick="edit_subjects('{{ $course->id  }}')"><i class="fa fa-pencil"></i> </button>
                                <button type="button" class="btn btn-danger btn-circle" onclick="delete_subjects('{{ $course->id  }}')"><i class="fa fa-times"></i> </button>
                            </td>                               
                        </tr>
                        @endif 
                        @endforeach
                    <tbody>
                       
                    </tbody>
                </table>
            </div>
            <script>
            $(document).ready(function() {
                $('#docente').DataTable( {
                    dom: 'Bfrtip',
                    buttons: [
                    'csv', 'excel', 'pdf', 'print'
                    ]
                });
            });
            </script>