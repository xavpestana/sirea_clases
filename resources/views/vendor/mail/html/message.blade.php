@component('mail::layout')
{{-- Header --}}
@slot('header')
@component('mail::header', ['url' => config('app.url')])
{{-- config('app.name') --}}
<img src="{{asset('themeforest/assets/images/sirea-logo.png')}}" alt="{{config('app.name')}}">
@endcomponent
@endslot

{{-- Body --}}
{{ $slot }}

{{-- Subcopy --}}
@isset($subcopy)
@slot('subcopy')
@component('mail::subcopy')
{{ $subcopy }}
@endcomponent
@endslot
@endisset

{{-- Footer --}}
@slot('footer')
@component('mail::footer')
{{-- © {{ date('Y') }} {{ config('app.name') }}. @lang('All rights reserved.') --}}

© {{ date('Y') }} Clases IT. @lang('All rights reserved.')<br/>
<img src="{{asset('themeforest/assets/images/clasesit-logo.png')}}" alt="{{config('app.name')}}">
@endcomponent
@endslot
@endcomponent
