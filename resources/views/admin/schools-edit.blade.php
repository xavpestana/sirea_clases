@extends('layouts.app')

@section('extra-css')
    <link rel="stylesheet" href="{{asset('themeforest/main/css/dropify.min.css')}}">
@endsection

@section('content')

@inject('states', 'App\Services\States')
@inject('genders', 'App\Services\Genders')
<div class="row">
    @isset($school)
    <form class="form-horizontal m-t-40" method="post" action="@role('school'){{ route('school.update', $school) }}@else{{ route('schools.update', $school) }}@endrole"  enctype="multipart/form-data">
        @method('patch')
    @else
    <form class="form-horizontal m-t-40" method="post" action="{{ route('schools.store') }}" enctype="multipart/form-data">
    @endisset
        <div class="col-lg-12 col-md-12">
            <div class="card">
                <div class="card-body @error('logo') is-invalid @enderror">
                    <h4 class="card-title">Escudo de la Institución</h4>
                    <label for="input-file-now-custom-1">La imagen debe ser el escudo del colegio preferiblemente fondo transparente, los formatos deben ser .jpg, .jpeg. o .png. Tamaño máximo 5mb</label>
                    @isset($school) @php($logo = $school->logo)@else @php($logo="default.jpg") @endisset
                    <input type="file" id="logo" name="logo" class="dropify form-control " data-default-file="{{ asset('storage/pictures/logos/'.$logo) }}" data-allowed-file-extensions="jpg jpeg png" data-max-file-size="5M"/>
                </div>
            </div>
            @error('logo')
            <div class="invalid-feedback text-center">
              {{ $message }}
            </div>
            @enderror
        </div>

        <div class="col-sm-12">
            <div class="card card-body">
                <h4 class="card-title">
                    @isset($school)
                    Modificar datos de {{ $school->school_type }} {{ $school->name }}
                    @else
                    Ingresar datos para agregar nuevo colegio
                    @endisset
                </h4>
                {{--<h6 class="card-subtitle"> Información importante de la </h6>--}}
                @csrf
                <div class="row">
                    <div class="form-group col-md-4">
                        <label for="dea_code">Código DEA</label>
                        <input type="text" class="form-control" id="dea_code" name="dea_code" value="@isset($school){{ $school->dea_code }}@else{{ old('dea_code') }}@endisset">
                    </div>
                    <div class="form-group col-md-4">
                        <label for="stadistic_code">Código Estadístico</label>
                        <input type="text" class="form-control" id="stadistic_code" name="stadistic_code" value="@isset($school){{ $school->stadistic_code }}@else{{ old('stadistic_code') }}@endisset">
                    </div>
                    <div class="form-group col-md-4">
                        <label for="rif">Rif</label>
                        <input type="text" class="form-control" id="rif" name="rif" value="@isset($school){{ $school->rif }}@else{{ old('rif') }}@endisset">
                    </div>                
                </div>

                <div class="form-group">
                    <label for="school_type">Tipo de Institución</label>
                    <input type="text" class="form-control @error('school_type') is-invalid @enderror" id="school_type" name="school_type" value="@isset($school){{ $school->school_type }}@else{{ old('school_type') }}@endisset">
                    @error('school_type')
                    <div class="invalid-feedback">
                      {{ $message }}
                    </div>
                    @enderror
                </div>
                
                <div class="form-group">
                    <label for="name">Nombre de la Institución</label>
                    <input type="text" class="form-control @error('name') is-invalid @enderror" id="name" name="name" value="@isset($school){{ $school->name }}@else{{ old('name') }}@endisset">
                    @error('name')
                    <div class="invalid-feedback">
                      {{ $message }}
                    </div>
                    @enderror
                </div>

                <div class="row">
                    <div class="form-group col-md-3">
                        <label for="state">Estado</label>
                        <select class="form-control @error('state') is-invalid @enderror" id="state" name="state">
                            @foreach ($states->get() as $index => $state)
                            <option value="{{ $index }}" @if((isset($school) && $school->parish->municipality->state->id==$index) or (old('state')==$index)) selected @endif>{{ $state }}</option>
                            @endforeach
                        </select>
                        @error('state')
                        <div class="invalid-feedback">
                          {{ $message }}
                        </div>
                        @enderror                        
                    </div>

                    <div class="form-group col-md-3">
                        <label for="city">City</label>
                        <select class="form-control" id="city_id" name="city_id" data-value="@isset($school){{ $school->city_id }}@endisset">
                        </select>
                    </div>

                    <div class="form-group col-md-3">
                        <label for="municipality">Municipio</label>
                        <select class="form-control @error('municipality') is-invalid @enderror" id="municipality" name="municipality" data-value="@isset($school){{ $school->parish->municipality->id }}@endisset">
                        </select>
                        @error('municipality')
                        <div class="invalid-feedback">
                          {{ $message }}
                        </div>
                        @enderror   
                    </div>

                    <div class="form-group col-md-3">
                        <label for="parish_id">Parroquia</label>
                        <select class="form-control @error('parish_id') is-invalid @enderror" id="parish_id" name="parish_id" data-value="@isset($school){{ $school->parish_id }}@endisset">
                        </select>
                        @error('parish_id')
                        <div class="invalid-feedback">
                            {{ $message }}
                        </div>
                        @enderror 
                    </div>
                </div>

                <div class="form-group">
                    <label for="address">Dirección</label>
                    <textarea class="form-control @error('address') is-invalid @enderror" rows="5" id="address" name="address">@isset($school){{ $school->address }}@else{{ old('address') }}@endisset</textarea>
                    @error('address')
                    <div class="invalid-feedback">
                        {{ $message }}
                    </div>
                    @enderror
                </div>

                <div class="form-group">
                    <label for="phone">Teléfono</label>
                    <input type="phone" id="phone" name="phone" class="form-control @error('phone') is-invalid @enderror" value="@isset($school){{ $school->phone }}@else{{ old('phone') }}@endisset">
                    @error('phone')
                    <div class="invalid-feedback">
                        {{ $message }}
                    </div>
                    @enderror
                </div>

                <div class="form-group">
                    <label for="fax">Fax <i>(opcional)</i></label>
                    <input type="fax" id="fax" name="fax" class="form-control" value="@isset($school){{ $school->fax }}@else{{ old('fax') }}@endisset">
                </div>

                <div class="form-group">
                    <label for="email">Correo electrónico</label>
                    <input type="email" id="email" name="email" class="form-control @error('email') is-invalid @enderror" value="@isset($school){{ $school->email }}@else{{ old('email') }}@endisset">
                    @error('email')
                    <div class="invalid-feedback">
                        {{ $message }}
                    </div>
                    @enderror
                </div>

                <div class="form-group">
                    <label for="url">Sitio Web del colegio  <i>(opcional)</i></label>
                    <input type="url" id="url" name="url" class="form-control @error('url') is-invalid @enderror" value="@isset($school){{ $school->url }}@else{{ old('url') }}@endisset">
                    @error('url')
                    <div class="invalid-feedback">
                        {{ $message }}
                    </div>
                    @enderror
                </div>

                <div class="row pt-4 pb-4">
                    <div class="col-sm-6">
                        <div class="demo-switch-title">Si desea solicitar información adicional de las familias (ingresos, vivienda, transporte, aspectos sociales), habilite el botón</div>
                        <div class="switch">
                            <label>
                                <input type="checkbox" value="1"
                                @if((isset($school) && $school->additional_data == '1') or (old('additional_data')=='1'))
                                    checked 
                                @endif
                                name="additional_data" id="additional_data"
                                >
                                <span class="lever switch-col-green"></span>
                            </label>
                        </div>
                    </div>
                </div>
                
                <h3>Información del director</h3>

                <div class="form-group">
                    <label for="director_name">Nombre y Apellido del Director</label>
                    <input type="text" class="form-control @error('director_name') is-invalid @enderror" id="director_name" name="director_name" value="@isset($director){{ $director->name }}@else{{ old('director_name') }}@endisset">
                    @error('director_name')
                    <div class="invalid-feedback">
                      {{ $message }}
                    </div>
                    @enderror
                </div>

                <div class="row">
                    <div class="form-group col-md-6">
                        <label for="document_type">Tipo de documento</label>
                        <select class="form-control @error('document_type') is-invalid @enderror" id="document_type" name="document_type" >
                            <option value="">Seleccione</option>
                            <option @if((isset($director) && $director->document_type=='V' ) or (old('document_type')=='V' )) selected @endif value="V" >V </option>
                            <option @if((isset($director) && $director->document_type=='E' ) or (old('document_type')=='E' )) selected @endif value="E" >E </option>
                        </select>
                        @error('document_type')
                        <div class="invalid-feedback">
                          {{ $message }}
                        </div>
                        @enderror
                    </div>

                    <div class="form-group col-md-6">
                        <label for="document_number">Documento</label>
                        <input type="text" class="form-control @error('document_number') is-invalid @enderror" id="document_number" name="document_number" value="@isset($director){{$director->document_number}}@else{{ old('document_number') }}@endisset" placeholder="ej: 12345678" maxlength="30">
                        @error('document_number')
                        <div class="invalid-feedback">
                          {{ $message }}
                        </div>
                        @enderror
                    </div>                    
                </div>

                <div class="form-group col-md-6">
                    <label for="gender">Género</label>
                    <div class="demo-radio-button @error('gender_id') is-invalid @enderror">
                        @foreach ($genders->get() as $index => $gender)
                            <input name="gender_id" type="radio" id="{{ $gender }}" class="radio-col-green" value="{{ $index }}"
                            @if((isset($director) && $director->gender_id == $index) or (old('gender')==$index))
                                checked 
                            @endif
                            />
                            <label for="{{ $gender }}">{{ $gender }}</label>
                        @endforeach
                    </div>
                    @error('gender_id')
                    <div class="invalid-feedback">
                      {{ $message }}
                    </div>
                    @enderror
                </div>                       


    


                <div class="row">
                    <div class="col-md-6">
                    <button type="submit" class="btn btn-success waves-effect waves-light m-r-10">Guardar</button>   
                    </div>
                    <div class="col-md-6">
                    <a href="{{ url()->previous() }}" class="btn btn-inverse waves-effect waves-light">Cancelar</a>    
                    </div>
                </div>
            </div>
        </div>
    </form>
</div>
@endsection


@section('extra-js')
<script src="{{asset('themeforest/main/js/dropify.min.js')}}"></script>
<script>
$(function() {
    loadCity();
    $('#state').on('change', loadCity);
    $('#municipality').on('change', loadParish);

    function loadParish() {
        var municipality_id = $('#municipality').val();
        if($.trim(municipality_id) != ''){
            $.get('/parishes', {municipality_id: municipality_id}, function(parishes){
                $('#parish_id').empty();
                $('#parish_id').append("<option value=''>Selecciona</option>");
                $.each(parishes, function(index, value){
                    $('#parish_id').append("<option value='"+index+"'>"+value+"</option>");
                });
            });
        }
    }    

    function loadCity() {
        var state_id = $('#state').val();

        if ($.trim(state_id) != '') {
            $.get('/cities', {state_id: state_id}, function (cities) {
                var city = $('#city_id').data('value') != '' ? $('#city_id').data('value') : '';
                var oldCity = $('#city_id').data('old') != '' ? $('#city_id').data('old') : '';
                $('#city_id').empty();
                $('#city_id').append("<option value=''>Selecciona</option>");
                $.each(cities, function (index, value) {
                    $('#city_id').append("<option value='" + index + "'" + (city == index ? 'selected' : oldCity == index ? 'selected' : '') + ">" + value +"</option>");
                })
            });

            $.get('/municipalities', {state_id: state_id}, function(municipalities){
                var municipality = $('#municipality').data('value') != '' ? $('#municipality').data('value') : '';
                var oldMunicipality = $('#municipality').data('old') != '' ? $('#municipality').data('old') : '';
                $('#municipality').empty();
                $('#municipality').append("<option value=''>Selecciona</option>");
                $.each(municipalities, function(index, value){
                    $('#municipality').append("<option value='" + index + "'" + (municipality == index ? 'selected' : oldMunicipality == index ? 'selected' : '') + ">" + value +"</option>");
                });

                var municipality_id = $('#municipality').val();

                $.get('/parishes', {municipality_id: municipality_id}, function(parishes){
                    var parish = $('#parish_id').data('value') != '' ? $('#parish_id').data('value') : '';
                    var oldParish = $('#parish_id').data('old') != '' ? $('#parish_id').data('old') : '';
                    $('#parish_id').empty();
                    $('#parish_id').append("<option value=''>Selecciona</option>");
                    $.each(parishes, function(index, value){
                        $('#parish_id').append("<option value='"+ index + "'" + (parish == index ? 'selected': oldParish == index ? 'selected' : '') + ">" + value +"</option>");
                    });
                });
            });
        }
    }

    $('.dropify').dropify({
        messages: {
            default: 'Arrastra aquí un archivo o haz clic para subirlo',
            replace: 'Arrastra aquí un archivo o haz clic para reemplazarlo',
            remove: 'Eliminar',
            error: 'Lo siento, el archivo es demasiado grande'
        },
        tpl: {
            wrap:            '<div class="dropify-wrapper"></div>',
            loader:          '<div class="dropify-loader"></div>',
            message:         '<div class="dropify-message"><span class="file-icon" /> <h4>Arrastra aquí un archivo o haz clic para subirlo</h4></div>',
            preview:         '<div class="dropify-preview"><span class="dropify-render"></span><div class="dropify-infos"><div class="dropify-infos-inner"><p class="dropify-infos-message">Puede cambiar esta imagen haciendo click aquí o arrastrando la nueva imagen</p></div></div></div>',
            filename:        '<p class="dropify-filename"><span class="file-icon">Imagen Actual</span></p>',
            clearButton:     '<button type="button" class="dropify-clear">Eliminar</button>',
            errorLine:       '<h4 class="dropify-error"></h4>',
            errorsContainer: '<div class="dropify-errors-container"><ul></ul></div>'
        },
        error: {
            'fileSize': 'El tamaño del archivo es muy grande (3mb max).',
            'minWidth': 'el ancho de la imagen es muy pequeño (200px min).',
            'maxWidth': 'el ancho de la imagen es muy grande (200px max).',
            'minHeight': 'el alto de la imagen es muy pequeño (1000px min).',
            'maxHeight': 'el alto de la imagen es muy grande (1000px max).',
            'imageFormat': 'El formato de imagen no está permitido, solo (jpg, jpeg, png).'
        }
    });

    // Used events
    var drEvent = $('#input-file-events').dropify();

    drEvent.on('dropify.beforeClear', function(event, element) {
        return confirm("Do you really want to delete \"" + element.file.name + "\" ?");
    });

    drEvent.on('dropify.afterClear', function(event, element) {
        alert('File deleted');
    });

    drEvent.on('dropify.errors', function(event, element) {
        console.log('Has Errors');
    });

    var drDestroy = $('#input-file-to-destroy').dropify();
    drDestroy = drDestroy.data('dropify')
    $('#toggleDropify').on('click', function(e) {
        e.preventDefault();
        if (drDestroy.isDropified()) {
            drDestroy.destroy();
        } else {
            drDestroy.init();
        }
    })

});
</script>


@endsection
