@extends('layouts.app')

@section('content')
<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-body">
                <div class="d-flex no-block">
                    <h4 class="card-title">Listados de Colegios</h4>
                    <div class="ml-auto">
                        <a class="btn btn-primary waves-effect waves-light" href="{{ route('schools.create') }}"><span class="btn-label"><i class="mdi mdi-plus"></i></span>Agregar nuevo colegio</a>
                    </div>
                </div>
                <div class="table-responsive m-t-40">
                    <table id="myTable" class="table table-bordered table-striped">
                        <thead>
                            <tr>
                                <th>Tipo</th>
                                <th>Nombre</th>
                                <th>Acciones</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($schools as $school)
                            <tr>
                                <td style="width: 40%">{{ $school->school_type }}</td>
                                <td style="width: 40%">{{ $school->name }}</td>
                                <td style="width: 20%">
                                    <div class="btn-group" role="group" aria-label="Basic example">
                                        <button type="button" data-toggle="modal" data-target="#show-school"
                                            data-logo="{{ asset('storage/pictures/logos/'.$school->logo) }}"
                                            data-name="{{ $school->school_type }} {{ $school->name }}"
                                            data-address="{{ $school->address }}"
                                            data-phone="{{ $school->phone }}"
                                            data-email="{{ $school->email }}"
                                            data-url="{{ $school->url }}"
                                            data-parish="@isset($school->parish){{ $school->parish->name }}, municipio {{ $school->parish->municipality->name }}. @isset($school->city){{ $school->city->name }}@endisset estado {{ $school->parish->municipality->state->name }}@endisset"
                                            class="btn btn-info">
                                            <i class="ti-eye"></i>
                                        </button>
                                        <a href="{{ url('admin/colegios/'.$school->id.'/editar') }}" class="btn btn-warning"><i class="ti-marker-alt"></i></a>
                                        <button type="button" class="btn btn-danger"><i class="ti-trash"></i></button>
                                    </div>
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- sample modal content -->
<div id="show-school" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="vcenter" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="vcenter">Información del Colegio</h4>
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            </div>
            <div class="modal-body">
            <!-- <div class="card card-body"> -->
                <div class="row">
                    <div class="col-md-4 col-lg-3 text-center">
                        <a id="school-logo" href="javascript:void(0)"></a>
                    </div>
                    <div class="col-md-8 col-lg-9">
                        <h3 id="school-name" class="box-title m-b-0"></h3> <small id="school-url"></small>
                        <address id="school-address">

                        </address>
                    </div>
                </div>
            <!-- </div> -->
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-info waves-effect" data-dismiss="modal">Cerrar</button>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
<!-- /.modal -->


@endsection


@section('extra-js')
<script>

        $('#show-school').on('show.bs.modal', function (event) {
            var button = $(event.relatedTarget) // Button that triggered the modal
            var schoolLogo = button.data('logo')
            var schoolName = button.data('name')
            var schoolAddress = button.data('address')
            var schoolPhone = button.data('phone')
            var schoolEmail = button.data('email')
            var schoolUrl = button.data('url')
            var schoolParish = button.data('parish')
            var modal = $(this)
            modal.find('#school-logo').html("<img src='"+schoolLogo+"' alt='user' class='img-responsive'>")
            modal.find('#school-name').text(schoolName)
            modal.find('#school-url').text(schoolUrl)
            modal.find('#school-address').html(schoolAddress+". Parroquia "+schoolParish+"<br> <br> <abbr title='Phone'>P:</abbr> "+schoolPhone)
        })

</script>
@endsection