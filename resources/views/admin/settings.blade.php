@extends('layouts.app')

@section('content')
    <!-- Row -->
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-header border-0">
                    <h6>Gestión de Usuarios</h6>
                </div>
                <div class="card-body">
                    <div class="row">
                        <div class="col-md-2">
                            <div class="card shadow-lg bg-white rounded h-100">
                                <div class="card-body">
                                    <div class="row">
                                        <div class="col-12 text-center">
                                            <a href="{{ route('users.index') }}" title="Registros de Usuarios">
                                                <i class="mdi mdi-48px mdi-account-multiple-plus text-info"></i>
                                                <h6 class="card-subtitle font-weight-bold">Usuarios</h6>
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- Column -->
                        <!-- Column -->
                        <div class="col-md-2">
                            <div class="card shadow-lg bg-white rounded h-100">
                                <div class="card-body">
                                    <div class="row">
                                        <div class="col-12 text-center">
                                            <a href="#" title="Registros de Roles y Permisos">
                                                <i class="mdi mdi-48px mdi-account-convert text-info"></i>
                                                <h6 class="card-subtitle font-weight-bold">Roles / Permisos</h6>
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- Column -->
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-header border-0">
                    <h6>Registros Generales</h6>
                </div>
                <div class="card-body">
                    <div class="row">
                        <div class="col-md-2">
                            <div class="card shadow-lg bg-white rounded h-100">
                                <div class="card-body">
                                    <div class="row">
                                        <div class="col-12 text-center">
                                            <a href="#" title="Registros de Países">
                                                <i class="ti ti-map text-info" style="font-size: 48px;"></i>
                                                <h6 class="card-subtitle font-weight-bold">Países</h6>
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- Column -->
                        <!-- Column -->
                        <div class="col-md-2">
                            <div class="card shadow-lg bg-white rounded h-100">
                                <div class="card-body">
                                    <div class="row">
                                        <div class="col-12 text-center">
                                            <a href="#" title="Registros de Estados">
                                                <i class="ti ti-map-alt text-info" style="font-size: 48px;"></i>
                                                <h6 class="card-subtitle font-weight-bold">Estados</h6>
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- Column -->
                        <!-- Column -->
                        <div class="col-md-2">
                            <div class="card shadow-lg bg-white rounded h-100">
                                <div class="card-body">
                                    <div class="row">
                                        <div class="col-12 text-center">
                                            <a href="{{ route('schools.index') }}" title="Registros de Estados">
                                                <i class="ti ti-home text-info" style="font-size: 48px;"></i>
                                                <h6 class="card-subtitle font-weight-bold">Colegios</h6>
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- Column -->
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
