@extends('layouts.app')

@section('title-view', 'Registro General - Familias')

@section('extra-css')
    <link href="{{ asset('themeforest/main/css/jquery.toast.css', Request::secure()) }}" id="theme" rel="stylesheet">
    <!--alerts CSS -->
    <link href="{{ asset('themeforest/main/css/sweetalert.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ asset('themeforest/main/css/jquery.dataTables.min.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ asset('themeforest/main/css/dataTables.bootstrap4.min.css') }}" rel="stylesheet" type="text/css">
@endsection

@section('content')
<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-body">
                <div class="d-flex no-block">
                    <h4 class="card-title">Listados de familias</h4>
                    <div class="ml-auto">
                        <button class="btn btn-primary waves-effect waves-light" type="button" data-toggle="modal" data-target="#new-family"><span class="btn-label"><i class="mdi mdi-account-multiple-plus"></i></span>Agregar nuevo usuario Familia</button>
                    </div>
                </div>
                <div class="table-responsive m-t-40">
                    <table id="table-family" class="display nowrap table table-hover table-striped table-bordered js-exportable" cellspacing="0" width="100%">
                        <thead>
                            <tr>
                                <th>Apellidos</th>
                                <th>Correo electrónico</th>
                                <th class="no-sort">Teléfono</th>
                                <th>Estatus</th>
                                <th class="no-sort">Acciones</th>
                            </tr>
                        </thead>
                        <tfoot>
                            <tr>
                                <th>Apellidos</th>
                                <th>Correo electrónico</th>
                                <th>Teléfono</th>
                                <th>Estatus</th>
                                <th style="width: 10%">Acciones</th>
                            </tr>
                        </tfoot>
                        <tbody>
                            @foreach ($families as $family)
                            <tr data-id="{{ $family->id }}">
                                <td>{{ $family->last_name_1 }} {{ $family->last_name_2 }}</td>
                                <td>{{ $family->user->email }}</td>
                                <td>{{ $family->phone }}</td>
                                <td>
                                    @if(!$family->parish_id)
                                        <h3><span class="label label-danger">Sin Perfil</span></h3>
                                    @else
                                        @if($family->user->disable == 0 )
                                            <h3><span class="label label-info">Activa</span></h3>
                                        @else
                                                <h3><span class="label label-danger">Inactiva</span></h3>
                                        @endif
                                    @endif
                                </td>
                                <td>
                                    <div class="btn-group" role="group" aria-label="Basic example">
                                        <button type="button" data-toggle="modal" data-target="#show-family"
                                            class="btn btn-info"
                                            data-family = "{{ $family->last_name_1 }} {{ $family->last_name_2 }}"
                                            data-email = "{{ $family->user->email }}"
                                            data-phone = "{{ $family->phone }}"
                                            data-address =  "{{ $family->address }}"
                                            data-parish = "@if($family->parish){{ $family->parish->name }}, municipio {{ $family->parish->municipality->name }} de {{ $family->parish->municipality->state->name }} @endif"
                                            >
                                            <a data-toggle="tooltip" data-placement="top" title="Ver datos de familia">
                                            <i class="ti-eye"></i>
                                            </a>
                                        </button>
                                        <a href="{{ url('colegio/familias/'.$family->id.'/editar') }}" class="btn btn-warning" data-toggle="tooltip" title="Editar familia"><i class="ti-marker-alt"></i></a>
                                        <button class="btn btn-primary family-password" data-toggle="tooltip" data-id="{{$family->id}}" title="Cambio de Contraseña"><i class="ti-lock"></i></button>
                                        <button type="button" class="btn btn-danger family-delete" data-name="{{ $family->last_name_1 }} {{ $family->last_name_2 }}"  data-id="{{ $family->id }}"  data-toggle="tooltip" title="Eliminar familia"><i class="ti-trash"></i></button>
                                        <!--Bloqueo de familia por usuario-->
                                        <button type="button" class="btn btn-warning" data-toggle="modal" data-target="#lock_family"  onclick="lock('{{ $family->id }}')">
                                            <a href="javascript:void(0);" data-placement="top" data-toggle="modal" class="text-white" title="Habilitar/Suspender familia"><i class="ti-user"></i></a>
                                        </button>
                                    </div>
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- sample modal content -->
<div id="show-family" class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="myLargeModalLabel">Información de la famiilia</h4>
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            </div>
            <div class="modal-body">
                <small class="text-muted">Correo Electrónico de la familia </small>
                <h4 id="family-email"></h4>
                <small class="text-muted p-t-30 db">Dirección de residencia</small>
                <h6 id="family-address"></h6>
                <small class="text-muted p-t-30 db">Teléfono</small>
                <h6 id="family-phone"></h6>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-info waves-effect" data-dismiss="modal">Cerrar</button>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
<!-- /.modal -->

<!-- sample modal content -->
<div id="new-family" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="vcenter" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="vcenter">Nuevo usuario Familia</h4>
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            </div>
                        <form id="new-user-family-form">
                            @csrf
            <div class="modal-body">
                <div class="row">
                    <div class="col-sm-12 col-xs-12">
                        <p>Debe ingresar una cuenta correo electrónico del representante que será el acceso a la cuenta de la familia, al crear el usuario se enviará un mensaje al correo con el usuario y contraseña para su acceso.</p>
                        <div class="form-group">
                            <label for="email">Correo Electrónico del solicitante</label>
                            <input type="email" class="form-control" id="email" name="email" placeholder="ejemplo: usuario@email.com" required>

                        </div>
                        <div class="form-group">
                            <label for="phone">Telefono del solicitante</label>
                            <input type="text" class="form-control" id="phone" name="phone" placeholder="Telefono" required>

                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <div class="spinner-border text-primary" role="status">
                  <span class="sr-only">Loading...</span>
                </div>

                <button type="submit" id="b-new-user-family" class="btn btn-success waves-effect waves-light m-r-10 tst3 btn btn-success">Crear Usuario</button>
                <button type="button" class="btn btn-inverse waves-effect waves-light" data-dismiss="modal">Cancelar</button>
            </div>
                        </form>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
<!-- /.modal -->
<div id="lock_family" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div id="family_lock" class="modal-content">
            
        </div>
    </div>
</div>

@endsection


@section('extra-js')
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.0/jquery.validate.min.js"></script>
<script src="{{ asset('themeforest/main/js/jquery.toast.js') }}"></script>
<script src="{{ asset('themeforest/main/js/sweetalert.min.js') }}"></script>
<script src="{{ asset('themeforest/main/js/jquery.dataTables.min.js') }}"></script>

<script src="{{ asset('themeforest/main/js/datatable/dataTables.bootstrap4.min.js') }}"></script>
<script src="{{ asset('themeforest/main/js/datatable/pdfmake.min.js') }}"></script>
<script src="{{ asset('themeforest/main/js/datatable/vfs_fonts.js') }}"></script>
<script src="{{ asset('themeforest/main/js/datatable/dataTables.buttons.min.js') }}"></script>
<script src="{{ asset('themeforest/main/js/datatable/buttons.flash.min.js') }}"></script>
<script src="{{ asset('themeforest/main/js/datatable/buttons.html5.min.js') }}"></script>
<script src="{{ asset('themeforest/main/js/datatable/buttons.print.min.js') }}"></script>
<script>
$(function(){
    $('.spinner-border').hide()

    @if (\Session::has('error'))
           $.toast({
            heading: 'Error',
            text: '{{ Session::get('error') }}',
            position: 'top-right',
            loaderBg:'#ff6849',
            icon: 'error',
            hideAfter: 3500
          });
    @endif

    @if (\Session::has('success'))
           $.toast({
            heading: '!Operación exitosa!',
            text: '{{ Session::get('success') }}',
            position: 'top-right',
            loaderBg:'#ff6849',
            icon: 'success',
            hideAfter: 3500, 
            stack: 6
          });
    @endif  

    $('#b-new-user-family').click(function(event) {
        event.preventDefault();
        $('.spinner-border').show()
        var email = $('#email').val()
        var phone = $('#phone').val()
        $.ajax({
            headers: {'X-CSRF-Token': $('meta[name="_token"]').attr('content')},
            url: "{{route('families.store')}}",
            type: 'POST',
            data: {name: email, email: email, phone: phone},
        })
        .done(function(response) {
            location.reload();
            $('#app').empty().append($(response))
            $('.spinner-border').hide()
            $('#new-family').modal('toggle')
            $('#new-user-family-form')[0].reset()
            $.toast({
                heading: 'Nuevo Usuario Agregado',
                text: 'Se ha enviado un correo al usuario con los datos de su acceso.',
                position: 'top-right',
                loaderBg:'#ff6849',
                icon: 'success',
                hideAfter: 3500, 
                stack: 6
            });
        })
        .fail(function(response) {
            $('.spinner-border').hide()

           $.toast({
            heading: 'Usuario no agregado',
            text: 'Verifique que el correo no ha sido agregado anteriormente.',
            position: 'top-right',
            loaderBg:'#ff6849',
            icon: 'error',
            hideAfter: 3500
            
          });
        })       
    });

$(".family-password").click(function(){
    var familyId = $(this).data('id')
    swal({   
        title: "¿Cambiar Contraseña?",   
        text: "Al hacer clic Cambiar se generará una nueva contraseña aleatoria que sera enviada al correo del usuario",   
        type: "warning",   
        showCancelButton: true,   
        confirmButtonColor: "#DD6B55",
        confirmButtonText: "Cambiar",   
        closeOnConfirm: false 
    }, function(){
        $.ajax({
            headers: {'X-CSRF-Token': $('meta[name="_token"]').attr('content')},
            url: '/colegio/familias/change-password/'+familyId,
            type: 'get',
            dataType: 'json',
            //data:{id:familyId}
        })
        .done(function(data) {
            swal("Cambio de contraseña exitoso"); 
        }) 
    })  
})


$('.family-delete').click(function(){
    var row = $(this).parent('tr')
    var id = row.data('id')
    var familyName = $(this).data('name')
    var familyId = $(this).data('id')
    swal({   
        title: "¿Eliminar familia?",   
        text: "La cuenta de "+familyName+" sera eliminada del sistema",   
        type: "warning",   
        showCancelButton: true,   
        confirmButtonColor: "#DD6B55",   
        confirmButtonText: "Si, eliminarlo",   
        closeOnConfirm: false 
    }, function(){
        $.ajax({
            headers: {'X-CSRF-Token': $('meta[name="_token"]').attr('content')},
            url: '/colegio/familias/'+familyId,
            type: 'delete',
            dataType: 'json',
            data:{id:familyId}
        })
        .done(function(data) {
            $('#app').empty().append($(data))
            swal("Eliminada", "La familia "+familyName+" se ha eliminado del sistema.", "success")
            //row.fadeOut();
            location.reload();
        }); 
    });
});

if ($("#new-user-family-form").length > 0) {
    $("#new-user-family-form").validate({
        rules: { 
            email: {
                required: true,
                maxlength: 50,
                email: true,
            }
        },
        messages: {
            email: {
                required: "Por favor ingrese un correo electrónico válido",
                email: "Por favor ingrese un correo electrónico válido",
                maxlength: "la dirección de correo no debe pasar de 50 caractéres",
            },

        },
    })
}

$('#show-family').on('show.bs.modal', function (event) {
    var button = $(event.relatedTarget) // Button that triggered the modal
    var familyName = button.data('family')
    var familyAddress = button.data('address')
    var familyPhone = button.data('phone')
    var familyEmail = button.data('email')
    var familyParish = button.data('parish')
    var modal = $(this)
    modal.find('#myLargeModalLabel').text('Familia '+familyName)
    modal.find('#family-phone').text(familyPhone)
    modal.find('#family-email').text(familyEmail)
    modal.find('#family-address').text(familyAddress+', '+familyParish)
})

//datatable
var table = $('#table-family').DataTable({
    "language": {
        "url": "//cdn.datatables.net/plug-ins/1.10.21/i18n/Spanish.json"
    },
            dom: 'Bfrtip',
            buttons: [
            {
                extend: 'copy',
                exportOptions: {
                    columns: [ 0, 1, 2, 3 ]
                }
            },
            {
                extend: 'csv',
                exportOptions: {
                    columns: [ 0, 1, 2, 3 ]
                }
            },
            {
                extend: 'excel',
                exportOptions: {
                    columns: [ 0, 1, 2, 3 ]
                }
            },
            {
                extend: 'pdf',
                exportOptions: {
                    columns: [ 0, 1, 2, 3 ]
                }
            },
            {
                extend: 'print',
                exportOptions: {
                    columns: [ 0, 1, 2, 3 ]
                }
            }],
    "displayLength": 10,
    "columnDefs": [ {
        "targets": 'no-sort',
        "orderable": false
    } ],
    "order": [[ 0, "asc" ]]
});

})

//lock family

function lock(family){
$.ajax({
         type : "GET",
         data : {"family_id" : family, "_token": "{{ csrf_token() }}" },
         url : "{{ route('lock.create') }}",
         success : function(data){
            $('#family_lock').html(data);
         }
      });
}

function lock_user(id, family_id){
$.ajax({
         type : "POST",
         data : {"user_id" : id, "family_id": family_id, "_token": "{{ csrf_token() }}" },
         url : "{{ route('lock.store') }}",
         success : function(data){
            $('#family_lock').html(data);
         }
      });
}
</script>
@endsection
