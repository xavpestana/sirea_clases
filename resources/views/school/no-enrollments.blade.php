@extends('layouts.app')

@section('title-view', 'Pendientes')

@section('extra-css')
    <link href="{{ asset('themeforest/main/css/jquery.toast.css', Request::secure()) }}" id="theme" rel="stylesheet">
    <!--alerts CSS -->
    <link href="{{ asset('themeforest/main/css/sweetalert.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ asset('themeforest/main/css/jquery.dataTables.min.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ asset('themeforest/main/css/dataTables.bootstrap4.min.css') }}" rel="stylesheet" type="text/css">
@endsection

@section('content')
<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-body">
                <div class="d-flex no-block">
                    <h4 class="card-title">Pendientes por matricular en Año Escolar ({{ $actual_season->season }})</h4>
                </div>
                <div class="table-responsive m-t-40">
                    @if($no_enrollments->count() > 0)
                    <table id="table-student" class="display nowrap table table-hover table-striped table-bordered js-exportable" cellspacing="0" width="100%">
                        <thead>
                            <tr>
                                <th>Documento</th>
                                <th>Nombre y Apellidos</th>
                                <th>Grado a cursar</th>
                                <th class="no-sort">Acciones</th>
                            </tr>
                        </thead>
                        <tfoot>
                            <tr>
                                <th>Documento</th>                                
                                <th>Nombre y Apellidos</th>
                                <th>Grado a cursar</th>
                                <th>Acciones</th>
                            </tr>
                        </tfoot>
                        <tbody>
                            @foreach ($no_enrollments as $no_enrollment)
                            <tr>
                                <td>{{ $no_enrollment->profile->document_type }}-{{ $no_enrollment->profile->document_number }}</td>
                                <td>{{ $no_enrollment->profile->first_name_1 }} {{ $no_enrollment->profile->first_name_2 }} {{ $no_enrollment->profile->last_name_1 }} {{ $no_enrollment->profile->last_name_2 }}</td>
                                <td>{{ $no_enrollment->grade }}</td>
                                <td>
                                    <div class="btn-group" role="group" aria-label="Basic example">
                                        <button type="button" data-toggle="modal" data-target="#modal-inscribir"
                                            class="btn btn-info"
                                            data-avatar="{{asset('storage/pictures/avatars/'.$no_enrollment->profile->avatar)}}"
                                            data-email="{{$no_enrollment->profile->user->email}}"
                                            data-name="{{$no_enrollment->profile->first_name_1}} {{$no_enrollment->profile->first_name_2}} {{$no_enrollment->profile->last_name_1}} {{$no_enrollment->profile->last_name_2}}"
                                            data-document="{{$no_enrollment->profile->document_type}}-{{$no_enrollment->profile->document_number}}"
                                            data-grade="{{ $no_enrollment->grade }}"
                                            data-sections="{{ $actual_season->sections->where('grade.name', $no_enrollment->grade ) }}"
                                            data-id="{{ $no_enrollment->id}}"
                                            >
                                            <a data-toggle="tooltip" data-placement="top" title="Inscribir estudiante">
                                            <i class="mdi mdi-book-plus"></i>
                                            </a>
                                        </button>
                                        <a href="{{ route('school.students.edit', $no_enrollment->id) }}" class="btn btn-warning" data-toggle="tooltip" title="Editar estudiante"><i class="ti-marker-alt"></i></a>
                                        <button id="btn-download" type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                        <i class="fa fa-file-pdf-o"></i>
                                        </button>
                                        <div class="dropdown-menu" aria-labelledby="btn-download">
                                          <a class="dropdown-item" href="{{ route('pdf.planilla', $no_enrollment->id) }}">Planilla Matriculación</a>
                                          {{-- <a class="dropdown-item" href="{{ route('pdf.student-carnet', '1') }}">Carnet</a> --}}
                                          {{-- <a class="dropdown-item" href="{{ route('bc') }}">bc</a> --}}
                                        </div>
                                    </div>
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                    @else

                    <p>No existen estudiantes pendientes por matriculación en el presente año escolar</p>

                    @endif
                </div>
            </div>
        </div>
    </div>
</div>

<!-- sample modal content -->
<div id="modal-inscribir" class="modal fade bs-example-modal" tabindex="-1" role="dialog" aria-labelledby="vcenter" aria-hidden="true">
    <form method="post" action="{{route('inscribir')}}" id="form-inscribir">
    @csrf
    <input type="hidden" name="student_id" id="student_id">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="vcenter">Inscripción de Estudiante <strong id="student-family"></strong></h4>
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            </div>

            <div class="modal-body">
                <div class="row p-t-30 p-l-20">
                    <div class="col-md-4 col-lg-3">
                        <a id="student-avatar" ></a>
                    </div>
                    <div class="col-md-8 col-lg-9 text-center">
                        <h3 id="student-name" class="box-title m-b-0"></h3>
                        <small class="text-muted">Usuario / Correo Electrónico</small>
                        <h6 id="student-email"></h6>
                        <small class="text-muted">Documento de Identidad</small>
                        <h6 id="student-document"></h6>
                    </div>
                </div>
                <div class="row p-t-30 p-l-20">
                    <div class="col-md-6">Grado o Año a cursar:</div>
                    <h3 class="col-md-6" id="student-grade"></h3>
                </div>
                <div class="row p-t-30 p-l-20 justify-content-center">
                    <h4>Seleccione el grupo o sección a asignar</h4>
                </div>
                <div class="row p-t-30 p-l-20 justify-content-center">
                    <div class="demo-radio-button">
                        <div class="col-md-12" id="sections-list">
                        {{--Aqui se pinta la lista de secciones--}}     
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <div id="loading">
                    Inscribiendo...
                    <div class="spinner-border text-primary" role="status">
                      <span class="sr-only">Loading...</span>
                    </div>
                </div>
                <button type="submit" id="b-new-user-family" class="btn btn-success waves-effect waves-light m-r-10 tst3 btn btn-success">Inscribir</button>
                <button type="button" class="btn btn-info waves-effect" data-dismiss="modal">Cancelar</button>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
    </form>
</div>
<!-- /.modal -->
@endsection

@section('extra-js')
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.0/jquery.validate.min.js"></script>
<script src="{{ asset('themeforest/main/js/jquery.toast.js') }}"></script>
<script src="{{ asset('themeforest/main/js/sweetalert.min.js') }}"></script>
<script src="{{ asset('themeforest/main/js/jquery.dataTables.min.js') }}"></script>

<script src="{{ asset('themeforest/main/js/datatable/dataTables.bootstrap4.min.js') }}"></script>
<script src="{{ asset('themeforest/main/js/datatable/pdfmake.min.js') }}"></script>
<script src="{{ asset('themeforest/main/js/datatable/vfs_fonts.js') }}"></script>
<script src="{{ asset('themeforest/main/js/datatable/dataTables.buttons.min.js') }}"></script>
<script src="{{ asset('themeforest/main/js/datatable/buttons.flash.min.js') }}"></script>
<script src="{{ asset('themeforest/main/js/datatable/buttons.html5.min.js') }}"></script>
<script src="{{ asset('themeforest/main/js/datatable/buttons.print.min.js') }}"></script>

<script>
$(function(){
    @if (\Session::has('error'))
           $.toast({
            heading: 'Error',
            text: '{{ Session::get('error') }}',
            position: 'top-right',
            loaderBg:'#ff6849',
            icon: 'error',
            hideAfter: 3500
          });
    @endif

    @if (\Session::has('success'))
           $.toast({
            heading: '!Operación exitosa!',
            text: '{{ Session::get('success') }}',
            position: 'top-right',
            loaderBg:'#ff6849',
            icon: 'success',
            hideAfter: 3500, 
            stack: 6
          });
    @endif

    $('#loading').hide()


    //$('#b-new-user-family').attr("disabled", true)
    //$('#b-new-user-family').prop('disabled', true);

    var table = $('#table-student').DataTable({
        "language": {
            "url": "//cdn.datatables.net/plug-ins/1.10.21/i18n/Spanish.json"
        },
        dom: 'Bfrtip',
        buttons: [
        {
            extend: 'copy',
            exportOptions: {
                columns: [ 0, 1, 2 ]
            }
        },
        {
            extend: 'csv',
            exportOptions: {
                columns: [ 0, 1, 2 ]
            }
        },
        {
            extend: 'excel',
            exportOptions: {
                columns: [ 0, 1, 2 ]
            }
        },
        {
            extend: 'pdf',
            exportOptions: {
                columns: [ 0, 1, 2 ]
            }
        },
        {
            extend: 'print',
            exportOptions: {
                columns: [ 0, 1, 2 ]
            }
        }],
        "displayLength": 10,
        "columnDefs": [ {
            "targets": 'no-sort',
            "orderable": false
        } ],
        "order": [[ 1, "asc" ]]
    })

})

$('#modal-inscribir').on('show.bs.modal', function (event) {
    var button = $(event.relatedTarget) // Button that triggered the modal
    var studentAvatar = button.data('avatar')
    var studentName = button.data('name')
    var studentEmail = button.data('email')
    var studentDocument = button.data('document')
    var studentGrade = button.data('grade')
    var sections = button.data('sections')
    var student_id = button.data('id')
    var modal = $(this)
    modal.find('#student-avatar').html("<img src='"+studentAvatar+"' alt='user' class='img-circle img-responsive img-avatar'>")
    modal.find('#student-name').text(studentName)
    modal.find('#student-email').text(studentEmail)
    modal.find('#student-document').text(studentDocument)
    modal.find('#student-grade').text(studentGrade)
    modal.find('#student_id').val(student_id)

    $.each(sections, function(i, item) {
        console.log(sections[i].name);
        $('#sections-list').append('<div class="m-b-10">'+
                '<label class="custom-control custom-radio">'+
                    '<input name="section_id" type="radio" value="'+sections[i].id+'" class="custom-control-input">'+
                    '<span class="custom-control-label">'+sections[i].name+'</span>'+
                '</label>'+
            '</div>')
    });
})

$('#modal-inscribir').on('hidden.bs.modal', function(){
    $('#sections-list').html('');
});

//Bloqueo del boton al inscribir
$("#form-inscribir").closest('form').on('submit', function(e) {
    e.preventDefault()
    $('#b-new-user-family').attr('disabled', true)
    $('#loading').show()
    this.submit() // ahora hace el submit de tu formulario.
});


</script>
@endsection
