@extends('layouts.app')

@section('content')
    <div class="row">
        <div class="col-12">
            <user-school :user_school_id="{!! (isset($user_school)) ? $user_school->id : "null" !!}"
                route_list='{{ url('colegio/usuarios') }}'>
            </user-school>
        </div>
    </div>
@endsection
