@extends('layouts.app')

@section('content')
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-header border-0">
                    Lista de Usuarios Escolares<a class="btn btn-primary btn-xs float-right" href="{{ route('user-schools.create') }}"><i class="fa fa-plus-circle"></i></a>
                </div>
                <div class="card-body">
                    <user-school-list route_list="{{ url('colegio/usuarios/show/vue-list') }}"
						route_delete="#"
						route_edit="#"
						route_show="{{ url('colegio/usuarios/{id}') }}">
					</user-school-list>
                </div>
            </div>
        </div>
    </div>
@endsection
