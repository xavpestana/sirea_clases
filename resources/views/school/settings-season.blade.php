@extends('layouts.app')

@section('content')
<div class="row">
    <div class="col-md-12">
        <div class="card card-body">
            <h3 class="box-title m-b-0">Nuevo Año Escolar</h3>
            <p class="text-muted m-b-30 font-13"> Por favor ingrese la información detallada para configurar el nuevo año escolar</p>
            <div class="row">
                <div class="col-sm-12 col-xs-12">
                    <form method="POST" action="{{ route('season.store') }}">
                    	@csrf
                        <div class="form-group">
                            <label for="season">Nombre</label>
                            <input type="text" class="form-control @error('season') is-invalid @enderror" id="season" name="season" value="{{ old('name') }}" >
		                    @error('season')
		                    <div class="invalid-feedback">
		                      {{ $message }}
		                    </div>
		                    @enderror
                        </div>
                        <div class="form-group">
                            <label for="periods_qty">Cantidad de Periodos o Momentos</label>
                            <input type="number" class="form-control @error('periods_qty') is-invalid @enderror" id="periods_qty" name="periods_qty" value="3" min="0" max="5">
		                    @error('periods_qty')
		                    <div class="invalid-feedback">
		                      {{ $message }}
		                    </div>
		                    @enderror                            
                        </div>
                        <div class="form-group">
                            <label for="max_score">Calificación máxima de las evaluaciones</label>
                            <input type="number" class="form-control @error('max_score') is-invalid @enderror" id="max_score" name="max_score" value="20" min="0">
		                    @error('max_score')
		                    <div class="invalid-feedback">
		                      {{ $message }}
		                    </div>
		                    @enderror                            
                        </div>
		                <div class="form-group col-md-6 @error('sections_number') is-invalid @enderror">
		                    <label for="sections">Identificación de secciones</label>
		                    <div class="demo-radio-button @error('sections_number') is-invalid @enderror">
	                            <input name="sections_number" type="radio" id="alphabetic" class="radio-col-green" value="0" checked 
	                            />
	                            <label for="alphabetic">Alfabética (A, B, C, ...)</label>
	                            <input name="sections_number" type="radio" id="numeric" class="radio-col-green" value="1"
	                            />
	                            <label for="numeric">Numerica (1, 2, 3, ...)</label>
		                    </div>
		                    @error('sections_number')
		                    <div class="invalid-feedback">
		                      {{ $message }}
		                    </div>
		                    @enderror
		                </div>
		                <div class="form-group">
                            <label for="initial_pay">Pago Inicial</label>
                            <input type="number" class="form-control @error('initial_pay') is-invalid @enderror" id="initial_pay" name="initial_pay" min="0" value="{{ old('initial_pay') }}">
		                    @error('initial_pay')
		                    <div class="invalid-feedback">
		                      {{ $message }}
		                    </div>
		                    @enderror                           
                        </div>
           				<h4 class="box-title m-b-0">Niveles y Grados de Año Escolar</h4>
            			<p class="text-muted m-b-30 font-13"> Seleccione los grados que se cursarán en este Año Escolar, ademas la cantidad de secciones para cada grado </p>
						@foreach ($levels as $level)
                            <h3 class="box-title">{{ $level->name }}</h3>
                            <hr class="m-t-0 m-b-40">
                            <div class="row">
							@foreach ($level->grades as $grade)
                                <div class="col-md-4">
                                    <div class="form-group row">
		                                <div class="col-md-8">
											<input type="checkbox" id="chk-{{ $grade->id }}" class="filled-in chk-col-green chk-grade" />
	                                    	<label for="chk-{{ $grade->id }}" class="control-label text-right">{{ $grade->name }}</label>
	                                    	<fieldset disabled>
                                            <input type="number" class="form-control" name="grade[{{ $grade->id }}]" id="grade[{{ $grade->id }}]" min="0" value="0">
	                                    	</fieldset>
	                                    </div>
	                                </div>
	                            </div>
							@endforeach
							</div>
						@endforeach
                        <button type="submit" class="btn btn-success waves-effect waves-light m-r-10">Crear Año Escolar</button>
                        <button class="btn btn-inverse waves-effect waves-light">Cancelar</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection


@section('extra-js')
<script>
	$('.chk-grade').on('click',function(){
		if($(this).is(':checked')){
			$(this).siblings().removeAttr('disabled')
			$(this).siblings().children('input').focus()
		}else{
			$(this).siblings().attr('disabled', 'disabled')
			$(this).siblings().children('input').val(0)
		}
	})
</script>
@endsection