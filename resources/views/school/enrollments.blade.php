@extends('layouts.app')

@section('title-view', 'Estudiantes Inscritos')

@section('extra-css')
    <link href="{{ asset('themeforest/main/css/jquery.toast.css', Request::secure()) }}" id="theme" rel="stylesheet">
    <!--alerts CSS -->
    <link href="{{ asset('themeforest/main/css/sweetalert.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ asset('themeforest/main/css/jquery.dataTables.min.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ asset('themeforest/main/css/dataTables.bootstrap4.min.css') }}" rel="stylesheet" type="text/css">
@endsection

@section('content')
<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-body">
                <div class="d-flex no-block">
                    <h4 class="card-title">Listado de Matriculados en Año Escolar ({{ $actual_season->season }})</h4>
                    <div class="ml-auto">
                        <a class="btn btn-success waves-effect waves-light" href="{{route('enrollments-excel')}}"><span class="btn-label"><i class="mdi mdi-file-excel"></i></span>Descargar Listado General</a>


                        <a class="btn btn-primary waves-effect waves-light" href="{{route('no-enrollments')}}"><span class="btn-label"><i class="mdi mdi-account-plus"></i></span>Inscribir Estudiantes</a>
                    </div>
                </div>
                <div class="table-responsive m-t-40">
                    @if($enrollments->count() > 0)
                    <table id="table-student" class="display nowrap table table-hover table-striped table-bordered js-exportable" cellspacing="0" width="100%">
                        <thead>
                            <tr>
                                <th>cedula</th>
                                <th>Nombres</th>
                                <th>Apellidos</th>
                                <th>Correo</th>
                                <th>Grupo</th>
                                <th>Familia</th>
                                <th>Correo de familia</th>
                                <th class="no-sort">Acciones</th>
                            </tr>
                        </thead>
                        <tfoot>
                            <tr>
                                <th>cedula</th>
                                <th>Nombres</th>
                                <th>Apellidos</th>
                                <th>Correo</th>
                                <th>Grupo</th>
                                <th>Familia</th>
                                <th>Correo de familia</th>
                                <th>Acciones</th>
                            </tr>
                        </tfoot>
                        <tbody>
                            @foreach ($enrollments as $enrollment)
                            <tr>
                                <td>{{ $enrollment->student->profile->document_number }}</td>

                                <td>{{ $enrollment->student->profile->first_name_1 }} {{ $enrollment->student->profile->first_name_2 }}</td>

                                <td>{{ $enrollment->student->profile->last_name_1 }} {{ $enrollment->student->profile->last_name_2 }}</td>
                                <td>{{ $enrollment->student->profile->user->email }}</td>
                                <td>{{ $enrollment->section->grade->name }} {{ $enrollment->section->name }}</td>
                                <td>{{ $enrollment->student->family->last_name_1 }} {{ $enrollment->student->family->last_name_2 }} </td>
                                <td>{{ $enrollment->student->family->user->email }}</td>
                                <td>
                                    <div class="btn-group" role="group" aria-label="Basic example">
                                        <button type="button" class="btn btn-danger enrollment-delete" data-id="{{ $enrollment->id }}"  data-toggle="tooltip" title="Eliminar inscripción"><i class="ti-trash"></i></button>
                                    </div>
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                    @else

                    <p>Todavía no se han inscrito estudiantes en el presente Año Escolar</p>

                    @endif
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('extra-js')
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.0/jquery.validate.min.js"></script>
<script src="{{ asset('themeforest/main/js/jquery.toast.js') }}"></script>
<script src="{{ asset('themeforest/main/js/sweetalert.min.js') }}"></script>
<script src="{{ asset('themeforest/main/js/jquery.dataTables.min.js') }}"></script>

<script src="{{ asset('themeforest/main/js/datatable/dataTables.bootstrap4.min.js') }}"></script>
<script src="{{ asset('themeforest/main/js/datatable/pdfmake.min.js') }}"></script>
<script src="{{ asset('themeforest/main/js/datatable/vfs_fonts.js') }}"></script>
<script src="{{ asset('themeforest/main/js/datatable/dataTables.buttons.min.js') }}"></script>
<script src="{{ asset('themeforest/main/js/datatable/buttons.flash.min.js') }}"></script>
<script src="{{ asset('themeforest/main/js/datatable/buttons.html5.min.js') }}"></script>
<script src="{{ asset('themeforest/main/js/datatable/buttons.print.min.js') }}"></script>

<script>
$(function(){
    @if (\Session::has('error'))
           $.toast({
            heading: 'Error',
            text: '{{ Session::get('error') }}',
            position: 'top-right',
            loaderBg:'#ff6849',
            icon: 'error',
            hideAfter: 3500
          });
    @endif

    @if (\Session::has('success'))
           $.toast({
            heading: '!Operación exitosa!',
            text: '{{ Session::get('success') }}',
            position: 'top-right',
            loaderBg:'#ff6849',
            icon: 'success',
            hideAfter: 3500, 
            stack: 6
          });
    @endif  

    var table = $('#table-student').DataTable({
        "language": {
            "url": "//cdn.datatables.net/plug-ins/1.10.21/i18n/Spanish.json"
        },
        dom: 'Bfrtip',
        buttons: [
        {
            extend: 'copy',
            exportOptions: {
                columns: [ 0, 1, 2, 3, 4, 5, 6 ]
            }
        },
        {
            extend: 'csv',
            exportOptions: {
                columns: [ 0, 1, 2, 3, 4, 5, 6 ]
            }
        },
        {
            extend: 'excel',
            exportOptions: {
                columns: [ 0, 1, 2, 3, 4, 5, 6 ]
            }
        },
        {
            extend: 'pdf',
            exportOptions: {
                columns: [ 0, 1, 2, 3, 4, 5, 6 ]
            }
        },
        {
            extend: 'print',
            exportOptions: {
                columns: [ 0, 1, 2, 3, 4, 5, 6 ]
            }
        }],
        "displayLength": 10,
        "columnDefs": [ {
            "targets": 'no-sort',
            "orderable": false
        } ],
        "order": [[ 0, "asc" ]]
    })

})

$('.enrollment-delete').on('click', function(){
    var enrollmentId = $(this).data('id')
    swal({   
        title: "¿Eliminar inscripción?",   
        text: "Al eliminar pasara a la lista de estudiantes pendientes por inscribir",   
        type: "warning",   
        showCancelButton: true,   
        confirmButtonColor: "#DD6B55",   
        confirmButtonText: "Si, eliminar",   
        closeOnConfirm: false 
    }, function(){
        $.ajax({
            headers: {'X-CSRF-Token': $('meta[name="_token"]').attr('content')},
            url: '/pagos/inscripciones/'+enrollmentId,
            type: 'delete',
            dataType: 'json',
            data:{id:enrollmentId}
        })
        .done(function(data) {
            //$('#app').empty().append($(data));
            swal("Eliminada", "El estudiante pasó a la lista de pendientes por inscribir.", "success"); 
            location.reload();
        }); 
    });
})

</script>
@endsection
