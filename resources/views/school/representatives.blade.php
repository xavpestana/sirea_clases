@extends('layouts.app')

@section('title-view', 'Registro General - Representantes')

@section('extra-css')
    <link href="{{ asset('themeforest/main/css/jquery.toast.css', Request::secure()) }}" id="theme" rel="stylesheet">
    <!--alerts CSS -->
    <link href="{{ asset('themeforest/main/css/sweetalert.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ asset('themeforest/main/css/jquery.dataTables.min.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ asset('themeforest/main/css/dataTables.bootstrap4.min.css') }}" rel="stylesheet" type="text/css">
@endsection


@section('content')
<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-body">
                <h4 class="card-title">Listado de Representantes</h4>
                <h6 class="card-subtitle">En la tabla se muestran todos los representatantes que se han agregado desde las cuentas de familia</h6>
                <div class="table-responsive m-t-40">
                    <table id="table-representative" class="display nowrap table table-hover table-striped table-bordered" cellspacing="0" width="100%">
                        <thead>
                            <tr>
                                <th>Familia</th>
                                <th>Nombre Completo</th>
                                <th class="no-sort">Teléfono</th>
                                <th>Parentesco</th>
                                <th class="no-sort">Acciones</th>
                            </tr>
                        </thead>
                        <tfoot>
                            <tr>
                                <th>Familia</th>
                                <th>Nombre Completo</th>
                                <th>Teléfono</th>
                                <th>Parentesco</th>
                                <th>Acciones</th>
                            </tr>
                        </tfoot>
                        <tbody>
                            @foreach ($representatives as $representative)
                            <tr>
                                <td>{{ $representative->family->last_name_1 }} {{ $representative->family->last_name_2 }}</td>
                                <td>{{ $representative->first_name_1 }} {{ $representative->first_name_2 }} {{ $representative->last_name_1 }} {{ $representative->last_name_2 }}</td>
                                <td>{{ $representative->phone }} </td>
                                <td>{{ $representative->relationship->name }}</td>
                                <td>
                                    <div class="btn-group" role="group" aria-label="Basic example">
                                        <button type="button" data-toggle="modal" data-target="#show-representative"
                                            class="btn btn-info"
                                            data-avatar="{{asset('storage/pictures/avatars/'.$representative->avatar)}}"
                                            data-family="{{ $representative->family->last_name_1 }} {{ $representative->family->last_name_2 }}"
                                            data-name="{{ $representative->first_name_1 }} {{ $representative->first_name_2 }} {{ $representative->last_name_1 }} {{ $representative->last_name_2 }}"
                                            data-document="{{ $representative->document_type }}-{{ $representative->document_number }}"
                                            data-phone="{{$representative->phone}}"
                                            data-birthdate="{{date('d-m-Y', strtotime($representative->birthdate))}}"
                                            data-principal="{{$representative->principal}}"
                                            data-country="{{$representative->country->name}}"
                                            data-gender="{{$representative->gender->name}}"
                                            data-relationship="{{$representative->relationship->name}}"
                                            data-correo="{{$representative->correo}}"
                                            >
                                            <a data-toggle="tooltip" data-placement="top" title="Ver datos de representante">
                                            <i class="ti-eye"></i>
                                            </a>
                                        </button>
                                        <a href="{{route('school.representatives.edit', $representative->id)}}" class="btn btn-warning" data-toggle="tooltip" title="Editar representante"><i class="ti-marker-alt"></i></a>
                                    </div>
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>



<!-- sample modal content -->
<div id="show-representative" class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="vcenter" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="vcenter">Representante de familia <strong id="representative-family"></strong></h4>
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-4 col-lg-3 text-center">
                        <a id="representative-avatar" ></a>
                    </div>
                    <div class="col-md-8 col-lg-9 text-center">
                        <h3 id="representative-name" class="box-title m-b-0"></h3>
                        <small class="text-muted">Documento de Identidad</small>
                        <h6 id="representative-document"></h6>
                        <small class="text-muted">Correo</small>
                        <h6 id="representative-correo"></h6>
                        <small class="text-muted">Tipo:</small>
                        <h6 id="representative-principal"></h6>
                    </div>
                </div>
                <div class="row p-t-30 p-l-20">
                    <div class="col-md-4 col-lg-4">
                        <small class="text-muted">Fecha de nacimiento</small>
                        <h6 id="representative-birthdate"></h6>
                    </div>
                    <div class="col-md-4 col-lg-4">
                        <small class="text-muted">País de nacimiento</small>
                        <h6 id="representative-country"></h6>
                    </div>        
                    <div class="col-md-4 col-lg-4">
                        <small class="text-muted">Teléfono</small>
                        <h6 id="representative-phone"></h6>
                    </div>
                    <div class="col-md-3 col-lg-4">
                        <small class="text-muted">Genero</small>
                        <h6 id="representative-gender"></h6>
                    </div>
                    <div class="col-md-3 col-lg-4">
                        <small class="text-muted">Parentesco</small>
                        <h6 id="representative-relationship"></h6>
                    </div>                                 
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-info waves-effect" data-dismiss="modal">Cerrar</button>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
<!-- /.modal -->



{{-- <div id="show-representative" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="vcenter" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="vcenter">Representante de la familia <strong id="representative-family"></strong></h4>
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-4 col-lg-3 text-center">
                        <a id="representative-avatar" href="javascript:void(0)"></a>
                    </div>
                    <div class="col-md-8 col-lg-9">
                        <h3 id="representative-name" class="box-title m-b-0"></h3> <small id="representative-principal"></small>
                        <address id="representative-address">

                        </address>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-info waves-effect" data-dismiss="modal">Cerrar</button>
            </div>
        </div>
    </div>
</div> --}}
@endsection

@section('extra-js')
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.0/jquery.validate.min.js"></script>
<script src="{{ asset('themeforest/main/js/jquery.toast.js') }}"></script>
<script src="{{ asset('themeforest/main/js/sweetalert.min.js') }}"></script>
<script src="{{ asset('themeforest/main/js/jquery.dataTables.min.js') }}"></script>

<script src="{{ asset('themeforest/main/js/datatable/dataTables.bootstrap4.min.js') }}"></script>
<script src="{{ asset('themeforest/main/js/datatable/pdfmake.min.js') }}"></script>
<script src="{{ asset('themeforest/main/js/datatable/vfs_fonts.js') }}"></script>
<script src="{{ asset('themeforest/main/js/datatable/dataTables.buttons.min.js') }}"></script>
<script src="{{ asset('themeforest/main/js/datatable/buttons.flash.min.js') }}"></script>
<script src="{{ asset('themeforest/main/js/datatable/buttons.html5.min.js') }}"></script>
<script src="{{ asset('themeforest/main/js/datatable/buttons.print.min.js') }}"></script>
<script>
 $(function(){
    @if (\Session::has('error'))
           $.toast({
            heading: 'Error',
            text: '{{ Session::get('error') }}',
            position: 'top-right',
            loaderBg:'#ff6849',
            icon: 'error',
            hideAfter: 3500
          });
    @endif

    @if (\Session::has('success'))
           $.toast({
            heading: '!Operación exitosa!',
            text: '{{ Session::get('success') }}',
            position: 'top-right',
            loaderBg:'#ff6849',
            icon: 'success',
            hideAfter: 3500, 
            stack: 6
          });
    @endif  

    
       //datatable
    var table = $('#table-representative').DataTable({
        "language": {
            "url": "//cdn.datatables.net/plug-ins/1.10.21/i18n/Spanish.json"
        },
        "displayLength": 10,
        "columnDefs": [ {
            "targets": 'no-sort',
            "orderable": false
        } ],
        dom: 'Bfrtip',
        buttons: [
        {
            extend: 'copy',
            exportOptions: {
                columns: [ 0, 1, 2, 3 ]
            }
        },
        {
            extend: 'csv',
            exportOptions: {
                columns: [ 0, 1, 2, 3 ]
            }
        },
        {
            extend: 'excel',
            exportOptions: {
                columns: [ 0, 1, 2, 3 ]
            }
        },
        {
            extend: 'pdf',
            exportOptions: {
                columns: [ 0, 1, 2, 3 ]
            }
        },
        {
            extend: 'print',
            exportOptions: {
                columns: [ 0, 1, 2, 3 ]
            }
        }],
        "order": [[ 1, "asc" ]]
    })
    $('#show-representative').on('show.bs.modal', function (event) {
        var button = $(event.relatedTarget) // Button that triggered the modal
        var representativeAvatar = button.data('avatar')
        var representativeName = button.data('name')
        var representativeDocument = button.data('document')
        var representativeFamily = button.data('family')
        var representativePhone = button.data('phone')
        var representativeBirthdate = button.data('birthdate')
        var representativePrincipal = button.data('principal')
        var representativeCountry = button.data('country')
        var representativeGender = button.data('gender')
        var representativeRelationship = button.data('relationship')
        var representativecorreo = button.data('correo')
        var modal = $(this)
        modal.find('#representative-avatar').html("<img src='"+representativeAvatar+"' alt='user' class='img-circle img-responsive'>")
        modal.find('#representative-family').text(representativeFamily)
        if(representativePrincipal == 1) modal.find('#representative-principal').html('<h3><span class="badge badge-info">Principal</span></h3>')
        else modal.find('#representative-principal').html('<h3><span class="badge badge-success">Secundario</span></h3>')
        modal.find('#representative-name').text(representativeName)
        modal.find('#representative-document').text(representativeDocument)
        modal.find('#representative-birthdate').text(representativeBirthdate)
        modal.find('#representative-country').text(representativeCountry)
        modal.find('#representative-phone').text(representativePhone)
        modal.find('#representative-gender').text(representativeGender)
        modal.find('#representative-relationship').text(representativeRelationship)
        modal.find('#representative-correo').text(representativecorreo)
        modal.find('#representative-address').html("<br> <br> <abbr title='Phone'>P:</abbr> "+representativePhone)
    })
 })   



</script>
@endsection
