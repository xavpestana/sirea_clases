@extends('layouts.app')

@section('title-view', 'Registro General - Estudiantes')

@section('extra-css')
    <link href="{{ asset('themeforest/main/css/jquery.toast.css', Request::secure()) }}" id="theme" rel="stylesheet">
    <!--alerts CSS -->
    <link href="{{ asset('themeforest/main/css/sweetalert.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ asset('themeforest/main/css/jquery.dataTables.min.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ asset('themeforest/main/css/dataTables.bootstrap4.min.css') }}" rel="stylesheet" type="text/css">
@endsection


@section('content')
<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-body">
                <h4 class="card-title">Listado de Estudiantes</h4>
                <h6 class="card-subtitle">En la tabla se muestran todos los estudiantes que se han agregado desde las cuentas de familia</h6>
                <div class="table-responsive m-t-40">
                    <table id="table-student" class="display nowrap table table-hover table-striped table-bordered" cellspacing="0" width="100%">
                        <thead>
                            <tr>
                                <th>Familia</th>
                                <th>Documento</th>
                                <th>Nombres y Apellidos</th>
                                <th>Estatus</th>
                                <th class="no-sort">Acciones</th>
                            </tr>
                        </thead>
                        <tfoot>
                            <tr>
                                <th>Familia</th>
                                <th>Documento</th>
                                <th>Nombres y Apellidos</th>
                                <th>Estatus</th>
                                <th class="no-sort">Acciones</th>
                            </tr>
                        </tfoot>
                        <tbody>
                            @foreach ($students as $student)
                            <tr>
                                <td>{{ $student->family->last_name_1 }} {{ $student->family->last_name_2 }}</td>
                                <td>{{ $student->profile->document_type }}-{{ $student->profile->document_number }}</td>
                                <td>{{ $student->profile->first_name_1 }} {{ $student->profile->first_name_2 }} {{ $student->profile->last_name_1 }} {{ $student->profile->last_name_2 }}</td>
                                <td></td>
                                <td>
                                    <div class="btn-group" role="group" aria-label="Basic example">
                                        <button type="button" data-toggle="modal" data-target="#show-student"
                                            class="btn btn-info"
                                            data-avatar="{{asset('storage/pictures/avatars/'.$student->profile->avatar)}}"
                                            data-email="{{$student->profile->user->email}}"
                                            data-family="{{$student->family->last_name_1}} {{$student->family->last_name_2}}"
                                            data-name="{{$student->profile->first_name_1}} {{$student->profile->first_name_2}} {{$student->profile->last_name_1}} {{$student->profile->last_name_2}}"
                                            data-document="{{$student->profile->document_type}}-{{$student->profile->document_number}}"
                                            data-birthdate="{{$student->profile->birthdate}}"
                                            data-phone="{{$student->profile->phone}}"
                                            data-country="{{$student->profile->country->name}}"
                                            data-gender="{{$student->profile->gender->name}}"
                                            data-address="{{$student->profile->address}}"

                                            data-shirt="{{$student->shirt_size}}"
                                            data-pant="{{$student->pant_size}}"
                                            data-shoe="{{$student->shoe_size}}"
                                            data-weight="{{$student->weight}}"
                                            data-height="{{$student->height}}"
                                            data-blood="{{$student->blood_type}}"
                                            data-motor="{{$student->motor_deficiency}}"
                                            data-intelectual="{{$student->intellectual_deficiency}}"
                                            data-hearing="{{$student->hearing_impairment}}"
                                            data-visual="{{$student->visual_deficiency}}"
                                            data-respiratory="{{$student->respiratory_deficiency}}"
                                            data-health="{{$student->health_problem}}"
                                            >
                                            <a data-toggle="tooltip" data-placement="top" title="Ver datos del estudiante">
                                            <i class="ti-eye"></i>
                                            </a>
                                        </button>
                                        <a href="{{ route('school.students.edit', $student->id) }}" class="btn btn-warning" data-toggle="tooltip" title="Editar estudiante"><i class="ti-marker-alt"></i></a>
                                        <button id="btn-download" type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                        <i class="fa fa-file-pdf-o"></i>
                                        </button>
                                        <div class="dropdown-menu" aria-labelledby="btn-download">
                                          <a class="dropdown-item" href="{{ route('pdf.planilla', $student->id) }}">Planilla Matriculación</a>
                                          {{-- <a class="dropdown-item" href="{{ route('pdf.student-carnet', '1') }}">Carnet</a> --}}
                                          {{-- <a class="dropdown-item" href="{{ route('bc') }}">bc</a> --}}
                                        </div>
                                    </div>
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- sample modal content -->
<div id="show-student" class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="vcenter" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="vcenter">Estudiante <strong id="student-family"></strong></h4>
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-4 col-lg-3 text-center">
                        <a id="student-avatar" ></a>
                    </div>
                    <div class="col-md-8 col-lg-9 text-center">
                        <h3 id="student-name" class="box-title m-b-0"></h3>
                        <small class="text-muted">Usuario / Correo Electrónico</small>
                        <h6 id="student-email"></h6>
                        <small class="text-muted">Documento de Identidad</small>
                        <h6 id="student-document"></h6>
                    </div>
                </div>
                <div class="row p-t-30 p-l-20">
                    <div class="col-md-4 col-lg-4">
                        <small class="text-muted">Fecha de nacimiento</small>
                        <h6 id="student-birthdate"></h6>
                    </div>
                    <div class="col-md-4 col-lg-4">
                        <small class="text-muted">País de nacimiento</small>
                        <h6 id="student-country"></h6>
                    </div>        
                    <div class="col-md-4 col-lg-4">
                        <small class="text-muted">Teléfono</small>
                        <h6 id="student-phone"></h6>
                    </div>
                    <div class="col-md-3 col-lg-3">
                        <small class="text-muted">Genero</small>
                        <h6 id="student-gender"></h6>
                    </div>
                    <div class="col-md-3 col-lg-3">
                        <small class="text-muted">Talla de camisa</small>
                        <h6 id="student-shirt"></h6>
                    </div>
                    <div class="col-md-3 col-lg-3">
                        <small class="text-muted">Talla de pantalon</small>
                        <h6 id="student-pant"></h6>
                    </div>
                    <div class="col-md-3 col-lg-3">
                        <small class="text-muted">Talla de calzado</small>
                        <h6 id="student-shoe"></h6>
                    </div>
                    <div class="col-md-4 col-lg-4">
                        <small class="text-muted">Altura</small>
                        <h6 id="student-height"></h6>
                    </div>
                    <div class="col-md-4 col-lg-4">
                        <small class="text-muted">Peso</small>
                        <h6 id="student-weight"></h6>
                    </div>
                    <div class="col-md-4 col-lg-4">
                        <small class="text-muted">Tipo de Sangre</small>
                        <h6 id="student-blood"></h6>
                    </div>

                    <div class="col-md-12 col-lg-12">
                        <h3>Compromisos Especiales:</h3>
                    </div>                    
                    <div class="col-md-3 col-lg-3">
                        <small class="text-muted">Motora</small>
                        <h6 id="student-motor"></h6>
                    </div>
                    <div class="col-md-3 col-lg-3">
                        <small class="text-muted">Intelectual</small>
                        <h6 id="student-intelectual"></h6>
                    </div>
                    <div class="col-md-3 col-lg-3">
                        <small class="text-muted">Auditiva</small>
                        <h6 id="student-hearing"></h6>
                    </div>
                    <div class="col-md-3 col-lg-3">
                        <small class="text-muted">Visual</small>
                        <h6 id="student-visual"></h6>
                    </div>
                    <div class="col-md-3 col-lg-3">
                        <small class="text-muted">Respiratoria</small>
                        <h6 id="student-respiratory"></h6>
                    </div>
                    <div class="col-md-12 col-lg-12">
                        <small class="text-muted">Otro problema de salud:</small>
                        <h6 id="student-health"></h6>
                    </div>                    
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-info waves-effect" data-dismiss="modal">Cerrar</button>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
<!-- /.modal -->

@endsection

@section('extra-js')

<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.0/jquery.validate.min.js"></script>
<script src="{{ asset('themeforest/main/js/jquery.toast.js') }}"></script>
<script src="{{ asset('themeforest/main/js/sweetalert.min.js') }}"></script>
<script src="{{ asset('themeforest/main/js/jquery.dataTables.min.js') }}"></script>

<script src="{{ asset('themeforest/main/js/datatable/dataTables.bootstrap4.min.js') }}"></script>
<script src="{{ asset('themeforest/main/js/datatable/pdfmake.min.js') }}"></script>
<script src="{{ asset('themeforest/main/js/datatable/vfs_fonts.js') }}"></script>
<script src="{{ asset('themeforest/main/js/datatable/dataTables.buttons.min.js') }}"></script>
<script src="{{ asset('themeforest/main/js/datatable/buttons.flash.min.js') }}"></script>
<script src="{{ asset('themeforest/main/js/datatable/buttons.html5.min.js') }}"></script>
<script src="{{ asset('themeforest/main/js/datatable/buttons.print.min.js') }}"></script>

<script>
$(function(){
    @if (\Session::has('error'))
           $.toast({
            heading: 'Error',
            text: '{{ Session::get('error') }}',
            position: 'top-right',
            loaderBg:'#ff6849',
            icon: 'error',
            hideAfter: 3500
          });
    @endif

    @if (\Session::has('success'))
           $.toast({
            heading: '!Operación exitosa!',
            text: '{{ Session::get('success') }}',
            position: 'top-right',
            loaderBg:'#ff6849',
            icon: 'success',
            hideAfter: 3500, 
            stack: 6
          });
    @endif  

    var table = $('#table-student').DataTable({
        "language": {
            "url": "//cdn.datatables.net/plug-ins/1.10.21/i18n/Spanish.json"
        },
        dom: 'Bfrtip',
        buttons: [
        {
            extend: 'copy',
            exportOptions: {
                columns: [ 0, 1, 2, 3 ]
            }
        },
        {
            extend: 'csv',
            exportOptions: {
                columns: [ 0, 1, 2, 3 ]
            }
        },
        {
            extend: 'excel',
            exportOptions: {
                columns: [ 0, 1, 2, 3 ]
            }
        },
        {
            extend: 'pdf',
            exportOptions: {
                columns: [ 0, 1, 2, 3 ]
            }
        },
        {
            extend: 'print',
            exportOptions: {
                columns: [ 0, 1, 2, 3 ]
            }
        }],
        "displayLength": 10,
        "columnDefs": [ {
            "targets": 'no-sort',
            "orderable": false
        } ],
        "order": [[ 2, "asc" ]]
    })
})



    $('#show-student').on('show.bs.modal', function (event) {
        var button = $(event.relatedTarget) // Button that triggered the modal
        var studentAvatar = button.data('avatar')
        var studentName = button.data('name')
        var studentEmail = button.data('email')
        var studentDocument = button.data('document')
        var studentFamily = button.data('family')
        var studentPhone = button.data('phone')
        var studentBirthdate = button.data('birthdate')
        var studentCountry = button.data('country')
        var studentGender = button.data('gender')
        var studentShirt = button.data('shirt')
        var studentPant = button.data('pant')
        var studentShoe = button.data('shoe')
        var studentWeight = button.data('weight')
        var studentHeight = button.data('height')
        var studentBlood = button.data('blood')
        var studentMotor = button.data('motor')
        var studentIntelectual = button.data('intelectual')
        var studentHearing = button.data('hearing')
        var studentVisual = button.data('visual')
        var studentRespiratory = button.data('respiratory')
        var studentHealth = button.data('health')


        var modal = $(this)
        modal.find('#student-avatar').html("<img src='"+studentAvatar+"' alt='user' class='img-circle img-responsive img-avatar'>")
        modal.find('#student-name').text(studentName)
        modal.find('#student-email').text(studentEmail)
        modal.find('#student-document').text(studentDocument)
        modal.find('#student-family').text(studentFamily)

        modal.find('#student-phone').text(studentPhone) 
        modal.find('#student-birthdate').text(studentBirthdate) 
        modal.find('#student-country').text(studentCountry)
        modal.find('#student-gender').text(studentGender) 
        modal.find('#student-shirt').text(studentShirt) 
        modal.find('#student-pant').text(studentPant) 
        modal.find('#student-shoe').text(studentShoe)
        modal.find('#student-weight').text(studentWeight) 
        modal.find('#student-height').text(studentHeight) 
        modal.find('#student-blood').text(studentBlood) 
        modal.find('#student-motor').text(evaluar(studentMotor)) 
        modal.find('#student-intelectual').text(evaluar(studentIntelectual)) 
        modal.find('#student-hearing').text(evaluar(studentHearing)) 
        modal.find('#student-visual').text(evaluar(studentVisual)) 
        modal.find('#student-respiratory').text(evaluar(studentRespiratory)) 
        modal.find('#student-health').text(studentHealth) 
    })

    function evaluar(deficiency){
        if(deficiency === 0) return 'no';
        if(deficiency === 1) return 'si';
        if(deficiency == null) return 'no definido';
    }
</script>

@endsection

