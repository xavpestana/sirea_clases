@extends('layouts.app')

@section('title-view', 'Academico - Listado de Asignaturas')

@section('extra-css')
<!-- Latest compiled and minified CSS -->
<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-select@1.13.14/dist/css/bootstrap-select.min.css">
<link href="{{ asset('themeforest/multiselect/css/multi-select.css', Request::secure()) }}" rel="stylesheet" type="text/css" />
<link href="https://cdn.datatables.net/1.10.21/css/jquery.dataTables.min.css" rel="stylesheet">
@endsection
@section('content')
@isset($seasons->sections_number)
<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-body">
                <div class="row">
                    <div class="col-lg-6 col-sm-4 col-md-4 col-xs-12">
                        <h4 class="card-title">Listado de Asignaturas o cursos</h4>
                    </div>
                    <div class="col-lg-3 col-sm-4 col-md-4 col-xs-12 ml-auto">
                        <button class="btn btn-block btn-success" data-toggle="modal" data-target="#exampleModal" data-whatever="@getbootstrap">Agregar nuevo</button>
                    </div>
                </div>
                <div id="table" class="table-responsive m-t-40">
                    <svg class="circular" viewBox="25 25 50 50">
                        <circle class="path" cx="50" cy="50" r="20" fill="none" stroke-width="2" stroke-miterlimit="10" /> 
                    </svg>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel1">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="exampleModalLabel1">Nueva asignatura</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            </div>
            <form id="form_new_course" accept-charset="utf-8">
                @csrf
            <div class="modal-body">
                <div class="row">
                    <div class="form-group col-md-12">
                        <label for="document">Nombre de Asignatura o curso <span style="color: red">*</span></label>
                        <input type="text" class="form-control" id="name" name="name" required="">
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label>Grado</label>
                            <select class="form-control custom-select" id="id_grade" name="id_grade" onchange="Grado()">
                                <option value="nada">-- Seleccione un grado --</option>
                                @foreach($grades as $grade)
                                <option value="{{ $grade->id }}">{{ $grade->name }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label>Sección</label>
                            <span id="select_level">
                                <select class="form-control custom-select">
                                    <option disabled="">Seleccione un grado</option>
                                </select>
                            </span>
                        </div>
                    </div>
                    <div class="col-md-10 mx-auto">
                            <label>Docentes a cargo  <span style="color: red">*</span></label>
                            <select id='pre-selected-options' multiple='multiple' name="teacher[]" required="">
                                @foreach($teachers as $teacher)
                                <option value='{{ $teacher->profile->user_id }}'>{{ $teacher->profile->first_name_1 }} {{ $teacher->profile->last_name_1 }}</option>
                                @endforeach
                            </select>
                            <div class="button-box m-t-20"> <a id="select-all" class="btn btn-danger" href="#">Seleccionar todos</a> <a id="deselect-all" class="btn btn-success" href="#">Deseleccionar todos</a> </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                <button id="save" type="submit" class="btn btn-primary">Guardar</button>
            </div>
            </form>
        </div>
    </div>
</div>
<div class="modal fade" id="editModal" tabindex="-1" role="dialog" aria-labelledby="editModalLabel1">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="editModalLabel1">Editar asignatura</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            </div>
            <form id="form_updt_subjects" accept-charset="utf-8">
                @csrf
            <div id="edit_subjects" class="modal-body">
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                <button id="update" type="submit" class="btn btn-primary">Guardar</button>
            </div>
            </form>
        </div>
    </div>
</div>
@endif
@endsection

@section('extra-js')
<script src="https://cdn.jsdelivr.net/npm/sweetalert2@9"></script>
<script src="https://cdn.datatables.net/1.10.21/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.6.2/js/dataTables.buttons.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.6.2/js/buttons.flash.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/pdfmake.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/vfs_fonts.js"></script>
<script src="https://cdn.datatables.net/buttons/1.6.2/js/buttons.html5.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.6.2/js/buttons.print.min.js"></script>
<script type="text/javascript" src="{{ asset('themeforest/multiselect/js/jquery.multi-select.js', Request::secure()) }}"></script>
<script>
/*******  Validate forms  ********/
(function() {
   'use strict';
        window.addEventListener('load', function() {
        // Fetch all the forms we want to apply custom Bootstrap validation styles to
        var forms = document.getElementsByClassName('needs-validation');
        // Loop over them and prevent submission
        var validation = Array.prototype.filter.call(forms, function(form) {
            form.addEventListener('submit', function(event) {
                if (form.checkValidity() === false) {
                    event.preventDefault();
                    event.stopPropagation();
                }
            form.classList.add('was-validated');
        }, false);
        });
        }, false);
})();
        $('#pre-selected-options').multiSelect();
        $('#select-all').click(function() {
            $('#pre-selected-options').multiSelect('select_all');
            return false;
        });
        $('#deselect-all').click(function() {
            $('#pre-selected-options').multiSelect('deselect_all');
            return false;
        });
/*******  Mostrar Materias  ********/

$('#table').load('{{ route('courses.table') }}');


/*******  Guardar materias  ********/
$( "#form_new_course" ).submit(function(e) {
e.preventDefault();
    var id_grade = $("#id_grade").val();
    if (id_grade != 'nada') {
  $("#save").prop('disabled', true);
  datos = $("#form_new_course").serialize();
     
    $.ajax({
        type:'GET',
        data:datos,
        url:"{{ route('courses.create') }}",
        success:function(r)
        {
          if (r==0) {
            $('#form_new_course')[0].reset();
            $('#table').load('{{ route('courses.table') }}');
            $("#save").prop('disabled', false);
            $('#pre-selected-options').multiSelect('deselect_all');
            Swal.fire({
                position: 'center-center',
                icon: 'success',
                title: 'La asignatura se ha guardado exitosamente',
                showConfirmButton: false,
                timer: 1500
            }) 
          }else{
            $("#save").prop('disabled', false);
            Swal.fire({
                icon: 'error',
                title: 'Oops...',
                text: 'No se guardo correctamente, intente denuevo!',
                })
          }
        }
        })
            }else{
                alert('Seleccione un grado');
            }
    });

/********  Editar Materias  *********/

function edit_subjects(id) {
$('#edit_subjects').html(' ');
$.ajax({
            type : "GET",
            data : {"id" : id, "_token": "{{ csrf_token() }}"},
            url : "{{ url('colegio/courses/') }}/"+id+"/editar",
            success : function(data){
                $('#edit_subjects').html(data);
            }
        });
}

/********  Update Materias  *********/

$( "#form_updt_subjects" ).submit(function(e) {
e.preventDefault();

  $("#update").prop('disabled', true);
  datos = $("#form_updt_subjects").serialize();
  var id = $("#id_course").val();
  
$.ajax({
        type:'PUT',
        data:datos,
        url:"{{ url('colegio/courses/') }}/"+id,
        success:function(r)
        {
          if (r==0) {  
            $('#updt-options').multiSelect('deselect_all');       
            $('#table').load('{{ route('courses.table') }}');
            $("#update").prop('disabled', false);
            $('#editModal').modal('hide')
            Swal.fire({
                position: 'center-center',
                icon: 'success',
                title: 'La materia se ha actualizado satisfactoriamente',
                showConfirmButton: false,
                timer: 1500
            }) 
          }else{
            $("#update").prop('disabled', false);
            Swal.fire({
                icon: 'error',
                title: 'Oops...',
                text: 'No se guardo correctamente, intente denuevo!',
                })
          }
        }
        })
});

function delete_subjects(id) {
      Swal.fire({
  title: '¿Está seguro?',
  text: "No podrá revertir esto!",
  icon: 'warning',
  showCancelButton: true,
  confirmButtonColor: '#3085d6',
  cancelButtonColor: '#d33',
  confirmButtonText: 'Yes, delete it!'
}).then((result) => {
  if (result.value) {
   
   $.ajax({
            type : "DELETE",
            data : {"id" : id, "_token": "{{ csrf_token() }}"},
            url : "{{ url('colegio/courses/') }}/"+id,
            success : function(r){
                if (r==0){
                  $('#table').load('{{ route('courses.table') }}');

                  Swal.fire(
      'Eliminado!',
      'El materia ha sido eliminado con exito.',
      'success'
    )
                }else{
                  Swal.fire({
                icon: 'error',
                title: 'Oops...',
                text: 'No se elimino correctamente, intente denuevo!',
                })
                }
            }
        });
    
  }
})
}

/**********Grado*********/
@isset($seasons->id)
function Grado() {
  var id = $("#id_grade").val();
    $("#select_level").html('<svg class="circular" viewBox="25 25 50 50"> <circle class="path" cx="50" cy="50" r="20" fill="none" stroke-width="2" stroke-miterlimit="10" /> </svg>');
    $.ajax({
        type:'GET',
        data: {"id_grade" : id,"id_season" : {{ $seasons->id }}, "_token": "{{ csrf_token() }}"},
        url:"{{ route('courses.sections') }}",
        success:function(data)
        {
            $("#select_level").html(data);
        }
        })
}
@endif
</script>
@endsection

