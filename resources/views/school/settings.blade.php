@extends('layouts.app')

@section('title-view', 'Configuraciones')

@section('extra-css')
    <!--alerts CSS -->
    <link href="{{ asset('themeforest/main/css/sweetalert.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ asset('themeforest/main/css/jquery.toast.css', Request::secure()) }}" id="theme" rel="stylesheet">
@endsection

@section('content')
<!-- Row -->
<div class="row">
{{--    <div class="col-12 m-t-30">
        <h4 class="m-b-0">Configuración Básica</h4>
        <p class="text-muted m-t-0 font-12">Información necesaria sobre la institucion y configuraciones iniciales para identificar el año escolar</p>
    </div>--}}
    <div class="col-md-12">
        @php($actual = $seasons->where('status', 1)->first())
        <div class="card @if($actual) card-outline-info @else card-outline-danger @endif">
            <div class="card-header">
                <h4 class="m-b-0 text-white">Configuración de Año Escolar</h4>
            </div>
            <div class="card-body">
                @if($actual)
                <h3 class="card-title">Año Escolar actual: {{$actual->season}}</h3>
                <hr>
                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group row">
                            <label class="control-label text-right col-md-6">Cantidad de Periodos o Momentos:</label>
                            <div class="col-md-6">
                                <p class="form-control-static">{{ $actual->periods_qty }}</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group row">
                            <label class="control-label text-right col-md-6">Escala máxima de evaluación:</label>
                            <div class="col-md-6">
                                <p class="form-control-static">{{ $actual->max_score }}</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group row">
                            <label class="control-label text-right col-md-6">Pago inicial:</label>
                            <div class="col-md-6">
                                <p class="form-control-static">{{ $actual->initial_pay }}</p>
                            </div>
                        </div>
                    </div>
                </div>
                
                <h3 class="card-title">Periodos del Año Escolar</h3>
                <hr>
                <p class="text-muted m-t-0 font-12">Cambie el Periodo del Año Escolar para que aparezca por defecto seeccionado en las evaluaciones y asignaiones de los docentes</p>
                <div class="btn-group col-md-12 pb-5" data-toggle="buttons">
                    @foreach ($actual->periods as $period)
                        <label class="btn btn-primary @if($period->status === 1) active @endif">
                            <div class="custom-control custom-radio">
                                <input type="radio" id="period-[{{ $period->number }}]" data-id="{{ $period->id }}" data-number="{{ $period->number }}" name="periods" class="custom-control-input periods" @if($period->status === 1) checked @endif>
                                <label class="custom-control-label" for="period-[{{ $period->number }}]">Periodo {{ $period->number}}</label>
                            </div>
                        </label>
                    @endforeach
                </div>

                <h3 class="card-title">Cantidad de Grupos o Secciones por grado</h3>
                <hr>
                <p class="text-muted m-t-0 font-12">Verifique la cantidad de secciones creadas para cada grado, si desea agregar mas puede hacer clic en los botones (+) del grado que lo requiera </p>

                <div class="row">
                    @foreach($grades as $grade)
                        <div class="col-md-4">
                            <div class="form-group row">
                                <label class="control-label text-right col-md-8">{{ $grade->name }}:</label>
                                <div class="col-md-2">
                                    <p class="form-control-static">{{ $actual->sections->where('grade_id', $grade->id)->count() }}</p>
                                </div>
                                <div class="col-md-2">
                                    <button type="button" data-grade-name="{{ $grade->name }}" data-grade="{{ $grade->id }}" data-season="{{ $actual->id }}" data-number="{{ $actual->sections->where('grade_id', $grade->id)->count() }}" class="btn waves-effect waves-light btn-sm btn-info add-section"><i class="fa fa-plus"></i></button>
                                </div>
                            </div>
                        </div>
                    @endforeach
                </div>

                <h3 class="card-title">Finalizar Año Escolar</h3>
                <hr>
                <div class="row">
                    <div class="col-lg-8 col-md-8">

                        <p class="text-muted m-t-0 font-12">Al hacer clic en el siguiente botón se finalizar el año escolar actual, se habilita un enlace para la creación de nuevo año escolar.</p>
                    </div>
                    <div class="col-lg-4 col-md-4 text-center">
                        <button type="button" class="btn waves-effect waves-light btn-block btn-danger" id="finalizar">FINALIZAR AÑO ESCOLAR</button>
                    </div>
                </div>
                @else


                <h3 class="card-title">No existe un año escolar activo</h3>
                <p class="card-text">Necesita agregar un año escolar activo para realizar procesos de inscripción, pagos y crear periodos para evaluaciones, puede crear un nuevo año escolar haciendo clic en el siguiente botón</p>
                <a href="{{ route('season.create') }}" class="btn btn-inverse">Crear Año Escolar</a>
                @endif
            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-lg-12">
        <div class="card card-outline-info">
            <div class="card-header">
                <h4 class="m-b-0 text-white">Información de la institución</h4>
            </div>
            <div class="card-body">
                <div class="form-body">
                    <h3 class="card-title">Nombre: {{ $school->school_type }} {{ $school->name }}</h3>
                    <hr>
                    <div class="row">
                        <div class="col-md-3">                        
                            <img class="img-fluid" src="{{ asset('storage/pictures/logos/'.$school->logo) }}" alt="">
                        </div>    
                        <div class="col-md-9">
                            <div class="row">
                                <div class="col-md-4">
                                    <small class="text-muted">Rif</small>
                                    <h6>{{ $school->rif }}</h6>
                                </div>
                                <div class="col-md-4">
                                    <small class="text-muted">Código Estadístico </small>
                                    <h6>{{ $school->stadistic_code }}</h6>
                                </div>
                                <div class="col-md-4">
                                    <small class="text-muted">Código DEA</small>
                                    <h6>{{ $school->dea_code }}</h6>
                                </div>

                                <div class="col-md-12">
                                    <small class="text-muted">Dirección</small>
                                    <h6>
                                        @isset($school->parish)
                                        {{ $school->address }}, parroquia {{ $school->parish->name }}, municipio {{ $school->parish->municipality->name }}. @isset($school->city){{ $school->city->name }}@endisset - {{ $school->parish->municipality->state->name }}
                                        @endisset
                                    </h6>
                                </div>
                                <div class="col-md-6">
                                    <small class="text-muted">Teléfono</small>
                                    <h6>{{ $school->phone }}</h6>
                                </div>
                                <div class="col-md-6">
                                    <small class="text-muted">Fax</small>
                                    <h6>{{ $school->fax }}</h6>
                                </div>
                                <div class="col-md-6">
                                    <small class="text-muted">Correo Electrónico</small>
                                    <h6>{{ $school->email }}</h6>
                                </div>
                                <div class="col-md-6">
                                    <small class="text-muted">Url</small>
                                    <h6>{{ $school->url }}</h6>
                                </div>
                                @isset($director)
                                <div class="col-md-12">
                                    <h4>Datos del Director</h4>
                                </div>

                                <div class="col-md-4">
                                    <small class="text-muted">Nombre y Apellido</small>
                                    <h6>{{ $director->name }}</h6>
                                </div>
                                <div class="col-md-4">
                                    <small class="text-muted">Documento de identidad</small>
                                    <h6>{{ $director->document_type }}-{{ $director->document_number }}</h6>
                                </div>
                                <div class="col-md-4">
                                    <small class="text-muted">Género</small>
                                    <h6>{{ $director->gender->name}}</h6>
                                </div>
                                @endisset

                                <div class="col-md-12 pt-4 text-center">
                                    <a href="{{ route('school.edit', auth()->user()->userSchool->school_id)  }}" class="btn btn-info">Editar Información</a>
                                </div>    
                            </div>
                        </div>    
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection


@section('extra-js')
<script src="{{ asset('themeforest/main/js/sweetalert.min.js') }}"></script>
<script src="{{ asset('themeforest/main/js/jquery.toast.js') }}"></script>

<script>
    $(function(){
        @if (\Session::has('error'))
               $.toast({
                heading: 'Error',
                text: '{{ Session::get('error') }}',
                position: 'top-right',
                loaderBg:'#ff6849',
                icon: 'error',
                hideAfter: 3500
              });
        @endif

        @if (\Session::has('success'))
               $.toast({
                heading: '!Operación exitosa!',
                text: '{{ Session::get('success') }}',
                position: 'top-right',
                loaderBg:'#ff6849',
                icon: 'success',
                hideAfter: 3500, 
                stack: 6
              });
        @endif  


        $('.periods').on('click', function(e){
            e.preventDefault();
            var periodId = $(this).data('id');
            var periodNumber = $(this).data('number');
            swal({   
                title: "¿Cambiar Periodo?",   
                text: "Al selecionar el Periodo "+periodNumber+" las evaluaciones y demas asignaciones serán aplicadas para dicho periodo del año escolar",   
                type: "warning",   
                showCancelButton: true,   
                confirmButtonColor: "#DD6B55",   
                confirmButtonText: "Si, Cambiar",   
                closeOnConfirm: false 
            }, function(){
                $.ajax({
                    headers: {'X-CSRF-Token': $('meta[name="_token"]').attr('content')},
                    url: "/colegio/periodo/"+periodId,
                    type: 'patch',
                    dataType: 'json',
                    data:{id:periodId}
                })
                .done(function(data) {
                    $('#app').empty().append($(data));
                    swal({
                    title: "Periodo activado",
                    text: "El periodo "+periodNumber+" esta habitado ahora en el sistema",
                    type: "success"
                    },function (){
                        location.reload();
                    });
                }); 
            });
        })
        $('.add-section').on('click', function(e){
            e.preventDefault();
            var gradeId = $(this).data('grade');
            var gradeName = $(this).data('grade-name');
            var seasonId = $(this).data('season');
            var number = $(this).data('number');
            swal({   
                title: "¿Agregar Sección?",   
                text: "Se agregar una nueva seccion al "+gradeName+" para el presente Año escolar",   
                type: "warning",   
                showCancelButton: true,   
                confirmButtonColor: "#DD6B55",   
                confirmButtonText: "Si, Agregar",   
                closeOnConfirm: false 
            }, function(){
                $.ajax({
                    headers: {'X-CSRF-Token': $('meta[name="_token"]').attr('content')},
                    url: "{{ route('section.store') }}",
                    type: 'post',
                    dataType: 'json',
                    data:{grade_id:gradeId, season_id: seasonId, number:number}
                })
                .done(function(data) {
                    $('#app').empty().append($(data));
                    swal({
                    title: "Sección agregrada",
                    text: "El "+gradeName+" tiene una nueva sección o grupo disponible",
                    type: "success"
                    },function (){
                        location.reload();
                    });
                }); 
            });
        })
        $('#finalizar').on('click', function(e){
            e.preventDefault();
            var actual = "@isset($actual){{ $actual->id }}@endisset";
            var actualName = "@isset($actual){{ $actual->season }}@endisset";
            swal({   
                title: "¿Finalizar Año escolar?",   
                text: "Se procederá a la culminación en el sistema del Año escolar "+actualName+".",   
                type: "warning",   
                showCancelButton: true,   
                confirmButtonColor: "#DD6B55",   
                confirmButtonText: "Si, Finalizar",   
                closeOnConfirm: false 
            }, function(){
                $.ajax({
                    headers: {'X-CSRF-Token': $('meta[name="_token"]').attr('content')},
                    url: "@isset($actual){{ route('season.update',$actual) }}@endisset",
                    type: 'patch',
                    dataType: 'json',
                    data:{id:actual}
                })
                .done(function(data) {
                    swal({
                    title: "Año Escolar "+actualName+" Finalizado",
                    text: "El sistema está listo para la creacion de un nuevo año escolar en el menú de configuración",
                    type: "success"
                    },function (){
                        location.reload();
                    });
                }); 
            });
        })
    })
</script>
@endsection
