@extends('layouts.app')

@section('extra-css')
    <link href="{{ asset('themeforest/main/css/jquery.toast.css', Request::secure()) }}" id="theme" rel="stylesheet">
    <!--alerts CSS -->
    <link href="{{ asset('themeforest/main/css/sweetalert.css') }}" rel="stylesheet" type="text/css">
<!-- wysihtml5 CSS -->
<link rel="stylesheet" href="{{ asset('themeforest/main/css/html5-editor/bootstrap-wysihtml5.css')}}" />
<!-- Dropzone css 
<link href="../assets/plugins/dropzone-master/dist/dropzone.css" rel="stylesheet" type="text/css" />-->
@endsection

@section('title-view', 'Nuevo mensaje masivo')

@section('content')
<div class="row">
    <div class="col-lg-12">
        <div class="card">
                <form action="{{ route('send-message') }}" method="post">
            <div class="row">
                <div class="col-xlg-3 col-lg-3 col-md-4 ">
                    <div class="card-body inbox-panel">
                        @csrf
                        <h3 class="card-title">Destinatarios</h3>
                        <div class="demo-radio-button">
                            <div class="demo-checkbox">
                                <input type="checkbox" id="chk-docentes" name="chk-docentes"class="chk-col-green" />
                                <label for="chk-docentes">Docentes</label>
                            </div>
                            <input name="todos" type="radio" id="radio-all" class="radio-col-green" value="1" checked/>
                            <label for="radio-all">Todas las familias</label>
                            <input name="todos" type="radio" id="radio-some" class="radio-col-green" value="0"/>
                            <label for="radio-some">Ninguna familia</label>                            
                            <input name="todos" type="radio" id="radio-group" class="radio-col-green" value="2"  />
                            <label for="radio-group">Familias de: </label>
                            <div class="demo-checkbox pl-3" id="chk-groups">

                            @foreach ($grades as $grade)
                                @if($sections->has($grade->id))
                                <div>
                                    <input type="checkbox" id="grade[{{ $grade->id }}]" class="chk-col-green chk-grade" />
                                    <label for="grade[{{ $grade->id }}]">{{ $grade->name }}</label>
                                    <div class="demo-checkbox pl-3">
                                        @foreach ($sections[$grade->id] as $section)
                                        <input type="checkbox" id="section[{{ $section->id }}]" name="section[{{ $section->id }}]" class="chk-col-grey chk-sections" value="{{ $section->id }}" />
                                        <label for="section[{{ $section->id }}]">{{ $section->grade->name }} {{ $section->name }}</label>
                                        @endforeach
                                    </div>
                                </div>
                                @endif
                            @endforeach

                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-xlg-9 col-lg-9 col-md-8 bg-light-part b-l">
                    <div class="card-body">
                        <h3 class="card-title">Redacción del mensaje</h3>
                        <div class="form-group">
                            <input type="text" name="subject" id="subject" class="form-control" placeholder="Tema:" required>
                        </div>
                        <div class="form-group">
                            <textarea name="body" id="body" class="textarea_editor form-control" rows="15" placeholder="Escriba el mensaje aqui" required></textarea>
                        </div>
                        <div class="row m-t-20">
                            <div class="col-lg-3">
                                <button type="submit" class="btn btn-block btn-success "><i class="fa fa-envelope-o"></i> Enviar</button>
                            </div>
                            <div class="col-lg-9">
                                                       
                            </div>
                        </div>
                    </div>
                </div>
            </div>
                </form>
        </div>
    </div>
</div>
@endsection


@section('extra-js')
<script src="{{ asset('themeforest/main/js/jquery.toast.js') }}"></script>
<script src="{{ asset('themeforest/main/js/sweetalert.min.js') }}"></script>
<!--Custom JavaScript -->
<script src="{{ asset('themeforest/main/js/html5-editor/wysihtml5-0.3.0.js') }}"></script>
<script src="{{ asset('themeforest/main/js/html5-editor/bootstrap-wysihtml5.js') }}"></script>
{{-- <script src="../assets/plugins/dropzone-master/dist/dropzone.js"></script> --}}
<script>
$(function() {
    $('.textarea_editor').wysihtml5()
    $('#chk-groups').hide()

    @if (\Session::has('error'))
           $.toast({
            heading: 'Error',
            text: '{{ Session::get('error') }}',
            position: 'top-right',
            loaderBg:'#ff6849',
            icon: 'error',
            hideAfter: 3500
          });
    @endif

    @if (\Session::has('success'))
           $.toast({
            heading: '!Operación exitosa!',
            text: '{{ Session::get('success') }}',
            position: 'top-right',
            loaderBg:'#ff6849',
            icon: 'success',
            hideAfter: 3500, 
            stack: 6
          });
    @endif 

    $('#radio-all').on('click',function(event) {
        $('#chk-groups').hide()
        $('.chk-grade').prop('checked', false)
        $('.chk-sections').prop('checked', false)
    })
    $('#radio-some').on('click',function(event) {
        $('#chk-groups').hide()
        $('.chk-grade').prop('checked', false)
        $('.chk-sections').prop('checked', false)
    })
    $('#radio-group').on('click',function(event) {
        $('#chk-groups').show()
    })

    $('.chk-grade').on('change', function(event) {
        if ($(this).prop('checked')) {
            $(this).prop('checked', true)
            if (!$(this).siblings().children(':checkbox').prop('checked')) {
                $(this).siblings().children(':checkbox').prop('checked', true)
            }
        }else{
            $(this).prop('checked', false)
            $(this).siblings().children(':checkbox').prop('checked', false)
        }
    })

    $('.chk-sections').on('change', function(event){
        if ($(this).prop('checked')) {
            $(this).prop('checked', true)
            if($(this).siblings('.chk-sections').length > 0){
                if ($(this).siblings('.chk-sections').prop('checked')) {
                    $(this).parent().siblings('.chk-grade').prop('checked', true)
                }
            }
            else{
                $(this).parent().siblings('.chk-grade').prop('checked', true)
            }
        }else{
            $(this).prop('checked', false)
            $(this).parent().siblings('.chk-grade').prop('checked', false)
        }    
    })
})
</script>
@endsection
