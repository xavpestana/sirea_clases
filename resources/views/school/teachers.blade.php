@extends('layouts.app')

@section('title-view', 'Registro General - Docentes')

@section('extra-css')
<link href="https://cdn.datatables.net/1.10.21/css/jquery.dataTables.min.css" rel="stylesheet">
@endsection
@section('content')
<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-body">
                <div class="row">
                    <div class="col-lg-6 col-sm-4 col-md-4 col-xs-12">
                        <h4 class="card-title">Listado de docentes</h4>
                    </div>
                    <div class="col-lg-3 col-sm-4 col-md-4 col-xs-12 ml-auto">
                        <button class="btn btn-block btn-success" data-toggle="modal" data-target="#exampleModal" data-whatever="@getbootstrap">Agregar docente</button>
                    </div>
                </div>
                <div id="table" class="table-responsive m-t-40">
                    <svg class="circular" viewBox="25 25 50 50">
                        <circle class="path" cx="50" cy="50" r="20" fill="none" stroke-width="2" stroke-miterlimit="10" /> 
                    </svg>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel1">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="exampleModalLabel1">Nuevo docente</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            </div>
            <form id="form_new_teacher" accept-charset="utf-8">
                @csrf
            <div class="modal-body">
                <div class="row">
                    <div class="form-group col-md-3">
                        <label for="tipo">Tipo <span style="color: red">*</span></label>
                        <select id="tipo" name="type_document" class="form-control" required="">
                            <option value="V">V</option>
                            <option value="E">E</option>
                        </select>
                    </div>
                    <div class="form-group col-md-9">
                        <label for="document">Nro de documento <span style="color: red">*</span></label>
                        <input type="text" class="form-control" id="document" name="document" value="" required="">
                    </div>
                    <div class="form-group col-md-6">
                        <label for="name_teacher">Nombres <span style="color: red">*</span></label>
                        <input type="text" class="form-control" id="name_teacher" name="name_teacher" required="">
                    </div>
                    <div class="form-group col-md-6">
                        <label for="last_name_teacher">Apellidos <span style="color: red">*</span></label>
                        <input type="text" class="form-control" id="last_name_teacher" name="last_name_teacher" required="">
                    </div>
                    <div class="form-group col-md-5">
                        <label for="gender">Sexo <span style="color: red">*</span></label>
                        <select id="gender" name="gender" class="form-control" required="">
                            <option value="1">Masculino</option>
                            <option value="2">Femenino</option>
                        </select>
                    </div>
                    <div class="form-group col-md-7">
                        <label for="email">Corre electronico <span style="color: red">*</span></label>
                        <input type="text" class="form-control" id="email" name="email" required="">
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                <button id="save" type="submit" class="btn btn-primary">Guardar</button>
            </div>
            </form>
        </div>
    </div>
</div>
<div class="modal fade" id="editModal" tabindex="-1" role="dialog" aria-labelledby="editModalLabel1">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="editModalLabel1">Editar docente</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            </div>
            <form id="form_edit_teacher" accept-charset="utf-8">
                @csrf
            <div id="edit_profile" class="modal-body">
                
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                <button id="update" type="submit" class="btn btn-primary">Guardar</button>
            </div>
            </form>
        </div>
    </div>
</div>
<div class="modal fade" id="pswModal" tabindex="-1" role="dialog" aria-labelledby="pswModalLabel1">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="pswModalLabel1">Editar password</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            </div>
            <form id="form_edit_psw" accept-charset="utf-8">
                @csrf
            <div class="modal-body">
                <div class="row  px-5">
                   <label for="document">Contraseña nueva <span style="color: red">*</span></label>
                    <div class="input-group">
                        
                        <input type="text" class="form-control" id="new_password" name="new_password" required="">
                            <div class="input-group-append">
                                <button id="generar_psw" class="btn btn-info" type="button">Generar</button>
                            </div>
                        <input type="text" id="id_psw" name="id_user" required="" hidden="">
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                <button id="psw" type="submit" class="btn btn-primary">Guardar</button>
            </div>
            </form>
        </div>
    </div>
</div>
@endsection

@section('extra-js')
<script src="https://cdn.jsdelivr.net/npm/sweetalert2@9"></script>
<script src="https://cdn.datatables.net/1.10.21/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.6.2/js/dataTables.buttons.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.6.2/js/buttons.flash.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/pdfmake.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/vfs_fonts.js"></script>
<script src="https://cdn.datatables.net/buttons/1.6.2/js/buttons.html5.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.6.2/js/buttons.print.min.js"></script>
<script>

/*******  Validate forms  ********/
(function() {
   'use strict';
        window.addEventListener('load', function() {
        // Fetch all the forms we want to apply custom Bootstrap validation styles to
        var forms = document.getElementsByClassName('needs-validation');
        // Loop over them and prevent submission
        var validation = Array.prototype.filter.call(forms, function(form) {
            form.addEventListener('submit', function(event) {
                if (form.checkValidity() === false) {
                    event.preventDefault();
                    event.stopPropagation();
                }
            form.classList.add('was-validated');
        }, false);
        });
        }, false);
})();
/*******  Mostrar docentes  ********/
$('#table').load('{{ route('teachers.table') }}');

/*******  Guardar docentes  ********/
$( "#form_new_teacher" ).submit(function(e) {
e.preventDefault();

  $("#save").prop('disabled', true);
  datos = $("#form_new_teacher").serialize();
          
$.ajax({
        type:'GET',
        data:datos,
        url:"{{ route('teachers.create') }}",
        })
        .done(function(response) {
            $('#form_new_teacher')[0].reset();
            $('#table').load('{{ route('teachers.table') }}');
            $("#save").prop('disabled', false);
            Swal.fire({
                position: 'center-center',
                icon: 'success',
                title: 'El docente se ha guardado exitosamente',
                showConfirmButton: false,
                timer: 1500
            }) 
        })
        .fail(function(response) {
            $("#save").prop('disabled', false);
            Swal.fire({
                icon: 'error',
                title: 'Oops...',
                text: 'Verifique que el correo no ha sido agregado anteriormente.',
                })
        }) 
});
/********  Borrar docente  *********/

function delete_teacher(id) {
      Swal.fire({
  title: '¿Está seguro?',
  text: "No podrá revertir esto!",
  icon: 'warning',
  showCancelButton: true,
  confirmButtonColor: '#3085d6',
  cancelButtonColor: '#d33',
  confirmButtonText: 'Yes, delete it!'
}).then((result) => {
  if (result.value) {
   
   $.ajax({
            type : "DELETE",
            data : {"id" : id, "_token": "{{ csrf_token() }}"},
            url : "{{ url('colegio/teachers/') }}/"+id,
            success : function(r){
    if (r==0){
        $('#table').load('{{ route('teachers.table') }}');
        Swal.fire(
        'Eliminado!',
        'El docente ha sido eliminado con exito.',
        'success'
    )
    }
    if (r==2){
        Swal.fire({
            icon: 'error',
            title: 'Oops...',
            text: 'El docente tiene materias activas, elimine la materia o modifiquela para eliminar el docente!',
       })
    }
    if (r==1){
        Swal.fire({
            icon: 'error',
            title: 'Oops...',
            text: 'No se guardo correctamente, intente denuevo!',
        })
    }
           }
        });
    
  }
})
}

/********  Editar docente  *********/

function edit_teacher(id) {
$('#edit_profile').html(' ');
$.ajax({
            type : "GET",
            data : {"id" : id, "_token": "{{ csrf_token() }}"},
            url : "{{ url('colegio/teachers/') }}/"+id+"/editar",
            success : function(data){
                $('#edit_profile').html(data);
            }
        });
}

/********  Update docente  *********/

$( "#form_edit_teacher" ).submit(function(e) {
e.preventDefault();

  $("#update").prop('disabled', true);
  datos = $("#form_edit_teacher").serialize();
  var id = $("#id_user").val()
$.ajax({
        type:'PUT',
        data:datos,
        url:"{{ url('colegio/teachers/') }}/"+id,
        success:function(r)
        {
          if (r==0) {         
            $('#table').load('{{ route('teachers.table') }}');
            $("#update").prop('disabled', false);
            $('#editModal').modal('hide')
            Swal.fire({
                position: 'center-center',
                icon: 'success',
                title: 'El docente se ha actualizado exitosamente',
                showConfirmButton: false,
                timer: 1500
            }) 
          }else{
            $("#update").prop('disabled', false);
            Swal.fire({
                icon: 'error',
                title: 'Oops...',
                text: 'No se guardo correctamente, intente denuevo!',
                })
          }
        }
        })
});

/********  Password docente  *********/

function updt_psw(id) {
   $("#id_psw").val(id);
}

$( "#generar_psw" ).click(function(e) {
    var abecedario = ["a","b","c","d","e","f","g","h","i","j","k","l","m","n","o","p","q","r","s","t","u","v","w","x","y","z","0","1","2","3","4","5","6","7","8","9"];
    var psw='';
    for(var i = 0; i<8; i++){
        numeroAleatorio = parseInt(Math.random()*abecedario.length);
        psw = psw + abecedario[numeroAleatorio];
    }
    $("#new_password").val(psw);
});
$( "#form_edit_psw" ).submit(function(e) {
e.preventDefault();

  $("#psw").prop('disabled', true);
  datos = $("#form_edit_psw").serialize();
$.ajax({
        type:'POST',
        data:datos,
        url:"{{ route('teachers.psw') }}",
        success:function(r)
        {
          if (r==0) {      
          $('#form_edit_psw')[0].reset();   
            $("#psw").prop('disabled', false);
            $('#pswModal').modal('hide')
            Swal.fire({
                position: 'center-center',
                icon: 'success',
                title: 'El docente se ha actualizado exitosamente',
                showConfirmButton: false,
                timer: 1500
            }) 
          }else{
            $("#psw").prop('disabled', false);
            Swal.fire({
                icon: 'error',
                title: 'Oops...',
                text: 'No se guardo correctamente, intente denuevo!',
                })
          }
        }
        })
});
</script>
@endsection

