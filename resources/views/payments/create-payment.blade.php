<div class="modal-header">
    <h5 class="modal-title" id="myLargeModalLabel">Agregar Pago del alumno {{ $student->profile->first_name_1 }} {{ $student->profile->first_name_2 }} {{ $student->profile->last_name_1 }} {{ $student->profile->last_name_2 }}</h5>
    
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>

</div>
<div class="modal-body">
@if(is_null($student_plan))
    <p class="text-danger">Este alumno no posee un plan de pago, en el siguiente registro se le asignara el plan que le indique.</p>
    @else
    <h4 class="card-title">Plan del estudiante: <span class="text-muted"> {{ $student_plan->plan->name }}</span></h4>    
    @endif
 <form id="form_payment">
    <input type="hidden" id="student_id" name="student_id" value="{{ $student->id }}">
    @csrf
    <div class="row mb-3" hidden>
        <div class="col-md-4">
            <h4 class="text-danger">Número de factura</h4>
            <input type="number" class="form-control" name="control_number" readonly="">
        </div>
    </div>
    <div class="row">
    	<div class="col-sm-12 col-xs-12">
    		<h4>Datos de Recibo y factura</h4>
    		@php
    		$representative = $student->family->representatives->where('principal','1')->first();
    		@endphp
    	</div>
    	<div class="col-sm-6 col-xs-6">
            <div class="form-group">
            	<label id="date">Rif o C.I <span style="color:red">*</span></label>
                <input type="text" class="form-control decimal-inputmask" id="document" name="document" placeholder="Documento de identidad" value="{{ $representative->document_type ?? '' }}-{{ $representative->document_number ?? 'J-xxxxxxxx-x' }}" required>
            </div>
        </div>
        <div class="col-sm-6 col-xs-6">
            <div class="form-group">
            	<label id="name">Nombre o razon social <span style="color:red">*</span></label>
                <input type="text" class="form-control" id="name" name="name" placeholder="Nombre o razon social" value="{{ $representative->first_name_1 ?? '' }} {{ $representative->first_name_2 ?? '' }} {{ $representative->last_name_1 ?? '' }} {{ $representative->last_name_2 ?? '' }}" required>
            </div>
        </div>
        <div class="col-sm-6 col-xs-6">
            <div class="form-group">
            	<label id="address">Dirección <span style="color:red">*</span></label>
                <input type="text" class="form-control" id="address" name="address" placeholder="Dirección" value="{{ $student->family->address ?? '' }}" required>
            </div>
        </div>
        <div class="col-sm-6 col-xs-6">
            <div class="form-group">
            	<label id="phone">Telefono <span style="color:red">*</span></label>
                <input type="text" class="form-control" id="phone" name="phone" placeholder="Telefono" value="{{ $representative->phone ?? '' }}" required>
            </div>
        </div>
    </div>
    <div class="row border-top pt-4">
    	<div class="col-sm-12 col-xs-12">
    		<h4>Datos de Pago</h4>
    	</div>
        <div class="col-sm-4 col-xs-3">
            <div class="form-group">
                <label for="payment_method">Metodo de pago <span style="color:red">*</span></label>
                <select id="payment_method" name="method" class="form-control" required>
                    @foreach($PaymentMethods as $method)
                    <option value="{{ $method->id }}"> {{ $method->name }} </option>
                    @endforeach
                </select>
            </div>
        </div>
        <div class="col-sm-4 col-xs-3">
            <div class="form-group">
                <label for="bank">Transferencia hecha a:</label>
                <select id="bank" name="bank" class="form-control">
                    <option value="0">Seleccione en caso de transferencia</option>
                    @foreach($bankAccounts as $bankAccount)
                    <option value="{{ $bankAccount->bank->name }}"> {{ $bankAccount->bank->name }} </option>
                    @endforeach
                </select>
            </div>
        </div>
        <div class="col-sm-4 col-xs-6">
            <div class="form-group">
                <label id="reference">Codigo de Referencia (si existe)</label>
                <input type="number" class="form-control" id="reference" name="reference" placeholder="Codigo de Referencia (si existe)" min="0" value="0" required>
            </div>
        </div>
        <div class="col-sm-3 col-xs-3">
            <div class="form-group">
                <label for="amount">Monto <span style="color:red">*</span></label>
                <input type="number" step="any" class="form-control" id="amount" name="amount" placeholder="Monto" min="0" onblur="converter()" required>
            </div>
        </div>
        <div class="col-sm-3 col-xs-3">
            <div class="form-group">
            	<label for="coin">Monto expresado en: <span style="color:red">*</span></label>
            	<select id="coin" name="coin" class="form-control"  onchange="converter()">
            		<option value="Bsf">Bolivares</option>
                    @if(!is_null(session('USD_CLASES')))
            		<option value="USD">Dolares</option>
                    @endif
                    @if(!is_null(session('COP_CLASES')))
            		<option value="COP">Pesos Colombianos</option>
                    @endif
            	</select>
            </div>
        </div>
        
            @if(is_null($student_plan))
            <div class="col-sm-3 col-xs-3">
            <div class="form-group">
                <label for="plan">Agregar Plan:</label>
                <select id="plan" name="plan_id" class="form-control" onchange="show_plan()" required>
                    <option value="">Seleccione Plan</option>
                    @foreach($plans as $plan)
                    <option value="{{ $plan->id }}">{{ $plan->name }}</option>
                    @endforeach
                </select>
            </div>
            </div>
            @else
            <input type="hidden" id="plan" value="{{ $student_plan->plan_id }}">
            @endif
        
        <div class="col-sm-3 col-xs-3">
            <div class="form-group">
                <label for="amountbsf">Monto en bolivares</label>
                <input type="number" step="any" class="form-control" id="amountbsf"  placeholder="Monto" min="0" readonly="" required>
            </div>
        </div>
        <div class="col-sm-12 col-xs-12">
            <div class="form-group">
                <label for="amountbsf">Observacion</label>
                <input type="text" class="form-control" id="observation" name="observation"  placeholder="Observacion, en caso de descuentos o algun cambio del plan" maxlength="200">
            </div>
        </div>
        <div style="width: 100%" class="row justify-content-center">
            <div id="show_installments" class="col-sm-10 col-xs-10">
            </div>
        </div>
        <div id="submit_save" align="right" class="col-sm-4 col-xs-4 mx-auto">
            <button type="submit" class="btn btn-outline-success save" disabled><i class="fa fa-check"></i> Realizar pago</button>
        </div>
    </div>
    </form>
    <span id="data"></span>
    </div>  
<script type="text/javascript">

    (function() {
   'use strict';
        window.addEventListener('load', function() {
        // Fetch all the forms we want to apply custom Bootstrap validation styles to
        var forms = document.getElementsByClassName('needs-validation');
        // Loop over them and prevent submission
        var validation = Array.prototype.filter.call(forms, function(form) {
            form.addEventListener('submit', function(event) {
                if (form.checkValidity() === false) {
                    event.preventDefault();
                    event.stopPropagation();
                }
            form.classList.add('was-validated');
        }, false);
        });
        }, false);
})();
        /********  Nueva cuenta de colegio  *********/

$( "#form_payment" ).submit(function(e) {
e.preventDefault();
  $('#submit_save').html('Cargando porfavor espere...');
  $(".btn").prop('disabled', true);
  

  payment();
});

/**Mostrar las cuotas**/
 function show_plan(){
    var id = document.getElementById("plan").value;
    var amount = document.getElementById("amount").value;
    var coin = document.getElementById("coin").value;
    var bsf = document.getElementById("amountbsf").value;
    var coin_amount = document.getElementById("coin").value;

       // $('#show_installments').html(" ");
        $('#show_installments').load("{{route('payments.installments')}}/?plan_id="+id+"&amount="+amount+"&coin="+coin+"&bsf="+bsf+"&coin_amount="+coin_amount+"&student_id={{ $student->id }}");
    }

function converter(){

  var amount = document.getElementById("amount").value;
  var coin = document.getElementById("coin").value;
  
    if (coin == 'Bsf') {
        var resultado = parseFloat(amount * 1);
        $('#amountbsf').val(resultado.toFixed(2));
    }
    @if(!is_null(session('USD_CLASES')))
        if (coin == 'USD') {
            var resultado = parseFloat(amount)*parseFloat({{ session('USD_CLASES') }});
            $('#amountbsf').val(resultado.toFixed(2));
        }
    @endif
    @if(!is_null(session('COP_CLASES')))
        if (coin == 'COP') {
            var resultado = (parseFloat(amount)*parseFloat({{ session('COP_CLASES') }}));
            $('#amountbsf').val(resultado.toFixed(2));
        }
    @endif

    @if(is_null($student_plan))
        $('#show_installments').html(" ");
        $("#plan").val("");
    @else
        show_plan();
    @endif

}

@if(!is_null($student_plan))
    show_plan();
@endif
function calculator(result){
    alert(result);
}
</script>