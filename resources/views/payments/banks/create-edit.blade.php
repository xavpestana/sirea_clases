@extends('layouts.app')

@section('content')
    <div class="row">
        <div class="col-12">
            <bank :bank_id="{!! (isset($bank)) ? $bank->id : "null" !!}"
                route_list='{{ url('pagos/bancos') }}'>
            </bank>
        </div>
    </div>
@endsection
