<form id="form_updt_account">
    @csrf
<div class="modal-header">
    <h4 class="modal-title" id="myLargeModalLabel">Agregar Cuenta Bancaria</h4>
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
</div>
<div class="modal-body">
    
    <div class="row">
        <div class="col-sm-6 col-xs-6">
            <div class="form-group">
                <input type="text" class="form-control" id="account" name="account" placeholder="Cuenta bancaria con 20 digitos" title="Cuenta Incorrecta, debe ser minmo 20 numeros, no debe contener letras ni simbolos especiales" maxlength="20" minlength="20" pattern="[0-9]{20}" value="{{ $bankAccounts->account_number }}" required>
            </div>
        </div>
        <div class="col-sm-6 col-xs-6">
            <div class="form-group">
                <select class="form-control" name="bank">
                    @foreach($banks as $bank)
                    <option value="{{ $bank->id }}" @if($bankAccounts->bank_id == $bank->id) selected="" @endif>{{ $bank->name }}</option>
                    @endforeach
                </select>
            </div>
        </div>
    </div>
</div>                                            
<div class="modal-footer">
<button type="submit" class="btn btn-success waves-effect text-left save">Guardar</button>
</div>
</form>

<script type="text/javascript">
        /********  Nueva cuenta de colegio  *********/

$( "#form_updt_account" ).submit(function(e) {
e.preventDefault();

  $(".save").prop('disabled', true);
  datos = $("#form_updt_account").serialize();
$.ajax({
        type:'PUT',
        data:datos,
        url:"{{ route('accountsBank.update', ['cuenta' => $bankAccounts->id]) }}",
        })
        .done(function(response) {
            $(".save").prop('disabled', false);
            $('#accounts').load('{{ route('accountsBank.show', ['cuenta' => auth()->user()->id]) }}');
            Swal.fire({
                position: 'center-center',
                icon: 'success',
                title: 'Cuenta actualizada exitosamente',
                showConfirmButton: false,
                timer: 1500
            }) 
        })
        .fail(function(response) {
            $(".save").prop('disabled', false);
            Swal.fire({
                icon: 'error',
                title: 'Oops...',
                text: 'El numero de cuenta ya esta registrado en nuestra base de datos, intente denuevo!',
                })
        })
});
</script>