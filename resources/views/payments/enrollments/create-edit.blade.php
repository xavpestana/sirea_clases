@extends('layouts.app')

@section('content')
    <div class="row">
        <div class="col-12">
            <enrollment :enrollment_id="{!! (isset($enrollment)) ? $enrollment->id : "null" !!}"
                route_list='{{ url('pagos/inscripciones') }}'>
            </enrollment>
        </div>
    </div>
@endsection
