@extends('layouts.app')

@section('title-view', 'Pagos - Sin facturas')

@section('extra-css')
<link href="https://cdn.datatables.net/1.10.21/css/jquery.dataTables.min.css" rel="stylesheet">
@endsection

@section('content')
    <div class="row">
        <div class="col-12">
            <div class="card">
            	<div class="card-header">
                    <form id="form_coin" class="needs-validation">
                        @csrf()
                    <div class="row justify-content-center">
                        <div class="col-md-3">
                            <label for="USD">1 USD es igual a: (Bsf)</label>
                            <input id="USD" type="number" class="form-control" name="usd" value="{{ $USD ?? '' }}" step="any">
                        </div>
                        <div class="col-md-3">
                            <label for="USD">1 COP es igual a: (Bsf)</label>
                            <input id="COP" type="number" class="form-control" name="cop" value="{{ $COP ?? '' }}" step="any">
                        </div>
                        <div align="center" class="col-md-3">
                            <button type="submit" class="btn waves-effect waves-light btn-rounded btn-success mt-4">Actualizar</button>
                        </div>
                        <div align="justify" class="col-md-7">
                            <small>Estos valores son volatiles y se guardan en las cookies del ordenador por seguridad, por lo que se debe configurar en cada ordenador y cuando inicie sesion</small>
                        </div>
                    </div>
                    </form>
            		<h4 class="m-b-0 mb-3 mt-3">Registros de pagos sin facturas</h4>
                            <p><strong>NOTA:</strong> cada pago registrado sera independiente al pago con factura, estos registros solo generan un recibo online, mas no una factura</p>
            	</div>
                <div class="card-body">
                    <div class="row">
                        <div class="col-md-12">
                            <form id="alternate_payments" class="m-t-40 row needs-validation">
                                @csrf()
                                <div class="form-group col-md-8 m-t-20">
                                    <label for="email" class="col-12 col-form-label">Correo para envio de correo:</label>
                                    <input id="email" class="form-control" type="email" placeholder="Ingrese Email para envio de recibo" name="email" required="">
                                </div>
                                    <div class="form-group col-md-6 m-t-20">
                                        <div class="form-group row">
                                        <label for="example-datetime-local-input" class="col-3 col-form-label">He recibido de:</label>
                                        <div class="col-9">
                                            <input class="form-control" type="text" placeholder="Ingrese Nombre" name="name" id="example-datetime-local-input" required="">
                                        </div>
                                    </div> </div>
                                    <div class="form-group col-md-6 m-t-20">
                                        <div class="form-group row">
                                        <label for="example-datetime-local-input" class="col-3 col-form-label">La cantidad de:</label>
                                        <div class="col-9">
                                            <input class="form-control" step="any" type="number" placeholder="Ingrese Cantidad" name="quantity" id="example-datetime-local-input" required="">
                                        </div>
                                    </div> </div>
                                    <div class="form-group col-md-3 m-t-20">
                                            <div class="form-group">
                                                <label for="coin">Monto expresado en: <span style="color:red">*</span></label>
                                                <select id="coin" name="coin" class="form-control"  onchange="converter()">
                                                    <option value="Bsf">Bolivares</option>
                                                    @if(!is_null(session('USD_CLASES')))
                                                    <option value="USD">Dolares</option>
                                                    @endif
                                                    @if(!is_null(session('COP_CLASES')))
                                                    <option value="COP">Pesos Colombianos</option>
                                                    @endif
                                                </select>
                                            </div>
                                    </div>
                                    <div class="form-group col-md-9 m-t-20">
                                        <label>Por concepto de:</label>
                                        <textarea name="concept" class="form-control" rows="5" required=""></textarea>
                                    </div>
                                    <div align="center" class="col-md-12">
                            <button type="submit" class="btn waves-effect waves-light btn-rounded btn-success mt-4 mb-4">Guardar Pago</button>
                            <div class="col-md-12">
                                <small>Los pagos expresados en dolares, el recibo sera enviado en bolivares, este sistema de pagos no generara facturas de ningun modo</small>
                            </div>
                        </div>
                                </form>
                        </div>
                        <div class="col-md-12 mt-4">
                            <div id="payments" class="table-responsive">
                            </div>
                        </div>
                    </div>
                	
                </div>
                
                
            </div>
        </div>

    </div>
@endsection
@section('extra-js')
<script src="https://cdn.jsdelivr.net/npm/sweetalert2@9"></script>
<script src="https://cdn.datatables.net/1.10.21/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.6.2/js/dataTables.buttons.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.6.2/js/buttons.flash.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/pdfmake.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/vfs_fonts.js"></script>
<script src="https://cdn.datatables.net/buttons/1.6.2/js/buttons.html5.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.6.2/js/buttons.print.min.js"></script>

<script type="text/javascript">
	(function() {
   'use strict';
        window.addEventListener('load', function() {
        // Fetch all the forms we want to apply custom Bootstrap validation styles to
        var forms = document.getElementsByClassName('needs-validation');
        // Loop over them and prevent submission
        var validation = Array.prototype.filter.call(forms, function(form) {
            form.addEventListener('submit', function(event) {
                if (form.checkValidity() === false) {
                    event.preventDefault();
                    event.stopPropagation();
                }
            form.classList.add('was-validated');
        }, false);
        });
        }, false);
})();

$('#payments').load("{{route('ap.list')}}");


$( "#alternate_payments" ).submit(function(e) {
e.preventDefault();
  //$('#submit_save').html('Cargando porfavor espere...');
  $(".btn").prop('disabled', true);
  payment();
});

function payment() {
  datos = $("#alternate_payments").serialize();
    
    $.ajax({
        type:'POST',
        data:datos,
        url:"{{ route('ap.store') }}",
        })
        .done(function(response) {
            $(".btn").prop('disabled', false);
           $('#payments').load("{{route('ap.list')}}");
           // $('#modal_all').html('Cargando porfavor espere...');
           // $('#modal_all').load("{{url('pagos/facturar/')}}/"+ student_id);
           // $('#line').load('{{ route('paidrepre.index') }}');
            Swal.fire({
                position: 'center',
                icon: 'success',
                title: 'Pago Agregado exitosamente',
                showConfirmButton: false,
                timer: 1500
            }) 
        })
        .fail(function(xhr, status, error) {
         $(".btn").prop('disabled', false);
         if (xhr.status == '400') {
            Swal.fire({
                icon: 'error',
                title: 'Oops...',
                text: 'No hay cuotas seleccionadas, intente denuevo!',
                })
         } else {
            Swal.fire({
                icon: 'error',
                title: 'Oops...',
                text: 'Algo a sucedido, intente denuevo!',
                })
         }
            
            
        }) 
}

$( "#form_coin" ).submit(function(e) {
e.preventDefault();

  $(".save").prop('disabled', true);
  datos = $("#form_coin").serialize();
$.ajax({
        type:'POST',
        data:datos,
        url:"{{ route('coin.save') }}",
        })
        .done(function(response) {
            $(".save").prop('disabled', false);
            Swal.fire({
                position: 'center-center',
                icon: 'success',
                title: 'Actualizado exitosamente',
                showConfirmButton: false,
                timer: 1500
            }) 
            location.reload();
        })
        .fail(function(response) {

            $(".save").prop('disabled', false);
            Swal.fire({
                icon: 'error',
                title: 'Oops...',
                text: 'Algo ha sucedido, intente denuevo!',
                })
        }) 
});

 function send(id){
    $('.preloader_mail').show();
    $('.send').attr('disabled', true);
$.ajax({
        type:'GET',
        url:"{{route('ap.send')}}/?invoice="+id,
        })
        .done(function(response) {
            $('.preloader_mail').hide();
            $('.send').removeAttr('disabled');
            Swal.fire({
                position: 'center',
                icon: 'success',
                title: 'Factura Enviada Exitosamente',
                showConfirmButton: false,
                timer: 1500
            }) 
        })
        .fail(function(xhr, status, error) {
            $('.send').removeAttr('disabled');
            $('.preloader_mail').hide();
            Swal.fire({
                icon: 'error',
                title: 'Oops...',
                text: 'Algo a sucedido, intente denuevo!',
                })
        }) 
    }

function annull(id){
        Swal.fire({
            title: 'Desea rechazar el pago?',
            text: "Estos cambios no se pueden revertir!",
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Si, anular!'
                }).then((result) => {
                    if (result.isConfirmed) {
                        $('.btn').attr('disabled', true);
                        $.ajax({
                            type:'DELETE',
                            data : {"id" : id, "_token": "{{ csrf_token() }}"},
                            url:"{{url('pagos/recibo/')}}/"+id,
                        })
                        .done(function(response) {
                            $('.btn').attr('disabled', false);
                           $('#payments').load("{{route('ap.list')}}");
                            Swal.fire(
                                'Pago rechazado!',
                                'El pago ha sido rechazado con exito.',
                                'success'
                            )
                        })
                    }
                })
    }
</script>
@endsection
