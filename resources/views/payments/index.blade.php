@extends('layouts.app')

@section('title-view', 'Registro de pagos - Pagos')

@section('extra-css')
<link href="https://cdn.datatables.net/1.10.21/css/jquery.dataTables.min.css" rel="stylesheet">
@endsection

@section('content')
    <div class="row">
        <div class="col-12">
            <div class="card">
            	<div class="card-header">
            		<form id="form_coin">
            			@csrf()
            		<div class="row justify-content-center">
            			<div class="col-md-3">
            				<label for="USD">1 USD es igual a: (Bsf)</label>
            				<input id="USD" type="number" class="form-control" name="usd" value="{{ $USD ?? '' }}" step="any">
            			</div>
            			<div class="col-md-3">
            				<label for="USD">1 COP es igual a: (Bsf)</label>
            				<input id="COP" type="number" class="form-control" name="cop" value="{{ $COP ?? '' }}" step="any">
            			</div>
            			<div align="center" class="col-md-3">
            				<button type="submit" class="btn waves-effect waves-light btn-rounded btn-success mt-4">Actualizar</button>
            			</div>
            			<div align="justify" class="col-md-7">
            				<small>Estos valores son volatiles y se guardan en las cookies del ordenador por seguridad, por lo que se debe configurar en cada ordenador y cuando inicie sesion</small>
            			</div>
            		</div>
            		</form>
            	</div>
                <div class="card-body">
                	<div id="students" class="table-responsive">
                		
                	</div>
                </div>
                <div class="card-header">
                    <div class="row justify-content-center">
                        <div class="col-md-12 pt-4">
                            <h4 class="m-b-0 mb-3">Registros de pagos pendientes</h4>
                            <p><strong>NOTA:</strong> Los datos para verificación de datos no necesariamente son los mismos para impresion de factura, la verificación y autenticidad de los datos es responsabilidad del colegio, no de la empresa, por tanto, la empresa no se hace responsable en caso de falsificación por parte de representante, el colegio tiene el poder de aceptar o negar el pago recibido por el representante</p>
                        </div>
                    </div>
                    
                </div>
                <div class="card-body">
                    <div id="line" class="table-responsive">
                       
                    </div>
                </div>
            </div>
        </div>

         <div class="modal fade bs-example-modal-lg " tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none;">
                                    <div class="modal-dialog  modal-xl">
                                        <div id="modal_all" class="modal-content">
                                            
                                        </div>
                                        <!-- /.modal-content -->
                                    </div>
                                    <!-- /.modal-dialog -->
                                </div>
    </div>
@endsection
@section('extra-js')
<script src="https://cdn.jsdelivr.net/npm/sweetalert2@9"></script>
<script src="https://cdn.datatables.net/1.10.21/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.6.2/js/dataTables.buttons.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.6.2/js/buttons.flash.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/pdfmake.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/vfs_fonts.js"></script>
<script src="https://cdn.datatables.net/buttons/1.6.2/js/buttons.html5.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.6.2/js/buttons.print.min.js"></script>

<script type="text/javascript">
	(function() {
   'use strict';
        window.addEventListener('load', function() {
        // Fetch all the forms we want to apply custom Bootstrap validation styles to
        var forms = document.getElementsByClassName('needs-validation');
        // Loop over them and prevent submission
        var validation = Array.prototype.filter.call(forms, function(form) {
            form.addEventListener('submit', function(event) {
                if (form.checkValidity() === false) {
                    event.preventDefault();
                    event.stopPropagation();
                }
            form.classList.add('was-validated');
        }, false);
        });
        }, false);
})();

function payment() {
    var student_id = $('#student_id').val();
  datos = $("#form_payment").serialize();
$.ajax({
        type:'POST',
        data:datos,
        url:"{{ route('payments.store') }}",
        })
        .done(function(response) {
            $(".btn").prop('disabled', false);
            $('#data').load("{{route('invoice.send')}}/?invoice="+response);
            $('#modal_all').html('Cargando porfavor espere...');
            $('#modal_all').load("{{url('pagos/facturar/')}}/"+ student_id);
            $('#line').load('{{ route('paidrepre.index') }}');
            Swal.fire({
                position: 'center',
                icon: 'success',
                title: 'Pago Agregado exitosamente',
                showConfirmButton: false,
                timer: 1500
            }) 
        })
        .fail(function(xhr, status, error) {
         $(".btn").prop('disabled', false);
         if (xhr.status == '400') {
            Swal.fire({
                icon: 'error',
                title: 'Oops...',
                text: 'No hay cuotas seleccionadas, intente denuevo!',
                })
         } else {
            Swal.fire({
                icon: 'error',
                title: 'Oops...',
                text: 'Algo a sucedido, intente denuevo!',
                })
         }
            
            
        }) 
}

$('#students').load('{{ route('payments.list_students') }}');
$('#line').load('{{ route('paidrepre.index') }}');
$( "#form_coin" ).submit(function(e) {
e.preventDefault();

  $(".save").prop('disabled', true);
  datos = $("#form_coin").serialize();
$.ajax({
        type:'POST',
        data:datos,
        url:"{{ route('coin.save') }}",
        })
        .done(function(response) {
            $(".save").prop('disabled', false);
            Swal.fire({
                position: 'center-center',
                icon: 'success',
                title: 'Actualizado exitosamente',
                showConfirmButton: false,
                timer: 1500
            }) 
            location.reload();
        })
        .fail(function(response) {

            $(".save").prop('disabled', false);
            Swal.fire({
                icon: 'error',
                title: 'Oops...',
                text: 'Algo ha sucedido, intente denuevo!',
                })
        }) 
});

/*Nueva cuenta*/
    function pago(id,ins){
        $('#modal_all').html(" ");
        $('#modal_all').load("{{route('payments.create')}}/?id_student="+id);
    }

/*Nueva cuenta*/
    function registro(id){
        $('#modal_all').html(" ");
        $('#modal_all').load("{{url('pagos/facturar/')}}/"+id);
    }
/*Nueva cuenta*/
    function register_payment(id){
        $('#modal_all').html(" ");
        $('#modal_all').load("{{url('pagos/representantes/')}}/"+id);
    }
function annull(id){
        Swal.fire({
            title: 'Desea rechazar el pago?',
            text: "Estos cambios no se pueden revertir!",
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Si, anular!'
                }).then((result) => {
                    if (result.isConfirmed) {
                        $('.btn').attr('disabled', true);
                        $.ajax({
                            type:'DELETE',
                            data : {"id" : id, "_token": "{{ csrf_token() }}"},
                            url:"{{url('pagos/representantes/')}}/"+id,
                        })
                        .done(function(response) {
                            $('.btn').attr('disabled', false);
                            location.reload();
                            Swal.fire(
                                'Pago rechazado!',
                                'El pago ha sido rechazado con exito.',
                                'success'
                            )
                        })
                    }
                })
    }
</script>
@endsection
