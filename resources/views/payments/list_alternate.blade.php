<table id="table_register" class="table table-striped table-hover">
	<thead>
		<tr>
			<th>N de recibo</th>
			<th>Recibido por:</th>
			<th>Monto de:</th>
			<th>Concepto de:</th>
			<th><span class="preloader_mail">
            <svg class="circular" viewBox="25 25 50 50">
                <circle class="path" cx="50" cy="50" r="20" fill="none" stroke-width="2" stroke-miterlimit="10"></circle> </svg>
        </span></th>
		</tr>
	</thead>
	<tbody>
		@foreach ($payments as $payment)
			<tr>
				<td>{{ $payment->number_invoice }}</td>
				<td>{{ $payment->name }}</td>
				<td>{{ $payment->amount }} {{ $payment->coin }}</td>
				<td>{{ $payment->concept }}</td>
				<td>
					<div class="btn-group">
                    	<button type="button" class="btn waves-effect waves-light btn-rounded btn-warning send"  title="Enviar factura" onclick="send('{{ $payment->id }}')">
                        	<i class="mdi mdi-file-send"></i> Enviar recibo
                    	</button>
                    	<button type="button" class="btn waves-effect waves-light btn-rounded btn-danger"  title="Enviar factura" onclick="annull('{{ $payment->id }}')">
                        	<i class="mdi mdi-file-send"></i> Eliminar recibo
                    	</button>
                	</div>
				</td>
			</tr>
		@endforeach
	</tbody>
</table>

<script>
	$('#table_register').DataTable( {
                    dom: 'Bfrtip',
                    "aaSorting": [[ 0, "desc" ]],
                });
	$('.preloader_mail').hide();
</script>