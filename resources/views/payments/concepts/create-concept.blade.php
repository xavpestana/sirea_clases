<form id="form_concept">
    @csrf
<div class="modal-header">
    <h4 class="modal-title" id="myLargeModalLabel">Agregar Concepto de Pago</h4>
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
</div>
<div class="modal-body">
    
    <div class="row">
        <div class="col-sm-6 col-xs-6">
            <div class="form-group">
                <input type="text" class="form-control" id="concept" name="concept" placeholder="Concepto" required>
            </div>
        </div>
    </div>
</div>                                            
<div class="modal-footer">
<button type="submit" class="btn btn-success waves-effect text-left save">Guardar</button>
</div>
</form>

<script type="text/javascript">
        /********  Nueva cuenta de colegio  *********/

$( "#form_concept" ).submit(function(e) {
e.preventDefault();

  $(".save").prop('disabled', true);
  datos = $("#form_concept").serialize();
$.ajax({
        type:'POST',
        data:datos,
        url:"{{ route('concepts.store') }}",
        })
        .done(function(response) {
            $(".save").prop('disabled', false);
            $('#concepts').load('{{ route('concepts.index', ['cuenta' => auth()->user()->id]) }}');
            Swal.fire({
                position: 'center-center',
                icon: 'success',
                title: 'Concepto de pago agregado exitosamente',
                showConfirmButton: false,
                timer: 1500
            }) 
        })
        .fail(function(response) {
            $(".save").prop('disabled', false);
            Swal.fire({
                icon: 'error',
                title: 'Oops...',
                text: 'El numero de cuenta ya esta registrado en nuestra base de datos, intente denuevo!',
                })
        }) 
});
</script>