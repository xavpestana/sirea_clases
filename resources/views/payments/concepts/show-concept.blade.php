<table class="table table-hover earning-box">
	<thead>
		<tr>
			<th>Nombre del concepto</th>
			<th></th>
		</tr>
	</thead>
	<tbody>
		
			@foreach($concepts as $concept)
			<tr>
			<td>{{ $concept->name }}</td>
			<td>
				<div class="btn-group" role="group" aria-label="First group">
					<button type="button" class="btn waves-effect waves-light btn-rounded btn-sm btn-danger" title="Eliminar cuenta"><i class="mdi mdi-delete" onclick="eliminar_concept('{{ $concept->id }}')"></i></button>
				</div>
			</td>
			</tr>
			@endforeach
		
	</tbody>
</table>