<table id="online" class="table table-striped table-bordered">
    <thead>
        <tr>
            <th>Status</th>
            <th>Alumno</th>
            <th>Datos de factura</th>
            <th>Datos de pago</th>
            <th>Monto</th>
            <th>Cuota y observacion</th>
        </tr>
    </thead>
    <tbody>
        @foreach ($paymentRegister as $payment)
        
        @php
            $student = $payment->Student->profile;
        @endphp
            <tr>
                <td>
                    @if ($payment->status == 0)
                        <h5 class="text-warning">En proceso</h5>
                    @elseif ($payment->status == 1)
                        <h5 class="text-success">Aprobado</h5>
                    @elseif ($payment->status == 2)
                        <h5 class="text-danger">Negado</h5>
                    @endif</td>
                <td>{{ $student->last_name_1 }} {{ $student->last_name_2 }} {{ $student->first_name_1 }} {{ $student->first_name_2 }}</td>
                <td>{{ $payment->identity }}<br>
                    {{ $payment->name }}<br>
                    {{ $payment->address }}<br>
                    {{ $payment->phone }}</td>
                <td>{{ $payment->paymentMethod->name }}<br>
                    @if ($payment->transfer_code != 0)
                        Referencia {{ $payment->transfer_code }}<br>
                    @endif
                    @if ($payment->school_bank_account != '0')
                        {{ $payment->school_bank_account }}<br>
                    @endif</td>
                <td>{{ $payment->amount }} {{ $payment->coin }}</td>
                <td>{{ $payment->observation }}</td>
            </tr>
        @endforeach
    </tbody>
</table> 
<script>
 $('#online').DataTable( {
        "language": {
                "url": "//cdn.datatables.net/plug-ins/1.10.11/i18n/Spanish.json"
            },
        dom: 'Bfrtip',
        "aaSorting": [[ 5, "desc" ]],
        buttons: [
                'copyHtml5',
                'excelHtml5',
                'csvHtml5',
            ]
    });
</script>