<form id="form_student_email">
    @csrf()
<table id="table_student" class="display nowrap table table-hover table-striped table-bordered" cellspacing="0" width="100%">
	<thead>
        <tr>
            <th>Familia</th>
            <th>Representantes</th>
            <th>Documento</th>
            <th>Nombre y Apellido del alumno</th>
            <th>Grado</th>
            <th>Seccion</th>
            <th>Plan actual</th>
            <th>Numero de<br>cuotas vencidas</th>
            <th>Cuotas vencidas</th>
            <th class="no-sort">Acciones</th>
        </tr>
    </thead>
    <tbody>

    @foreach ($students as $student)

         @if($student->studentPlan->count() != 0)
        
            @php
                $plan_asign = $student->studentPlan->where('season_id', $actual_season->id)->first();
                $installments = $plan_asign->plan->Installments
                                ->where('expire_at', '<', $today->format('Y-m-d'))
                                ->where('expire_at','<>',null);
                $defaulters = $installments;

                foreach($installments as $installment){
                    $installmentPayment = App\installmentPayment::where('student_id', $student->id)
                                                                ->where('installment_id', $installment->id)
                                                                ->first();

                    if(!is_null($installmentPayment)){
                       $installmentPayment->pluck('installment_id');
                        $installmentPayment = $installmentPayment->toArray();
                        $defaulters = $defaulters->except($installmentPayment);
                    }
                }
            @endphp
            @if($defaulters->count() != 0)
        <tr>
            <td>{{ $student->family->last_name_1 }} {{ $student->family->last_name_2 }}
                <input type="hidden" name="student_id[]" value="{{ $student->id }}">
            </td>
            <td>
                <ul>
                    
                
                @foreach ($student->family->representatives as $representatives)
                    <li>{{ $representatives->last_name_1 }} {{ $representatives->last_name_2 }} {{ $representatives->first_name_1 }} {{ $representatives->first_name_2 }}</li>
                @endforeach
                </ul>
            </td>
            <td>{{ $student->profile->document_type }}-{{ $student->profile->document_number }}</td>
            <td>{{ $student->profile->first_name_1 }} {{ $student->profile->first_name_2 }} {{ $student->profile->last_name_1 }} {{ $student->profile->last_name_2 }}</td>
            <td>{{ $student->grade }}</td>
            <td>
                @if ($student->enrollments->last())
                    {{  $student->enrollments->last()->section->name }}
                @else
                No inscrito
                @endif
                </td>
            <td>{{ $plan_asign->plan->name }}</td>
            <td>
                {{ $defaulters->count() }}
            </td>
            <td>  
                @foreach($defaulters as $defaulter)
                <ul>
                    <li> {{ $defaulter->description }} ({{ $defaulter->amount }} {{ $defaulter->coin }})
                        <br>Venció el: {{ $defaulter->expire_at->format('d-M-y') }}
                    </li>
                </ul>
                @endforeach
            </td>
            <td>
                <div class="btn-group" role="group" aria-label="Basic example">
                    <button type="button" class="btn btn-success" data-toggle="modal" data-target=".bs-example-modal-lg" onclick="pago('{{ $student->id }}')"><i class="ti-marker-alt"></i> Pago</button>
                </div>
            </td>
        </tr>
        @endif
        @endif
    @endforeach
	</tbody>
</table>
</form> 
<script>
    $('#table_student').DataTable( {
        "language": {
                "url": "//cdn.datatables.net/plug-ins/1.10.11/i18n/Spanish.json"
            },
        dom: 'Bfrtip',
        "aaSorting": [[ 7, "desc" ]],
        buttons: [
                'copyHtml5',
                'excelHtml5',
                'csvHtml5',
            ]
    });
</script>