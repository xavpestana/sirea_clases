
<div class="modal-header">
    <h4 class="modal-title" id="myLargeModalLabel">Cuotas del plan {{ $plan->name }}</h4>
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
</div>
<div class="modal-body">
	<!--
		Cuotas Nuevas
	-->
    <form id="form_installments">
    @csrf
    <div class="row">
    	<div class="col-sm-12 col-xs-12">
    		<h2>Añadir cuota nueva</h2>
    	</div>
        <div class="col-sm-3 col-xs-3">
            <div class="form-group">
            	<label id="name">Nombre de la cuota <span style="color:red">*</span></label>
            	
                <input type="text" class="form-control" id="name" name="name" placeholder="Nombre de la cuota" required>
                <input type="hidden" name="plan_id" value="{{ $plan->id }}">
            </div>
        </div>
        <div class="col-sm-3 col-xs-3">
            <div class="form-group">
            	<label id="date">Fecha de expiracion</label>
                <input type="date" class="form-control" id="date" name="date" placeholder="Fecha de expiracion">
            </div>
        </div>
        <div class="col-sm-3 col-xs-3">
            <div class="form-group">
            	<label id="date">Monto de la cuota <span style="color:red">*</span></label>
                <input type="number" class="form-control decimal-inputmask" id="amount" name="amount" placeholder="Monto de la cuota" step="0.01" title="El monto decimal debe ser un punto" pattern="[0-9]+([\.,][0-9]+)?" required>
            </div>
        </div>
        <div class="col-sm-3 col-xs-3">
            <div class="form-group">
            	<label for="monto">Monto expresado en: <span style="color:red">*</span></label>
            	<select id="monto" name="coin" class="form-control">
            		<option value="Bsf">Bolivares</option>
            		<option value="USD">Dolares</option>
            		<option value="COP">Pesos Colombianos</option>
            	</select>
            </div>
        </div>
        <div align="justify" class="col-sm-7 col-xs-7">
        	<p class="ml-3"><b>Leyenda:</b></p>
        	<ul>
        		<li> <span style="color:red">*</span> Campos obligatorios</li>
        		<li> Las cuotas que no tienen fecha de expiracion no generaran recordatorios</li>
                <li> las cuotas que no tienen fecha de expiracion pueden ser tomados como pagos no Obligatorios</li>
        	</ul>
        </div>
        <div align="right" class="col-sm-4 col-xs-4 ml-auto">
            <button type="submit" class="btn btn-outline-success save"><i class="fa fa-check"></i> Guardar Cuota</button>
        </div>
    </div>
    </form>
    <div id="updt"></div>

<!--
        Cuotas Agregadas
        expire_at ->format('d M Y')
        formato hace = expire_at ->diffforHumans()
    -->
    <div id="installments" class="table-responsive">
        
    </div>                                            

</div>
<script type="text/javascript">
        /********  Nueva cuenta de colegio  *********/
$('#installments').load('{{ route('installments.index', ['plan_id' => $plan->id]) }}');
$( "#form_installments" ).submit(function(e) {
e.preventDefault();

  $(".save").prop('disabled', true);
  datos = $("#form_installments").serialize();
  
$.ajax({
        type:'POST',
        data:datos,
        url:"{{ route('installments.store') }}",
        })
        .done(function(response) {
            $(".save").prop('disabled', false);
            $('#installments').load('{{ route('installments.index', ['plan_id' => $plan->id]) }}');
            Swal.fire({
                position: 'center-center',
                icon: 'success',
                title: 'Cuota agregada exitosamente',
                showConfirmButton: false,
                timer: 1500
            }) 
        })
        .fail(function(response) {
            $(".save").prop('disabled', false);
            Swal.fire({
                icon: 'error',
                title: 'Oops...',
                text: 'Hubo un problema, intente denuevo!',
                })
        }) 
}); 
/*Actualizar cuota*/
    function updt_installments(id){
        $.ajax({
        type:'GET',
        url:"{{ url('pagos/installments/') }}/"+id+"/editar",
        success:function(data)
        {
         $('#updt').html(data);
        }
        })
    }
       /*Eliminar cuota*/
    function delete_installments(id){
        $.ajax({
        type:'DELETE',
        data:{"_token": "{{ csrf_token() }}"},
        url:"{{ url('pagos/installments/') }}/"+id,
        success:function(r)
        {
            if (r == 0) {
        $('#installments').load('{{ route('installments.index', ['plan_id' => $plan->id ]) }}');
            Swal.fire({
                position: 'center-center',
                icon: 'success',
                title: 'Cuota eliminada correctamente',
                showConfirmButton: false,
                timer: 1500
            }) 
            } else {
                Swal.fire({
                icon: 'error',
                title: 'Oops...',
                text: 'intente denuevo!',
                })
            }
        }
        })
    }
</script>