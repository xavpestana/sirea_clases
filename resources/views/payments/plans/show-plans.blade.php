<table class="table table-hover earning-box">
	<thead>
		<tr>
			<th>Nombre</th>
			<th>Periodo</th>
			<th>Numero de cuotas</th>
			<th></th>
		</tr>
	</thead>
	<tbody>
		
			@foreach($plans as $plan)
			<tr>
			<td>{{ $plan->name }}</td>
			<td>{{ $plan->season }}</td>
			<td>
			@php
			$installment = App\Installment::where('plan_id',$plan->id)->count();
			@endphp
			{{ $installment }}
			</td>
			<td>
				<div class="btn-group" role="group" aria-label="First group">
					<button type="button" data-toggle="modal" data-target=".bs-example-modal-lg" class="btn waves-effect waves-light btn-rounded btn-sm btn-success" title="editar cuotas" onclick="edit_installments('{{ $plan->id }}')"><i class="mdi mdi-eye"></i></button>
					<button type="button" class="btn waves-effect waves-light btn-rounded btn-sm btn-danger" title="Eliminar cuenta" onclick="eliminar_plan('{{ $plan->id }}')"><i class="mdi mdi-delete"></i></button>
				</div>
			</td>
			</tr>
			@endforeach
		
	</tbody>
</table>