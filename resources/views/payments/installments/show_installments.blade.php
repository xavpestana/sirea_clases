<table id="table_installments" class="table table-hover earning-box">
	<thead>
		<tr>
			<th>Nombre</th>
			<th>Expiracion</th>
			<th>Monto</th>
			<th></th>
		</tr>
	</thead>
	<tbody>
		
			@foreach($installments as $installment)
			<tr>
			<td>{{ $installment->description }}</td>
			<td> @if(!is_null($installment->expire_at)) {{ $installment->expire_at->format('d-M-Y') }} @else Sin fecha @endif</td>
			<td>{{ $installment->amount }} {{ $installment->coin }}</td>
			<td>
				<div class="btn-group" role="group" aria-label="First group">
					<a type="button" href="#updt" class="btn waves-effect waves-light btn-rounded btn-sm btn-success" title="editar cuotas" onclick="updt_installments('{{ $installment->id }}')"><i class="mdi mdi-check" ></i> Actualizar</a>
					<button type="button" class="btn waves-effect waves-light btn-rounded btn-sm btn-danger" title="editar cuotas" onclick="delete_installments('{{ $installment->id }}')"><i class="mdi mdi-check"></i> Eliminar</button>
				</div>
			</td>
			</tr>
			@endforeach
		
	</tbody>
</table>
<script>
            $(document).ready(function() {
                $('#table_installments').DataTable( {
                    dom: 'Bfrtip',
                    "aaSorting": [[ 1, "desc" ]],
                });
            });

     
            </script>