@extends('layouts.app')

@section('title-view', 'Configuracion - Pagos')

@section('extra-css')
<link href="https://cdn.datatables.net/1.10.21/css/jquery.dataTables.min.css" rel="stylesheet">
@endsection

@section('content')
<div class="row">
    <!-- ============================================================== -->
    <!-- CCuentas bancarias del colegio -->
    <!-- ============================================================== -->
    <div class="col-lg-6">
    	<form id="invoice_settings">
                @csrf
        <div class="card">
            <div class="bg-light p-20">
                <div class="d-flex">
                    <div class="align-self-center">
                        <h4 class="m-b-0">Ultimo Número de control de factura</h4>
                    </div>
                    <div class="ml-auto align-self-center">
                        <button type="submit" class="btn waves-effect waves-light btn-rounded btn-success" onclick="invoice()">Agregar</button>
                    </div>
                </div>
            </div>
            <div class="card-body">
                    <input type="number" class="form-control" name="control_number" @if(!is_null($invoiceSetting)) value="{{ $invoiceSetting->control_number }}" @else value="0" @endif>
            </div>
        </div>
        </form>
    </div>
    @if(!is_null($invoiceSetting))
    <div class="col-lg-6">
    	<div class="btn-group" data-toggle="buttons">
            <span class="btn btn-info @if($invoiceSetting->headers == true) active  @endif">
                <input type="checkbox" id="md_checkbox_21" class="filled-in chk-col-light-blue" @if($invoiceSetting->headers == true) checked=""  @endif onchange="headers()">
                <label class="" for="md_checkbox_21">Activar o desactivar Encabezado: <small>el cambio afecta todas las facturas</small></label>
            </span>
            <span class="btn btn-info @if($invoiceSetting->iva == true) active  @endif">
                <input type="checkbox" id="md_checkbox_22" class="filled-in chk-col-light-blue" @if($invoiceSetting->iva == true) checked=""  @endif onchange="iva()">
                <label class="" for="md_checkbox_22">Activar o desactivar IVA: <small>el cambio afecta todas las facturas</small></label>
            </span>
        </div>
        <div class="demo-radio-button mt-4">
            <button type="button" id="simple" class="btn btn-primary @if($invoiceSetting->simple == true) focus active @endif" data-toggle="button" aria-pressed="@if($invoiceSetting->simple == true) on @else off @endif" autocomplete="off" onclick="simple()">
                @if($invoiceSetting->simple == true)
                Activar factura Sencilla
                @else
                Activar factura doble (se recomienda desactivar los encabezados)
                @endif
            </button>
        </div>
    </div>
     @endif
</div>
<div class="row justify-content-center">
                    
                    <!-- ============================================================== -->
                    <!-- CCuentas bancarias del colegio -->
                    <!-- ============================================================== -->
                    <div class="col-lg-6">
                        <div class="card">
                            
                            <div class="bg-light p-20">
                                <div class="d-flex">
                                    <div class="align-self-center">
                                        <h4 class="m-b-0">Cuentas bancarias del Colegio</h4></div>
                                    <div class="ml-auto align-self-center">
                                        <button type="button" class="btn waves-effect waves-light btn-rounded btn-success" data-toggle="modal" data-target=".bs-example-modal-lg" onclick="accountBank()">Agregar</button></div>
                                </div>
                            </div>
                            <div class="card-body">
                                <div id="accounts" class="table-responsive">

                                </div>
                            </div>
                        </div>
                    </div>
                            <!-- ============================================================== -->
                            <!-- Conceptos de pago -->
                            <!-- ============================================================== -->
                    <div class="col-lg-6">
                        <div class="card">
                            
                            <div class="bg-light p-20">
                                <div class="d-flex">
                                    <div class="align-self-center">
                                        <h4 class="m-b-0">Conceptos de pagos del colegio</h4></div>
                                    <div class="ml-auto align-self-center">
                                        <button type="button" class="btn waves-effect waves-light btn-rounded btn-success" data-toggle="modal" data-target=".bs-example-modal-lg" onclick="createConcept()">Agregar</button></div>
                                </div>
                            </div>
                            <div class="card-body">
                                <div id="concepts" class="table-responsive">

                                </div>
                            </div>
                        </div>
                    </div>
                            <!-- ============================================================== -->
                            <!-- Conceptos de pago -->
                            <!-- ============================================================== -->
                    <div class="col-lg-8">
                        <div class="card">
                            
                            <div class="bg-light p-20">
                                <div class="d-flex">
                                    <div class="align-self-center">
                                        <h4 class="m-b-0">Planes de pago</h4></div>
                                    <div class="ml-auto align-self-center">
                                        <button type="button" class="btn waves-effect waves-light btn-rounded btn-success" data-toggle="modal" data-target=".bs-example-modal-lg" onclick="createPlan()">Agregar</button></div>
                                </div>
                            </div>
                            <div class="card-body">
                                <div id="plan" class="table-responsive">

                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal fade bs-example-modal-lg " tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none;">
                                    <div class="modal-dialog modal-lg">
                                        <div id="modal_all" class="modal-content">
                                            
                                        </div>
                                        <!-- /.modal-content -->
                                    </div>
                                    <!-- /.modal-dialog -->
                                </div>
</div>
@endsection
@section('extra-js')
<script src="https://cdn.jsdelivr.net/npm/sweetalert2@9"></script>
<script src="https://cdn.datatables.net/1.10.21/js/jquery.dataTables.min.js"></script>
<script type="text/javascript" charset="utf-8">
    (function() {
   'use strict';
        window.addEventListener('load', function() {
        // Fetch all the forms we want to apply custom Bootstrap validation styles to
        var forms = document.getElementsByClassName('needs-validation');
        // Loop over them and prevent submission
        var validation = Array.prototype.filter.call(forms, function(form) {
            form.addEventListener('submit', function(event) {
                if (form.checkValidity() === false) {
                    event.preventDefault();
                    event.stopPropagation();
                }
            form.classList.add('was-validated');
        }, false);
        });
        }, false);
})();
    
    /********* Cuentas Bancarias **********/
    $('#accounts').load('{{ route('accountsBank.show', ['cuenta' => auth()->user()->id]) }}');
    /*Nueva cuenta*/
    function accountBank(){
        $('#modal_all').html(" ");
        $('#modal_all').load("{{route('accountsBank.create')}}");
    }
    /*Actualizar cuenta*/
    function edit(id){
        $('#modal_all').html(" ");
        $.ajax({
        type:'GET',
        url:"{{ url('colegio/cuentas/') }}/"+id+"/editar",
        success:function(data)
        {
         $('#modal_all').html(data);
        }
        })
    }
    /*Eliminar Cuenta*/
    function eliminar(id){
        $.ajax({
        type:'DELETE',
        data:{"_token": "{{ csrf_token() }}"},
        url:"{{ url('colegio/cuentas/') }}/"+id,
        success:function(r)
        {
            if (r == 0) {
                 $('#accounts').load('{{ route('accountsBank.show', ['cuenta' => auth()->user()->id]) }}');
            Swal.fire({
                position: 'center-center',
                icon: 'success',
                title: 'Cuenta eliminada correctamente',
                showConfirmButton: false,
                timer: 1500
            }) 
            } else {
                Swal.fire({
                icon: 'error',
                title: 'Oops...',
                text: 'intente denuevo!',
                })
            }
        }
        })
    }
    /********* Conceptos de pago **********/

    $('#concepts').load('{{ route('concepts.index', ['cuenta' => auth()->user()->id]) }}');
    /*Nuevo concepto*/
    function createConcept(){
        $('#modal_all').html(" ");
        $('#modal_all').load("{{route('concepts.create')}}");
    }

    /*Eliminar Concepto*/
    function eliminar_concept(id){
        $.ajax({
        type:'DELETE',
        data:{"_token": "{{ csrf_token() }}"},
        url:"{{ url('pagos/conceptos/') }}/"+id,
        success:function(r)
        {
            if (r == 0) {
                 $('#concepts').load('{{ route('concepts.index', ['cuenta' => auth()->user()->id]) }}');
            Swal.fire({
                position: 'center-center',
                icon: 'success',
                title: 'Concepto eliminado correctamente',
                showConfirmButton: false,
                timer: 1500
            }) 
            } else {
                Swal.fire({
                icon: 'error',
                title: 'Oops...',
                text: 'intente denuevo!',
                })
            }
        }
        })
    }
    /********* Planes de pago **********/

    $('#plan').load('{{ route('plans.index') }}');

    /*Nuevo plan*/
    function createPlan(){
        $('#modal_all').html(" ");
        $('#modal_all').load("{{route('plans.create')}}");
    }
     /*Actualizar cuotas*/
    function edit_installments(id){
        $('#modal_all').html(" ");
        $.ajax({
        type:'GET',
        url:"{{ url('pagos/planes/') }}/"+id,
        success:function(data)
        {
         $('#modal_all').html(data);
        }
        })
    }

    /*Eliminar Plan*/
    function eliminar_plan(id){
        $.ajax({
        type:'DELETE',
        data:{"_token": "{{ csrf_token() }}"},
        url:"{{ url('pagos/planes/') }}/"+id,
        success:function(r)
        {
            if (r == 0) {
        $('#plan').load('{{ route('plans.index') }}');
            Swal.fire({
                position: 'center-center',
                icon: 'success',
                title: 'Plan eliminado correctamente',
                showConfirmButton: false,
                timer: 1500
            }) 
            } else {
                Swal.fire({
                icon: 'error',
                title: 'Oops...',
                text: 'intente denuevo!',
                })
            }
        }
        })
    }
    /*Control de facturas*/
   $( "#invoice_settings" ).submit(function(e) {
e.preventDefault();

  $(".save").prop('disabled', true);
  datos = $("#invoice_settings").serialize();
$.ajax({
        type:'POST',
        data:datos,
        url:"{{ route('invoice-settings.store') }}",
        })
        .done(function(response) {
            $(".save").prop('disabled', false);
            location.reload();
            Swal.fire({
                position: 'center',
                icon: 'success',
                title: 'Actualizado exitosamente',
                showConfirmButton: false,
                timer: 1500
            }) 
        })
        .fail(function(response) {
            $(".save").prop('disabled', false);
            Swal.fire({
                icon: 'error',
                title: 'Oops...',
                text: 'Hubo un problema, intente denuevo!',
                })
        }) 
});

   /*Control de IVA*/
   function iva() {
    $("#md_checkbox_21").prop('disabled', true);
    $.ajax({
            type : "POST",
            data:{"_token": "{{ csrf_token() }}"},
            url : "{{ route('payment-settings.iva') }}",
        })
    .done(function(response) {
        location.reload();
         $("#md_checkbox_21").prop('disabled', false);
            Swal.fire({
                position: 'center',
                icon: 'success',
                title: 'Actualizado exitosamente',
                showConfirmButton: false,
                timer: 1500
            }) 
        })
        .fail(function(response) {
            $("#md_checkbox_21").prop('disabled', false);
            Swal.fire({
                icon: 'error',
                title: 'Oops...',
                text: 'Hubo un problema, intente denuevo!',
                })
        }) 
    }

    /*Control de Encabezados*/
   function headers() {
    $("#md_checkbox_22").prop('disabled', true);
    $.ajax({
            type : "POST",
            data:{"_token": "{{ csrf_token() }}"},
            url : "{{ route('payment-settings.headers') }}",
        })
    .done(function(response) {
        location.reload();
         $("#md_checkbox_22").prop('disabled', false);
            Swal.fire({
                position: 'center',
                icon: 'success',
                title: 'Actualizado exitosamente',
                showConfirmButton: false,
                timer: 1500
            }) 
        })
        .fail(function(response) {
            $("#md_checkbox_22").prop('disabled', false);
            Swal.fire({
                icon: 'error',
                title: 'Oops...',
                text: 'Hubo un problema, intente denuevo!',
                })
        }) 
    }

    /*Control de simple*/
   function simple() {
    $("#simple").prop('disabled', true);
    $.ajax({
            type : "POST",
            data:{"_token": "{{ csrf_token() }}"},
            url : "{{ route('payment-settings.simple') }}",
        })
    .done(function(response) {
        location.reload();
         $("#simple").prop('disabled', false);
            Swal.fire({
                position: 'center',
                icon: 'success',
                title: 'Actualizado exitosamente',
                showConfirmButton: false,
                timer: 1500
            }) 
        })
        .fail(function(response) {
            $("#simple").prop('disabled', false);
            Swal.fire({
                icon: 'error',
                title: 'Oops...',
                text: 'Hubo un problema, intente denuevo!',
                })
        }) 
    }
</script>
@endsection