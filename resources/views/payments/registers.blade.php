<div class="modal-header">
    <h5 class="modal-title" id="myLargeModalLabel">Registros de pago del alumno: {{ $student->profile->first_name_1 }} {{ $student->profile->first_name_2 }} {{ $student->profile->last_name_1 }} {{ $student->profile->last_name_2 }}</h5>
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
</div>
<div class="modal-body">
    <div class="table-responsive">
        
    
<table id="table_register" class="display nowrap table table-hover table-striped table-bordered" cellspacing="0">
	 <thead>
        <tr>
            <th>Nro de Factura</th>
            <th>Pagos de:</th>
            <th>Metodo de Pago</th>
            <th>Fecha</th>
            <th>Estado</th>
            <th>Abono</th>
            <th>Monto de pago</th>
            <th class="no-sort">Acciones
            <span class="preloader_mail">
            <svg class="circular" viewBox="25 25 50 50">
                <circle class="path" cx="50" cy="50" r="20" fill="none" stroke-width="2" stroke-miterlimit="10"></circle> </svg>
        </span></th>
        </tr>
    </thead>
    <tbody>
    @foreach ($payments as $payment)
        <tr>
            <td>
                @if($payment->annulled == true)
                <span class="text-danger">{{ $payment->number_invoice }}</span>
                @else
                <span class="text-success">{{ $payment->number_invoice }}</span>
                @endif
            </td>
            <td>
                @if (!$payment->installmentPayment->isEmpty())
                @foreach($payment->installmentPayment as $installment)
                <ul>
                    <li>{{ $installment->Installment->description }} ({{ $installment->Installment->amount }} {{ $installment->Installment->coin }})</li>
                </ul>
                @endforeach
                @else
                @foreach($payment->quotasCancelled as $installment)
                <ul>
                    <li>{{ $installment->Installment->description }} ({{ $installment->Installment->amount }} {{ $installment->Installment->coin }}) <span class="text-danger">(anulado)</span></li>
                </ul>
                @endforeach
                @endif
            </td>
            <td>{{ $payment->paymentMethod->name }}</td>
            <td>{{ $payment->created_at->format('d-M-Y') }}</td>
            <td>
                @if($payment->annulled == true)
                <span class="text-danger">Anulado</span>
                @else
                <span class="text-success">Activo</span>
                @endif
            </td>
            <td>{{ $payment->credit }} {{ $payment->coin_credit }}</td>
            <td>{{ $payment->amount_bsf }} Bsf</td>
            <td>

                <div class="btn-group">
                    @if($payment->annulled == false)
                    <button type="button" id="send" class="btn waves-effect waves-light btn-rounded btn-warning"  title="Enviar factura" onclick="send('{{ $payment->id }}')">
                        <i class="mdi mdi-file-send"></i>
                    </button>
                    <button type="button" id="send" class="btn waves-effect waves-light btn-rounded btn-danger"  title="Anular factura" onclick="annull('{{ $payment->id }}')">
                        <i class="mdi mdi-close-box"></i>
                    </button>
                    <button type="button" class="btn waves-effect waves-light btn-rounded btn-info dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" title="Descargar factura">
                        <i class="mdi mdi-folder-download"></i>
                    </button>
                    <div class="dropdown-menu">
                        <a class="dropdown-item" href="{{ url('print/factura') }}/1/{{ $payment->id }}" target="_blank">Pagina Completa</a>
                        <div class="dropdown-divider"></div>
                        <a class="dropdown-item" href="{{ url('print/factura') }}/2/{{ $payment->id }}" target="_blank">Media Pagina</a>
                        <!--<div class="dropdown-divider"></div>
                        <a class="dropdown-item" href="{{ url('print/factura') }}/3/{{ $payment->id }}" target="_blank">Cuarto de Página</a>-->
                    </div>
                    @endif
                </div>
                
            </td>
        </tr>      
    @endforeach                  
	</tbody>
</table>
</div>
</div>
			<script>
                $('.preloader_mail').hide();
                $('#table_register').DataTable( {
                    dom: 'Bfrtip',
                    "aaSorting": [[ 0, "desc" ]],
                });
     /**Mostrar las cuotas**/
 function send(id){
    $('.preloader_mail').show();
    $('#send').attr('disabled', true);
$.ajax({
        type:'GET',
        url:"{{route('invoice.send')}}/?invoice="+id,
        })
        .done(function(response) {
            $('.preloader_mail').hide();
            $('#send').removeAttr('disabled');
            Swal.fire({
                position: 'center',
                icon: 'success',
                title: 'Factura Enviada Exitosamente',
                showConfirmButton: false,
                timer: 1500
            }) 
        })
        .fail(function(xhr, status, error) {
            $('#send').removeAttr('disabled');
            $('.preloader_mail').hide();
            Swal.fire({
                icon: 'error',
                title: 'Oops...',
                text: 'Algo a sucedido, intente denuevo!',
                })
        }) 
    }
    function annull(id){
        Swal.fire({
            title: 'Desea anular la factura?',
            text: "Estos cambios no se pueden revertir!",
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Si, anular!'
                }).then((result) => {
                    if (result.isConfirmed) {
                        $('.btn').attr('disabled', true);
                        $.ajax({
                            type:'DELETE',
                            data : {"id" : id, "_token": "{{ csrf_token() }}"},
                            url:"{{url('pagos/facturar/')}}/"+id,
                        })
                        .done(function(response) {
                            $('.btn').attr('disabled', false);
                            $('#modal_all').load("{{url('pagos/facturar/')}}/{{ $student->id }}");
                            Swal.fire(
                                'Factura anulada!',
                                'La factura ha sido anulada con exito.',
                                'success'
                            )
                        })
                    }
                })
    }
            </script>