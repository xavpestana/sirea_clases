const mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

 /** Copia el archivo bootbox a la carpeta public */
 mix.copy('node_modules/bootbox/dist/bootbox.min.js', 'public/themeforest/main/js/bootbox.min.js');

mix.js([
    'resources/js/app.js', 'resources/js/themeforest/main/js/jquery.slimscroll.js',
    'resources/js/themeforest/main/js/waves.js', 'resources/js/themeforest/main/js/sidebarmenu.js',
    'resources/js/themeforest/main/js/custom.js', 'resources/js/themeforest/main/js/jquery.sparkline.min.js',
], 'public/js')
    .sass('resources/sass/app.scss', 'public/css');
