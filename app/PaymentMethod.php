<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * @class PaymentMethod
 * @brief Datos de métodos de pago
 *
 * Gestiona el modelo de datos para los métodos de pago
 *
 * @author William Páez <paez.william8@gmail.com>
 * @license <a href='​http://www.gnu.org/licenses/gpl-2.0.html'>GNU Public License versión 2 (GPLv2)</a>
 */
class PaymentMethod extends Model
{
    /**
     * Lista de atributos que pueden ser asignados masivamente
     *
     * @var array $fillable
     */
    protected $fillable = ['name'];

    /**
     * Método que obtiene los pagos de familiares asociados a un método de pago
     *
     * @author  William Páez <paez.william8@gmail.com>
     * @return object Objeto con los registros relacionados al modelo Payment
     */
    public function payments()
    {
        return $this->hasMany(Payment::class);
    }
}
