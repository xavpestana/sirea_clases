<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * @class Season
 * @brief Datos de Años Esolares
 *
 * Gestiona el modelo de datos para los años escolares
 *
 * @author Paúl Rojas <paul.rojase@gmail.com>
 */
class Season extends Model
{
	protected $fillable = [
		'school_id',
		'season',
		'periods_qty',
		'max_score',
		'sections_number',
		'status',
		'initial_pay',
		'name_range_1',
		'name_range_2',
		'name_range_3',
		'name_range_4',
		'range_1',
		'range_2',
		'range_3',
		'range_4'
    ];
        
	public function school()
	{
		return $this->belongsTo(School::class);
	}

    public function periods()
    {
        return $this->hasMany(Period::class);
    }

    public function sections()
    {
    	return $this->hasMany(Section::class);
    }

    public function enrollments()
    {
        return $this->hasManyThrough('App\Enrollment', 'App\Section');
    }
}
