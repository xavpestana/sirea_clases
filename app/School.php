<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class School extends Model
{
    protected $fillable = [
        'dea_code',
        'stadistic_code',
        'rif',
        'school_type',
        'name',
        'parish_id',
        'address',
        'phone',
        'fax',
        'email',
        'url',
        'logo',
        'city_id',
        'additional_data'
    ];

    public function parish()
    {
        return $this->belongsTo(Parish::class);
    }

    /**
     * Método que obtiene el usuario escolar asociado a una escuela
     *
     * @author  William Páez <paez.william8@gmail.com>
     * @return object Objeto con los registros relacionados al modelo UserSchool
     */
    public function userSchool()
    {
        return $this->hasOne(UserSchool::class);
    }

    /**
     * Método que obtiene las periodos escolares que pertenecen a un colegio
     *
     * @author  Paú Rojas <paul.rojase@gmail.com>
     * @return object Objeto con los registros relacionados al modelo School
     */
    public function seasons()
    {
        return $this->hasMany(Season::class);
    }

    /**
     * Método que obtiene el director que pertenece a un colegio
     *
     * @author  Paú Rojas <paul.rojase@gmail.com>
     * @return object Objeto con los registros relacionados al modelo School
     */
    public function director()
    {
        return $this->hasOne(Director::class);
    }

    /**
     * Método que obtiene la familia asociada a un colegio
     *
     * @author  Paúl Rojas <paul.rojase@gmail.com>
     * @return object Objeto con los registros relacionados al modelo User
     */
    public function families()
    {
        return $this->hasMany(Family::class);
    }

    /**
     * Método que obtiene el docente asociado con el colegio
     *
     * @author  Paúl Rojas <paul.rojase@gmail.com>
     * @return object Objeto con los registros relacionados al modelo User
     */
    public function teachers()
    {
        return $this->hasMany(Teacher::class);
    }

    /**
     * Método que obtiene los estudiantes pertenecientes a un colegio
     *
     * @author  Paúl Rojas <paul.rojase@gmail.com>
     * @return object Objeto con los registros relacionados al modelo User
     */
    public function students()
    {
        return $this->hasManyThrough('App\Student', 'App\Family');
    }

    /**
     * Método que obtiene los estudiantes pertenecientes a un colegio
     *
     * @author  Paúl Rojas <paul.rojase@gmail.com>
     * @return object Objeto con los registros relacionados al modelo User
     */
    public function representatives()
    {
        return $this->hasManyThrough('App\Representative', 'App\Family');
    }


    /**
     * Método que obtiene la Ciudad asociada a un Colegio
     *
     * @author  Paúl Rojas <paul.rojase@gmail.com>
     * @return object Objeto con los registros relacionados al modelo City
     */
    public function city()
    {
        return $this->belongsTo(City::class);
    }

    /**
     * Método que obtiene los planes de pago que pertenecen a un colegio
     *
     * @author  William Páez <paez.william8@gmail.com>
     * @return object Objeto con los registros relacionados al modelo Plan
     */
    public function plans()
    {
        return $this->hasMany(Plan::class);
    }

    /**
     * Método que obtiene los conceptos de inscripción que pertenecen a un colegio
     *
     * @author  William Páez <paez.william8@gmail.com>
     * @return object Objeto con los registros relacionados al modelo Concept
     */
    public function concepts()
    {
        return $this->hasMany(Concept::class);
    }

    /**
     * Método que obtiene los métodos de pago que pertenecen a un colegio
     *
     * @author  William Páez <paez.william8@gmail.com>
     * @return object Objeto con los registros relacionados al modelo PaymentMethod
     */
    public function paymentMethods()
    {
        return $this->hasMany(PaymentMethod::class);
    }

    /**
     * Obtiene todas las cuentas bancarias asociados al colegio
     *
     * @author  William Páez <paez.william8@gmail.com>
     * @return object Objeto con los registros relacionados al modelo BankAccount
     */
    public function bankAccounts()
    {
        return $this->morphMany(BankAccount::class, 'bank_accountable');
    }

    /**
     * Método que obtiene la configuración de factura asociado a un colegio
     *
     * @author  William Páez <paez.william8@gmail.com>
     * @return object Objeto con los registros relacionados al modelo InvoiceSetting
     */
    public function invoiceSetting()
    {
        return $this->belongsTo(InvoiceSetting::class);
    }

    public function Payment()
    {
        return $this->hasMany(Payment::class);
    }
}
