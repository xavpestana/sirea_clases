<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Student extends Model
{
    protected $fillable = [
        'family_id',
        'profile_id',
        'shirt_size',
        'pant_size',
        'shoe_size',
        'weight',
        'height',
        'blood_type',
        'motor_deficiency',
        'intellectual_deficiency',
        'hearing_impairment',
        'visual_deficiency',
        'respiratory_deficiency',
        'health_problem',
        'grade',
        'waist_size',
        'glasses',
        'medical_report',
        'surgical_intervention',
        'allergic',
        'medical_treatment',
        'medicine',
        'health_insurance',
        'chest_size',
        'arm_size',
        'medical_report_special',
        'practice_sports',
        'diabetes',
        'epilepsy'
    ];

    public function family()
    {
        return $this->belongsTo(Family::class);
    }

    public function profile()
    {
        return $this->belongsTo(Profile::class);
    }

    /**
     * Método que obtiene las inscripciones asociadas a un estudiante
     *
     * @author  William Páez <paez.william8@gmail.com>
     * @return object Objeto con los registros relacionados al modelo Inscripción
     */
    public function enrollments()
    {
        return $this->hasMany(Enrollment::class);
    }

    public function StudentPlan()
    {
        return $this->hasOne(StudentPlan::class);
    }
    public function Payment()
    {
        return $this->hasMany(Payment::class);
    }
    public function credit()
    {
        return $this->hasOne(credit::class);
    }
    public function installmentPayment()
    {
        return $this->hasMany(installmentPayment::class);
    }
}
