<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * @class Plan
 * @brief Datos de planes
 *
 * Gestiona el modelo de datos para los planes
 *
 * @author William Páez <paez.william8@gmail.com>
 * @license <a href='​http://www.gnu.org/licenses/gpl-2.0.html'>GNU Public License versión 2 (GPLv2)</a>
 */
class Plan extends Model
{
    /**
     * Lista de atributos que pueden ser asignados masivamente
     *
     * @var array $fillable
     */
    protected $fillable = ['name', 'season_id'];

    /**
     * Método que obtiene la Cuota asociada a un plan de Pago
     *
     * @author  William Páez <paez.william8@gmail.com>
     * @return object Objeto con los registros relacionados al modelo Installment
     */
    public function installments()
    {
        return $this->hasMany(Installment::class);
    }

    /**
     * Método que obtiene las inscripciones asociadas a un plan de pago
     *
     * @author  William Páez <paez.william8@gmail.com>
     * @return object Objeto con los registros relacionados al modelo Enrollment
     */
    public function enrollments()
    {
        return $this->hasMany(Enrollment::class);
    }

    /**
     * Método que obtiene el colegio asociado a un plan de pago
     *
     * @author  William Páez <paez.william8@gmail.com>
     * @return object Objeto con los registros relacionados al modelo School
     */
    public function school()
    {
        return $this->belongsTo(School::class);
    }
    public function studentPlan()
    {
        return $this->hasMany(StudentPlan::class);
    }
}
