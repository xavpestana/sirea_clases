<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * @class UserSchool
 * @brief Datos del usuario escolar
 *
 * Gestiona el modelo de datos para el Usuario Escolar
 *
 * @author William Páez <paez.william8@gmail.com>
 * @license <a href='​http://www.gnu.org/licenses/gpl-2.0.html'>GNU Public License versión 2 (GPLv2)</a>
 */
class UserSchool extends Model
{
    /**
     * Lista de atributos que pueden ser asignados masivamente
     *
     * @var array $fillable
     */
    protected $fillable = ['school_id', 'user_id', 'parent_id'];

    /**
     * Método que obtiene el usuario padre asociado a un usuario hijo
     *
     * @author  William Páez <paez.william8@gmail.com>
     * @return object Objeto con los registros relacionados al modelo UserSchool
     */
    public function parent()
    {
        return $this->belongsTo(UserSchool::class, 'parent_id');
    }

    /**
     * Método que obtiene los usuarios hijos asociados a un usuario padre
     *
     * @author  William Páez <paez.william8@gmail.com>
     * @return object Objeto con los registros relacionados al modelo UserSchool
     */
    public function childrens()
    {
        return $this->hasMany(UserSchool::class, 'parent_id');
    }

    /**
     * Método que obtiene la escuela asociado a un usuario escolar
     *
     * @author  William Páez <paez.william8@gmail.com>
     * @return object Objeto con los registros relacionados al modelo School
     */
    public function school()
    {
        return $this->belongsTo(School::class);
    }

    /**
     * Método que obtiene el usuario asociado a un usuario escolar
     *
     * @author  William Páez <paez.william8@gmail.com>
     * @return object Objeto con los registros relacionados al modelo User
     */
    public function user()
    {
        return $this->belongsTo(User::class);
    }

}
