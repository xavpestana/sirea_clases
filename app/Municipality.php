<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * @class Municipality
 * @brief Datos de municipios
 *
 * Gestiona el modelo de datos para los Municipios
 *
 * @author William Páez <paez.william8@gmail.com>
 * @license <a href='​http://www.gnu.org/licenses/gpl-2.0.html'>GNU Public License versión 2 (GPLv2)</a>
 */
class Municipality extends Model
{
    /**
     * Lista de atributos que pueden ser asignados masivamente
     *
     * @var array $fillable
     */
    protected $fillable = ['name', 'state_id', 'code'];

    /**
     * Método que obtiene el Estado asociado a un Municipio
     *
     * @author  William Páez <paez.william8@gmail.com>
     * @return object Objeto con los registros relacionados al modelo State
     */
    public function state()
    {
        return $this->belongsTo(State::class);
    }

    /**
     * Método que obtiene las Parroquias asociadas a un Municipio
     *
     * @author  William Páez <paez.william8@gmail.com>
     * @return object Objeto con los registros relacionados al modelo Parish
     */
    public function parishes()
    {
        return $this->hasMany(Parish::class);
    }
}
