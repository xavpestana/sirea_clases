<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * @class InvoiceSetting
 * @brief Datos de configuración de factura
 *
 * Gestiona el modelo de datos para la configuración de la factura
 *
 * @author William Páez <paez.william8@gmail.com>
 * @license <a href='​http://www.gnu.org/licenses/gpl-2.0.html'>GNU Public License versión 2 (GPLv2)</a>
 */
class InvoiceSetting extends Model
{
    /**
     * Lista de atributos que pueden ser asignados masivamente
     *
     * @var array $fillable
     */
    protected $fillable = ['control_number', 'school_id'];

    /**
     * Método que obtiene el colegio asociado a una configuración de factura
     *
     * @author  William Páez <paez.william8@gmail.com>
     * @return object Objeto con los registros relacionados al modelo School
     */
    public function school()
    {
        return $this->hasOne(School::class);
    }
}
