<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * @class State
 * @brief Datos de estados
 *
 * Gestiona el modelo de datos para los Estados
 *
 * @author William Páez <paez.william8@gmail.com>
 * @license <a href='​http://www.gnu.org/licenses/gpl-2.0.html'>GNU Public License versión 2 (GPLv2)</a>
 */
class State extends Model
{
    /**
     * Lista de atributos que pueden ser asignados masivamente
     *
     * @var array $fillable
     */
    protected $fillable = ['name', 'country_id', 'code'];

    /**
     * Método que obtiene el País asociado a un Estado
     *
     * @author  William Páez <paez.william8@gmail.com>
     * @return object Objeto con los registros relacionados al modelo Country
     */
    public function country()
    {
        return $this->belongsTo(Country::class);
    }

    /**
     * Método que obtiene los Municipios asociados a un Estado
     *
     * @author  William Páez <paez.william8@gmail.com>
     * @return object Objeto con los registros relacionados al modelo Municipality
     */
    public function municipalities()
    {
        return $this->hasMany(Municipality::class);
    }

    /**
     * Método que obtiene las Ciudades asociadass a un Estado
     *
     * @author  William Páez <paez.william8@gmail.com>
     * @return object Objeto con los registros relacionados al modelo City
     */
    public function cities()
    {
        return $this->hasMany(City::class);
    }
}
