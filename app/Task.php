<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Task extends Model
{
    protected $fillable = ['course_id ', 'message', 'file', 'request', 'receiver'];

    public function course()
    {
        return $this->belongsTo(Course::class);
    }
    public function comments()
    {
        return $this->hasMany(Task::class);
    }
}
