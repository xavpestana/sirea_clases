<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class quotasCancelled extends Model
{
     protected $fillable = [
        'payment_id',
        'installment_id',
        'student_id'
        
    ];
    public function Payment()
    {
        return $this->belongsTo(Payment::class);
    }
    public function Installment()
    {
        return $this->belongsTo(Installment::class);
    }
    public function Student()
    {
        return $this->belongsTo(Student::class);
    }
}
