<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class credit extends Model
{
    protected $fillable = [
    	'student_id',
        'amount',
        'coin'
        
    ];
    public function Student()
    {
        return $this->belongsTo(Student::class);
    }
}
