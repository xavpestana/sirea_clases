<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class StudentPlan extends Model
{
    protected $fillable = ['season_id ', 'student_id', 'plan_id'];

    public function plan()
    {
        return $this->belongsTo(Plan::class);
    }
    public function student()
    {
        return $this->belongsTo(Student::class);
    }
}
