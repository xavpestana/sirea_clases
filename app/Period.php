<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * @class Period
 * @brief Datos de Periodos o Lapsos Escolares
 *
 * Gestiona el modelo de datos para los Periodosas
 *
 * @author Paúl Rojas <paul.rojase@gmail.com>
 */
class Period extends Model
{
	protected $fillable = ['number', 'season_id', 'status'];

    public function season()
    {
        return $this->belongsTo(Season::class);
    }
}
