<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * @class City
 * @brief Datos de ciudades
 *
 * Gestiona el modelo de datos para las Ciudades
 *
 * @author William Páez <paez.william8@gmail.com>
 * @license <a href='​http://www.gnu.org/licenses/gpl-2.0.html'>GNU Public License versión 2 (GPLv2)</a>
 */
class City extends Model
{
    /**
     * Lista de atributos que pueden ser asignados masivamente
     *
     * @var array $fillable
     */
    protected $fillable = ['name', 'state_id', 'code'];

    /**
     * Método que obtiene el Estado asociado a una Ciudad
     *
     * @author  William Páez <paez.william8@gmail.com>
     * @return object Objeto con los registros relacionados al modelo State
     */
    public function state()
    {
        return $this->belongsTo(State::class);
    }

    /**
     * Método que obtiene los Colegios asociados a una Ciudad
     *
     * @author  Paúl Rojas <paul.rojase@gmail.com>
     * @return object Objeto con los registros relacionados al modelo School
     */
    public function schools()
    {
        return $this->hasMany(School::class);
    }

    /**
     * Método que obtiene las Familias asociadas a una Ciudad
     *
     * @author  Paúl Rojas <paul.rojase@gmail.com>
     * @return object Objeto con los registros relacionados al modelo Family
     */
    public function families()
    {
        return $this->hasMany(Family::class);
    }

    /**
     * Método que obtiene los Docentes asociadas a una Ciudad
     *
     * @author  Paúl Rojas <paul.rojase@gmail.com>
     * @return object Objeto con los registros relacionados al modelo Teacher
     */
    public function teachers()
    {
        return $this->hasMany(Teacher::class);
    }

}
