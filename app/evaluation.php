<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class evaluation extends Model
{
    protected $fillable = ['user_id', 'evaluation_plan_id', 'description'];

    public function evaluationPlan()
    {
        return $this->belongsTo(evaluationPlan::class);
    }
}
