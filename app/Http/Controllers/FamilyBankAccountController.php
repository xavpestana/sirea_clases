<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Foundation\Validation\ValidatesRequests;
use App\BankAccount;
use App\Family;

/**
 * @class FamilyBankAccountController
 * @brief Controlador de cuentas bancarias de la familia
 *
 * Clase que gestiona las cuentas bancarias
 *
 * @author William Páez <paez.william8@gmail.com>
 * @license <a href='​http://www.gnu.org/licenses/gpl-2.0.html'>GNU Public License versión 2 (GPLv2)</a>
 */
class FamilyBankAccountController extends Controller
{
    use ValidatesRequests;

    /**
     * Define la configuración de la clase
     *
     * @author William Páez <paez.william8@gmail.com>
     */
    public function __construct()
    {
        /** Establece permisos de acceso para cada método del controlador */
        $this->middleware('permission:family.bank.accounts.list', ['only' => 'vueList']);
        $this->middleware('permission:family.bank.accounts.create', ['only' => ['create', 'store']]);
        $this->middleware('permission:family.bank.accounts.show', ['only' => 'show']);
        $this->middleware('permission:family.bank.accounts.edit', ['only' => ['edit', 'update']]);
        $this->middleware('permission:family.bank.accounts.delete', ['only' => 'destroy']);
    }

    /**
     * Muestra todos los registros de cuentas bancarias de la familia
     *
     * @author William Páez <paez.william8@gmail.com>
     * @return \Illuminate\View\View    Muestra los datos organizados en una tabla
     */
    public function index()
    {
        return view('family.bank-accounts.index');
    }

    /**
     * Muestra el formulario de registro de cuentas bancarias
     *
     * @author William Páez <paez.william8@gmail.com>
     * @return \Illuminate\View\View    Vista con el formulario
     */
    public function create()
    {
        return view('family.bank-accounts.create-edit');
    }

    /**
     * Valida y registra nueva cuanta bancaria de la familia
     *
     * @author  William Páez <paez.william8@gmail.com>
     * @param  \Illuminate\Http\Request $request    Solicitud con los datos a guardar
     * @return \Illuminate\Http\JsonResponse        Json: result en verdadero y redirect con la url a donde ir
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'account_number' => ['required', 'numeric', 'regex:/^\d{20}$/u', 'unique:bank_accounts,account_number'],
            'bank_id' => ['required'],
        ]);

        $family = Family::where('school_id', auth()->user()->family->school_id)->first();
        $family->bankAccounts()->save(new BankAccount([
            'account_number' => $request->account_number,
            'bank_id' => $request->bank_id,
        ]));
        $request->session()->flash('message', ['type' => 'store']);
        return response()->json([
            'result' => true, 'redirect' => route('family-bank-accounts.index')
        ], 200);
    }

    /**
     * Muestra los datos de una cuenta bancaria
     *
     * @author  William Páez <paez.william8@gmail.com>
     * @param  integer $id                          Identificador del dato a mostrar
     * @return \Illuminate\Http\JsonResponse        Json con el dato de la cuenta bancaria
     */
    public function show($id)
    {
        $family = Family::where('school_id', auth()->user()->family->school_id)
            ->with('bankAccounts')->first();
        $bankAccount = $family->bankAccounts()->where('id', $id)->with('bank')->first();
        return response()->json(['record' => $bankAccount], 200);
    }

    /**
     * Muestra el formulario de actualización de cuenta bancaria del la familia
     *
     * @author William Páez <paez.william8@gmail.com>
     * @param  integer $id              Identificador con el dato a actualizar
     * @return \Illuminate\View\View    Vista con el formulario y el objeto con el dato a actualizar
     */
    public function edit($id)
    {
        $family = Family::where('school_id', auth()->user()->family->school_id)
            ->with('bankAccounts')->first();
        $bankAccount = $family->bankAccounts()->where('id', $id)->with('bank')->first();
        if (!$bankAccount) {
            abort(403, 'El usuario no tiene los permisos para entrar a esta página.');
        }
        return view('family.bank-accounts.create-edit', compact('bankAccount'));
    }

    /**
     * Actualiza la información de la cuenta bancaria
     *
     * @author  William Páez <paez.william8@gmail.com>
     * @param  \Illuminate\Http\Request  $request   Solicitud con los datos a actualizar
     * @param  integer $id                          Identificador del dato a actualizar
     * @return \Illuminate\Http\JsonResponse        Json con la redirección y mensaje de confirmación de la operación
     */
    public function update(Request $request, $id)
    {
        $family = Family::where('school_id', auth()->user()->family->school_id)
            ->with('bankAccounts')->first();
        $bankAccount = $family->bankAccounts()->where('id', $id)->with('bank')->first();
        if (!$bankAccount) {
            abort(403, 'El usuario no tiene los permisos para entrar a esta página.');
        }
        $this->validate($request, [
            'account_number' => [
                'required', 'numeric', 'regex:/^\d{20}$/u', 'unique:bank_accounts,account_number,'.$bankAccount->id
            ],
            'bank_id' => ['required'],
        ]);
        $bankAccount->account_number = $request->account_number;
        $bankAccount->bank_id = $request->bank_id;
        $bankAccount->save();
        $request->session()->flash('message', ['type' => 'update']);
        return response()->json([
            'result' => true, 'redirect' => route('family-bank-accounts.index')
        ], 200);
    }

    /**
     * Elimina la información de la cuenta bancaria
     *
     * @author  William Páez <paez.william8@gmail.com>
     * @param  integer $id                      Identificador del dato a eliminar
     * @return \Illuminate\Http\JsonResponse    Json con mensaje de confirmación de la operación
     */
    public function destroy($id)
    {
        $family = Family::where('school_id', auth()->user()->family->school_id)
            ->with('bankAccounts')->first();
        $bankAccount = $family->bankAccounts()->where('id', $id)->with('bank')->first();
        if (!$bankAccount) {
            abort(403, 'El usuario no tiene los permisos para entrar a esta página.');
        }
        $bankAccount->delete();
        return response()->json(['record' => $bankAccount, 'message' => 'Success'], 200);
    }

    /**
     * Muestra las cuentas bancarias de la familia
     *
     * @author  William Páez <paez.william8@gmail.com>
     * @return \Illuminate\Http\JsonResponse    Json con los datos de las cuentas bancarias
     */
    public function vueList()
    {
        $family = Family::where('school_id', auth()->user()->family->school_id)
            ->with(['bankAccounts' => function ($query) {
                $query->with('bank');
            }])->first();
        return response()->json(['records' => $family->bankAccounts], 200);
    }

    /**
     * Obtiene las cuentas bancarias del familiar
     *
     * @author  William Páez <paez.william8@gmail.com>
     * @return \Illuminate\Http\JsonResponse    JSON con los datos de los conceptos
     */
    public function listFamilyBankAccounts()
    {
        $data = [];
        $data[0] = [
            'id' => '',
            'text' => 'Seleccione...'
        ];

        $family = Family::where('school_id', auth()->user()->family->school_id)
            ->with(['bankAccounts' => function ($query) {
                $query->with('bank');
            }])->first();
        foreach ($family->bankAccounts as $bankAccount) {
            $data[] = [
                'id' => $bankAccount->id,
                'text' => $bankAccount->account_number.' - '.$bankAccount->bank->name,
            ];
        }
        return response()->json($data);
    }
}
