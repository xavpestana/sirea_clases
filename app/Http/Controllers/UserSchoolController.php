<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Foundation\Validation\ValidatesRequests;
use App\UserSchool;
use App\User;
use App\Season;
use App\Grade;
use App\Director;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;
use App\Mail\UserRegister;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;
use Spatie\Permission\Models\Role;

/**
 * @class UserSchoolController
 * @brief Controlador de usuarios escolares (admin de escuela)
 *
 * Clase que gestiona los usuarios escolares
 *
 * @author William Páez <paez.william8@gmail.com>
 * @license <a href='​http://www.gnu.org/licenses/gpl-2.0.html'>GNU Public License versión 2 (GPLv2)</a>
 */
class UserSchoolController extends Controller
{
    use ValidatesRequests;

    /**
     * Define la configuración de la clase
     *
     * @author William Páez <paez.william8@gmail.com>
     */
    public function __construct()
    {
        /** Establece permisos de acceso para cada método del controlador */
        $this->middleware('permission:user.schools.list', ['only' => ['index', 'vueList']]);
        $this->middleware('permission:user.schools.create', ['only' => ['create', 'store']]);
        $this->middleware('permission:user.schools.show', ['only' => 'show']);
        //$this->middleware('permission:user.schools.edit', ['only' => ['edit', 'update']]);
        //$this->middleware('permission:user.schools.delete', ['only' => 'destroy']);
    }

    /**
     * Muestra todos los registros de usuarios escolares
     *
     * @author William Páez <paez.william8@gmail.com>
     * @return \Illuminate\View\View    Muestra los datos organizados en una tabla
     */
    public function index()
    {
        return view('school.user-schools.index');
    }

    /**
     * Muestra el formulario de registro de usuarios escolares
     *
     * @author William Páez <paez.william8@gmail.com>
     * @return \Illuminate\View\View    Vista con el formulario
     */
    public function create()
    {
        if (!is_null(auth()->user()->userSchool->parent_id)) {
            abort(403, 'El usuario no tiene los permisos para entrar a esta página.');
        }
        return view('school.user-schools.create-edit');
    }

    /**
     * Valida y registra nuevo usuario escolar
     *
     * @author  William Páez <paez.william8@gmail.com>
     * @param  \Illuminate\Http\Request $request    Solicitud con los datos a guardar
     * @return \Illuminate\Http\JsonResponse        Json: result en verdadero y redirect con la url a donde ir
     */
    public function store(Request $request)
    {
        if (!is_null(auth()->user()->userSchool->parent_id)) {
            abort(403, 'El usuario no tiene los permisos para entrar a esta página.');
        }
        $this->validate($request, [
            'name' => ['required'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
        ]);

        //$password = 'usuario12345';
        $password = Str::random();
        $user = User::create([
            'name' => $request->name,
            'email' => $request->email,
            'password' => bcrypt($password),
        ]);

        $userSchoolRole = Role::where('name', 'school')->first();
        $user->assignRole($userSchoolRole);

        $us = UserSchool::where('user_id', Auth::user()->id)->first();
        $userSchool = UserSchool::create([
            'user_id' => $user->id,
            'school_id' => $us->school_id,
            'parent_id' => $us->id,
        ]);

        Mail::to($user)->send(new UserRegister($user, $password));
        $request->session()->flash('message', ['type' => 'store']);
        return response()->json([
            'result' => true, 'redirect' => route('user-schools.index')
        ], 200);
    }

    /**
     * Muestra los datos de un usuario escolar
     *
     * @author  William Páez <paez.william8@gmail.com>
     * @param  integer $id                          Identificador del dato a mostrar
     * @return \Illuminate\Http\JsonResponse        Json con el dato del usuario escolar
     */
    public function show($id)
    {
        $userSchool = UserSchool::where('id', $id)->with([
            'user', 'school',
            'parent' => function ($query) {
                $query->with('user');
            },
        ])->first();
        return response()->json(['record' => $userSchool], 200);
    }

    /**
     * Muestra el formulario de actualización de usuario escolar
     *
     * @author William Páez <paez.william8@gmail.com>
     * @param  integer $id              Identificador con el dato a actualizar
     * @return \Illuminate\View\View    Vista con el formulario y el objeto con el dato a actualizar
     */
    public function edit($id)
    {
        //
    }

    /**
     * Actualiza la información del usuario escolar
     *
     * @author  William Páez <paez.william8@gmail.com>
     * @param  \Illuminate\Http\Request  $request   Solicitud con los datos a actualizar
     * @param  integer $id                          Identificador del dato a actualizar
     * @return \Illuminate\Http\JsonResponse        Json con la redirección y mensaje de confirmación de la operación
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Elimina la información del usuario escolar
     *
     * @author  William Páez <paez.william8@gmail.com>
     * @param  integer $id                      Identificador del dato a eliminar
     * @return \Illuminate\Http\JsonResponse    Json con mensaje de confirmación de la operación
     */
    public function destroy($id)
    {
        //
    }

    /**
     * Muestra los usuarios escolares del usuario escolar administrador
     *
     * @author  William Páez <paez.william8@gmail.com>
     * @return \Illuminate\Http\JsonResponse    Json con los datos de los usuarios escolares
     */
    public function vueList()
    {
        return response()->json([
            'records' => UserSchool::where(
                'parent_id',
                Auth::user()->userSchool->id
            )->with(['user', 'childrens'])->get()
        ], 200);
    }

    public function settings()
    {
        $school = auth()->user()->userSchool->school;

        $seasons = Season::where('school_id', $school->id)->get();

        $director = $school->director;

        $grades = Grade::all();

        return view('school.settings', compact('school', 'seasons', 'director', 'grades'));
    }
}
