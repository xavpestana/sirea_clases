<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use App\Course;
use App\Section;
use App\Level;
use App\Grade;
use App\User;
use App\Season;
use App\Teacher;
use App\Courses_sections_user;

class CourseController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $grades = Grade::all();
        $seasons = Season::where('status','1')
                        ->where('school_id',auth()->user()->UserSchool->school_id)
                        ->first();
        
        $teachers = Teacher::where('school_id', auth()->user()->UserSchool->school_id)->get();

        return view('school.courses', compact('grades', 'teachers', 'seasons'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {   

        $Course = new Course;
        $Course->grade_id = $request->id_grade;
        $Course->name = $request->name;
        $Course->save();

        foreach ($request->teacher as $teacher) {

            $Courses_sections_user = new Courses_sections_user;
            $Courses_sections_user->course_id = $Course->id;
            $Courses_sections_user->section_id = $request->sec;
            $Courses_sections_user->user_id = $teacher;
            $Courses_sections_user->save();
        }

        return 0;
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Course  $course
     * @return \Illuminate\Http\Response
     */
    public function show(Course $course)
    {
        //
    }

    /**
     * Show table subjects and teachers
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function table(Request $request)
    {
            $seasons = Season::where('status','1')
                        ->where('school_id',auth()->user()->UserSchool->school_id)
                        ->first();
                        
            $courses = Course::all();
        return view('course.tables.subjects',compact('courses','seasons'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Course  $course
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request)
    {
        $course = Course::where('id',$request->id)->first();
        $teachers = Teacher::where('school_id', auth()->user()->UserSchool->school_id)->get();

        //dd($course);
        return view('course.modal.edit_subjects', compact('course', 'teachers'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Course  $course
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Course $course)
    {
        Course::where('id', $request->id_course)
          ->update([
            'name' => $request->name,
       ]);

          Courses_sections_user::where('course_id', $request->id_course)->delete();
 
          foreach ($request->teacher as $teacher) {
            $Courses_sections_user = new Courses_sections_user;
            $Courses_sections_user->course_id = $request->id_course;
            $Courses_sections_user->section_id = $request->id_section;
            $Courses_sections_user->user_id = $teacher;
            $Courses_sections_user->save();
        }

        return 0;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Course  $course
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        Courses_sections_user::where('course_id', $request->id)->delete();
        Course::where('id', $request->id)->delete();
    }
    /**
     * Utilities Courses.
     *
     * @param  \App\Course  $course
     * @return \Illuminate\Http\Response
     */
    public function sections(Request $request)
    {
        $sections = Section::where('grade_id',$request->id_grade)
                        ->where('season_id',$request->id_season)
                        ->get();
                        
        return view('teacher.utilities.select_level', compact('sections'));
    }
}
