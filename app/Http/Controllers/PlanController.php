<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Support\Facades\DB;
use App\Plan;
use App\Season;
use App\Installment;

/**
 * @class PlanController
 * @brief Controlador de plan de pago
 *
 * Clase que gestiona los planes de pago
 *
 * @author William Páez <paez.william8@gmail.com>
 * @license <a href='​http://www.gnu.org/licenses/gpl-2.0.html'>GNU Public License versión 2 (GPLv2)</a>
 */
class PlanController extends Controller
{
    use ValidatesRequests;


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        $plans = DB::table('plans')
            ->join('seasons', 'seasons.id', '=', 'plans.season_id')
            ->where('seasons.school_id', auth()->user()->userSchool->school_id)
            ->select('plans.id', 'plans.name', 'seasons.season',)
            ->orderBy('seasons.id', 'desc')
            ->get();

            //dd($plans);
        return view('payments.plans.show-plans', compact('plans'));
    }

    /**
     * Muestra el formulario de registro de plan de pago
     *
     * @author William Páez <paez.william8@gmail.com>
     * @return \Illuminate\View\View    Vista con el formulario
     */
    public function create()
    {
        return view('payments.plans.create-plans');
    }

    /**
     * Valida y registra nuevo plan de pago
     *
     * @author  William Páez <paez.william8@gmail.com>
     * @param  \Illuminate\Http\Request $request    Solicitud con los datos a guardar
     * @return \Illuminate\Http\JsonResponse        Json: result en verdadero y redirect con la url a donde ir
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => ['required'],
        ]);
        
         $season = Season::where('status','1')
                        ->where('school_id', auth()->user()->userSchool->school_id)
                        ->first();

        $plan = Plan::create([
            'name' => $request->name,
            'season_id' => $season->id,
        ]);

        if ($plan) {
            return $plan->id;
        } else {
            return 1;
        }
        
    }

    /**
     * Muestra los datos de un plan de pago
     *
     * @author  William Páez <paez.william8@gmail.com>
     * @param  integer $id                          Identificador del dato a mostrar
     * @return \Illuminate\Http\JsonResponse        Json con el dato del concepto de inscripción
     */
    public function show($id)
    {
        $plan = Plan::find($id);
        return view('payments.plans.edit-plan',compact('plan'));
    }

    /**
     * Muestra el formulario de actualización de plan de pago
     *
     * @author William Páez <paez.william8@gmail.com>
     * @param  integer $id              Identificador con el dato a actualizar
     * @return \Illuminate\View\View    Vista con el formulario y el objeto con el dato a actualizar
     */
    public function edit($id)
    {
        $plan = Plan::where('id', $id)->where(
            'school_id',
            auth()->user()->userSchool->school_id
        )->with('installments')->first();
        if (!$plan) {
            abort(403, 'El usuario no tiene los permisos para entrar a esta página.');
        }
        return view('payments.plans.create-edit', compact('plan'));
    }

    /**
     * Actualiza la información del plan de pago
     *
     * @author  William Páez <paez.william8@gmail.com>
     * @param  \Illuminate\Http\Request  $request   Solicitud con los datos a actualizar
     * @param  integer $id                          Identificador del dato a actualizar
     * @return \Illuminate\Http\JsonResponse        Json con la redirección y mensaje de confirmación de la operación
     */
    public function update(Request $request, $id)
    {
        $plan = Plan::where('id', $id)->where(
            'school_id',
            auth()->user()->userSchool->school_id
        )->with('installments')->first();
        if (!$plan) {
            abort(403, 'El usuario no tiene los permisos para entrar a esta página.');
        }
        $this->validate($request, [
            'name' => ['required'],
        ]);
        $i = 0;
        foreach ($request->installments as $installment) {
            $this->validate($request, [
                'installments.'.$i.'.description' => ['required'],
                'installments.'.$i.'.amount' => ['required', 'numeric'],
            ]);
            $i++;
        }

        $plan->name = $request->name;
        foreach ($plan->installments as $installment) {
            $inst = Installment::find($installment['id']);
            $inst->delete();
        }
        foreach ($request->installments as $installment) {
            $inst = Installment::create([
                'description' => $installment['description'],
                'amount' => $installment['amount'],
                'plan_id' => $plan->id,
            ]);
        }
        $plan->save();
        $request->session()->flash('message', ['type' => 'update']);
        return response()->json([
            'result' => true, 'redirect' => route('payment-settings.index')
        ], 200);
    }

    /**
     * Elimina la información de plan de pago
     *
     * @author  William Páez <paez.william8@gmail.com>
     * @param  integer $id                      Identificador del dato a eliminar
     * @return \Illuminate\Http\JsonResponse    Json con mensaje de confirmación de la operación
     */
    public function destroy($id)
    {
        $plan = Plan::find($id);
        $plan->delete();

        return 0;
    }
}
