<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Foundation\Validation\ValidatesRequests;
use App\BankAccount;
use App\Bank;
use App\School;

/**
 * @class BankAccountController
 * @brief Controlador de cuentas bancarias
 *
 * Clase que gestiona las cuentas bancarias
 *
 * @author William Páez <paez.william8@gmail.com>
 * @license <a href='​http://www.gnu.org/licenses/gpl-2.0.html'>GNU Public License versión 2 (GPLv2)</a>
 */
class BankAccountController extends Controller
{
    use ValidatesRequests;

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Muestra el formulario de registro de cuentas bancarias
     *
     * @author William Páez <paez.william8@gmail.com>
     * @return \Illuminate\View\View    Vista con el formulario
     */
    public function create()
    {
        $banks = Bank::all();
        return view('payments.bank-accounts.create-account', compact('banks'));
    }

    /**
     * Valida y registra nueva cuanta bancaria del colegio
     *
     * @author  William Páez <paez.william8@gmail.com>
     * @param  \Illuminate\Http\Request $request    Solicitud con los datos a guardar
     * @return \Illuminate\Http\JsonResponse        Json: result en verdadero y redirect con la url a donde ir
     */
    public function store(Request $request)
    {
        $validator = $this->validate($request, [
            'account' => ['required', 'numeric', 'regex:/^\d{20}$/u', 'unique:bank_accounts,account_number'],
            'bank' => ['required'],
        ]);

        $school = School::where('id', auth()->user()->userSchool->school_id)->first();
        $school->bankAccounts()->save(new BankAccount([
            'account_number' => $request->account,
            'bank_id' => $request->bank,
        ]));

        return 0;
    }

    /**
     * Muestra los datos de una cuenta bancaria
     *
     * @author  William Páez <paez.william8@gmail.com>
     * @param  integer $id                          Identificador del dato a mostrar
     * @return \Illuminate\Http\JsonResponse        Json con el dato de la cuenta bancaria
     */
    public function show($id)
    {
        $bankAccounts = BankAccount::where('bank_accountable_type', 'App\School')
                                        ->where('bank_accountable_id', auth()->user()->userSchool->school_id)
                                        ->get();
            
        return view('payments.bank-accounts.show-account', compact('bankAccounts'));
    }

    /**
     * Muestra el formulario de actualización de cuanta bancaria del colegio
     *
     * @author William Páez <paez.william8@gmail.com>
     * @param  integer $id              Identificador con el dato a actualizar
     * @return \Illuminate\View\View    Vista con el formulario y el objeto con el dato a actualizar
     */
    public function edit($id)
    {
        $banks = Bank::all();
        $bankAccounts = BankAccount::find($id);
        
        return view('payments.bank-accounts.edit-account', compact('banks', 'bankAccounts'));
    }

    /**
     * Actualiza la información de la cuenta bancaria
     *
     * @author  William Páez <paez.william8@gmail.com>
     * @param  \Illuminate\Http\Request  $request   Solicitud con los datos a actualizar
     * @param  integer $id                          Identificador del dato a actualizar
     * @return \Illuminate\Http\JsonResponse        Json con la redirección y mensaje de confirmación de la operación
     */
    public function update(Request $request, $id)
    {
        $school = School::where('id', auth()->user()->userSchool->school_id)
            ->with('bankAccounts')->first();
        $bankAccount = $school->bankAccounts()->where('id', $id)->with('bank')->first();
        $bankAccount->account_number = $request->account;
        $bankAccount->bank_id = $request->bank;
        $bankAccount->save();
        return 0;
    }

    /**
     * Elimina la información de la cuenta bancaria
     *
     * @author  William Páez <paez.william8@gmail.com>
     * @param  integer $id                      Identificador del dato a eliminar
     * @return \Illuminate\Http\JsonResponse    Json con mensaje de confirmación de la operación
     */
    public function destroy($id)
    {
        $bankAccount = BankAccount::find($id);
        $bankAccount->delete();
        return 0;
    }

    /**
     * Obtiene las cuentas bancarias del colegio
     *
     * @author  William Páez <paez.william8@gmail.com>
     * @return \Illuminate\Http\JsonResponse    JSON con los datos de los conceptos
     */
    public function listBankAccounts()
    {
        $data = [];
        $data[0] = [
            'id' => '',
            'text' => 'Seleccione...'
        ];

        $school = School::where('id', auth()->user()->family->school_id)
            ->with(['bankAccounts' => function ($query) {
                $query->with('bank');
            }])->first();
        foreach ($school->bankAccounts as $bankAccount) {
            $data[] = [
                'id' => $bankAccount->id,
                'text' => $bankAccount->account_number.' - '.$bankAccount->bank->name,
            ];
        }
        return response()->json($data);
    }
}
