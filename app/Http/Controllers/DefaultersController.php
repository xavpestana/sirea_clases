<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use App\Mail\DefaultersEmail;
use App\Installment;
use App\Student;
use App\installmentPayment;
use Carbon\Carbon;


class DefaultersController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $USD=session('USD_CLASES');
        $COP=session('COP_CLASES');
        return view('payments.defaulters.index',compact('USD', 'COP'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
    public function send(Request $request)
    {   

        $students = $request->student_id;
        $students = Student::find($students);

        $school = auth()->user()->userSchool->school;
        $actual_season = $school->seasons->where('status')->first();

        $today = Carbon::now();


        $school = array(
                    'dea_code'=> $school->dea_code, 
                    'stadistic_code'=> $school->stadistic_code,
                    'school_type'=> $school->school_type,
                    'name'=> $school->name,
                    'phone'=> $school->phone,
                    'fax'=> $school->fax,
                    'email'=> $school->email,
                    'address'=> $school->address,
                    'parroquia'=> $school->parish->name,
                    'municipio'=> $school->parish->municipality->name,
                    'logo'=> $school->logo
                );


        foreach ($students as $student) {
            $recipient = $student->family->user->email;

            $plan_asign = $student->studentPlan->where('season_id', $actual_season->id)->first();
                $installments = $plan_asign->plan->Installments
                                ->where('expire_at', '<', $today->format('Y-m-d'))
                                ->where('expire_at','<>',null);
                $defaulters = $installments;

                foreach($installments as $installment){
                    $installmentPayment = installmentPayment::where('student_id', $student->id)
                                                            ->where('installment_id', $installment->id)
                                                            ->first();

                    if(!is_null($installmentPayment)){
                       $installmentPayment->pluck('installment_id');
                        $installmentPayment = $installmentPayment->toArray();
                        $defaulters = $defaulters->except($installmentPayment);
                    }
                }
            Mail::to($recipient)->send(new DefaultersEmail($defaulters, $school, $student));
        }
    }
    public function list()
    {
        $school = auth()->user()->userSchool->school;

        $students = $school->students;
        $actual_season = $school->seasons->where('status')->first();
        $today = Carbon::now();
        return view('payments.defaulters.students', compact('students', 'actual_season','today'));
    }
}
