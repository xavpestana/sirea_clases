<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Carbon\Carbon;
use App\Installment;
use App\Payment;
use App\Plan;

class ReportsController extends Controller
{

	/**
     * Vista principal
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $today = Carbon::now();
        return view('reports.index', compact('today'));
    }
    /**
     * Reporte diario
     *
     * @return \Illuminate\Http\Response
     */
    public function table(Request $request)
    {
        date_default_timezone_set('America/Caracas');
    	$date = now();
        $hoy = now()->format('Y-m-d');
    	$school = auth()->user()->userSchool->school;
    	$actual_season = $school->seasons->where('status')->first();


        $reports = Payment::whereDate('created_at','>=',$request->desde)
                                ->whereDate('created_at','<=',$request->hasta)
                                ->where('school_id', auth()->user()->userSchool->school_id)
                                ->where('annulled', false)
                                ->get();
        return view('reports.tables',compact('reports', 'actual_season', 'hoy'));
    }
    public function nullable()
    {
        $date = now();
        $hoy = now()->format('Y-m-d');
        $school = auth()->user()->userSchool->school;
        $actual_season = $school->seasons->where('status')->first();

        
            $reports = Payment::where('school_id', auth()->user()->userSchool->school_id)
                                ->where('annulled',true)
                                ->get();

        return view('reports.nullable',compact('reports', 'actual_season', 'hoy'));
    }
    /**
     * Vista principal
     *
     * @return \Illuminate\Http\Response
     */
    public function report_paid()
    {
        $season = auth()->user()->userSchool->school->seasons->where('status','1')->first();
        $plans = Plan::where('season_id', $season->id)->get();

        return view('reports.payments', compact('plans'));
    }
    /**
     * Vista principal
     *
     * @return \Illuminate\Http\Response
     */
    public function conteo($installment)
    {
        $installments = Installment::find($installment);

        return view('reports.modal', compact('installments'));
    }
}
