<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Payment;
use App\Receipt;
use App\Family;
use App\School;
use App\InvoiceSetting;

class FamilyPaymentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('family.payments.index');
    }

    /**
     * Muestra el formulario de registro de pagos de las familias
     *
     * @author William Páez <paez.william8@gmail.com>
     * @return \Illuminate\View\View    Vista con el formulario
     */
    public function create()
    {
        return view('family.payments.create-edit');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'receipt_id' => ['required'],
            'payment_method_id' => ['required'],
            'bank_account_id' => ['required'],
            'family_bank_account_id' => ['required'],
            'transfer_code' => ['required', 'numeric', 'unique:payments,transfer_code']
        ]);

        $receipt = Receipt::find($request->receipt_id);
        $receipt->paid = true;
        $receipt->save();
        $school = School::where('id', auth()->user()->family->school_id)
            ->with(['bankAccounts' => function ($query) {
                $query->with('bank');
            }])->first();
        $bankAccount = $school->bankAccounts()->where('id', $request->bank_account_id)->with('bank')->first();

        $family = Family::where('school_id', auth()->user()->family->school_id)
            ->with('bankAccounts')->first();
        $familyBankAccount = $family->bankAccounts()->where(
            'id',
            $request->family_bank_account_id
        )->with('bank')->first();

        $invoiceSetting = InvoiceSetting::where('school_id', auth()->user()->family->school_id)->first();

        $payment = Payment::create([
            'receipt_id' => $request->receipt_id,
            'name' => $receipt->name,
            'amount' => $receipt->amount,
            'payment_method_id' => $request->payment_method_id,
            'school_bank_account' => $bankAccount->account_number.' - '.$bankAccount->bank->name,
            'family_bank_account' => $familyBankAccount->account_number.' - '.$familyBankAccount->bank->name,
            'transfer_code' => $request->transfer_code,
            'invoice_number' => $invoiceSetting->control_number,
        ]);

        $invoiceSetting->control_number++;
        $invoiceSetting->save();

        $request->session()->flash('message', ['type' => 'store']);
        return response()->json([
            'result' => true, 'redirect' => route('family-payments.index')
        ], 200);
    }

    /**
     * Muestra los datos del pago realizado por un familiar
     *
     * @author  William Páez <paez.william8@gmail.com>
     * @param  integer $id                          Identificador del dato a mostrar
     * @return \Illuminate\Http\JsonResponse        Json con el dato del pago
     */
    public function show($id)
    {
        $payment = Payment::where('id', $id)->with([
            'receipt' => function ($query) {
                $query->with(['enrollment' => function ($query) {
                    $query->with([
                        'student' => function ($query) {
                            $query->with(['profile','family' => function ($query) {
                                $query->where('id', auth()->user()->family->id);
                            }]);
                        },
                        'section' => function ($query) {
                            $query->with('season');
                        },
                    ]);
                }]);
            }, 'paymentMethod',
        ])->first();
        return response()->json(['record' => $payment], 200);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    /**
     * Muestra el pago de un familiar
     *
     * @author  William Páez <paez.william8@gmail.com>
     * @return \Illuminate\Http\JsonResponse    Json con los datos del pago de un familair
     */
    public function vueList()
    {
        $payments = Payment::with([
            'receipt' => function ($query) {
                $query->with(['enrollment' => function ($query) {
                    $query->with([
                        'student' => function ($query) {
                            $query->with(['family' => function ($query) {
                                $query->where('id', auth()->user()->family->id);
                            }]);
                        },
                        'section' => function ($query) {
                            $query->with('season');
                        },
                    ]);
                }]);
            },
        ])->get();
        $records = [];
        foreach ($payments as $payment) {
            if ($payment->receipt->enrollment->student->family) {
                $records[] = $payment;
            }
        }
        return response()->json(['records' => $records], 200);
    }
}
