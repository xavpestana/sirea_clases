<?php
namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Providers\RouteServiceProvider;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Auth;
use Redirect;

use Illuminate\Http\Request;
use App\School;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = RouteServiceProvider::HOME;

    /*protected function redirectTo()*/
    /*protected function redirectPath()
    {
        $user = auth()->user();
        if ($user->hasRole('administator')) return 'administrador/';
        if ($user->hasRole('family')) return 'familia/';
        if ($user->hasRole('student')) return 'estudiante/';
        if ($user->hasRole('teacher')) return 'docente/';
    }*/


    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    /*public function showLoginForm(Request $request)
    {
        if($request->has('colegio')){
            $school = School::find($request->colegio);
            return view('auth.login', compact('school'));
        }else{
            return view('auth.login');
        }
    }

    public function logout(){
        auth()->logout();
        Session()->flush();
        return Redirect::to('login');
    }*/
}
