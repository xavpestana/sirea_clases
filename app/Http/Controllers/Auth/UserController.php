<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Foundation\Validation\ValidatesRequests;
use App\User;
use App\UserSchool;
use Spatie\Permission\Models\Role;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Mail;
use App\Mail\UserRegister;

/**
 * @class UserController
 * @brief Controlador de usuarios
 *
 * Clase que gestiona los usuarios
 *
 * @author William Páez <paez.william8@gmail.com>
 * @license <a href='​http://www.gnu.org/licenses/gpl-2.0.html'>GNU Public License versión 2 (GPLv2)</a>
 */
class UserController extends Controller
{
    use ValidatesRequests;

    /**
     * Define la configuración de la clase
     *
     * @author William Páez <paez.william8@gmail.com>
     */
    public function __construct()
    {
        /** Establece permisos de acceso para cada método del controlador */
        $this->middleware('permission:users.list', ['only' => ['index', 'vueList']]);
        $this->middleware('permission:users.create', ['only' => ['create', 'store']]);
        $this->middleware('permission:users.show', ['only' => 'show']);
        $this->middleware('permission:users.edit', ['only' => ['edit', 'update']]);
        $this->middleware('permission:users.delete', ['only' => 'destroy']);
    }

    /**
     * Muestra todos los registros de usuarios
     *
     * @author William Páez <paez.william8@gmail.com>
     * @return \Illuminate\View\View    Muestra los datos organizados en una tabla
     */
    public function index()
    {
        return view('auth.index');
    }

    /**
     * Muestra el formulario de registro de usuarios
     *
     * @author William Páez <paez.william8@gmail.com>
     * @return \Illuminate\View\View    Vista con el formulario
     */
    public function create()
    {
        return view('auth.create-edit');
    }

    /**
     * Valida y registra nuevo usuario
     *
     * @author  William Páez <paez.william8@gmail.com>
     * @param  \Illuminate\Http\Request $request    Solicitud con los datos a guardar
     * @return \Illuminate\Http\JsonResponse        Json: result en verdadero y redirect con la url a donde ir
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => ['required'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
            'roles' => ['array'],
        ]);
        if ($request->user_type_id == 1) {
            $this->validate($request, [
                'state_id' => ['required'],
                'municipality_id' => ['required'],
                'parish_id' => ['required'],
                'school_id' => ['required'],
            ]);

            if (UserSchool::whereNull('parent_id')->where('school_id', $request->school_id)->exists()) {
                return response()->json([
                    'errors' => ['school_id' => ['Solo puede haber un usuario escolar admin por colegio']]
                ], 422);
            }
        }

        //$password = 'usuario12345';
        $password = Str::random();
        $user = User::create([
            'name' => $request->name,
            'email' => $request->email,
            'password' => bcrypt($password)
        ]);

        if (isset($request->roles)) {
            $user->assignRole($request->roles);
        }
        if ($request->user_type_id == 1) {
            $userSchool = UserSchool::create([
                'user_id' => $user->id,
                'school_id' => $request->school_id,
            ]);
        }

        Mail::to($user)->send(new UserRegister($user, $password));
        $request->session()->flash('message', ['type' => 'store']);
        return response()->json([
            'result' => true, 'redirect' => route('users.index')
        ], 200);
    }

    /**
     * Muestra los datos de un usuario
     *
     * @author  William Páez <paez.william8@gmail.com>
     * @param  integer $id                          Identificador del dato a mostrar
     * @return \Illuminate\Http\JsonResponse        Json con el dato de usuario
     */
    public function show($id)
    {
        $user = User::where('id', $id)->with(['roles'])->first();
        return response()->json(['record' => $user], 200);
    }

    /**
     * Muestra el formulario de actualización de usuario
     *
     * @author William Páez <paez.william8@gmail.com>
     * @param  integer $id              Identificador con el dato a actualizar
     * @return \Illuminate\View\View    Vista con el formulario y el objeto con el dato a actualizar
     */
    public function edit($id)
    {
        $user = User::find($id);
        return view('auth.create-edit', compact('user'));
    }

    /**
     * Actualiza la información del usuario
     *
     * @author  William Páez <paez.william8@gmail.com>
     * @param  \Illuminate\Http\Request  $request   Solicitud con los datos a actualizar
     * @param  integer $id                          Identificador del dato a actualizar
     * @return \Illuminate\Http\JsonResponse        Json con la redirección y mensaje de confirmación de la operación
     */
    public function update(Request $request, $id)
    {
        $user = User::find($id);
        foreach ($user->getRoleNames() as $role) {
            $user->removeRole($role);
        }
        foreach ($request->roles as $role) {
            $user->assignRole($role['name']);
        }
        /*if (isset($request->roles)) {
            $user->syncRoles($request->roles);
        }*/

        $request->session()->flash('message', ['type' => 'update']);
        return response()->json([
            'result' => true, 'redirect' => route('users.index')
        ], 200);
    }

    /**
     * Elimina la información del usuario
     *
     * @author  William Páez <paez.william8@gmail.com>
     * @param  integer $id                      Identificador del dato a eliminar
     * @return \Illuminate\Http\JsonResponse    Json con mensaje de confirmación de la operación
     */
    public function destroy($id)
    {
        $user = User::find($id);
        $user->delete();
        return response()->json(['record' => $user, 'message' => 'Success'], 200);
    }

    /**
     * Muestra los usuarios
     *
     * @author  William Páez <paez.william8@gmail.com>
     * @return \Illuminate\Http\JsonResponse    Json con los datos de los usuarios
     */
    public function vueList()
    {
        return response()->json(['records' => User::with(['roles'])->get()], 200);
    }

    /**
     * Obtiene las roles sin usar template_choices
     *
     * @author  William Páez <paez.william8@gmail.com>
     * @return \Illuminate\Http\JsonResponse    Json con los datos de los roles
     */
    public function getJsonRoles()
    {
        return response()->json(['jsonRoles' => Role::all()], 200);
    }
    /**
     * Obtiene los roles
     *
     * @author  William Páez <paez.william8@gmail.com>
     * @return \Illuminate\Http\JsonResponse    JSON con los datos de los roles
     */
    public function getRoles()
    {
        $temp = template_choices(Role::class, 'name', [], true);
        array_shift($temp);
        return response()->json($temp);
    }
}
