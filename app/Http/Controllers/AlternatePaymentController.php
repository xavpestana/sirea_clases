<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use App\Mail\AlternateReceived;
use App\alternatePayment;

class AlternatePaymentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        $USD=session('USD_CLASES');
        $COP=session('COP_CLASES');

        return view('payments.alternate',compact('USD', 'COP'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
          date_default_timezone_set('America/Caracas');
       
       $number_invoices = alternatePayment::where('school_id',auth()->user()->userSchool->school_id)->max('number_invoice');

        if (!is_null($number_invoices)) {
            $number = $number_invoices+1;
        }else{
            //para primer pago
            $number = 1;
        }

        /*Tipo de cambio*/
        $change_amount_usd = session('USD_CLASES'); 
        $change_amount_cop = session('COP_CLASES'); 

        /*Guardando el pago*/
        $payment = new alternatePayment;
        $payment->school_id = auth()->user()->userSchool->school_id;
        $payment->email = $request->email;
        $payment->number_invoice = $number;
        $payment->name = $request->name;
        $payment->amount = $request->quantity;
        $payment->coin = $request->coin;
        $payment->concept = $request->concept;
        $payment->change_amount_USD = $change_amount_usd;
        $payment->change_amount_COP = $change_amount_cop;
        $payment->save();
        
        return $payment->id;
    }
    public function list()
    {
        $payments = alternatePayment::where('school_id',auth()->user()->userSchool->school_id)->get();

        return view('payments.list_alternate',compact('payments'));
    }
    public function send(Request $request)
    {

        $payment = alternatePayment::find($request->invoice);
       
        $invoice = array(
                    'number_invoice'=> $payment->number_invoice, 
                    'email'=> $payment->email,
                    'amount'=> $payment->amount,
                    'name'=> $payment->name,
                    'coin'=> $payment->coin,
                    'concept'=> $payment->concept,
                    'change_amount_USD'=> $payment->change_amount_USD,
                    'change_amount_COP'=> $payment->change_amount_COP,
                );

        Mail::to($payment->email)->send(new AlternateReceived($invoice));
         
        return 0;
    }
    /**
     * Display the specified resource.
     *
     * @param  \App\alternatePayment  $alternatePayment
     * @return \Illuminate\Http\Response
     */
    public function show(alternatePayment $alternatePayment)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\alternatePayment  $alternatePayment
     * @return \Illuminate\Http\Response
     */
    public function edit(alternatePayment $alternatePayment)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\alternatePayment  $alternatePayment
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, alternatePayment $alternatePayment)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\alternatePayment  $alternatePayment
     * @return \Illuminate\Http\Response
     */
    public function destroy($alternatePayment)
    {

        $payments = alternatePayment::find($alternatePayment)->delete();
        return 0;
    }
}
