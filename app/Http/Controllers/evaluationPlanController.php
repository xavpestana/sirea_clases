<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\evaluationPlan;
use App\Season;
use App\Period;

class evaluationPlanController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $course_id = $request->course_id;
        $season = Season::where('status','1')
                        ->where('school_id',auth()->user()->profile->teacher->school_id)
                        ->first();
        $period = Period::where('season_id',$season->id)
                        ->where('status',1)
                        ->first();
       $students = DB::table('users')
            ->join('profiles', 'users.id', '=', 'profiles.user_id')
            ->join('students', 'profiles.id', '=', 'students.profile_id')
            ->join('enrollments', 'students.id', '=', 'enrollments.student_id')
            ->select('users.*','profiles.*')
            ->where('enrollments.section_id',$request->section_id)
            ->orderBy('profiles.last_name_1', 'asc')
            ->get();

            $plan = evaluationPlan::where('period_id',$period->id)
                                    ->where('section_id', $request->section_id)
                                    ->get();
                                    
        return view('evaluations.primary.index', compact('students','plan','course_id'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
            $plan = new evaluationPlan;
            $plan->period_id = $request->period;
            $plan->section_id = $request->section;
            $plan->percents = $request->percents;
            $plan->description = $request->descriptions;
            $plan->fecha = $request->dates;
            $plan->save();

            if ($plan) {
                return 0;
            } else {
                return 1;
            }
            
        
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {

        $season = Season::where('status','1')
                        ->where('school_id',auth()->user()->profile->teacher->school_id)
                        ->first();
        $period = Period::where('season_id',$season->id)
                        ->where('status',1)
                        ->first();
        $plan = evaluationPlan::where('period_id',$period->id)
                                    ->where('section_id', $id)
                                    ->get();
        return view('evaluations.plan', compact('plan'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        
        $plan = evaluationPlan::where('id', $id)->delete();
        if ($plan) {
                return 0;
            } else {
                return 1;
            }
            
    }
}
