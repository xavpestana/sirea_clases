<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Arr;
use App\Representative;
use App\School;
use App\Student;
use App\Family;
use App\User;

class ApiController extends Controller
{
	// retorna las familias de un colegio
    public function families($school)
    {
    	$families = Family::where('school_id',$school)->get();
    		
    $result =[];
    	foreach ($families as $family) {
    		$representatives = $family->representatives->where('principal',1)->first();
    		
    		if (is_null($representatives)) {
    				$familia = array(
    			"id_family"=> $family->id,
    			"last_name_1"=> $family->last_name_1,
    			"last_name_2"=> $family->last_name_2,
    			"id_repre"=> null,
    			"document_type"=> null,
    			"document_number"=> null,
    			"first_name_1_r"=> null,
    			"first_name_2_r"=> null,
    			"last_name_1_r"=> null,
    			"last_name_2_r"=> null,
    			"phone"=> $family->phone,
    			"address"=> $family->address,
    			"email"=> $family->user->email
            );
    			} else {
    				$familia = array(
    			"id_family"=> $family->id,
    			"last_name_1"=> $family->last_name_1,
    			"last_name_2"=> $family->last_name_2,
    			"id_repre"=> $family->representatives->where('principal',1)->first()->id,
    			"document_type"=> $family->representatives->where('principal',1)->first()->document_type,
    			"document_number"=> $family->representatives->where('principal',1)->first()->document_number,
    			"first_name_1_r"=> $family->representatives->where('principal',1)->first()->first_name_1,
    			"first_name_2_r"=> $family->representatives->where('principal',1)->first()->first_name_2,
    			"last_name_1_r"=> $family->representatives->where('principal',1)->first()->last_name_1,
    			"last_name_2_r"=> $family->representatives->where('principal',1)->first()->last_name_2,
    			"phone"=> $family->phone,
    			"address"=> $family->address,
    			"email"=> $family->user->email
            );
    			}
	    array_push($result,$familia);
    	}
    	return json_encode($result, JSON_UNESCAPED_UNICODE);
    
    }
    //representantes de un colegio
    public function representatives($school)
    {

    	$families = Family::where('school_id',$school)->get();
    		
    $result =[];
    	foreach ($families as $family) {
    		$representatives = $family->representatives->where('principal',1)->first();
    		if (is_null($family->city)) {
    			$city = null;
    			$state = null;
    		} else {
    			$city = $family->city->name;
    			$state = $family->city->state->name;
    		}
    		
    		if (is_null($representatives)) {
    				$familia = array(
    			"id_family"=> $family->id,
    			"id_repre"=> null,
    			"document_type"=> null,
    			"document_number"=> null,
    			"first_name_1_r"=> null,
    			"first_name_2_r"=> null,
    			"last_name_1_r"=> null,
    			"last_name_2_r"=> null,
    			"phone"=> $family->phone,
    			"address"=> $family->address,
    			"email"=> $family->user->email,
    			"city"=> $city,
    			"state"=> $state
            );
    			} else {
    				$familia = array(
    			"id_family"=> $family->id,
    			"id_repre"=> $family->representatives->where('principal',1)->first()->id,
    			"document_type"=> $family->representatives->where('principal',1)->first()->document_type,
    			"document_number"=> $family->representatives->where('principal',1)->first()->document_number,
    			"first_name_1_r"=> $family->representatives->where('principal',1)->first()->first_name_1,
    			"first_name_2_r"=> $family->representatives->where('principal',1)->first()->first_name_2,
    			"last_name_1_r"=> $family->representatives->where('principal',1)->first()->last_name_1,
    			"last_name_2_r"=> $family->representatives->where('principal',1)->first()->last_name_2,
    			"phone"=> $family->phone,
    			"address"=> $family->address,
    			"email"=> $family->user->email,
    			"city"=> $city,
    			"state"=> $state
            );
    			}
	    array_push($result,$familia);
    	}
    	return json_encode($result, JSON_UNESCAPED_UNICODE);

    }
    public function students($school)
    {
            $school = School::find($school);
            $students = $school->students;
            $result =[];

            foreach ($students as $student) {
            	$representatives = $student->family->representatives->where('principal',1)->first();
            	
            	$enrollments = $student->enrollments->last();

            	if (is_null($enrollments)) {
            		$grade=null;
            		$grade_name=null;
            		$level=null;
            		$level_name=null;
            	} else {
            		$grade=$enrollments->section->grade->id;
            		$grade_name=$enrollments->section->grade->name;
            		$level=$enrollments->section->grade->level->id;
            		$level_name=$enrollments->section->grade->level->name;
            	}

            	if (is_null($student->family->city)) {
    			$city = null;
    			$state = null;
    			} else {
    			$city = $student->family->city->name;
    			$state = $student->family->city->state->name;
    			}
            	
            	if (is_null($representatives)) {
            		$estudiante = array(
            			"id_student"=> $student->id,
            			"id_family"=> $student->family_id,
            			"id_repre"=> null,
            			"document_type"=> $student->profile->document_type,
            			"document_number"=> $student->profile->document_number,
            			"first_name_1_s"=> $student->profile->first_name_1,
    					"first_name_2_s"=> $student->profile->first_name_2,
    					"last_name_1_s"=> $student->profile->last_name_1,
    					"last_name_2_s"=> $student->profile->last_name_2,
    					"birthdate"=> $student->profile->birthdate,
    					"gender_id"=> $student->profile->gender_id,
    					"address"=> $student->family->address,
    					"phone"=> $student->profile->phone,
    					"email"=> $student->profile->user->email,
    					"grade_id"=> $grade,
    					"grade"=> $grade_name,
    					"level_id"=> $level,
    					"level"=> $level_name,
    					"city"=> null,
    					"state"=> null

            	);
            	}else{
            		$estudiante = array(
            			"id_student"=> $student->id,
            			"id_family"=> $student->family_id,
            			"id_repre"=> $representatives->id,
            			"document_type"=> $student->profile->document_type,
            			"document_number"=> $student->profile->document_number,
            			"first_name_1_s"=> $student->profile->first_name_1,
    					"first_name_2_s"=> $student->profile->first_name_2,
    					"last_name_1_s"=> $student->profile->last_name_1,
    					"last_name_2_s"=> $student->profile->last_name_2,
    					"birthdate"=> $student->profile->birthdate,
    					"gender_id"=> $student->profile->gender_id,
    					"address"=> $student->family->address,
    					"phone"=> $student->profile->phone,
    					"email"=> $student->profile->user->email,
    					"grade_id"=> $grade,
    					"grade"=> $grade_name,
    					"level_id"=> $level,
    					"level"=> $level_name,
    					"city"=> $city,
    					"state"=> $state

            	);
            	}
            	array_push($result,$estudiante);
            }

            return json_encode($result, JSON_UNESCAPED_UNICODE);
    }
    public function sesion($school, $document)
    {

        $school = School::find($school);
        //$representatives = $school->representatives->first();
        $representatives = $school->representatives->where('document_number', $document)->first();
        if (is_null($representatives)) {
            return json_encode(array(
                'status' => 'error',
                'code' => '403',
                'response' => 'Usuario no encontrado'), JSON_UNESCAPED_UNICODE);
        } else {
            return json_encode(array(
                'status' => 'success',
                'code' => '200',
                'response' => 'Usuario encontrado'), JSON_UNESCAPED_UNICODE);
        }
    }
}
