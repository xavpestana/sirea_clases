<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Foundation\Validation\ValidatesRequests;
use App\Plan;
use App\Concept;
use App\Enrollment;
use App\Receipt;
use App\Season;

use App\Exports\EnrollmentsExport;
use Maatwebsite\Excel\Facades\Excel;


/**
 * @class EnrollmentController
 * @brief Controlador de inscripciones
 *
 * Clase que gestiona las inscripciones
 *
 * @author William Páez <paez.william8@gmail.com>
 * @license <a href='​http://www.gnu.org/licenses/gpl-2.0.html'>GNU Public License versión 2 (GPLv2)</a>
 */
class EnrollmentController extends Controller
{
    use ValidatesRequests;

    /**
     * Define la configuración de la clase
     *
     * @author William Páez <paez.william8@gmail.com>
     */
    public function __construct()
    {
        /** Establece permisos de acceso para cada método del controlador */
        $this->middleware('permission:enrollments.list', ['only' => ['index', 'vueList']]);
        $this->middleware('permission:enrollments.create', ['only' => ['create', 'store']]);
        $this->middleware('permission:enrollments.show', ['only' => 'show']);
        $this->middleware('permission:enrollments.edit', ['only' => ['edit', 'update']]);
        $this->middleware('permission:enrollments.delete', ['only' => 'destroy']);
    }

    /**
     * Muestra todos los registros de inscripciones
     *
     * @author William Páez <paez.william8@gmail.com>
     * @return \Illuminate\View\View    Muestra los datos organizados en una tabla
     */
    public function index()
    {

        $school = auth()->user()->userSchool->school;

        $actual_season = $school->seasons->where('status')->first();

        $season_id = $actual_season->id;

        $enrollments = $actual_season->enrollments;

        return view('school.enrollments', compact('enrollments', 'actual_season'));

        //return view('payments.enrollments.index');
    }

    public function noEnrollments()
    {
        $school = auth()->user()->userSchool->school;

        $students = $school->students;

        $actual_season = $school->seasons->where('status')->first();

        if($actual_season){
            $enrollments = $actual_season->enrollments->pluck('student_id');
            $enrollments = $enrollments->toArray();
            $no_enrollments = $students->except($enrollments);;
        }else{
            $no_enrollments = null;
            $actual_season = null;
        }
        return view('school.no-enrollments', compact('no_enrollments', 'actual_season'));
    }

    public function inscribir(Request $request)
    {
        if($request->section_id == null) return back()->with('error', 'Debe seleccionar un grupo');
        $enrollment = Enrollment::create([
            'student_id' => $request->student_id,
            'section_id' => $request->section_id
        ]);
        return back()->with('success', 'Estudiante inscrito exitosamente');
    }

    /**
     * Muestra el formulario de registro de inscripciones
     *
     * @author William Páez <paez.william8@gmail.com>
     * @return \Illuminate\View\View    Vista con el formulario
     */
    public function create()
    {
        return view('payments.enrollments.create-edit');
    }

    /**
     * Valida y registra nueva inscripción de estuduante
     *
     * @author  William Páez <paez.william8@gmail.com>
     * @param  \Illuminate\Http\Request $request    Solicitud con los datos a guardar
     * @return \Illuminate\Http\JsonResponse        Json: result en verdadero y redirect con la url a donde ir
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'student_id' => ['required'],
            'season_id' => ['required'],
            'level_id' => ['required'],
            'grade_id' => ['required'],
            'section_id' => ['required'],
            'plan_id' => ['required'],
            'concepts' => ['array', 'min:1'],
        ]);
        $band = Enrollment::where('student_id', $request->student_id)->whereHas('section', function ($query) {
            $query->whereHas('season', function ($query) {
                $season = Season::where('status', 1)->where('school_id',auth()->user()->userSchool->school_id)->first();
                $query->where('id', $season->id);
            });
        })->get();
        if (!$band->isEmpty()) {
            return response()->json([
                'errors' => ['student_id' => ['El estudiante ya se encuentra inscrito en el año escolar activo']]
            ], 422);
        }

        $enrollment = Enrollment::create([
            'student_id' => $request->student_id,
            'section_id' => $request->section_id,
            'plan_id' => $request->plan_id,
        ]);

        $sum = 0;
        foreach ($request->concepts as $concept) {
            $conc = Concept::find($concept['id']);
            $enrollment->concepts()->attach($conc);
            $sum += $conc->amount;
        }
        Receipt::create([
            'name' => 'Total a pagar en conceptos de inscripción',
            'amount' => $sum,
            'enrollment_id' => $enrollment->id,
        ]);
        $plan = Plan::where('id', $enrollment->plan_id)->with('installments')->first();
        foreach ($plan->installments as $installment) {
            Receipt::create([
                'name' => $installment->description,
                'amount' => $installment->amount,
                'enrollment_id' => $enrollment->id,
            ]);
        }
        $request->session()->flash('message', ['type' => 'store']);
        return response()->json([
            'result' => true, 'redirect' => route('enrollments.index')
        ], 200);
    }

    /**
     * Muestra los datos de una inscripción
     *
     * @author  William Páez <paez.william8@gmail.com>
     * @param  integer $id                          Identificador del dato a mostrar
     * @return \Illuminate\Http\JsonResponse        Json con el dato de la inscripción
     */
    public function show($id)
    {
        $enrollment = Enrollment::where('id', $id)->with([
            'student' => function ($query) {
                $query->with(['profile', 'family' => function ($query) {
                    $query->where('school_id', auth()->user()->userSchool->school_id);
                }]);
            },
            'section' => function ($query) {
                $query->with(['season','grade' => function ($query) {
                    $query->with('level');
                }]);
            },
            'plan' => function ($query) {
                $query->with('installments');
            }, 'concepts',
        ])->first();
        //$enrollment['section_id'] = $enrollment->section->id;
        //$enrollment['season_id'] = $enrollment->section->season->id;
        /*if (!$enrollment->student->family) {
            abort(403, 'El usuario no tiene los permisos para entrar a esta página.');
        }*/
        return response()->json(['record' => $enrollment], 200);
    }

    /**
     * Muestra el formulario de actualización de inscripción
     *
     * @author William Páez <paez.william8@gmail.com>
     * @param  integer $id              Identificador con el dato a actualizar
     * @return \Illuminate\View\View    Vista con el formulario y el objeto con el dato a actualizar
     */
    public function edit($id)
    {
        $enrollment = Enrollment::where('id', $id)->with([
            'student' => function ($query) {
                $query->with(['family' => function ($query) {
                    $query->where('school_id', auth()->user()->userSchool->school_id);
                }]);
            },
        ])->first();
        if (!$enrollment->student->family) {
            abort(403, 'El usuario no tiene los permisos para entrar a esta página.');
        }
        return view('payments.enrollments.create-edit', compact('enrollment'));
    }

    /**
     * Actualiza la información de la inscripción
     *
     * @author  William Páez <paez.william8@gmail.com>
     * @param  \Illuminate\Http\Request  $request   Solicitud con los datos a actualizar
     * @param  integer $id                          Identificador del dato a actualizar
     * @return \Illuminate\Http\JsonResponse        Json con la redirección y mensaje de confirmación de la operación
     */
    public function update(Request $request, $id)
    {
        $enrollment = Enrollment::where('id', $id)->with([
            'student' => function ($query) {
                $query->with(['family' => function ($query) {
                    $query->where('school_id', auth()->user()->userSchool->school_id);
                }]);
            },
        ])->first();
        if (!$enrollment->student->family) {
            abort(403, 'El usuario no tiene los permisos para entrar a esta página.');
        }

        $this->validate($request, [
            'student_id' => ['required'],
            'season_id' => ['required'],
            'level_id' => ['required'],
            'grade_id' => ['required'],
            'section_id' => ['required'],
            'plan_id' => ['required'],
            'concepts' => ['array', 'min:1'],
        ]);
        $band = Enrollment::where('student_id', $request->student_id)->whereHas('section', function ($query) {
            $query->whereHas('season', function ($query) {
                $season = Season::where('status', 1)->first();
                $query->where('id', $season->id);
            });
        })->get();
        if ($band and $request->student_id != $enrollment->student_id) {
            return response()->json([
                'errors' => ['student_id' => ['El estudiante ya se encuentra inscrito en el año escolar activo']]
            ], 422);
        }

        $enrollment->student_id = $request->student_id;
        $enrollment->section_id = $request->section_id;
        $enrollment->plan_id = $request->plan_id;
        $enrollment->save();

        foreach ($enrollment->concepts as $concept) {
            $conc = Concept::find($concept['id']);
            $enrollment->concepts()->detach($conc);
        }
        $enrollment->receipts()->delete();
        $sum = 0;
        foreach ($request->concepts as $concept) {
            $conc = Concept::find($concept['id']);
            $enrollment->concepts()->attach($conc);
            $sum += $conc->amount;
        }
        Receipt::create([
            'name' => 'Total a pagar en conceptos de inscripción',
            'amount' => $sum,
            'enrollment_id' => $enrollment->id,
        ]);
        $plan = Plan::where('id', $enrollment->plan_id)->with('installments')->first();
        foreach ($plan->installments as $installment) {
            Receipt::create([
                'name' => $installment->description,
                'amount' => $installment->amount,
                'enrollment_id' => $enrollment->id,
            ]);
        }
        $request->session()->flash('message', ['type' => 'update']);
        return response()->json([
            'result' => true, 'redirect' => route('enrollments.index')
        ], 200);
    }

    /**
     * Elimina la información de la inscripción
     *
     * @author  William Páez <paez.william8@gmail.com>
     * @param  integer $id                      Identificador del dato a eliminar
     * @return \Illuminate\Http\JsonResponse    Json con mensaje de confirmación de la operación
     */
    public function destroy($id)
    {
        $enrollment = Enrollment::find($id);
        $enrollment->delete();

        return response()->json(['message' => 'Success'], 200);

/*        $enrollment = Enrollment::where('id', $id)->with([
            'student' => function ($query) {
                $query->with(['family' => function ($query) {
                    $query->where('school_id', auth()->user()->userSchool->school_id);
                }]);
            },
        ])->first();
        if (!$enrollment->student->family) {
            abort(403, 'El usuario no tiene los permisos para entrar a esta página.');
        }
        $enrollment->delete();
        return response()->json(['record' => $enrollment, 'message' => 'Success'], 200);*/
    }

    /**
     * Muestra las inscripciones de un colegio en específico
     *
     * @author  William Páez <paez.william8@gmail.com>
     * @return \Illuminate\Http\JsonResponse    Json con los datos de las inscripciones
     */
    public function vueList()
    {
        $enrollments = Enrollment::with([
            'student' => function ($query) {
                $query->with(['profile', 'family' => function ($query) {
                    $query->where('school_id', auth()->user()->userSchool->school_id);
                }]);
            },
            'section' => function ($query) {
                $query->with(['season','grade' => function ($query) {
                    $query->with('level');
                }]);
            },
        ])->get();
        $records = [];
        foreach ($enrollments as $enrollment) {
            if ($enrollment->student->family) {
                $records[] = $enrollment;
            }
        }
        return response()->json(['records' => $records], 200);
    }

    /**
     * Obtiene las inscripciones
     *
     * @author  William Páez <paez.william8@gmail.com>
     * @return \Illuminate\Http\JsonResponse    JSON con los datos de inscripciones
     */
    public function listEnrollments()
    {
        $data = [];
        $data[0] = [
            'id' => '',
            'text' => __('Seleccione...')
        ];
        $enrollments = Enrollment::with([
            'student' => function ($query) {
                $query->with(['profile', 'family' => function ($query) {
                    $query->where('school_id', auth()->user()->userSchool->school_id);
                }]);
            },
            'section' => function ($query) {
                $query->with(['season','grade' => function ($query) {
                    $query->with('level');
                }]);
            },
            'plan' => function ($query) {
                $query->with('installments');
            }, 'concepts',
        ])->get();
        foreach ($enrollments as $enrollment) {
            $data[] = [
                'id' => $enrollment->id,
                'text' => $enrollment->student->profile->first_name_1.' '.$enrollment->student->profile->last_name_1,
                'plan' => $enrollment->plan,
                'concepts' => $enrollment->concepts,
            ];
        }
        return response()->json($data);
    }

    public function enrollmentsExcel()
    {
        $school = auth()->user()->userSchool->school;

        $actual_season = $school->seasons->where('status')->first();

        $enrollments = $actual_season->enrollments;

        //dd($enrollments);

       // return (new EnrollmentsExport($enrollments))->download('Listado_General_Inscritos_'.now()->format('d-m-Y').'.xlsx', \Maatwebsite\Excel\Excel::XLSX);
        return Excel::download(new EnrollmentsExport, 'Listado_General_Inscritos_'.now()->format('d-m-Y').'.xlsx');
    }
}
