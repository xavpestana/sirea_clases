<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Installment;

class InstallmentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $installments = Installment::where('plan_id', $request->plan_id)->get();
        return view('payments.installments.show_installments', compact('installments'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
                $installment = new Installment();
                $installment->description = $request->name;
                $installment->amount = $request->amount;
                $installment->plan_id = $request->plan_id;
                $installment->expire_at = $request->date;
                $installment->coin = $request->coin;
                $installment->save();
                
                return 0;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $installment = Installment::find($id);
       // dd($installment);
        return view('payments.installments.edit_installments', compact('installment'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {


        $installment = Installment::find($id);
        $installment->description = $request->name;
        $installment->amount = $request->amount;
        $installment->plan_id = $request->plan_id;
        $installment->expire_at = $request->date;
        $installment->coin = $request->coin;
        $installment->save();
        return 0;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $installment = Installment::find($id);
        $installment->delete();
        return 0;
    }

    /**
     * Obtiene las cuotas que pertenecen a un plan de pago
     *
     * @author  William Páez <paez.william8@gmail.com>
     * @return \Illuminate\Http\JsonResponse    JSON con los datos de las cuotas
     */
    public function listInstallments($plan_id)
    {
        $installments = template_choices(
            Installment::class,
            ['description','-','amount'],
            ['plan_id' => $plan_id],
            true
        );
        array_shift($installments);
        return response()->json($installments);
    }
}
