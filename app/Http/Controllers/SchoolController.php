<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Support\Facades\Validator;
use App\School;
use App\Director;
use App\InvoiceSetting;
use Redirect;
use Illuminate\Support\Facades\View;
use Illuminate\Support\Str;
use Image;
use PDF;

class SchoolController extends Controller
{
    use ValidatesRequests;

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $schools = School::all();
        return view('admin.schools', compact('schools'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.schools-edit');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $this->validator($request->all())->validate();

        if ($request->logo) {
            $logo = $this->saveLogo($request);
        } else {
            $logo = 'default.jpg';
        }

        if (!$request->has('additional_data')) {
            $request->request->add(['additional_data' => 0]);
        }

        $school = School::create([
            'logo'            => $logo,
            'dea_code'        => $request->dea_code,
            'stadistic_code'  => $request->stadistic_code,
            'rif'             => $request->rif,
            'school_type'     => $request->school_type,
            'name'            => $request->name,
            'parish_id'       => $request->parish_id,
            'address'         => $request->address,
            'email'           => $request->email,
            'phone'           => $request->phone,
            'fax'             => $request->fax,
            'url'             => $request->url,
            'city_id'         => $request->city_id,
            'additional_data' => $request->additional_data
        ]);

        $director = Director::create([
            'school_id' => $school->id,
            'name' => $request->director_name,
            'document_type' => $request->document_type,
            'document_number' => $request->document_number,
            'gender_id' => $request->gender_id
        ]);

        /**
         * Configura en 0 (cero) el valor del número de control de la factura
         */
        $invoiceSetting = InvoiceSetting::create([
            'school_id' => $school->id,
        ]);
        return $this->index()->with('success', 'Datos del colegio creados');
        //return view('admin.schools', compact('schools'))->with('success','Datos del colegio actualizados');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\School  $school
     * @return \Illuminate\Http\Response
     */
    public function show(School $school)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\School  $school
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
      if(auth()->user()->hasRole('school') && !auth()->user()->hasRole('admin')) {
        if(auth()->user()->userSchool->school_id != $id) return back();
      }
      $school = School::find($id);
      $director = $school->director;
      return view('admin.schools-edit', compact('school', 'director'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\School  $school
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        $this->validator($request->all())->validate();

        $school = School::find($id);

        if(!$request->has('additional_data')) {$request->request->add(['additional_data' => '0']);}

        if($request->has('logo')) {

            $this->deleteLogo($school->logo);

            $school->update($request->all());

            $school->logo = $this->saveLogo($request);

            $school->save();

        }else{
            $school->update($request->all());
        }

        $director = $school->director;

        if($director){
            $director->name = $request->director_name;
            $director->document_type = $request->document_type;
            $director->document_number = $request->document_number;
            $director->gender_id = $request->gender_id;
            $director->save();
        }else
        {
            $director = Director::create([
                'school_id' => $school->id,
                'name' => $request->director_name,
                'document_type' => $request->document_type,
                'document_number' => $request->document_number,
                'gender_id' => $request->gender_id
            ]);
        }

        if(auth()->user()->hasRole('school')){
            return redirect()->route('school.settings')->with('success','Datos del colegio actualizados');
        }else{
        return $this->index()->with('success','Datos del colegio actualizados');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\School  $school
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $school = school::find($id);

        $this->deleteLogo($school->logo);

        $school->delete();

        $schools = School::all();

        $view = View::make('admin.schools')->with('schools', $schools);

        if($request->ajax()){

            $sections = $view->renderSections();

            return Response::json($sections['content']);

        }else return back();
    }

    /**
     * Obtiene los colegios de una parroquia
     *
     * @author  William Páez <paez.william8@gmail.com>
     * @return \Illuminate\Http\JsonResponse    JSON con los datos de los colegios
     */
    public function listSchools($parish_id)
    {
        return response()->json(
            template_choices(School::class, 'name', ['parish_id' => $parish_id], true)
        );
    }


    protected function validator(array $data)
    {
        return Validator::make($data, [
            'school_type' => ['required', 'string', 'max:50'],
            'name'        => ['required', 'string', 'max:50'],
            'parish_id'   => ['required','integer'],
            'city_id'     => ['required','integer'],
            'address'     => ['required', 'string', 'max:500'],
            'phone'       => ['required', 'max:50'],
            'email'       => ['required','string', 'email', 'max:50'],
            'logo'        => ['image', 'mimes:jpeg,png,jpg'],
            'director_name' => ['required', 'string','max:100'],
            'document_type' => ['required'],
            'document_number' => ['required'],
            'gender_id' => ['required']
        ]);
    }

    public function saveLogo(Request $request)
    {
        $originalImage= $request->file('logo');
        $image = Image::make($originalImage);
        $originalPath = public_path().'/storage/pictures/logos/';
        //Nombre aleatorio para la image
        $tempName = Str::random(20) . '.' . $originalImage->getClientOriginalExtension();

        //Redimensinoar la imagen
        if($image->width() >= $image->height()) $image->heighten(400);
        else $image->widen(400);
        $image->resizeCanvas(400,400);

        $image->save($originalPath.$tempName);

        return $tempName;
    }

    public function deleteLogo($logo)
    {
        $originalPath = public_path().'/storage/pictures/logos/';

        if ($logo != 'default.jpg')
        {
          if(\File::exists($originalPath.$logo)){
            \File::delete($originalPath.$logo);
          }
        }
    }
}
