<?php

namespace App\Http\Controllers;

use App\Season;
use App\Period;
use App\Level;
use App\Grade;
use App\Section;
use App\Director;
use Illuminate\Http\Request;

use Illuminate\Support\Facades\Validator;
use Redirect, Response;

class SeasonController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $school_id = auth()->user()->userSchool()->school_id;
        $seasons = Season::where('school_id', $school_id);

        return view('school.seasons', compact('seasons'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $levels = Level::all();

        return view('school.settings-season', compact('levels'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $this->validator($request->all())->validate();

        $school_id = auth()->user()->userSchool->school_id;

        $previousSeasons = Season::where('school_id', $school_id)->get();

        foreach ($previousSeasons as $previousSeason) {
            $previousSeason->status = 0;
            $previousSeason->save();
        }

        $season = Season::create([
            'season' => $request->season,
            'school_id' => $school_id,
            'periods_qty' => $request->periods_qty,
            'max_score' => $request->max_score,
            'sections_number' => $request->sections_number,
            'status' => 1,
            'initial_pay' => $request->initial_pay
        ]);

        /*        $previousPeriods = Period::where('season_id','<>', $season->id)
        ->where('school_id', $school_id)
        ->get();

        foreach ($previousPeriods as $previousPeriod) {
            $previousPeriod->status = 0;
            $previousPeriod->save();
        }*/

        foreach ($previousPeriods as $previousPeriod) {
            $previousPeriod->status = 0;
            $previousPeriod->save();
        }

        for ($i=0; $i < $season->periods_qty; $i++) {
            echo $i;
            if($i == 0){ $status = 1;}else{$status = 0;}
            Period::create([
                'number' => $i + 1,
                'season_id' => $season->id,
                'status' => $status
            ]);
        }

        foreach ($request->grade as $gradeId => $sectionQty) {
            if($season->sections_number == 1) $name = 1;
            else $name = 'A';
            for ($i=0; $i < $sectionQty; $i++) {
                Section::create([
                    'season_id' => $season->id,
                    'grade_id' =>$gradeId,
                    'name' => $name
                ]);
                $name++;
            }
        }

        return redirect()->route('school.settings')->with('success','Periodo creado exitosamente');;
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Season  $season
     * @return \Illuminate\Http\Response
     */
    public function show(Season $season)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Season  $season
     * @return \Illuminate\Http\Response
     */
    public function edit(Season $season)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Season  $season
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $season = Season::find($id);
        $season->status = 0;
        $season->save();

        $school = auth()->user()->userSchool->school;

        $seasons = Season::where('school_id', $school->id)->get();

        $director = Director::where('school_id', $school->id)->first();

        $grades = Grade::all();

        $view =  view('school.settings', compact('school','seasons', 'director', 'grades'));

        if($request->ajax()){
            $sections = $view->renderSections();
            return Response::json($sections['content']);
        }else return back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Season  $season
     * @return \Illuminate\Http\Response
     */
    public function destroy(Season $season)
    {
        //
    }

    protected function validator(array $data)
    {
        return Validator::make($data, [
            'season' => ['required', 'string', 'max:50'],
            'periods_qty' => ['required', 'integer'],
            'max_score' => ['required', 'integer'],
            'sections_number' => ['required', 'integer'],
            'initial_pay' => ['required', 'integer']
        ]);
    }

    public function settings()
    {
        $school = auth()->user()->userSchool->school;

        $seasons = Season::where('school_id', $school->id)->get();

        $director = Director::where('school_id', $school->id)->first();

        $grades = Grade::all();

        return view('school.settings', compact('school','seasons', 'director', 'grades'));
    }

    /**
     * Obtiene los años escolares
     *
     * @author  William Páez <paez.william8@gmail.com>
     * @return \Illuminate\Http\JsonResponse    JSON con los datos de los años escolares
     */
    public function listSeasons()
    {
        return response()->json(
            template_choices(
                Season::class,
                'season',
                ['status' => 1, 'school_id' => auth()->user()->userSchool->school_id],
                true
            )
        );
    }
}
