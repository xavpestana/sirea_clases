<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use App\Courses_sections_user;
use App\fileResponse;
use App\responseActivity;
use App\teacherNotify;
use App\Enrollment;
use App\Season;
use App\Course;
use App\Task;

class StudentActivitiesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {

        $course = Course::find($id);

        $season = Season::where('status','1')
                        ->where('school_id',auth()->user()->profile->student->family->school_id)
                        ->first();

        $notifies = teacherNotify::where('course_id',$id)
                        ->where('user_id','!=',auth()->user()->id)
                        ->where('method', 'comment')
                        ->orderBy('created_at', 'desc')
                        ->limit(20)
                        ->get();

        return view('student.activities',compact('course', 'season', 'id', 'notifies'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {

        $response = responseActivity::find($request->id);
        unlink(public_path($response->fileResponse->url));
        
        $file_response = fileResponse::where('id', $response->fileResponse->id)->delete();
        $response_activity = responseActivity::where('id', $request->id)->delete();
        
       
        return 0;
    }

    /**
     * Actividades del estudiante.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function activities(Request $request)
    {
            $section = Enrollment::where('student_id', $request->id)->first();
            $courses = Courses_sections_user::where('section_id', $section->section_id)
                        ->groupBy('course_id')
                        ->get();
                
        return view('student.sidebar',compact('courses'));
    }

    /**
     * Publicacions de sus materias
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function published(Request $request)
    {
            $section = Enrollment::where('student_id', $request->id)->first();
            $courses = Courses_sections_user::where('section_id', $section->section_id)
                        ->groupBy('course_id')
                        ->get();
                
        return view('student.publish',compact('courses'));
    }
    /**
     * ver modal de respuesta
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function response(Request $request)
    {
                 $task = Task::find($request->id);
                 
        return view('student.response',compact('task'));
    }
}
