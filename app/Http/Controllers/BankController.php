<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Foundation\Validation\ValidatesRequests;
use App\Bank;

/**
 * @class BankController
 * @brief Controlador de bancos
 *
 * Clase que gestiona los bancos
 *
 * @author William Páez <paez.william8@gmail.com>
 * @license <a href='​http://www.gnu.org/licenses/gpl-2.0.html'>GNU Public License versión 2 (GPLv2)</a>
 */
class BankController extends Controller
{
    use ValidatesRequests;

    /**
     * Define la configuración de la clase
     *
     * @author William Páez <paez.william8@gmail.com>
     */
    public function __construct()
    {
        /** Establece permisos de acceso para cada método del controlador */
        $this->middleware('permission:banks.list', ['only' => 'vueList']);
        $this->middleware('permission:banks.create', ['only' => ['create', 'store']]);
        $this->middleware('permission:banks.show', ['only' => 'show']);
        $this->middleware('permission:banks.edit', ['only' => ['edit', 'update']]);
        $this->middleware('permission:banks.delete', ['only' => 'destroy']);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Muestra el formulario de registro de bancos
     *
     * @author William Páez <paez.william8@gmail.com>
     * @return \Illuminate\View\View    Vista con el formulario
     */
    public function create()
    {
        return view('payments.banks.create-edit');
    }

    /**
     * Valida y registra nuevo banco
     *
     * @author  William Páez <paez.william8@gmail.com>
     * @param  \Illuminate\Http\Request $request    Solicitud con los datos a guardar
     * @return \Illuminate\Http\JsonResponse        Json: result en verdadero y redirect con la url a donde ir
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'code' => ['required', 'unique:banks,code'],
            'name' => ['required', 'unique:banks,name'],
            'short_name' => ['required']
        ]);

        $bank = Bank::create([
            'code' => $request->code,
            'name' => $request->name,
            'short_name' => $request->short_name
        ]);

        $request->session()->flash('message', ['type' => 'store']);
        return response()->json([
            'result' => true, 'redirect' => route('payment-settings.index')
        ], 200);
    }

    /**
     * Muestra los datos de un banco
     *
     * @author  William Páez <paez.william8@gmail.com>
     * @param  integer $id                          Identificador del dato a mostrar
     * @return \Illuminate\Http\JsonResponse        Json con el dato del banco
     */
    public function show($id)
    {
        $bank = Bank::where('id', $id)->first();
        return response()->json(['record' => $bank], 200);
    }

    /**
     * Muestra el formulario de actualización del banco
     *
     * @author William Páez <paez.william8@gmail.com>
     * @param  integer $id              Identificador con el dato a actualizar
     * @return \Illuminate\View\View    Vista con el formulario y el objeto con el dato a actualizar
     */
    public function edit($id)
    {
        $bank = Bank::find($id);
        return view('payments.banks.create-edit', compact('bank'));
    }

    /**
     * Actualiza la información del banco
     *
     * @author  William Páez <paez.william8@gmail.com>
     * @param  \Illuminate\Http\Request  $request   Solicitud con los datos a actualizar
     * @param  integer $id                          Identificador del dato a actualizar
     * @return \Illuminate\Http\JsonResponse        Json con la redirección y mensaje de confirmación de la operación
     */
    public function update(Request $request, $id)
    {
        $bank = bank::find($id);
        $this->validate($request, [
            'code' => ['required', 'unique:banks,code'],
            'name' => ['required', 'unique:banks,name'],
            'amount' => ['required', 'numeric']
        ]);
        $bank->code = $request->code;
        $bank->name = $request->name;
        $bank->amount = $request->amount;
        $bank->save();
        $request->session()->flash('message', ['type' => 'update']);
        return response()->json([
            'result' => true, 'redirect' => route('payment-settings.index')
        ], 200);
    }

    /**
     * Elimina la información del banco
     *
     * @author  William Páez <paez.william8@gmail.com>
     * @param  integer $id                      Identificador del dato a eliminar
     * @return \Illuminate\Http\JsonResponse    Json con mensaje de confirmación de la operación
     */
    public function destroy($id)
    {
        $bank = Bank::find($id);
        $bank->delete();
        return response()->json(['record' => $bank, 'message' => 'Success'], 200);
    }

    /**
     * Muestra los bancos
     *
     * @author  William Páez <paez.william8@gmail.com>
     * @return \Illuminate\Http\JsonResponse    Json con los datos de bancos
     */
    public function vueList()
    {
        return response()->json(['records' => Bank::all()], 200);
    }

    /**
     * Obtiene los bancos
     *
     * @author  William Páez <paez.william8@gmail.com>
     * @return \Illuminate\Http\JsonResponse    JSON con los datos de los bancos
     */
    public function listBanks()
    {
        return response()->json(template_choices(Bank::class, ['name'], [], true));
    }
}
