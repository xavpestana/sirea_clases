<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Season;
use App\Course;
use App\Period;
use App\User;
use App\evaluation;
use App\teacherNotify;
use App\evaluationPlan;

class evaluationController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $course_id = $request->course_id;
        $season = Season::where('status','1')
                        ->where('school_id',auth()->user()->profile->teacher->school_id)
                        ->first();
        $period = Period::where('season_id',$season->id)
                        ->where('status',1)
                        ->first();
       $students = DB::table('users')
            ->join('profiles', 'users.id', '=', 'profiles.user_id')
            ->join('students', 'profiles.id', '=', 'students.profile_id')
            ->join('enrollments', 'students.id', '=', 'enrollments.student_id')
            ->select('users.*','profiles.*')
            ->where('enrollments.section_id',$request->section_id)
            ->orderBy('profiles.last_name_1', 'asc')
            ->get();

            $plan = evaluationPlan::where('period_id',$period->id)
                                    ->where('section_id', $request->section_id)
                                    ->get();
                                    
        return view('evaluations.secundary.index', compact('students','plan','course_id'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $course = Course::find($request->course_id);

        $season = Season::where('status','1')
                        ->where('school_id',auth()->user()->profile->teacher->school_id)
                        ->first();
        $period = Period::where('season_id',$season->id)
                        ->where('status',1)
                        ->first();
        
        $student = User::find($request->user_id);
        
        $plan = evaluationPlan::where('period_id',$period->id)
                                    ->where('section_id', $course->courses_sections_user->last()->section->id)
                                    ->get();
        //dd($plan);
        return view('evaluations.secundary.modal', compact('student','plan','course'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {

        $course = Course::find($id);

        $season = Season::where('status','1')
                        ->where('school_id',auth()->user()->profile->teacher->school_id)
                        ->first();
        $period = Period::where('season_id',$season->id)
                        ->where('status',1)
                        ->first();
        
        if ($course->grade->level->id =='5' || $course->grade->level->id =='6' ||  $course->grade->level->id =='7') {
            
            if (!is_null($period)) {
                $plan = evaluationPlan::where('period_id',$period->id)
                                    ->where('section_id', $course->courses_sections_user->last()->section->id)
                                    ->get();
                return view('evaluations.index', compact('course', 'season', 'period','plan'));
            } else {
               return 'Periodo no activo, vuelve atras y comuniquese con el colegio';
            }
        }else{
            if (!is_null($period)) {
                $plan = evaluationPlan::where('period_id',$period->id)
                                    ->where('section_id', $course->courses_sections_user->last()->section->id)
                                    ->get();
                return view('evaluations.primary', compact('course', 'season', 'period','plan'));
            } else {
               return 'Periodo no activo, vuelve atras y comuniquese con el colegio';
            }
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
       
        $final = $i = 0;
         $count = count($request->nota);
         
        foreach ($request->nota as $nota) {
            evaluation::where('evaluation_plan_id', $request->plan_id[$i])->where('user_id', $request->user_id)->delete();

            $evaluation = new evaluation;
            $evaluation->user_id = $request->user_id;
            $evaluation->evaluation_plan_id = $request->plan_id[$i];
            $evaluation->description = $nota;
            $evaluation->save();
            $i++;
        }
        return 0;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
