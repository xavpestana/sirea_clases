<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Response;
use App\Payment;
use App\Receipt;
use App\Student;
use App\School;
use App\Plan;
use App\credit;
use App\StudentPlan;
use App\Installment;
use App\PaymentMethod;
use App\paymentRegister;
use App\BankAccount;
use App\InvoiceSetting;
use App\installmentPayment;
use App\quotasCancelled;

/**
 * @class PaymentController
 * @brief Controlador de pagos
 *
 * Clase que gestiona los pagos
 *
 * @author William Páez <paez.william8@gmail.com>
 * @license <a href='​http://www.gnu.org/licenses/gpl-2.0.html'>GNU Public License versión 2 (GPLv2)</a>
 */
class PaymentController extends Controller
{
    use ValidatesRequests;

    /**
     * Muestra todos los registros de pagos
     *
     * @author William Páez <paez.william8@gmail.com>
     * @return \Illuminate\View\View    Muestra los datos organizados en una tabla
     */
    public function index(request $request)
    {
        $USD=session('USD_CLASES');
        $COP=session('COP_CLASES');
        
        return view('payments.index',compact('USD', 'COP'));
    }

    /**
     * Muestra el formulario de registro de pagos
     *
     * @author William Páez <paez.william8@gmail.com>
     * @return \Illuminate\View\View    Vista con el formulario
     */
    public function create(Request $request)
    {

        $season = auth()->user()->userSchool->school->seasons->where('status','1')->first();
        
        $student = Student::find($request->id_student);

        //busco plan del estudiante
        $student_plan = StudentPlan::where('season_id', $season->id)
                        ->where('student_id',$student->id)->first();
        //metodos de pago
        $PaymentMethods = PaymentMethod::all();

        //cuentas bancarias
        $bankAccounts = BankAccount::where('bank_accountable_type', 'App\School')
                                        ->where('bank_accountable_id', auth()->user()->userSchool->school_id)
                                        ->get();
        
        if (is_null($student_plan)) {

            $plans = Plan::where('season_id', $season->id)->get();
                    
        } else {
            $plans = null;
        }
        
        return view('payments.create-payment',compact('student_plan', 'student', 'plans','PaymentMethods','bankAccounts'));
    }

    /**
     * Agrego el pago y las configuraciones de pago del alumno.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        date_default_timezone_set('America/Caracas');
        //control de facturas
        
        $control_number = InvoiceSetting::where('school_id',auth()->user()->userSchool->school_id)->first();
        $number_invoices = Payment::where('school_id',auth()->user()->userSchool->school_id)->max('number_invoice');
        
        //procesos
        if (is_null($control_number)) {
            return 'Debe configurar el control de factura';
        }

        if (!is_null($number_invoices)) {
            $number = $number_invoices+1;
        }else{
            //para primer pago
            $number = $control_number->control_number+1;
        }
        
        //dd($request->credit);

        /*Guardo el plan si no existe*/
        if(isset($request->plan_id)){
        $sp = new StudentPlan;
        $sp->season_id = $request->season_id;
        $sp->student_id = $request->student_id;
        $sp->plan_id = $request->plan_id;
        $sp->save();
        }

        /*Tipo de cambio*/
        $change_amount_usd = session('USD_CLASES'); 
        $change_amount_cop = session('COP_CLASES'); 
      
        /*Arreglo de credito*/
        if ($request->coin == 'Bsf') { $paid_coin = 1; }
        if ($request->coin == 'USD') { $paid_coin = session('USD_CLASES'); }
        if ($request->coin == 'COP') { $paid_coin = session('COP_CLASES'); }
        $credit = $request->credit / $paid_coin;

        /*Guardando el pago*/
        $payment = new Payment;
        $payment->student_id = $request->student_id;
        $payment->school_id = auth()->user()->userSchool->school_id;
        $payment->number_invoice = $number;
        $payment->identity = $request->document;
        $payment->name = $request->name;
        $payment->address = $request->address;
        $payment->phone = $request->phone;
        $payment->school_bank_account = $request->bank;
        $payment->payment_method_id = $request->method;
        $payment->amount = $request->amount;
        $payment->coin = $request->coin;
        $payment->amount_bsf = $request->amountbsf;
        $payment->change_amount_USD = $change_amount_usd;
        $payment->change_amount_COP = $change_amount_cop;
        $payment->credit = $credit;
        $payment->coin_credit = $request->coin;
        $payment->verified = 1;
        $payment->annulled = 0;
        $payment->transfer_code = $request->reference;
        $payment->observation = $request->observation;
        $payment->save();
        
        /*Guardando cuotas pagas si existe*/
        if(isset($request->installments)){
            foreach ($request->installments as $installment) {
                $ip = new installmentPayment;
                $ip->payment_id = $payment->id;
                $ip->installment_id = $installment;
                $ip->student_id = $request->student_id;
                $ip->save();
            }
        }

       
        /*Guardando saldo positivo*/
        $credit_before = credit::where('student_id', $request->student_id)->first();
        if (!is_null($credit_before)) {
            $cb = $credit_before->amount;
            $cbc = $credit_before->coin;
            $credit_before->delete();
        }else{
            $cb = 0;
            $cbc = 'Bsf';
        }

        DB::table('credits')
    ->updateOrInsert(
        ['student_id' => $request->student_id, 'amount' => $credit, 'coin' => $request->coin, 'amount_last' => $cb, 'coin_last' => $cbc],
    );

        if (isset($request->register_id)) {
        $PaymentMethods = paymentRegister::find($request->register_id);
        $PaymentMethods->status = 1;
        $PaymentMethods->save();
        }
        
        return $payment->id;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $payments = Payment::where('student_id', $id)->where('annulled', false)->get();
        if (!$payments->isEmpty()) {
            $student = $payments[0]->Student;
            return view('payments.registers',compact('payments', 'student'));
        }else{
            return '<div><h4>No hay pagos registrados</h4></div>';
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {

        $payment = Payment::find($id);
        $installments = $payment->installmentPayment;

        foreach($installments as $installment){
                $iq = new quotasCancelled;
                $iq->payment_id = $installment->payment_id;
                $iq->installment_id = $installment->installment_id;
                $iq->student_id = $installment->student_id;
                $iq->save();
        }

        $payment->annulled = true;
        $payment->save();
        $deletedpayments = installmentPayment::where('payment_id', $id)->delete();
        $deletedRows = credit::where('student_id', $payment->student_id)->first();
        $credit_before = $deletedRows->amount_last ?? 0;
        $coin_before = $deletedRows->coin_last ?? 'Bsf';
        $deletedRows->delete();

        if (is_null($credit_before)) {
            $credit_before = 0;
            $coin_before = 0;
        }
        
        $credit = new credit;
        $credit->student_id =$payment->student_id;
        $credit->amount = $credit_before;
        $credit->coin = $coin_before;
        $credit->amount_last = 0;
        $credit->coin_last = 'Bsf';
        $credit->save();
        return 0;
    }

    /**
     * Muestra el pago de todos los familiares de un colegio
     *
     * @author  William Páez <paez.william8@gmail.com>
     * @return \Illuminate\Http\JsonResponse    Json con los datos del pago
     */
    public function liststudents()
    {
            $school_id = auth()->user()->userSchool->school_id;

            $school = School::find($school_id);

            $students = $school->students;

            $season = auth()->user()->userSchool->school->seasons->where('status','1')->first();

            
        return view('payments.students',compact('students', 'season'));
    }

     public function installments(Request $request)
    {
        $amount = $request->amount;
        $coin = $request->coin;
        $bsf = $request->bsf;
        $coin_amount = $request->coin_amount;
        $student_id = $request->student_id;
        $installments = Installment::where('plan_id', $request->plan_id)->orderBy('expire_at', 'asc')->get();
        //abonos
        $credit = credit::where('student_id', $student_id)->first();
        if(!is_null($credit)){
        if ($credit->coin == 'Bsf') { $bsf = $bsf + $credit->amount; }
        if ($credit->coin == 'USD') { $bsf = $bsf + ($credit->amount*session('USD_CLASES')); }
        if ($credit->coin == 'COP') { $bsf = $bsf + ($credit->amount*session('COP_CLASES')); }
        }
        return view('payments.show_installments',compact('installments','amount','coin','bsf','coin_amount','credit', 'student_id'));
    }

    public function annulledPayment($id)
    {
        
        $receipt = Receipt::find($payment->receipt_id);
        $receipt->paid = false;
        $receipt->save();
        return response()->json(['payment' => $payment, 'receipt' => $receipt], 200);
    }
}
