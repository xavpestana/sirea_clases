<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\User;
use App\Section;
use App\Grade;
use App\Enrollment;

use Illuminate\Support\Facades\Mail;
use App\Mail\MassEmails;

class MessageController extends Controller
{
    public function newMessage()
    {
        $school = auth()->user()->userSchool->school;
        $actual_season = $school->seasons->sortByDesc('id')->where('status', 1)->first();
        $sections = $actual_season->sections->groupBy('grade_id');
        //$sections = $sections->toArray();
        $grades = Grade::all();
        return view('school.message', compact('grades','sections'));
    }

    public function sendMessage(Request $request)
    {
        $school = auth()->user()->userSchool->school;
        $destinatarios = collect();

        if($request->has('chk-docentes')){
            foreach ($school->teachers as $teacher) {
                $destinatarios->push($teacher->profile->user);
            }
        }

        if($request->todos == 1){
            foreach ($school->families as $family) {
                $destinatarios->push($family->user);
            }
        }
        else{
            if($request->has('section')){
                $sections = $request->section;
                foreach ($sections as $key => $section) {
                    $enrollments = Enrollment::where('section_id', $section)->get();
                    foreach ($enrollments as $enrollment) {
                        $user = $enrollment->student->family->user;

                        if(!$destinatarios->contains('id',$user->id)){
                            $destinatarios->push($user);
                        }
                    }
                }
            }
        }

        if($destinatarios->count() > 0){
            foreach ($destinatarios as $destinatario) {
                Mail::to($destinatario)->send(new MassEmails($destinatario, $request->subject, $request->body, $school));
            }
            return back()->with('success', 'El correo será enviado a los destinatarios');
        } 
        else return back()->with('error', 'No ha seleccionado destinatarios para el correo');
    }
}
