<?php

namespace App\Http\Controllers;

use App\Period;
use App\Season;
use App\Director;
use App\Grade;
use Illuminate\Http\Request;

use Illuminate\Support\Facades\View;
use Redirect, Response;

class PeriodController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Period  $period
     * @return \Illuminate\Http\Response
     */
    public function show(Period $period)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Period  $period
     * @return \Illuminate\Http\Response
     */
    public function edit(Period $period)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Period  $period
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $period = Period::find($id);

        $seasonPeriods = $period->season->periods;

        foreach ($seasonPeriods as $key => $seasonPeriod) {
            if($seasonPeriod->id == $period->id){
               $period->status = 1; 
               $period->save();
            }
            else{
                $seasonPeriod->status = 0;
                $seasonPeriod->save();
            }
        } 

        $school = auth()->user()->userSchool->school;

        $seasons = Season::where('school_id', $school->id)->get();

        $director = Director::where('school_id', $school->id)->first();

        $grades = Grade::all();

        $view =  view('school.settings', compact('school','seasons', 'director', 'grades'));

        if($request->ajax()){
            $sections = $view->renderSections();
            return Response::json($sections['content']); 
        }else return back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Period  $period
     * @return \Illuminate\Http\Response
     */
    public function destroy(Period $period)
    {
        //
    }
}
