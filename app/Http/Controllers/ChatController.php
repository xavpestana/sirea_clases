<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use Carbon\Carbon;
use App\User;
use App\chat;
use App\message;
use App\Enrollment;
use App\teacherNotify;
use App\Courses_sections_user;

class ChatController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if (auth()->user()->hasRole('teacher')) {
            $courses = Courses_sections_user::where('user_id',auth()->user()->id)->get();
        }
        if (auth()->user()->hasRole('student')) {
            $section = Enrollment::where('student_id', auth()->user()->profile->student->id)->first();
            $courses = Courses_sections_user::where('section_id', $section->section_id)
                        ->groupBy('course_id')
                        ->get();
        }

        if($request->type){
            $id=$request->type;
            
            return view('chat.index',compact('id','courses'));
        }else{
            return view('chat.index', compact('courses'));
        }
    }
    /**
     * Busqueda de alumnos.
     *
     * @return \Illuminate\Http\Response
     */
    public function students(Request $request)
    {
        $id = $request->course;
        if (auth()->user()->hasRole('teacher')) {
           $students = DB::table('users')
            ->join('profiles', 'users.id', '=', 'profiles.user_id')
            ->join('students', 'profiles.id', '=', 'students.profile_id')
            ->join('enrollments', 'students.id', '=', 'enrollments.student_id')
            ->select('users.*','profiles.*')
            ->where('enrollments.section_id',$id)
            ->orderBy('profiles.last_name_1', 'asc')
            ->get();
        }
        if (auth()->user()->hasRole('student')) {
            $students = DB::table('users')
            ->join('profiles', 'users.id', '=', 'profiles.user_id')
            ->join('courses_sections_users', 'users.id', '=', 'courses_sections_users.user_id')
            ->select('users.*','profiles.*')
            ->where('courses_sections_users.section_id',$id)
            ->orderBy('profiles.last_name_1', 'asc')
            ->groupBy('courses_sections_users.course_id')
            ->get(); 
        }
        
       
            
        return view('chat.utilities.student', compact('students'));
    }

    /**
     * Busqueda de alumnos.
     *
     * @return \Illuminate\Http\Response
     */
    public function search(Request $request)
    {
        return 0;
    }
    /**
     * Contador de mensajes nuevos.
     *
     * @return \Illuminate\Http\Response
     */
    public function count_message(Request $request)
    {
        $id = $request->id;
        $count = 0;
        $exist_chat = DB::table('chats')
                    ->where('user_id',$id)
                    ->orwhere('user_id_r',$id)
                    ->get();
                   // dd($exist_chat);
                    if(!is_null($exist_chat)){
                        foreach ($exist_chat as $chat) {
                            if ($chat->user_id == $id) {
                                $count = $count + message::where('user_id',$chat->user_id_r)
                                                            ->where('chat_id', $chat->id)
                                                            ->where('new',1)
                                                            ->count(); 
                            }
                            if ($chat->user_id_r == $id) {
                                $count = $count + message::where('user_id',$chat->user_id)
                                                            ->where('chat_id', $chat->id)
                                                            ->where('new',1)
                                                            ->count();
                                                            
                            }
                                                            
                        }
                    }
                    
        return $count;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        
        $student = User::find($request->id);

        return view('chat.center', compact('student'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

            $id=auth()->user()->id;
            $date = Carbon::now();
        
        $exist_chat = DB::table('chats')
            ->where([
                    ['user_id', '=', $id],
                    ['user_id_r', '=', $request->user_id],])
            ->orwhere([
                    ['user_id_r', '=', $id],
                    ['user_id', '=', $request->user_id],])->first();
            
            if(is_null($exist_chat)){

                $chat = new chat();
                $chat->user_id = $id;
                $chat->user_id_r = $request->user_id;
                $chat->save();

                $message = new message();
                $message->chat_id = $chat->id;
                $message->message = $request->message;
                $message->user_id = $id;
                $message->new = '1';
                $message->save();

                $notifies = new teacherNotify();
                $notifies->user_id  = $id;
                $notifies->message = 'Ha enviado un mensaje';
                $notifies->user_id_r = $request->user_id;
                $notifies->method = 'msg';
                $notifies->save();


            }else{

                $chat = chat::find($exist_chat->id);
                $chat->updated_at = $date;
                $chat->save();

                $message = new message();
                $message->chat_id = $exist_chat->id;
                $message->message = $request->message;
                $message->user_id = $id;
                $message->new = '1';
                $message->save();

                $notifies = new teacherNotify();
                $notifies->user_id  = $id;
                $notifies->message = 'Ha enviado un mensaje';
                $notifies->user_id_r = $request->user_id;
                $notifies->method = 'msg';
                $notifies->save();
            }
        return 0;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($chat)
    {
        
       $id=auth()->user()->id;

            $exist_chat = DB::table('chats')
                        ->where([
                                ['user_id', '=', $id],
                                ['user_id_r', '=', $chat],])
                        ->orwhere([
                                ['user_id_r', '=', $id],
                                ['user_id', '=', $chat],])
                        ->first();
                        
            if(!is_null($exist_chat)){
                     
            $users = DB::table('users')
                    ->join('profiles', 'users.id', '=', 'profiles.user_id')
                    ->join('messages', 'users.id', '=', 'messages.user_id')
                    ->where('chat_id',$exist_chat->id)
                    ->select('profiles.user_id','profiles.last_name_1','profiles.avatar','profiles.first_name_1','messages.new','messages.message','messages.id')
                    ->orderBy('messages.created_at','desc')
                    ->get();
                    
                    }else{
                        $users=null;
                    }
            
            return view('chat.messages',compact('users'));
    }

    public function sidebar_message(Request $request){
        $users=null;

        $id=auth()->user()->id;

        $chats = DB::table('chats')
                        ->where('user_id',$id)
                        ->orwhere('user_id_r',$id)
                        ->orderBy('updated_at','desc')
                        ->get();
            
            return view('chat.utilities.sidebar',compact('chats'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $msg = message::where('id', $id)->delete();
        
        if($msg){
            return 0;
        }else{
            return 1;
        }
    }
}
