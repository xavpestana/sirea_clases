<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use App\Courses_sections_user;
use App\FileUpdate;
use App\Profile;
use App\Teacher;
use App\Season;
use App\Course;
use App\Comments;
use App\teacherNotify;
use App\User;
use App\Task;
use App\Period;
use App\fileResponse;
use App\responseActivity;


class TaskController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $courses = Courses_sections_user::where('user_id',auth()->user()->id)->get();
        return view('teacher.courses', compact('courses'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    
    public function store(Request $request)
    {

        $date = date('Y-m-d');
        $files = $request->file('file');
        $nameFile = $files->getClientOriginalName();
        $extension = pathinfo($nameFile, PATHINFO_EXTENSION);

        if (auth()->user()->hasRole('teacher')) {
        
        $fileName  = 'file_'.auth()->user()->profile->teacher->school_id.'_'.auth()->user()->id.'_'.date('Y_m_d_H_i_s');
            
        $path = public_path().'/uploads/teachers/'.auth()->user()->profile->teacher->school_id.'/'.auth()->user()->id.'/'.$date.'/';

            $files->move($path, $fileName.'.'.$extension);

            $request->session()->put('url', '/uploads/teachers/'.auth()->user()->profile->teacher->school_id.'/'.auth()->user()->id.'/'.$date.'/'.$fileName.'.'.$extension);
            $request->session()->put('extension', $extension);

            return 0;

        }

        if (auth()->user()->hasRole('student')) {

        $fileName  = 'file_'.auth()->user()->profile->student->family->school_id.'_'.auth()->user()->id.'_'.date('Y_m_d_H_i_s');
            
        $path = public_path().'/uploads/student/'.auth()->user()->profile->student->family->school_id.'/'.auth()->user()->id.'/'.$date.'/';

            $files->move($path, $fileName.'.'.$extension);

                $responseAct = new responseActivity();
                $responseAct->user_id = auth()->user()->id;
                $responseAct->task_id = $request->task_id;
                $responseAct->save();


                $fileUpload = new fileResponse();
                $fileUpload->task_id = $request->task_id;
                $fileUpload->url = '/uploads/student/'.auth()->user()->profile->student->family->school_id.'/'.auth()->user()->id.'/'.$date.'/'.$fileName.'.'.$extension;
                $fileUpload->extension = $extension;
                $fileUpload->response_activity_id = $responseAct->id;
                $fileUpload->save();
                
                
                $notifies = new teacherNotify();
                $notifies->user_id  = auth()->user()->id;
                $notifies->message = 'Ha respondido a una actividad';
                $notifies->course_id = $request->course_id;
                $notifies->method = 'response';
                $notifies->task_id = $request->task_id;
                $notifies->save();
            return 0;
        }
        
    }
    public function save(Request $request)
    {
        /*
        Utilidades
        */
        if ($request->response == 'on') {
            $response = 1;
        } else {
            $response = 0;
        }

        if ($request->all_section == 'on') {
            $todos = 0;
        } else {
            $todos = 1;
        }

        /*
        Creacion de usuario
        */
        $season = Season::where('status','1')
                        ->where('school_id',auth()->user()->profile->teacher->school_id)
                        ->first();

        $period = Period::where('season_id',$season->id)
                        ->where('status',1)
                        ->first();
        
        $task = new Task;
        $task->course_id = $request->id_course;
        $task->message = $request->message;
        $task->file = $request->file;
        $task->request = $response;
        $task->receiver = $todos;
        $task->period_id = $period->id;
        $task->save();

        //especificando alumnos

        if ($request->all_section != 'on') {
            foreach ($request->student as $student) {
                DB::table('student_task')->insert(
                ['task_id' => $task->id, 'user_id' => $student]
            );

                $notifies = new teacherNotify();
                $notifies->user_id  = auth()->user()->id;
                $notifies->message = 'Ha enviado una actividad';
                $notifies->user_id_r = $student;
                $notifies->course_id = $request->id_course;
                $notifies->method = 'activity';
                $notifies->save();
            }
        }else{
                $notifies = new teacherNotify();
                $notifies->user_id  = auth()->user()->id;
                $notifies->message = 'Ha enviado una actividad';
                $notifies->user_id_r = 0;
                $notifies->course_id = $request->id_course;
                $notifies->method = 'activity';
                $notifies->save();
        }

        if ($task) {

            if ($request->file == '1') {
                $fileUpload = new FileUpdate();
                $fileUpload->task_id = $task->id;
                $fileUpload->url = session('url');
                $fileUpload->extension = session('extension');
                $fileUpload->save();

                session()->forget('url');
                session()->forget('extension');
            }
            
            return 0;
        } else {
            return 1;
        }
        
        
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Task  $task
     * @return \Illuminate\Http\Response
     */
    public function show($task)
    {
        $course = Course::find($task);
        $season = Season::where('status','1')
                        ->where('school_id',auth()->user()->profile->teacher->school_id)
                        ->first();
        $notifies = teacherNotify::where('course_id',$task)
                        ->where('user_id','!=',auth()->user()->id)
                        ->where(function ($query) {
                            $query->where('method', 'comment')
                                  ->orWhere('method', 'response');
                        })
                        ->orderBy('created_at', 'desc')
                        ->limit(20)
                        ->get();

        return view('teacher.task', compact('course', 'season','notifies'));
    }
    public function students($id)
    {
        $users = DB::table('users')
            ->join('profiles', 'users.id', '=', 'profiles.user_id')
            ->join('students', 'profiles.id', '=', 'students.profile_id')
            ->join('enrollments', 'students.id', '=', 'enrollments.student_id')
            ->select('users.*','profiles.*')
            ->where('enrollments.section_id',$id)
            ->orderBy('profiles.last_name_1', 'asc')
            ->get();
        
        return view('teacher.utilities.student', compact('users'));
    }

    /**
     * Muestra los mensajes de profesor.
     *
     * @param  \App\Task  $task
     * @return \Illuminate\Http\Response
     */
    public function messages($task)
    {

        $tasks = Task::where('course_id', $task)->orderBy('created_at', 'desc')->get();
        //dd($tasks);
        return view('teacher.utilities.message', compact('tasks'));
    }

    public function comment(Request $request)
    {

     $comment = new Comments();
     $comment->user_id = auth()->user()->id;
     $comment->task_id = $request->task_id;
     $comment->comment = $request->comment;
     $comment->save();

                $notifies = new teacherNotify();
                $notifies->user_id  = auth()->user()->id;
                $notifies->message = 'Ha enviado un comentario';
                $notifies->user_id_r = 0;
                $notifies->method = 'comment';
                $notifies->course_id = $request->course_id;
                $notifies->task_id = $request->task_id;
                $notifies->save();
     
        return 0;
    }

    public function showcomment(Request $request)
    {
        $comments = Comments::where('task_id',$request->id)->get();
        $task = Task::find($request->id);
        //dd($task);
        return view('teacher.utilities.comments', compact('comments', 'task'));
    }
    public function showresponse(Request $request)
    {
        $responses = responseActivity::where('task_id', $request->id)->get();
        
        return view('teacher.modal.response', compact( 'responses'));
    }
    public function delete_comment(Request $request)
    {
        $delete = Comments::where('id', $request->id)->delete();
        
        if ($delete) {
            return 0;
        }
    }
    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Task  $task
     * @return \Illuminate\Http\Response
     */
    public function edit(Task $task)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Task  $task
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Task $task)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Task  $task
     * @return \Illuminate\Http\Response
     */
    public function destroy($task)
    {
        $task = Task::where('id', $task)->delete();
        $file = FileUpdate::where('task_id', $task)->delete();
        $task_file = DB::table('student_task')->where('task_id', '=', $task)->delete();
        
        if (!is_null($task) && !is_null($file)&& !is_null($task_file)) {
            return 0;
        } else {
            return 1;
        }
    }

    /**
     * Show profile
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function profile()
    {
        return view('teacher.profile');
    }
}
