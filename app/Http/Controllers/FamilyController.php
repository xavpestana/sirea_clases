<?php

namespace App\Http\Controllers;

use App\Family;
use App\User;
use App\Poll;
use Illuminate\Http\Request;

use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Auth;

use App\Mail\UserRegister;
use App\Mail\familyDocuments;
use App\Mail\ChangeFamilyPassword;

use Redirect, Response;

class FamilyController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if(isset(auth()->user()->userSchool->school_id)){
            $school_id = auth()->user()->userSchool->school_id;
            $families = Family::where('school_id', $school_id)->get();
        }else
        {
            $families = Family::all();

        }

        $view = view('school.families', compact('families'));

        if(isset($request)){
            if($request->ajax()){
                $sections = $view->renderSections();
                return Response::json($sections['content']); 
            }else return $view;
        }else return $view;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('family.edit');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $this->validatorEmail($request->all())->validate();

        $school = auth()->user()->userSchool->school;

        $school_id = $school->id;
       
        $password = Str::random();
        $user = User::create([
            'name' => $request->email,
            'email' => $request->email,
            'password' => bcrypt($password),
        ]);

        $user->assignRole('family'); 

        $family = Family::create([
            'user_id' => $user->id,
            'school_id' =>$school_id,
            'phone' =>$request->phone
        ]);

        //Mail::to($user)->send(new UserRegister($user, $password));
        //Mail::to($user)->send(new familyDocuments($user, $password));

        if(isset(auth()->user()->userSchool->school_id)){
            $school_id = auth()->user()->userSchool->school_id;
            $families = Family::where('school_id', $school_id)->get();
        }else
        {
            $families = Family::all();

        }

        $view = view('school.families', compact('families'));

        if(isset($request)){
            if($request->ajax()){
                $sections = $view->renderSections();
                return Response::json($sections['content']); 
            }else return $view;
        }else return $view;
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Family  $family
     * @return \Illuminate\Http\Response
     */
    public function show(Family $family)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Family  $family
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        if(!auth()->user()->hasRole('admin') && auth()->user()->hasRole('family') && auth()->user()->family->id != $id){
            return back();
        }

        $family = Family::find($id);

        return view('family.edit', compact('family'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Family  $family
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        $family = Family::find($id);

        $user = $family->user;

        if($request->email == $user->email) $this->validator2($request->all())->validate();
        else $this->validator($request->all())->validate();

        $family->update($request->all());
        
        $school = $family->school;

        if($school->additional_data){
            if($family->poll){
                $family->poll->update($request->all());
            }else{
                $poll = Poll::create([
                    'family_id' => $family->id,
                    'companion' => $request->companion,
                    'arrival' => $request->arrival,
                    'departure' => $request->departure,
                    'marital_status' => $request->marital_status,
                    'religion' => $request->religion,
                    'property_type' => $request->property_type,
                    'home_type' => $request->home_type,
                    'emergency_phone' => $request->emergency_phone,
                    'policy_description' => $request->policy_description
                ]);
            }
        }

        if($request->email != $user->email){
            
            $password = Str::random();

            $user->email = $request->email;

            $user->password = bcrypt($password);

            $user->save();

            Mail::to($user)->send(new UserRegister($user, $password));

/*            if( auth()->user()->hasRole('family') ) {
                Auth::logout();
                return redirect()->route('login');
            }   */ 
        }


        //$request->session()->flash('message', ['type' => 'store']);

        if(auth()->user()->hasRole('family')){
            return redirect()->route('home')->with('success', 'los datos de la familia han sido actualizados.');
        }
        else return $this->index()->with('success', 'los datos de la familia han sido actualizados.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Family  $family
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {

        $family = Family::find($id);

        $user = $family->user;

        $user->delete();
        Family::destroy($id);
        $school_id = auth()->user()->userSchool->school_id;

        $families = Family::where('school_id', $school_id)->get();

        $view = view('school.families', compact('families'));

        if($request->ajax()){
            $sections = $view->renderSections();
            return Response::json($sections['content']); 
        }else return back();
    }

    protected function validator(array $data)
    {
        return Validator::make($data, [
            'email'            => ['required', 'string', 'email', 'max:255', 'unique:users'],
            'last_name_1'      => ['required', 'string', 'max:255'],
            'last_name_2'      => ['required', 'string', 'max:255'],
            'phone'            => ['required'],
            'monthly_income'   => ['between:0,99.99'],
            'dependent_income' => ['integer'],
            'contributors'     => ['integer'],
            'parish_id'        => ['integer'],
            'address'          => ['string']
        ]);
    }

    protected function validatorEmail(array $data)
    {
        return Validator::make($data, [
            'email'            => ['required', 'string', 'email', 'max:255', 'unique:users'],
        ]);
    }

    protected function validator2(array $data)
    {
        return Validator::make($data, [
            'last_name_1'      => ['required', 'string', 'max:255'],
            'last_name_2'      => ['required', 'string', 'max:255'],
            'phone'            => ['required'],
            'monthly_income'   => ['between:0,99.99'],
            'dependent_income' => ['integer'],
            'contributors'     => ['integer'],
            'parish_id'        => ['integer'],
            'address'          => ['string']
        ]);
    }

    public function changePassword($id)
    {
        $family = Family::find($id);
        $user = $family->user;
        $password = Str::random(8);
        Mail::to($user)->send(new ChangeFamilyPassword($user, $password));
        $user->password = bcrypt($password);
        $user->save();
        return Response::json('success');
    }
}
