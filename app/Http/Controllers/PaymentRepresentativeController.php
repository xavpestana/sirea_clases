<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Carbon\Carbon;
use App\PaymentMethod;
use App\paymentRegister;
use App\BankAccount;
use App\Season;
use App\School;
use App\StudentPlan;
use App\Plan;

class PaymentRepresentativeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        $students = auth()->user()->family->students;
        $school = School::find(auth()->user()->family->school_id);

        $actual_season = $school->seasons->where('status')->first();
        $today = Carbon::now();
        //metodos de pago
        $PaymentMethods = PaymentMethod::all();
        //cuentas bancarias
        $bankAccounts = BankAccount::where('bank_accountable_type', 'App\School')
                                        ->where('bank_accountable_id', auth()->user()->family->school_id)
                                        ->get();

        return view('family.payments', compact('students', 'actual_season', 'today', 'PaymentMethods', 'bankAccounts'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
       //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
       $season = auth()->user()->userSchool->school->seasons->where('status','1')->first();
        
        $paymentRegister = paymentRegister::find($id);

        //busco plan del estudiante
        $student_plan = StudentPlan::where('season_id', $season->id)
                        ->where('student_id',$paymentRegister->student_id)->first();
        //metodos de pago
        $PaymentMethods = PaymentMethod::all();

        //cuentas bancarias
        $bankAccounts = BankAccount::where('bank_accountable_type', 'App\School')
                                        ->where('bank_accountable_id', auth()->user()->userSchool->school_id)
                                        ->get();
        

        
        if (is_null($student_plan)) {

            $plans = Plan::where('season_id', $season->id)->get();
                    
        } else {
            $plans = null;
        }
    return view('payments.family',compact('student_plan', 'paymentRegister', 'plans','PaymentMethods','bankAccounts'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $PaymentMethods = paymentRegister::find($id);
        $PaymentMethods->status = 2;
        $PaymentMethods->save();
        return $id;
    }
}
