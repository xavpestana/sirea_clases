<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Foundation\Validation\ValidatesRequests;
use App\Concept;

/**
 * @class ConceptController
 * @brief Controlador de conceptos de inscripción
 *
 * Clase que gestiona los conceptos de inscripción
 *
 * @author William Páez <paez.william8@gmail.com>
 * @license <a href='​http://www.gnu.org/licenses/gpl-2.0.html'>GNU Public License versión 2 (GPLv2)</a>
 */
class ConceptController extends Controller
{
    use ValidatesRequests;
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
    $concepts = Concept::where('school_id', auth()->user()->userSchool->school_id)->get();
    return view('payments.concepts.show-concept',compact('concepts'));
    }

    /**
     * Muestra el formulario de registro de conceptos de inscripción
     *
     * @author William Páez <paez.william8@gmail.com>
     * @return \Illuminate\View\View    Vista con el formulario
     */
    public function create()
    {
        return view('payments.concepts.create-concept');
    }

    /**
     * Valida y registra nuevo concepto de inscripción
     *
     * @author  William Páez <paez.william8@gmail.com>
     * @param  \Illuminate\Http\Request $request    Solicitud con los datos a guardar
     * @return \Illuminate\Http\JsonResponse        Json: result en verdadero y redirect con la url a donde ir
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'concept' => ['required', 'max:100']
        ]);

        $concept = Concept::create([
            'name' => $request->concept,
            'school_id' => auth()->user()->userSchool->school_id,
        ]);

        return 0;
    }

    /**
     * Muestra los datos de un concepto de inscripción
     *
     * @author  William Páez <paez.william8@gmail.com>
     * @param  integer $id                          Identificador del dato a mostrar
     * @return \Illuminate\Http\JsonResponse        Json con el dato del concepto de inscripción
     */
    public function show($id)
    {
        $concept = Concept::where('id', $id)->where('school_id', auth()->user()->userSchool->school_id)->first();
        return response()->json(['record' => $concept], 200);
    }

    /**
     * Muestra el formulario de actualización de concepto de inscripción
     *
     * @author William Páez <paez.william8@gmail.com>
     * @param  integer $id              Identificador con el dato a actualizar
     * @return \Illuminate\View\View    Vista con el formulario y el objeto con el dato a actualizar
     */
    public function edit($id)
    {
        $concept = Concept::where('id', $id)->where(
            'school_id',
            auth()->user()->userSchool->school_id
        )->first();
        if (!$concept) {
            abort(403, 'El usuario no tiene los permisos para entrar a esta página.');
        }
        return view('payments.concepts.create-edit', compact('concept'));
    }

    /**
     * Actualiza la información del concepto de inscripción
     *
     * @author  William Páez <paez.william8@gmail.com>
     * @param  \Illuminate\Http\Request  $request   Solicitud con los datos a actualizar
     * @param  integer $id                          Identificador del dato a actualizar
     * @return \Illuminate\Http\JsonResponse        Json con la redirección y mensaje de confirmación de la operación
     */
    public function update(Request $request, $id)
    {
        $concept = Concept::where('id', $id)->where(
            'school_id',
            auth()->user()->userSchool->school_id
        )->first();
        if (!$concept) {
            abort(403, 'El usuario no tiene los permisos para entrar a esta página.');
        }
        $this->validate($request, [
            'name' => ['required', 'max:100'],
            'amount' => ['required', 'numeric']
        ]);
        $concept->name = $request->name;
        $concept->amount = $request->amount;
        $concept->save();
        $request->session()->flash('message', ['type' => 'update']);
        return response()->json([
            'result' => true, 'redirect' => route('payment-settings.index')
        ], 200);
    }

    /**
     * Elimina la información de concepto de inscripción
     *
     * @author  William Páez <paez.william8@gmail.com>
     * @param  integer $id                      Identificador del dato a eliminar
     * @return \Illuminate\Http\JsonResponse    Json con mensaje de confirmación de la operación
     */
    public function destroy($id)
    {
        $concept = Concept::find($id);
        $concept->delete();
        
        return 0;
    }

    /**
     * Muestra los conceptos de inscripción
     *
     * @author  William Páez <paez.william8@gmail.com>
     * @return \Illuminate\Http\JsonResponse    Json con los datos de los conceptos de inscripción
     */
    public function vueList()
    {
        $concepts = Concept::where(
            'school_id',
            auth()->user()->userSchool->school_id
        )->get();
        return response()->json(['records' => $concepts], 200);
    }

    /**
     * Obtiene los conceptos de inscripción
     *
     * @author  William Páez <paez.william8@gmail.com>
     * @return \Illuminate\Http\JsonResponse    JSON con los datos de los conceptos
     */
    public function listConcepts()
    {
        return response()->json(template_choices(
            Concept::class,
            ['name','-','amount'],
            ['school_id' => auth()->user()->userSchool->school_id],
            true
        ));
    }

    /**
     * Obtiene las conceptos sin usar template_choices
     *
     * @author  William Páez <paez.william8@gmail.com>
     * @return \Illuminate\Http\JsonResponse    Json con los datos de los conceptos
     */
    public function listJsonConcepts()
    {
        $jsonConcepts = Concept::where(
            'school_id',
            auth()->user()->userSchool->school_id
        )->get();
        return response()->json(['jsonConcepts' => $jsonConcepts], 200);
    }
}
