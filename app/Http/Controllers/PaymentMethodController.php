<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Foundation\Validation\ValidatesRequests;
use App\PaymentMethod;

/**
 * @class PaymentMethodController
 * @brief Controlador de métodos de pago
 *
 * Clase que gestiona los métodos de pago
 *
 * @author William Páez <paez.william8@gmail.com>
 * @license <a href='​http://www.gnu.org/licenses/gpl-2.0.html'>GNU Public License versión 2 (GPLv2)</a>
 */
class PaymentMethodController extends Controller
{
    use ValidatesRequests;

    /**
     * Define la configuración de la clase
     *
     * @author William Páez <paez.william8@gmail.com>
     */
    public function __construct()
    {
        /** Establece permisos de acceso para cada método del controlador */
        $this->middleware('permission:payment.methods.list', ['only' => 'vueList']);
        $this->middleware('permission:payment.methods.create', ['only' => ['create', 'store']]);
        $this->middleware('permission:payment.methods.show', ['only' => 'show']);
        $this->middleware('permission:payment.methods.edit', ['only' => ['edit', 'update']]);
        $this->middleware('permission:payment.methods.delete', ['only' => 'destroy']);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Muestra el formulario de registro de métodos de pago
     *
     * @author William Páez <paez.william8@gmail.com>
     * @return \Illuminate\View\View    Vista con el formulario
     */
    public function create()
    {
        return view('payments.payment-methods.create-edit');
    }

    /**
     * Valida y registra nuevo método de pago
     *
     * @author  William Páez <paez.william8@gmail.com>
     * @param  \Illuminate\Http\Request $request    Solicitud con los datos a guardar
     * @return \Illuminate\Http\JsonResponse        Json: result en verdadero y redirect con la url a donde ir
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => ['required'],
        ]);

        $paymentMethod = PaymentMethod::create([
            'name' => $request->name,
        ]);

        $request->session()->flash('message', ['type' => 'store']);
        return response()->json([
            'result' => true, 'redirect' => route('payment-settings.index')
        ], 200);
    }

    /**
     * Muestra los datos de un método de pago
     *
     * @author  William Páez <paez.william8@gmail.com>
     * @param  integer $id                          Identificador del dato a mostrar
     * @return \Illuminate\Http\JsonResponse        Json con el dato del método de pago
     */
    public function show($id)
    {
        $paymentMethod = PaymentMethod::where('id', $id)->first();
        return response()->json(['record' => $paymentMethod], 200);
    }

    /**
     * Muestra el formulario de actualización del método de pago
     *
     * @author William Páez <paez.william8@gmail.com>
     * @param  integer $id              Identificador con el dato a actualizar
     * @return \Illuminate\View\View    Vista con el formulario y el objeto con el dato a actualizar
     */
    public function edit($id)
    {
        $paymentMethod = PaymentMethod::where('id', $id)->first();
        if (!$paymentMethod) {
            abort(403, 'El usuario no tiene los permisos para entrar a esta página.');
        }
        return view('payments.payment-methods.create-edit', compact('paymentMethod'));
    }

    /**
     * Actualiza la información del método de pago
     *
     * @author  William Páez <paez.william8@gmail.com>
     * @param  \Illuminate\Http\Request  $request   Solicitud con los datos a actualizar
     * @param  integer $id                          Identificador del dato a actualizar
     * @return \Illuminate\Http\JsonResponse        Json con la redirección y mensaje de confirmación de la operación
     */
    public function update(Request $request, $id)
    {
        $paymentMethod = PaymentMethod::where('id', $id)->first();
        if (!$paymentMethod) {
            abort(403, 'El usuario no tiene los permisos para entrar a esta página.');
        }
        $this->validate($request, [
            'name' => ['required'],
        ]);
        $paymentMethod->name = $request->name;
        $paymentMethod->save();
        $request->session()->flash('message', ['type' => 'update']);
        return response()->json([
            'result' => true, 'redirect' => route('payment-settings.index')
        ], 200);
    }

    /**
     * Elimina la información del método de pago
     *
     * @author  William Páez <paez.william8@gmail.com>
     * @param  integer $id                      Identificador del dato a eliminar
     * @return \Illuminate\Http\JsonResponse    Json con mensaje de confirmación de la operación
     */
    public function destroy($id)
    {
        $paymentMethod = PaymentMethod::where('id', $id)->first();
        if (!$paymentMethod) {
            abort(403, 'El usuario no tiene los permisos para entrar a esta página.');
        }
        $paymentMethod->delete();
        return response()->json(['record' => $paymentMethod, 'message' => 'Success'], 200);
    }

    /**
     * Muestra los métodos de pago
     *
     * @author  William Páez <paez.william8@gmail.com>
     * @return \Illuminate\Http\JsonResponse    Json con los datos de métodos de pago
     */
    public function vueList()
    {
        $paymentMethods = PaymentMethod::all();
        return response()->json(['records' => $paymentMethods], 200);
    }

    /**
     * Obtiene los métodos de pago del colegio
     *
     * @author  William Páez <paez.william8@gmail.com>
     * @return \Illuminate\Http\JsonResponse    JSON con los datos de los métodos de pago
     */
    public function listPaymentMethods()
    {
        return response()->json(template_choices(PaymentMethod::class, ['name'], [], true));
    }
}
