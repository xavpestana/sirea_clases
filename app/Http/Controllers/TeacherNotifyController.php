<?php

namespace App\Http\Controllers;

use App\teacherNotify;
use Illuminate\Http\Request;

class TeacherNotifyController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\teacherNotify  $teacherNotify
     * @return \Illuminate\Http\Response
     */
    public function show(teacherNotify $teacherNotify)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\teacherNotify  $teacherNotify
     * @return \Illuminate\Http\Response
     */
    public function edit(teacherNotify $teacherNotify)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\teacherNotify  $teacherNotify
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, teacherNotify $teacherNotify)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\teacherNotify  $teacherNotify
     * @return \Illuminate\Http\Response
     */
    public function destroy(teacherNotify $teacherNotify)
    {
        //
    }
}
