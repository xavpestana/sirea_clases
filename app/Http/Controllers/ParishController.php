<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Parish;

class ParishController extends Controller
{
    public function getParishes(Request $request)
    {
    	if ($request->ajax()) {
    		$parishes = Parish::where('municipality_id', $request->municipality_id)->get();

    		foreach ($parishes as $parish) {
    			$parishesArray[$parish->id] = $parish->name;
    		}
    	}

    	return response()->json($parishesArray);
    }

    /**
     * Obtiene las parroquias de un municipio
     *
     * @author  William Páez <paez.william8@gmail.com>
     * @return \Illuminate\Http\JsonResponse    JSON con los datos de las parroquias
     */
    public function listParishes($municipality_id)
    {
        return response()->json(
            template_choices(Parish::class, 'name', ['municipality_id' => $municipality_id], true)
        );
    }
}
