<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Representative;

use Redirect, Response;

use Illuminate\Support\Facades\View;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;
use Image;

class RepresentativeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        if(auth()->user()->hasRole('school')){
           
            $school = auth()->user()->userSchool->school;

            $representatives = $school->representatives;

            return view ('school.representatives', compact('representatives'));

        }elseif(auth()->user()->hasRole('family')){

            $representatives = auth()->user()->family->representatives;

            return view ('family.representatives', compact('representatives'));

        }elseif(auth()->user()->hasRole('admin')){
            
            $representatives = Representative::all();

            return view('school.representatives', compact('representatives'));

        }else return back();

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('family.representatives-edit');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validator($request->all())->validate();

        $family_id = auth()->user()->family->id;

        if(!$request->has('principal')) $request->request->add(['principal' => '0']);

        if($request->avatar) $avatar = $this->saveAvatar($request);
        else $avatar = 'default.jpg';

        $representative = Representative::create([
            'family_id' => $family_id,
            'document_type' => $request->document_type,
            'document_number' => $request->document_number,
            'first_name_1' => $request->first_name_1,
            'first_name_2' => $request->first_name_2,
            'last_name_1' => $request->last_name_1,
            'last_name_2' => $request->last_name_2,
            'birthdate' => $request->birthdate,
            'phone' => $request->phone,
            'country_id' => $request->country_id,
            'gender_id' => $request->gender_id,
            'principal' => $request->principal,
            'relationship_id' => $request->relationship_id,
            'avatar' => $avatar,
            'correo' => $request->email
        ]);

        //return $this->index()->with('success','Represenatnte creado exitosamente');

        return redirect()->route('representatives.index')->with('success','Nuevo representante agregado a la familia');  
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Representative  $representative
     * @return \Illuminate\Http\Response
     */
    public function show(Representative $representative)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Representative  $representative
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {   
        $representative = Representative::find($id);
        
        return view('family.representatives-edit', compact('representative'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Representative  $representative
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $representative = Representative::find($id);

        $this->validator($request->all())->validate();

        if(!$request->has('principal')) {$request->request->add(['principal' => '0']);}


        if($request->has('avatar')) {

            $this->deleteAvatar($representative->avatar);
    
            $representative->update($request->all());

            $representative->avatar = $this->saveAvatar($request);

            $representative->save();

        }else{
            $representative->update($request->all());
        }
        //return $this->index()->with('success','Datos del representante actualizados');

        if(auth()->user()->hasRole('family')){
            return redirect()->route('representatives.index')->with('success','Datos del representante actualizados');
        }
        else return redirect()->route('school.representatives.index')->with('success','Datos del representante actualizados');        
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Representative  $representative
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        $representative = Representative::find($id);

        $this->deleteAvatar($representative->avatar);

        $representative->delete();

        $representatives = auth()->user()->family->representatives;

        $view = View::make('family.representatives')->with('representatives', $representatives);

        if($request->ajax()){

            $sections = $view->renderSections();

            return Response::json($sections['content']); 

        }else return back()->with('success', 'Representante eliminado');

    }


    protected function validator(array $data)
    {
        return Validator::make($data, [
            'document_type' => ['required', 'string', 'max:2'],
            'document_number' => ['required', 'integer'],
            'first_name_1' => ['required', 'string', 'max:50'],
            'last_name_1' => ['required', 'string', 'max:50'],
            'last_name_2' => ['required', 'string', 'max:50'],
            'birthdate' => ['required', 'string'],//revisar
            'phone' => ['required', 'string', 'max:255'],//revisar
            'country_id' => ['required', 'integer'],
            'gender_id' => ['required', 'integer'],
            'relationship_id' => ['required'],
            'avatar' => ['image', 'mimes:jpeg,png,jpg,gif']
        ]);
    }

    public function saveAvatar(Request $request)
    {
        $originalImage= $request->file('avatar');
        $image = Image::make($originalImage);
        $originalPath = public_path().'/storage/pictures/avatars/';
        //Nombre aleatorio para la image
        $tempName = Str::random(20) . '.' . $originalImage->getClientOriginalExtension();

        //Redimensinoar la imagen
        if($image->width() >= $image->height()) $image->heighten(400);
        else $image->widen(400);
        $image->resizeCanvas(400,400);

        $image->save($originalPath.$tempName);

        return $tempName;
    }

    public function deleteAvatar($avatar)
    {

        $originalPath = public_path().'/storage/pictures/avatars/';

        if ($avatar != 'default.jpg')
        {
          if(\File::exists($originalPath.$avatar)){
            \File::delete($originalPath.$avatar);
          }
        }
    }


}
