<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Section;
use App\Season;
use App\Grade;
use App\Director;
use Illuminate\Support\Facades\View;
use Redirect;
use Response;

class SectionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $season = Season::find($request->season_id);

    	$sections_number = $season->sections_number;


    	if($sections_number) $name = 1;
    	else $name = 'A';


		for ($i=0; $i < $request->number ; $i++) {
			$name++;
		}

        $section = Section::create([
            'season_id' => $season->id,
            'grade_id' => $request->grade_id,
            'name' => $name
        ]);

        $school = auth()->user()->userSchool->school;

        $seasons = Season::where('school_id', $school->id)->get();

        $director = Director::where('school_id', $school->id)->first();

        $grades = Grade::all();

        $view =  view('school.settings', compact('school','seasons', 'director', 'grades'));

        if($request->ajax()){
            $sections = $view->renderSections();
            return Response::json($sections['content']);
        }else return back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    /**
     * Obtiene las secciones que pertenecen a un grado escolar
     *
     * @author  William Páez <paez.william8@gmail.com>
     * @return \Illuminate\Http\JsonResponse    JSON con los datos de los grados
     */
    public function listSections($grade_id)
    {
        $season = Season::where('school_id', auth()->user()->userSchool->school_id)->where('status', true)->first();
        return response()->json(template_choices(
            Section::class,
            'name',
            ['grade_id'=> $grade_id, 'season_id' => $season->id],
            true
        ));
    }
}
