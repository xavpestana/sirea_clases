<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Municipality;

class MunicipalityController extends Controller
{
    public function getMunicipalities(Request $request)
    {
    	if ($request->ajax()) {
    		$municipalities = Municipality::where('state_id', $request->state_id)->get();

    		foreach ($municipalities as $municipality) {
    			$municipalitiesArray[$municipality->id] = $municipality->name;
    		}
    	}

    	return response()->json($municipalitiesArray);
    }

    /**
     * Obtiene los municipios de un estado
     *
     * @author  William Páez <paez.william8@gmail.com>
     * @return \Illuminate\Http\JsonResponse    JSON con los datos de los municipios
     */
    public function listMunicipalities($state_id)
    {
        return response()->json(template_choices(Municipality::class, 'name', ['state_id' => $state_id], true));
    }
}
