<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\City;

class CityController extends Controller
{
    public function getCities(Request $request)
    {
    	if ($request->ajax()) {
    		$cities = City::where('state_id', $request->state_id)->get();

    		foreach ($cities as $city) {
    			$citiesArray[$city->id] = $city->name;
    		}
    	}

    	return response()->json($citiesArray);
    }
}
