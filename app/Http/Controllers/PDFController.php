<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Codedge\Fpdf\Fpdf\Fpdf;

use App\User;
use App\Enrollment;
use App\Student;
use App\representative;
use Carbon\Carbon;
use DNS1D;

class PDFController extends Controller
{
    private $fpdf;

    public function __construct()
    {
         
    }

    function Header($school)
    {
        $address = utf8_decode($school->address.', parroquia '.$school->parish->name.' municipio '.$school->parish->municipality->name.'. '.$school->city->name.', '.$school->parish->municipality->state->name);
        $this->fpdf->Image(PUBLIC_PATH('storage/pictures/logos/'.$school->logo),9,9,20,20);


        if($school->dea_code) $dea_code = 'Código DEA: '.$school->dea_code; else $dea_code = "";
        if($school->stadistic_code) $stadistic_code = 'Código Estadístico: '.$school->stadistic_code; else $stadistic_code = "";
        if($school->phone) $phone = 'teléfono: '.$school->phone; else $phone = "";
        if($school->fax) $fax = '- fax: '.$school->fax; else $fax = "";
        if($school->rif) $rif = 'rif: '.$school->rif; else $rif = "";
        
        $this->fpdf->SetFont('Arial','B',9);
        $this->fpdf->SetX(30);$this->fpdf->MultiCell(126,4,utf8_decode($school->school_type.' '.$school->name),0,'C');
        $this->fpdf->SetFont('Arial','',7);
        $this->fpdf->SetX(30);$this->fpdf->MultiCell(126,4,utf8_decode($dea_code.' - '.$stadistic_code) ,0,'C');
        $this->fpdf->SetX(30);$this->fpdf->MultiCell(126,4,$address,0,'C');
        $this->fpdf->SetX(30);$this->fpdf->MultiCell(126,4,utf8_decode($phone.' '.$fax ),0,'C');
        $this->fpdf->SetX(30);$this->fpdf->MultiCell(126,4,$rif,0,'C');
    }


    function Footer($school)
    {

        $address = utf8_decode($school->address.', parroquia '.$school->parish->name.' municipio '.$school->parish->municipality->name.'. '.$school->city->name.', '.$school->parish->municipality->state->name);

        if($school->phone) $phone = 'teléfono: '.$school->phone; else $phone = "";
        if($school->fax) $fax = '- fax: '.$school->fax; else $fax = "";
        if($school->rif) $rif = '- rif: '.$school->rif; else $rif = "";

        $this->fpdf->setFont("Times","",8);
        $this->fpdf->SetXY(10,-37);
        $this->fpdf->MultiCell(196,4,'_________________________________________________________________________________________________________________________________________',0,'L');
        $this->fpdf->SetX(10);$this->fpdf->MultiCell(196,4,$address,0,'C');
        $this->fpdf->SetX(10);$this->fpdf->MultiCell(196,4,utf8_decode($phone.' '.$fax.' '.$rif),0,'C');
        $this->fpdf->SetX(10);$this->fpdf->MultiCell(196,4,utf8_decode('documento generado por SIREA - Clases IT - https://clasesit.com'),0,'C');
    }

    public function newRepresentative($representative){
        $this->fpdf->SetFont('Arial','B',9);
        $this->fpdf->Cell(196,5,'Datos del Representente ('.$representative->relationship->name.')',7,1,"C", True);
        $this->fpdf->Ln(2);
        $this->fpdf->SetFont('Arial','B',7);
        $this->fpdf->Cell(49,4,"Primer Apellido",0,0,"L",True);
        $this->fpdf->Cell(49,4,"Segundo Apellido",0,0,"L",True);
        $this->fpdf->Cell(49,4,"Primer Nombre",0,0,"L",True);
        $this->fpdf->Cell(49,4,"Segundo Nombre",0,1,"L",True);
        
        $this->fpdf->SetFont('Arial','',7);
        $this->fpdf->Cell(49,4,utf8_decode($representative->last_name_1),0,0,"L");
        $this->fpdf->Cell(49,4,utf8_decode($representative->last_name_2),0,0,"L");
        $this->fpdf->Cell(49,4,utf8_decode($representative->first_name_1),0,0,"L");
        $this->fpdf->Cell(49,4,utf8_decode($representative->first_name_2),0,1,"L");

        $ageR1 = Carbon::parse($representative->birthdate)->age;
        $birthdateR1 = Carbon::parse($representative->birthdate)->format('d-m-Y');
        $this->fpdf->SetFont('Arial','B',7);
        $this->fpdf->Cell(49,4,"Documento",0,0,"L",True);
        $this->fpdf->Cell(49,4,utf8_decode("País de nacimiento"),0,0,"L",True);
        $this->fpdf->Cell(49,4,"Fecha de nacimiento",0,0,"L",True);
        $this->fpdf->Cell(49,4,"Edad",0,1,"L",True);
        $this->fpdf->SetFont('Arial','',7);
        $this->fpdf->Cell(49,4,$representative->document_type.'-'.$representative->document_number,0,0,"L");
        $this->fpdf->Cell(49,4,utf8_decode($representative->country->name),0,0,"L");
        $this->fpdf->Cell(49,4,$birthdateR1,0,0,"L");
        $this->fpdf->Cell(49,4,utf8_decode($ageR1.' años'),0,1,"L");
    }

    public function planilla($id)
    {
        $auth    = auth()->user();
        $student = Student::find($id);

        if(!$student) return back()->with('error', 'No existe el estudiante.');

        if($auth->hasRole('family')){
            $userFamily_id = $auth->family->id;
            if($userFamily_id != $student->family->id) return back()->with('error', 'Solo puede ver integrantes de su grupo familiar');
        }else{
            if($auth->userSchool->school_id != $student->family->school_id) return back()->with('error','Solo puede ver integrantes de su colegio');
        }

        $family  = $student->family;
        $school  = $family->school;
        $representatives = $family->representatives;
        $season          = $school->seasons->where('status', 1)->last();

        if(!$representatives) return back()->with('msg', 'la familia no tiene representantes');
        if(!$season) return back()->with('msg', 'No existe un año escolar');


        foreach ($representatives as $representative) {
            if($representative->relationship->name == "Madre") $representante1 = $representative;
            if($representative->relationship->name == "Padre") $representante2 = $representative;
            if($representative->relationship->name != "Padre" && $representative->relationship->name != "Madre") $representante3 = $representative;
            if($representative->relationship->name != "Padre" && $representative->relationship->name != "Madre" && $representative != $representante3) $representante4 = $representative;
        }

        $this->fpdf = new Fpdf('P', 'mm', 'letter');
        $this->fpdf->AddPage();

        $this->fpdf->AliasNbPages(); 

        $this->header($school);

        $this->fpdf->Image(PUBLIC_PATH('storage/pictures/avatars/'.$student->profile->avatar),182,9,24,24);

        $this->fpdf->SetFont('Helvetica','B',14);
        $this->fpdf->SetY(32);
        $this->fpdf->SetX(10);$this->fpdf->MultiCell(196,8,utf8_decode("PLANILLA"),0,'C');
        $this->fpdf->SetFont('Helvetica','B',12);
        $this->fpdf->SetX(10);$this->fpdf->MultiCell(196,8,utf8_decode('AÑO ESCOLAR: '.$season->season),0,'C');

        $this->fpdf->Ln(2);
        $this->fpdf->SetFont('Helvetica','',7);
        $this->fpdf->Cell(90,3,utf8_decode("* Lea detenidamente esta planilla, los datos suministrados deben ser exactos y ajustados a la realidad, de lo contrario será invalidada"),0,1,'L');
        $this->fpdf->Ln(2);
        $this->fpdf->SetFont('Arial','B',9);

        $this->fpdf->SetFillColor(228,228,228);
        $this->fpdf->Cell(196,5,"DATOS PERSONALES DEL ESTUDIANTE",0,1,"C", True);
        $this->fpdf->Ln(2);
        $this->fpdf->SetFont('Arial','B',7);
        $this->fpdf->Cell(49,4,"Primer Apellido",0,0,"L",True);
        $this->fpdf->Cell(49,4,"Segundo Apellido",0,0,"L",True);
        $this->fpdf->Cell(49,4,"Primer Nombre",0,0,"L",True);
        $this->fpdf->Cell(49,4,"Segundo Nombre",0,1,"L",True);
        
        $this->fpdf->SetFont('Arial','',7);
            
        $this->fpdf->Cell(49,4,utf8_decode($student->profile->last_name_1),0,0,"L");
        $this->fpdf->Cell(49,4,utf8_decode($student->profile->last_name_2),0,0,"L");
        $this->fpdf->Cell(49,4,utf8_decode($student->profile->first_name_1),0,0,"L");
        $this->fpdf->Cell(49,4,utf8_decode($student->profile->first_name_2),0,1,"L");
        
        $this->fpdf->SetFont('Arial','B',7);
        $this->fpdf->Cell(49,4,"Documento",0,0,"L",True);
        $this->fpdf->Cell(49,4,utf8_decode("País de Nacimiento"),0,0,"L",True);
        $this->fpdf->Cell(49,4,'Fecha de nacimiento',0,0,"L",True);                
        $this->fpdf->Cell(49,4,"Edad",0,1,"L",True);
        
        $age = Carbon::parse($student->profile->birthdate)->age;
        $birthdate = Carbon::parse($student->profile->birthdate)->format('d-m-Y');
        $this->fpdf->SetFont('Arial','',7);
        $this->fpdf->Cell(49,4,$student->profile->document_type.'-'.$student->profile->document_number,0,0,"L");;
        $this->fpdf->Cell(49,4,utf8_decode($student->profile->country->name),0,0,"L");
        $this->fpdf->Cell(49,4,$birthdate,0,0,"L");               
        $this->fpdf->Cell(49,4,utf8_decode($age.' años'),0,1,"L");

        $this->fpdf->SetFont('Arial','B',7);
        $this->fpdf->Cell(49,4,utf8_decode("Correo electrónico"),0,0,"L",True);
        $this->fpdf->Cell(49,4,utf8_decode("Teléfono"),0,0,"L",True);
        $this->fpdf->Cell(49,4,"",0,0,"L",True);
        $this->fpdf->Cell(49,4,"",0,1,"L",True);

        $this->fpdf->SetFont('Arial','',7);
            
        $this->fpdf->Cell(49,4,utf8_decode($student->profile->user->email),0,0,"L");
        $this->fpdf->Cell(49,4,utf8_decode($student->profile->phone),0,0,"L");
        $this->fpdf->Cell(49,4,"",0,0,"L");
        $this->fpdf->Cell(49,4,"",0,1,"L");

        $this->fpdf->SetFont('Arial','B',9);

        $this->fpdf->SetFillColor(228,228,228);
        $this->fpdf->Cell(196,5,'Familia '.$student->family->last_name_1.' '.$student->family->last_name_2,0,1,"C", True);

        $this->fpdf->Ln(2);
        $this->fpdf->SetFont('Arial','',7);
        //$this->fpdf->Cell(98,4,utf8_decode(),0,0,"L");
        $this->fpdf->Cell(196,4,utf8_decode('Correo Electronico: '.$student->family->user->email.'  - Télefono: '.$student->family->phone ),0,1,"C");

        $family_direction = $student->family->address.', parroquia '.$student->family->parish->name.', municipio '.$student->family->parish->municipality->name.'. '.$student->family->city->name.' edo. '.$student->family->city->state->name;
        $this->fpdf->Cell(196,4,utf8_decode('Dirección: '.$family_direction),0,1,"C");

        $this->fpdf->Ln(2);

        $r=0;
        if(isset($representante1)){
            $r++;
            if($r == 1)$this->fpdf->Image(PUBLIC_PATH('storage/pictures/avatars/'.$representante1->avatar),156,9,24,24);
            $this->newRepresentative($representante1);
        }
        if(isset($representante2)){
            $r++;
            if($r == 1)$this->fpdf->Image(PUBLIC_PATH('storage/pictures/avatars/'.$representante2->avatar),156,9,24,24);
            $this->newRepresentative($representante2);
        }
        if(isset($representante3) && $r < 2){
            $r++;
            if($r == 1)$this->fpdf->Image(PUBLIC_PATH('storage/pictures/avatars/'.$representante3->avatar),156,9,24,24);            
            $this->newRepresentative($representante3);
        }
        if(isset($representante4) && $r < 2){
            $this->newRepresentative($representante4);
        }

        $this->fpdf->Ln(2);

        $this->fpdf->SetFont('Arial','B',10);
        $this->fpdf->Cell(196,5,utf8_decode("INFORMACIÓN PARA LA INSCRIPCIÓN"),0,1,"C");
        $this->fpdf->Ln(2);

        $this->fpdf->SetFont('Arial','B',8);
        $this->fpdf->Cell(80,5,utf8_decode("Grado o Año a Cursar:"),0,0,"L",True);
        $this->fpdf->Cell(116,5,"Grupo asignado",0,1,"L",True);
        $this->fpdf->SetFont('Arial','B',14);
        $this->fpdf->Cell(80,10,utf8_decode($student->grade),0,0,"L");
        $this->fpdf->SetFont('Arial','',8);
        $this->fpdf->Cell(116,10,"_____________________",0,1,"L");
        $this->fpdf->SetFont('Arial','',8);
        $this->fpdf->Cell(196,10,"Observaciones: _______________________________________________________________________________________________________________",0,1,"L");

        $this->fpdf->Ln(2);
        $this->fpdf->SetFont('Arial','',7);
        $this->fpdf->SetX(10);
        $this->fpdf->MultiCell(0,4,utf8_decode('COMPROMISO DEL REPRESENTANTE: Hago constar por medio de la presente, que he leído, acepto y me comprometo a cumplir las condiciones establecidas en el contrato de Prestación de Servicio educativo para el año escolar '.$season->season.' así como los deberes y obligaciones conforme a las leyes y reglamentos vigentes del Estado Venezolano. Del mismo modo, mi representado y yo, nos comprometemos a respetar los acuerdos de Convivencia de la Institución.
Importante: El Proceso de Inscripción Año Escolar '.$season->season.' se concretará una vez efectuado el pago por concepto de inscripción y primer mes, la consignación física de la Planilla de Registro y firma del Contrato de Prestación de Servicios Educativos.'),1,'J');

/*El Proceso de Inscripción Año Escolar '.$season->season.' se concretará una vez sea posible la consignación física y presencial de la Planilla de Registro,  el Contrato de Prestación de Servicios Educativos Suscrito y la realización del pago por concepto de inscripción próximo a acordarse de acuerdo a la normativa que rige la materia.
            Compromiso: Hago constar por medio de la presente, que me comprometo a cumplir las obligaciones que me corresponden según las Disposiciones legales vigentes, el Reglamento Interno y las Recomendaciones procedentes de las autoridades del Plantel. Me comprometo a prestar la mayor colaboración para que el Instituto y el hogar logren la formación Humano-Cristiana de mi representado(s).*/

        $this->fpdf->SetFont('Arial','',8);
        $this->fpdf->Ln(11);
            $this->fpdf->Cell(98,4,"_________________________________________",0,0,"C");
            $this->fpdf->Cell(98,4,"_________________________________________",0,1,"C");
            $this->fpdf->Cell(98,4,"                       REPRESENTANTE, C.I.:",0,0,"L");
            $this->fpdf->Cell(98,4,"ESTUDIANTE",0,1,"C");
            $this->fpdf->Ln(8);
            $this->fpdf->Cell(98,4,"_________________________________________",0,0,"C");
            $this->fpdf->Cell(98,4,"_________________________________________",0,1,"C");
            $this->fpdf->Cell(98,4,"                                                  RECTOR",0,0,"L");
            $this->fpdf->Cell(98,4,"DIRECTOR",0,1,"C");

        $this->Footer($school);

        if($school->additional_data && $family->poll){

            $this->fpdf->AddPage();

            $this->fpdf->AliasNbPages(); 

            $this->header($school);

            $this->fpdf->Ln(2);
            $this->fpdf->SetFont('Arial','B',10);

            $this->fpdf->SetFillColor(228,228,228);
            $this->fpdf->Cell(196,5,utf8_decode('INFORMACIÓN ADICIONAL DE INTERÉS PARA EL COLEGIO'),0,1,"C", True);
            $this->fpdf->Ln(2);
            $this->fpdf->Cell(196,5,utf8_decode('Transporte'),0,1,"C", True);

            $this->fpdf->Ln(1);
            $this->fpdf->SetFont('Arial','B',8);
            $this->fpdf->Cell(196,5,"Personas con las que llega el estudiante al colegio",0,1,"L",True);
            $this->fpdf->SetFont('Arial','',8);
            $this->fpdf->Cell(196,5,utf8_decode($family->poll->companion),0,1,"L");

            $this->fpdf->SetFont('Arial','B',8);
            $this->fpdf->Cell(98,5,"Medio de llegada al colegio",0,0,"L",True);
            $this->fpdf->Cell(98,5,"Medio de salida del colegio",0,1,"L",True);
            
            $this->fpdf->SetFont('Arial','',8);
            $this->fpdf->Cell(98,5,utf8_decode($family->poll->arrival),0,0,"L");
            $this->fpdf->Cell(98,5,utf8_decode($family->poll->departure),0,1,"L");

            $this->fpdf->Ln(1);
            $this->fpdf->SetFont('Arial','B',10);
            $this->fpdf->SetFillColor(228,228,228);
            $this->fpdf->Cell(196,5,utf8_decode('Aspectos Sociales'),0,1,"C", True);

            $this->fpdf->SetFont('Arial','B',8);
            $this->fpdf->Ln(1);
            $this->fpdf->Cell(98,5,"Estado Civil de los padres",0,0,"L",True);
            $this->fpdf->Cell(98,5,utf8_decode("Religión de la familia"),0,1,"L",True);
            
            $this->fpdf->SetFont('Arial','',8);
            $this->fpdf->Cell(98,5,utf8_decode($family->poll->marital_status),0,0,"L");
            $this->fpdf->Cell(98,5,utf8_decode($family->poll->religion),0,1,"L");

            $this->fpdf->Ln(1);
            $this->fpdf->SetFont('Arial','B',10);
            $this->fpdf->SetFillColor(228,228,228);
            $this->fpdf->Cell(196,5,utf8_decode('Información de la vivienda familiar'),0,1,"C", True);

            $this->fpdf->SetFont('Arial','B',8);
            $this->fpdf->Ln(1);
            $this->fpdf->Cell(98,5,"Tipo de propiedad de la vivienda",0,0,"L",True);
            $this->fpdf->Cell(98,5,utf8_decode("Tipo de estructura de la vivienda"),0,1,"L",True);
            
            $this->fpdf->SetFont('Arial','',8);
            $this->fpdf->Cell(98,5,utf8_decode($family->poll->property_type),0,0,"L");
            $this->fpdf->Cell(98,5,utf8_decode($family->poll->home_type),0,1,"L");

            $this->fpdf->Ln(1);
            $this->fpdf->SetFont('Arial','B',10);
            $this->fpdf->SetFillColor(228,228,228);
            $this->fpdf->Cell(196,5,utf8_decode('Otros datos de interés'),0,1,"C", True);

            $this->fpdf->SetFont('Arial','B',8);
            $this->fpdf->Ln(1);
            $this->fpdf->Cell(196,5,utf8_decode("Teléfono para llamar en caso de emergencias"),0,1,"L",True);
            $this->fpdf->SetFont('Arial','',8);
            $this->fpdf->Cell(196,5,utf8_decode($family->poll->emergency_phone),0,1,"L");

            $this->fpdf->Cell(196,5,utf8_decode("Descripción del tipo de póliza de salud del estudiante en caso que la posea"),0,1,"L",True);
            
            $this->fpdf->SetFont('Arial','',8);
            $this->fpdf->Cell(196,5,utf8_decode($student->health_insurance),0,1,"L");

            $this->fpdf->Cell(196,5,utf8_decode("Datos de personas que acompañan y retiran al estudiante del colegio"),0,1,"L",True);
            
            $this->fpdf->SetFont('Arial','',8);
            $this->fpdf->Cell(196,5,utf8_decode($family->poll->companion),0,1,"L");            







            $this->fpdf->Ln(2);
            $this->fpdf->SetFont('Arial','',8);
            $this->fpdf->SetX(10);
     
            $this->fpdf->Ln(11);
            $this->fpdf->Cell(98,4,"_________________________________________",0,0,"C");
            $this->fpdf->Cell(98,4,"_________________________________________",0,1,"C");
            $this->fpdf->Cell(98,4,"                       REPRESENTANTE, C.I.:",0,0,"L");
            $this->fpdf->Cell(98,4,"ESTUDIANTE",0,1,"C");
            $this->fpdf->Ln(8);
            $this->fpdf->Cell(98,4,"_________________________________________",0,0,"C");
            $this->fpdf->Cell(98,4,"_________________________________________",0,1,"C");
            $this->fpdf->Cell(98,4,"                                                  RECTOR",0,0,"L");
            $this->fpdf->Cell(98,4,"DIRECTOR",0,1,"C");

            $this->Footer($school);
        }

        $this->fpdf->Output('planilla_'.$student->profile->first_name_1.'_'.$student->profile->last_name_1.'.pdf',"D");
        exit;
    }


    public function studentCarnet($id)
    {

        $auth    = auth()->user();
        $enrollment = Enrollment::find($id);

        if(!$enrollment) return back()->with('error', 'No existe el estudiante.');        

        if($auth->hasRole('family')){
            $userFamily_id = $auth->family->id;
            if($userFamily_id != $student->family->id) return back()->with('error', 'Solo puede ver integrantes de su grupo familiar');
        }else{
            if($auth->userSchool->school_id != $student->family->school_id) return back()->with('error','Solo puede ver integrantes de su colegio');
        }

        $student = $enrollment->student;
        $section = $enrollment->section;
        $family  = $student->family;
        $school  = $family->school;
        $season = $school->seasons->where('status', 1)->last();

        //Barcode = school_id+family_id+student_id+season_id+eronllment_id;
        
        $barcode = 'c'.$school->id.'f'.$family->id.'s'.$student->id.'t'.$season->id.'e'.$enrollment->id;


        $imageBarcode = '<img src="data:image/png;base64,' . DNS1D::getBarcodePNG('4', 'C39+') . '" alt="barcode"   />';

        $this->fpdf = new Fpdf('P', 'mm', 'letter');
        $this->fpdf->AddPage();

        $this->fpdf->AliasNbPages(); 

        $this->header($school);

        $this->fpdf->Image(PUBLIC_PATH('themeforest/assets/images/bg-carnet.png'),81, 102, 54, 85);
        $this->fpdf->Image(PUBLIC_PATH('storage/pictures/logos/'.$school->logo),84, 105, 14, 14);
        $this->fpdf->Image(PUBLIC_PATH('storage/pictures/avatars/'.$student->profile->avatar),96.75, 129.5, 30, 30);
        //$this->fpdf->Image('data:image/png;base64,' . DNS1D::getBarcodePNG("4", "C39+",3,33,array(1,1,1), true),96.75, 129.5, 30, 30);

        $this->fpdf->SetFont('Helvetica','B',7);
        $this->fpdf->SetXY(97, 108);
        $this->fpdf->MultiCell(36,3,utf8_decode($school->document_type.' '.$school->name),0,'C');
        $this->fpdf->SetFont('Helvetica','',6);
        $this->fpdf->SetX(97);
        $this->fpdf->MultiCell(36,3,utf8_decode(utf8_encode('ciudad, Estado')),0,'C');

        $this->fpdf->SetFont('Helvetica','B',9);
        $this->fpdf->SetXY(81, 120);
        $this->fpdf->MultiCell(55,4,utf8_decode('Año Escolar: Año'),0,'C');
        $this->fpdf->SetXY(83, 161);
        $this->fpdf->MultiCell(50,4,utf8_decode('Nombre Estudiante'),0,'C');

        $this->fpdf->SetFont('Helvetica','B',7);
        $this->fpdf->SetXY(81, 124);
        $this->fpdf->MultiCell(55,4,utf8_decode('Grado Sección: S'),0,'C');

        $this->fpdf->SetFont('Helvetica','B',12);
        $this->fpdf->SetXY(81, 170);
        $this->fpdf->Cell(55,5,"ESTUDIANTE",0,1,'C');

        $this->fpdf->Output();
    }

    public function  constancia($id)
    {
        $auth    = auth()->user();
        $enrollment = Enrollment::find($id);
        $student = $enrollment->student;

        if(!$enrollment) return back()->with('error', 'No existe el estudiante.');        

        if($auth->hasRole('family')){
            $userFamily_id = $auth->family->id;
            if($userFamily_id != $student->family->id) return back()->with('error', 'Solo puede ver integrantes de su grupo familiar');
        }else{
            if($auth->userSchool->school_id != $student->family->school_id) return back()->with('error','Solo puede ver integrantes de su colegio');
        }

        $section = $enrollment->section;
        $family  = $student->family;
        $school  = $family->school;
        $director = $school->director;

        if(!$director) return back()->with('error', 'Faltan datos por parte del colegio');

        $season = $school->seasons->where('status', 1)->last();

        $fecha = now();
        $date = $fecha->locale(); //con esto revise que el lenguaje fuera es 
        $mes = $fecha->monthName; //y con esta obtengo el mes al fin en español!

        if($director->gender->name = "Masculino")
        {
            $director2 = 'ciudadano '.strtoupper($director->name).', portador';
        }else{
            $director2 = 'ciudadana '.strtoupper($director->name).', portadora';
        }

        $student_name = $student->profile->first_name_1.' '.$student->profile->first_name_2.' '.$student->profile->last_name_1.' '.$student->profile->last_name_2;
        if($student->profile->gender->name = "Masculino")
        {
            $student2 = 'el estudiante: '.strtoupper($student_name);
        }else{
            $student2 = 'la estudiante: '.strtoupper($student_name);
        }

        $html = 'Quien suscribe, '.$director2.' de la Cédula de Identidad '.$director->document_type.'-'.$director->document_number.', Director de '.$school->school_type.' '.$school->name.', ubicado en '.$school->address.', hace constar por medio de la presente que '.$student2.', titular del documento de identidad: '.$student->profile->document_type.'-'.$student->profile->document_number.', natural de '.$student->family->parish->municipality->state->name.', cursa el '.$enrollment->section->grade->name.' DEL SUBSISTEMA DE EDUCACIÓN NIVEL '.strtoupper($enrollment->section->grade->level->name).'. En el Año Escolar '.$season->season;

        $this->fpdf = new Fpdf('P', 'mm', 'letter');
        $this->fpdf->AddPage();
        $this->fpdf->Image(PUBLIC_PATH('themeforest/assets/images/membrete.jpg'),25,9,166,19);
        $address = utf8_decode($school->address.', parroquia '.$school->parish->name.' municipio '.$school->parish->municipality->name.'. '.$school->city->name.', '.$school->parish->municipality->state->name);
        $this->fpdf->Image(PUBLIC_PATH('storage/pictures/logos/'.$school->logo),25,30,20,20);


        if($school->dea_code) $dea_code = 'Código DEA: '.$school->dea_code; else $dea_code = "";
        if($school->stadistic_code) $stadistic_code = 'Código Estadístico: '.$school->stadistic_code; else $stadistic_code = "";
        if($school->phone) $phone = 'teléfono: '.$school->phone; else $phone = "";
        if($school->fax) $fax = '- fax: '.$school->fax; else $fax = "";
        if($school->rif) $rif = 'rif: '.$school->rif; else $rif = "";
        
        $this->fpdf->setY(30);
        $this->fpdf->SetFont('Arial','B',9);
        $this->fpdf->SetX(45);$this->fpdf->MultiCell(126,4,utf8_decode($school->school_type.' '.$school->name),0,'C');
        $this->fpdf->SetFont('Arial','',7);
        $this->fpdf->SetX(45);$this->fpdf->MultiCell(126,4,utf8_decode($dea_code.' - '.$stadistic_code) ,0,'C');
        $this->fpdf->SetX(45);$this->fpdf->MultiCell(126,4,$address,0,'C');
        $this->fpdf->SetX(45);$this->fpdf->MultiCell(126,4,utf8_decode($phone.' '.$fax ),0,'C');
        $this->fpdf->SetX(45);$this->fpdf->MultiCell(126,4,$rif,0,'C');

        $this->fpdf->SetXY(25,70);
        $this->fpdf->SetFont('Times','B',16);
        $this->fpdf->Cell(166,5,'CONSTANCIA DE ESTUDIOS',0,1,'C');
        $this->fpdf->Ln(16);

        $this->fpdf->SetX(25);
        $this->fpdf->SetFont('Times','',14);
        $this->fpdf->MultiCell(160,8,utf8_decode($html),0,'J');
        
        $this->fpdf->Ln(4);
        $this->fpdf->SetX(25);
        $this->fpdf->MultiCell(160,8,utf8_decode('Constancia que se expide a solicitud de la parte interesada en '.utf8_encode($school->city->name).' a los '.date('d').' días, del mes de '.strtoupper($mes).' de '.date('Y').'.'),0,'J');
        $this->fpdf->Ln(4);
        $this->fpdf->SetXY(25,220);
        $this->fpdf->MultiCell(160,6,utf8_decode("____________________________"),0,'C');
        $this->fpdf->SetFont('Times','',12);
        $this->fpdf->SetX(25);
        $this->fpdf->MultiCell(160,6,utf8_decode($director->name),0,'C');
        $this->fpdf->SetX(25);
        $this->fpdf->MultiCell(160,6,$director->document_type.' - '.$director->document_number,0,'C');
        $this->fpdf->SetX(25);
        if($director->gender->name=='Masculino') $this->fpdf->MultiCell(160,6,'Director',0,'C');
        else $this->fpdf->MultiCell(160,10,'Directora',0,'C');

        $this->footer($school);

        $this->fpdf->Output('constancia_'.$student->profile->first_name_1.'_'.$student->profile->last_name_1.'.pdf', 'D');
    }

    public function bc()
    {
        //para Pruebas
echo DNS1D::getBarcodeSVG('4445645656', 'C128',3,33,'green', true);
echo DNS1D::getBarcodeHTML('4445645656', 'C128',3,33,'green', true);
echo '<img src="' . DNS1D::getBarcodePNG('4', 'C39+',3,33,array(1,1,1), true) . '" alt="barcode"   />';
echo DNS1D::getBarcodePNGPath('4445645656', 'C128',3,33,array(255,255,0), true);
echo '<img src="data:image/png;base64,' . DNS1D::getBarcodePNG('4', 'C39+',3,33,array(1,1,1), true) . '" alt="barcode"   />';
    }

}
