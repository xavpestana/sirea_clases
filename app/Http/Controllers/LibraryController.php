<?php

namespace App\Http\Controllers;

use App\Library;
use App\Courses_sections_user;
use Illuminate\Http\Request;

class LibraryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('library.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        foreach ($request->levels as $level) {
            $task = new Library;
            $task->name = session('name');
            $task->url = session('url');
            $task->extension = session('extension');
            $task->level = $level;
            $task->amount = $request->amount;
            $task->save();
        }
        
        session()->forget('url');
        session()->forget('extension');
        session()->forget('name');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $date = date('Y-m-d');
        $files = $request->file('file');
        $nameFile = $files->getClientOriginalName();
        $extension = pathinfo($nameFile, PATHINFO_EXTENSION);
        
        if (auth()->user()->hasRole('school')) {
            
                $path = public_path().'/library/';
                $files->move($path, $nameFile);
                $request->session()->put('url', '/library/'.$nameFile);
                $request->session()->put('extension', $extension);
                $request->session()->put('name', $nameFile);

    }

    return 0;
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Library  $library
     * @return \Illuminate\Http\Response
     */
    public function student(Request $request)
    {
        if (auth()->user()->profile->student->grade == '1er Grado' || auth()->user()->profile->student->grade == '2do Grado') {
            $level = 'kids';
        }
        if (auth()->user()->profile->student->grade == '3er Grado' || auth()->user()->profile->student->grade == '4to Grado' || auth()->user()->profile->student->grade == '5to Grado') {
            $level = 'reformers';
        }
        if (auth()->user()->profile->student->grade == '6to Grado' || auth()->user()->profile->student->grade == '1er Año' || auth()->user()->profile->student->grade == '2do Año') {
            $level = 'millenium';
        }
        if (auth()->user()->profile->student->grade == '3er Año' || auth()->user()->profile->student->grade == '4to Año' || auth()->user()->profile->student->grade == '5to Año') {
            $level = 'influencers';
        }

        $libraries = Library::where('level', $level)->get();
        
        return view('student.library', compact('libraries'));
    }

    /**
     * Docente.
     *
     * @return \Illuminate\Http\Response
     */
    public function teacher()
    {
        $courses = Courses_sections_user::where('user_id',auth()->user()->id)->get();
        //dd($courses[0]->Section->grade);
        return view('teacher.library',compact('courses'));
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Library  $library
     * @return \Illuminate\Http\Response
     */
    public function show(Library $library)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Library  $library
     * @return \Illuminate\Http\Response
     */
    public function edit(Library $library)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Library  $library
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Library $library)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Library  $library
     * @return \Illuminate\Http\Response
     */
    public function destroy(Library $library)
    {
        //
    }
}
