<?php

namespace App\Http\Controllers;

use App\quotasCancelled;
use Illuminate\Http\Request;

class QuotasCancelledController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\quotasCancelled  $quotasCancelled
     * @return \Illuminate\Http\Response
     */
    public function show(quotasCancelled $quotasCancelled)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\quotasCancelled  $quotasCancelled
     * @return \Illuminate\Http\Response
     */
    public function edit(quotasCancelled $quotasCancelled)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\quotasCancelled  $quotasCancelled
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, quotasCancelled $quotasCancelled)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\quotasCancelled  $quotasCancelled
     * @return \Illuminate\Http\Response
     */
    public function destroy(quotasCancelled $quotasCancelled)
    {
        //
    }
}
