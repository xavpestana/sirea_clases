<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Str;
use App\Mail\TeacherReceived;
use App\Mail\PasswordTeacherReceived;
use Illuminate\Http\Request;
use App\Courses_sections_user;
use App\Profile;
use App\User;
use App\Teacher;

use Image;

class TeacherController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('school.teachers');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $name = $request->name_teacher.' '.$request->last_name_teacher;

        /*
        Creacion de usuario
        */
        $user = new User;
        $user->name = $name;
        $user->email = $request->email;
        $user->password = Hash::make($request->document);
        $user->save();
        
       /*
        Creacion de perfil
        */

        $profile = new Profile;
        $profile->user_id = $user->id;
        $profile->document_type = $request->type_document;
        $profile->document_number = $request->document;
        $profile->first_name_1 = $request->name_teacher;
        $profile->last_name_1 = $request->last_name_teacher;
        $profile->last_name_2 = $request->last_name_teacher;
        $profile->birthdate = '2000-06-25';
        $profile->phone = '- - -';
        $profile->country_id = '1';
        $profile->gender_id = $request->gender;
        $profile->save();

        /*
        Usuario Teacher
        */

        $teacher = new Teacher;
        $teacher->profile_id = $profile->id;
        $teacher->school_id = auth()->user()->UserSchool->school_id;
        $teacher->save();

        $user->assignRole('teacher');

        if (!is_null($user) && !is_null($profile)&& !is_null($teacher)) {

            $datos = array('email'=> $request->email, 'password'=> $request->document);
            Mail::to($request->email)->send(new TeacherReceived($datos));
            return 0;
        } else {
            return 1;
        }
        
        
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
       //
    }
    
    /**
     * Show table teachers
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function table(Request $request)
    {
        $teachers = Teacher::where('school_id', auth()->user()->UserSchool->school_id)->get();
           
        return view('teacher.tables.teacher',compact('teachers'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Actualiza perfil del profesor.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function profile(Request $request)
    {
        //dd($request->all());

        $name = $request->first_name.' '.$request->last_name;

       Teacher::where('profile_id', auth()->user()->profile->id)
          ->update([
            'address' => $request->adress,
        ]);
          
        Profile::where('user_id', auth()->user()->id)
          ->update([
            'document_type' => $request->document_type,
            'document_number' => $request->document,
            'first_name_1' => $request->first_name,
            'last_name_1' => $request->last_name,
            'last_name_2' => $request->last_name,
            'phone' => $request->phone,
            'gender_id' => $request->gender,
        ]);

        $user = User::find(auth()->user()->id);
        $user->name = $name;
        if (!is_null($request->password)) {
            $user->password = Hash::make($request->password);
        }
        $user->save();

        return 0;
    }

    /**
     * Actualizar imagen de perfil.
     *
     * @param  request  $request
     * @return \Illuminate\Http\Response
     */
    public function avatar(Request $request)
    {
        
        $originalImage= $request->file('avatar');
        $image = Image::make($originalImage);
        $originalPath = public_path().'/storage/pictures/avatars/';
        //Nombre aleatorio para la image
        $tempName = Str::random(20) . '.' . $originalImage->getClientOriginalExtension();

        //Redimensinoar la imagen
        if($image->width() >= $image->height()) $image->heighten(400);
        else $image->widen(400);
        $image->resizeCanvas(400,400);

        $image->save($originalPath.$tempName);

        Profile::where('user_id', auth()->user()->id)
          ->update([
            'avatar' => $tempName,
        ]);

        return $tempName;
    }
    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $teacher = Teacher::where('profile_id', $id)->first();

        return view('teacher.modal.edit_profile', compact('teacher'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $name = $request->name_teacher.' '.$request->last_name_teacher;

        Profile::where('id', $request->profile_id)
          ->update([
            'document_type' => $request->type_document,
            'document_number' => $request->document,
            'first_name_1' => $request->name_teacher,
            'last_name_1' => $request->last_name_teacher,
            'last_name_2' => $request->last_name_teacher,
            'gender_id' => $request->gender,
        ]);

        $user = User::find($request->id_user);
        $user->name = $name;
        $user->email = $request->email;
        $user->save();

        return 0;
    }

    /**
     * Update the specified password in db.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function psw(Request $request)
    {
        $user = User::find($request->id_user);
        $user->password = Hash::make($request->new_password);
        $user->save();
        
        if ($user) {

            $datos = array('password'=> $request->new_password);
            Mail::to($user->email)->send(new PasswordTeacherReceived($datos));

            return 0;
        } else {
            return 1;
        }
        
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {

        $id_profile = Profile::where('user_id', $id)->first();
        $course_sections = Courses_sections_user::where('user_id', $id)->first();
        if (!$course_sections) {
            $user = User::where('id', $id)->delete();
            $profile = Profile::where('user_id', $id)->delete();
            $teacher = Teacher::where('profile_id', $id_profile->id)->delete();

            if (!is_null($user) && !is_null($profile) && !is_null($teacher)) {
                return 0;
            } else {
                return 1;
            }
        } else {
            return 2;
        }
        
        
        
        
    }
}
