<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Redirect;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $user = auth()->user();
        if($user->hasRole('admin')){
            $school = null;
        }else{
            if($user->hasRole('school')) $school = $user->userSchool->school;
            if($user->hasRole('family')) $school = $user->family->school;
            if($user->hasRole('student')) $school = $user->profile->student->school;
            if($user->hasRole('teacher')) $school = $user->profile->teacher->school;
        }

        if($school) return view('home', compact('school'));
        else return view('home');
    }
}
