<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Profile;
use App\Student;
use App\School;
use App\UserSchool;
use App\Level;
use App\Season;

use Redirect, Response;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\View;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Mail;
use App\Mail\UserRegister;

use Image, PDF;

class StudentController extends Controller
{
    /**
     * Muestra la lista de estudiantes para un colegio
     *
     * @author  Paúl Rojas <paul.rojase@gmail.com>
     * @return \Illuminate\Http\JsonResponse    vista con listado de estudaintes
     */
    public function index()
    {

        if(auth()->user()->hasRole('family')  && !auth()->user()->hasRole('admin')){

            $students = auth()->user()->family->students;

            $actual = Season::where('school_id', auth()->user()->family->school_id)->where('status',1)->orderby('created_at', 'desc')->first();

            return view ('family.students', compact('students', 'actual'));

        }

        if(isset(auth()->user()->userSchool->school_id)){

            $school_id = auth()->user()->userSchool->school_id;

            $school = School::find($school_id);

            $students = $school->students;

        }
        else{

            $students = Student::all();

        }

        return view('school.students', compact('students'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $levels = Level::all();

        return view('family.students-edit', compact('levels'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validator($request->all())->validate();

        $family_id = auth()->user()->family->id;

        $password = Str::random();

        if(!$request->has('motor_deficiency')) {$request->request->add(['motor_deficiency' => '0']);}
        if(!$request->has('intellectual_deficiency')) {$request->request->add(['intellectual_deficiency' => '0']);}
        if(!$request->has('hearing_impairment')) {$request->request->add(['hearing_impairment' => '0']);}
        if(!$request->has('visual_deficiency')) {$request->request->add(['visual_deficiency' => '0']);}
        if(!$request->has('respiratory_deficiency')) {$request->request->add(['respiratory_deficiency' => '0']);}
        if(!$request->has('glasses')) {$request->request->add(['glasses' => '0']);}
        if(!$request->has('medical_report')) {$request->request->add(['medical_report' => '0']);}
        if(!$request->has('surgical_intervention')) {$request->request->add(['surgical_intervention' => '0']);}
        if(!$request->has('medical_treatment')) {$request->request->add(['medical_treatment' => '0']);}
        if(!$request->has('medical_report_special')) {$request->request->add(['medical_report_special' => '0']);}
        if(!$request->has('practice_sports')) {$request->request->add(['practice_sports' => '0']);}
        if(!$request->has('diabetes')) {$request->request->add(['diabetes' => '0']);}
        if(!$request->has('epilepsy')) {$request->request->add(['epilepsy' => '0']);}

        $user = User::create([
            'name' => $request->email,
            'email' => $request->email,
            'password' => bcrypt($password)
        ]);

        

        if($request->avatar) $avatar = $this->saveAvatar($request);
        else $avatar = 'default.jpg';

        $profile = Profile::create([
            'user_id' => $user->id,
            'document_type' => $request->document_type,
            'document_number' => $request->document_number,
            'first_name_1' => $request->first_name_1,
            'first_name_2' => $request->first_name_2,
            'last_name_1' => $request->last_name_1,
            'last_name_2' => $request->last_name_2,
            'birthdate' => $request->birthdate,
            'phone' => $request->phone,
            'country_id' => $request->country_id,
            'gender_id' => $request->gender_id,
            'avatar' => $avatar
        ]);

        $student = Student::create([
            'family_id'               => $family_id,
            'profile_id'              => $profile->id,
            'shirt_size'              => $request->shirt_size,
            'pant_size'               => $request->pant_size,
            'shoe_size'               => $request->shoe_size,
            'weight'                  => $request->weight,
            'height'                  => $request->height,
            'blood_type'              => $request->blood_type,
            'motor_deficiency'        => $request->motor_deficiency,
            'intellectual_deficiency' => $request->intellectual_deficiency,
            'hearing_impairment'      => $request->hearing_impairment,
            'visual_deficiency'       => $request->visual_deficiency,
            'respiratory_deficiency'  => $request->respiratory_deficiency,
            'health_problem'          => $request->health_problem,
            'grade'                   => $request->grade,
            'waist_size'              => $request->waist_size,
            'glasses'                 => $request->glasses,
            'medical_report'          => $request->medical_report,
            'surgical_intervention'   => $request->surgical_intervention,
            'allergic'                => $request->allergic,
            'medical_treatment'       => $request->medical_treatment,
            'medicine'                => $request->medicine,
            'health_insurance'        => $request->health_insurance,
            'chest_size'              => $request->chest_size,
            'arm_size'                => $request->arm_size,
            'medical_report_special'  => $request->medical_report_special,
            'practice_sports'         => $request->practice_sports,
            'diabetes'                => $request->diabetes,
            'epilepsy'                => $request->epilepsy

        ]);

        $user->assignRole('student');

        Mail::to($user)->send(new UserRegister($user, $password));

        return $this->index()->with('success','Nuevo estudiante agregado');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Student  $student
     * @return \Illuminate\Http\Response
     */
    public function show(Student $student)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Student  $student
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $student = Student::find($id);

        $levels = Level::all();

        $family_id = $student->family_id;

        if(!auth()->user()->hasRole('admin') && auth()->user()->hasRole('family') && auth()->user()->family->id != $family_id){
            return back();
        }

        return view('family.students-edit', compact('student', 'levels'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        $student = Student::find($id);

        $user = $student->profile->user;

        if($request->email == $user->email) $this->validator2($request->all())->validate();
        else $this->validator($request->all())->validate();

        $profile = $user->profile;

        if($request->email != $user->email){

            $password = Str::random();

            $user->email = $request->email;

            $user->password = bcrypt($password);

            $user->save();

            Mail::to($user)->send(new UserRegister($user, $password));
        }


        if(isset($request->avatar)){

            $this->deleteAvatar($profile->avatar);

            $profile->update($request->all());

            $profile->avatar = $this->saveAvatar($request);

            $profile->save();

        }else{
            $profile->update($request->all());
        }

        if(!$request->has('motor_deficiency')) {$request->request->add(['motor_deficiency' => '0']);}
        if(!$request->has('intellectual_deficiency')) {$request->request->add(['intellectual_deficiency' => '0']);}
        if(!$request->has('hearing_impairment')) {$request->request->add(['hearing_impairment' => '0']);}
        if(!$request->has('visual_deficiency')) {$request->request->add(['visual_deficiency' => '0']);}
        if(!$request->has('respiratory_deficiency')) {$request->request->add(['respiratory_deficiency' => '0']);}
        if(!$request->has('glasses')) {$request->request->add(['glasses' => '0']);}
        if(!$request->has('medical_report')) {$request->request->add(['medical_report' => '0']);}
        if(!$request->has('surgical_intervention')) {$request->request->add(['surgical_intervention' => '0']);}
        if(!$request->has('medical_treatment')) {$request->request->add(['medical_treatment' => '0']);}
        if(!$request->has('medical_report_special')) {$request->request->add(['medical_report_special' => '0']);}
        if(!$request->has('practice_sports')) {$request->request->add(['practice_sports' => '0']);}
        if(!$request->has('diabetes')) {$request->request->add(['diabetes' => '0']);}
        if(!$request->has('epilepsy')) {$request->request->add(['epilepsy' => '0']);}

        $student->update($request->all());

        if(auth()->user()->hasRole('family')){
            return redirect()->route('students.index')->with('success','Datos del estudiante actualizados');
        }
        else return redirect()->route('school.students.index')->with('success','Datos del estudiante actualizados');

        /*return back()->with('success','Datos del estudiante actualizados');*/
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Student  $student->id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        $student = Student::find($id);

        $this->deleteAvatar($student->profile->avatar);

        User::destroy($student->profile->user->id);
        Profile::destroy($student->profile->id);
        $student->delete();

        $students = auth()->user()->family->students;

        $view = View::make('family.students')->with('students', $students);

        if($request->ajax()){
            $sections = $view->renderSections();
            return Response::json($sections['content']);
        }else return back();
    }

    public function enrolledList()
    {
        //
    }

    protected function validator(array $data)
    {

        return Validator::make($data, [
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
            'document_type' => ['required', 'string', 'max:2'],
            'document_number' => ['required', 'integer'],
            'first_name_1' => ['required', 'string', 'max:50'],
            'last_name_1' => ['required', 'string', 'max:50'],
            'last_name_2' => ['required', 'string', 'max:50'],
            'birthdate' => ['required', 'string'],//revisar
            'phone' => ['required', 'string', 'max:255'],//revisar
            'country_id' => ['required', 'integer'],
            'gender_id' => ['required', 'integer'],
            'shirt_size' => ['required', 'integer'],
            'pant_size' => ['required', 'integer'],
            'shoe_size' => ['required', 'integer'],
            'weight' => ['required', 'integer'],
            'height' => ['required', 'integer'],
            'blood_type' => ['required', 'string', 'max:3'],
            'avatar' => ['image', 'mimes:jpeg,png,jpg'],
            'grade' => ['required']
        ]);
    }

    protected function validator2(array $data)
    {
        return Validator::make($data, [
            'document_type' => ['required', 'string', 'max:2'],
            'document_number' => ['required', 'integer'],
            'first_name_1' => ['required', 'string', 'max:50'],
            'last_name_1' => ['required', 'string', 'max:50'],
            'last_name_2' => ['required', 'string', 'max:50'],
            'birthdate' => ['required', 'string'],//revisar
            'phone' => ['required', 'string', 'max:255'],//revisar
            'country_id' => ['required', 'integer'],
            'gender_id' => ['required', 'integer'],
            'shirt_size' => ['required', 'integer'],
            'pant_size' => ['required', 'integer'],
            'shoe_size' => ['required', 'integer'],
            'weight' => ['required', 'integer'],
            'height' => ['required', 'integer'],
            'blood_type' => ['required', 'string', 'max:3'],
            'avatar' => ['image', 'mimes:jpeg,png,jpg'],
            'grade' =>['required']
        ]);
    }



    public function saveAvatar(Request $request)
    {
        $originalImage= $request->file('avatar');
        $image = Image::make($originalImage);
        $originalPath = public_path().'/storage/pictures/avatars/';
        //Nombre aleatorio para la image
        $tempName = Str::random(20) . '.' . $originalImage->getClientOriginalExtension();

        //Redimensinoar la imagen
        if($image->width() >= $image->height()) $image->heighten(400);
        else $image->widen(400);
        $image->resizeCanvas(400,400);

        $image->save($originalPath.$tempName);

        return $tempName;
    }

    public function deleteAvatar($avatar)
    {
        $originalPath = public_path().'/storage/pictures/avatars/';

        if ($avatar != 'default.jpg')
        {
          if(\File::exists($originalPath.$avatar)){
            \File::delete($originalPath.$avatar);
          }
        }
    }

    /**
     * Obtiene los estudiantes de un colegio en específico
     *
     * @author  William Páez <paez.william8@gmail.com>
     * @return \Illuminate\Http\JsonResponse    JSON con los datos de los estudiantes
     */
    public function listStudents()
    {
        $data = [];
        $data[0] = [
            'id' => '',
            'text' => 'Seleccione...'
        ];
        $students = Student::whereHas('family', function ($query) {
            $query->where('school_id', auth()->user()->userSchool->school_id);
        })->get();
        foreach ($students as $student) {
            $data[] = [
                'id' => $student->id,
                'text' => $student->profile->first_name_1.' '.$student->profile->last_name_1,
            ];
        }
        return response()->json($data);
    }
}
