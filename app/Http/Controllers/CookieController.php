<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class CookieController extends Controller
{
    public function save(request $request)
    {
    	$value = $request->session()->put('USD_CLASES', $request->usd);
    	$value = $request->session()->put('COP_CLASES', $request->cop);
    	return 0;
    }
}
