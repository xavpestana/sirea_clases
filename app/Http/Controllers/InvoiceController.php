<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Codedge\Fpdf\Fpdf\Fpdf;

use Illuminate\Support\Facades\Mail;
use App\Mail\InvoiceReceived;
use App\InvoiceSetting;
use App\Payment;

class InvoiceController extends Controller
{

       private $fpdf;

    public function __construct()
    {
         
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function send(Request $request)
    {
        $payment = Payment::find($request->invoice);
        $installments = $payment->installmentPayment;
        $data_user= $payment->Student->profile;
        $to = $payment->Student->family->user->email;
        //dd($payment->installmentPayment[0]->Installment);
        
        $school = array(
                    'dea_code'=> $payment->school->dea_code, 
                    'stadistic_code'=> $payment->school->stadistic_code,
                    'school_type'=> $payment->school->school_type,
                    'name'=> $payment->school->name,
                    'phone'=> $payment->school->phone,
                    'fax'=> $payment->school->fax,
                    'email'=> $payment->school->email,
                    'address'=> $payment->school->address,
                    'parroquia'=> $payment->school->parish->name,
                    'municipio'=> $payment->school->parish->municipality->name,
                    'logo'=> $payment->school->logo
                );
        
        $user = array(
                    'document_type'=> $data_user->document_type, 
                    'document_number'=> $data_user->document_number, 
                    'first_name_1'=> $data_user->first_name_1, 
                    'first_name_2'=> $data_user->first_name_2, 
                    'last_name_1'=> $data_user->last_name_1, 
                    'last_name_2'=> $data_user->last_name_2, 
                    'birthdate'=> $data_user->birthdate, 
                    'phone'=> $data_user->phone,
                    'email'=> $to,
                    'grade'=> $payment->Student->grade,
                );
        $invoiceSetting = InvoiceSetting::where('school_id', auth()->user()->userSchool->school_id)->first();
        //dd($payment->Student->credit);
        Mail::to($to)->send(new InvoiceReceived($installments, $payment, $school, $user, $invoiceSetting));
         
        return 0;
    }

    /**
     * Imprimir factura
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    function Dimentions()
    {

    }
    public function print($dimension, $id)
    {

        $invoiceSetting = InvoiceSetting::where('school_id', auth()->user()->userSchool->school_id)->first();
        /*********Dimensiones**********/
        if ($dimension == '1') {
            $dimensiones = array(216,279.4);
            $init = 30;
            $logo_w_h = 47;
            $logo_x_y = 15;
            $sety_logo = 12;
            $setx_logo = 20;
            $header_font = 12;
            $setx_header = 70;
            $sety_header = 22;
            $header_w = 140;
            $header_h = 6;
            $sety_para = 70;
            $para_w = 126;
            $para_h = 5;
            $para_font = 10;
            $factura_x = 160;
            $factura_w = 116;
            $factura_h = 5;
            $table_y = 110;
            $table_h = 7;
            $w11 = 90;
            $w21 = 25;
            $w31 = 30;
            $w41 = 15;
            $w51 = 30;
            $data2x = 110;
            $w12 = 40;
            $w22 = 40;
            $ypd = 110;
            $yp1 = 10;
            $yp2 = 5;
            $d = "216x280";
        }
        if ($dimension == '2') {
            $dimensiones = array(140,216);
            $init = 20;
            $logo_w_h = 30;
            $logo_x_y = 5;
            $sety_logo = 11;
            $setx_logo = 10; //mueve el margen completo izquierdo
            $header_font = 9;
            $setx_header = 40;
            $sety_header = 11;
            $header_w = 100;
            $header_h = 4;
            $sety_para = 40;
            $para_w = 100;
            $para_h = 3;
            $para_font = 8;
            $factura_x = 140;//area de numero de factura
            $factura_w = 116;
            $factura_h = 4;
            $table_y = 64;//tablas
            $table_h = 4;
            $w11 = 75;
            $w21 = 15;
            $w31 = 30;
            $w41 = 15;
            $w51 = 30;
            $data2x = 95;
            $w12 = 40;
            $w22 = 40;
            $ypd = 64; //y de tabla 1
            $yp1 = 5;
            $yp2 = 3;
            $d = "140x216";
        }
        if ($dimension == '3') {
            $dimensiones = array(70,108);
            $init = 10;
            $logo_w_h = 15;
            $logo_x_y = 2;
            $sety_logo = 5;
            $setx_logo = 5; //mueve el margen completo izquierdo
            $header_font = 4;
            $setx_header = 20;
            $sety_header = 5;
            $header_w = 100;
            $header_h = 2;
            $sety_para = 20;
            $para_w = 35;
            $para_h = 1.5;
            $para_font = 3;
            $factura_x = 70;//area de numero de factura
            $factura_w = 40;
            $factura_h = 1.5;
            $table_y = 32;//tablas
            $table_h = 1.5;
            $w11 = 35;
            $w21 = 8;
            $w31 = 12;
            $w41 = 5;
            $w51 = 20;
            $data2x = 53;
            $w12 = 16;
            $w22 = 16;
            $ypd = 32; //y de tabla 1
            $yp1 = 1.5;
            $yp2 = 1;
            $d = "70x108";
        }

        /**********Datos********/
        $payment = Payment::find($id);
        $installments = $payment->installmentPayment;
        $school = $payment->school;
        $to = $payment->Student->family->user->email;
        $data_user= $payment->Student->profile;
        
        $address = utf8_decode($school->address.', Parroquia '.$school->parish->name.' Municipio '.$school->parish->municipality->name.'. '.$school->city->name.', '.$school->parish->municipality->state->name);
        if($school->dea_code) $dea_code = 'Código DEA: '.$school->dea_code; else $dea_code = "";
        if($school->stadistic_code) $stadistic_code = 'Código Estadístico: '.$school->stadistic_code; else $stadistic_code = "";
        if($school->phone) $phone = 'Teléfono: '.$school->phone; else $phone = "";
        if($school->fax) $fax = '- Fax: '.$school->fax; else $fax = "";
        if($school->rif) $rif = 'Rif: '.$school->rif; else $rif = "";

            $change_usd = $payment->change_amount_USD;
        $change_cop = $payment->change_amount_COP;
            $abono = $payment->credit;
            if($payment->coin_credit == 'Bsf'){
                $abono = $abono * 1;
            }
            if($payment->coin_credit == 'USD'){
                $abono = $abono * $change_usd;
            }
            if($payment->coin_credit == 'COP'){
                $abono = $abono * $change_cop;
            }

        /*********Creando factura**********/

        $this->fpdf = new Fpdf("L","mm", $dimensiones);
        $this->fpdf->AddPage();
        // Agregamos los datos de la empresa
        
        
        if($invoiceSetting->headers == true){
        
        $this->fpdf->setY($sety_logo);
        $this->fpdf->setX($setx_logo);
        $this->fpdf->Image(PUBLIC_PATH('storage/pictures/logos/'.$school->logo),$logo_x_y,$logo_x_y,$logo_w_h,$logo_w_h);
        $this->fpdf->SetFont('Helvetica','',$header_font);
        $this->fpdf->setY($sety_header);
        $this->fpdf->SetX($setx_header);
        $this->fpdf->MultiCell($header_w,$header_h,utf8_decode($school->school_type.' '.$school->name),0,'J');
        $this->fpdf->SetX($setx_header);$this->fpdf->MultiCell($header_w,$header_h,utf8_decode($dea_code.' - '.$stadistic_code) ,0,'J');
        $this->fpdf->SetX($setx_header);$this->fpdf->MultiCell($header_w,$header_h,$address,0,'J');
        $this->fpdf->SetX($setx_header);$this->fpdf->MultiCell($header_w,$header_h,utf8_decode($phone.' '.$fax ),0,'J');
        $this->fpdf->SetX($setx_header);$this->fpdf->MultiCell($header_w,$header_h,$rif,0,'J');
        }else{
        $this->fpdf->setY($init);
        $this->fpdf->setX($setx_logo);  
        }

           // Agregamos los datos del cliente
        $this->fpdf->SetFont('Helvetica','B',$para_font);    
        $this->fpdf->setY($sety_para);$this->fpdf->setX($setx_logo);
        $this->fpdf->MultiCell($para_w,$para_h,"PARA:", 0, 'J');
        $this->fpdf->SetFont('Helvetica','',$para_font);    
        $this->fpdf->setX($setx_logo);
        $this->fpdf->MultiCell($para_w,$para_h,utf8_decode("RIF o C.I: ".$payment->identity), 0, 'J');
        $this->fpdf->setX($setx_logo);
        $this->fpdf->MultiCell($para_w,$para_h,utf8_decode("Nombre: ".$payment->name), 0, 'J');
        $this->fpdf->setX($setx_logo);
        $this->fpdf->MultiCell($para_w,$para_h,utf8_decode("Direccion: ".$payment->address), 0, 'J');
        $this->fpdf->setX($setx_logo);
        $this->fpdf->MultiCell($para_w,$para_h,utf8_decode("Telefono: ".$payment->phone), 0, 'J');
        $this->fpdf->setX($setx_logo);
        $this->fpdf->MultiCell($para_w,$para_h,utf8_decode("Email: ".$to), 0, 'J');

        // Agregamos los datos de la factura
        $this->fpdf->SetFont('Helvetica','B',$para_font);    
        $this->fpdf->setY($sety_para);$this->fpdf->setX($factura_x);
        $this->fpdf->MultiCell($factura_w,$factura_h,"FACTURA: #0000".$payment->number_invoice);
        $this->fpdf->SetFont('Helvetica','',$para_font);    
        $this->fpdf->setX($factura_x);
        $this->fpdf->MultiCell($factura_w,$factura_h,"Fecha de pago: ". $payment->created_at->format('d/M/Y'));
        $this->fpdf->setX($factura_x);
        $this->fpdf->MultiCell($factura_w,$factura_h,"Metodo de pago: ". $payment->paymentMethod->name);
        $this->fpdf->setX($factura_x);
        $this->fpdf->MultiCell($factura_w,$factura_h,"Status: Cancelado");
        $this->fpdf->setX($factura_x);
        $this->fpdf->MultiCell($factura_w,$factura_h,"");

        /// Apartir de aqui empezamos con la tabla de productos
        $this->fpdf->setY($table_y);
        $this->fpdf->setX($setx_logo);
        /////////////////////////////
        //// Array de Cabecera
        $header = array("Concepto", "Vence","Unitario","Cantidad","Total");
            // Column widths
            $w = array($w11, $w21, $w31, $w41, $w51);
            // Header
            for($i=0;$i<count($header);$i++)
                    $this->fpdf->Cell($w[$i],$table_h,$header[$i],1,0,'C');
                    $this->fpdf->Ln();
            
        
            foreach ($installments as $installment)
            {
                // Data

        $amount = $installment->Installment->amount;
        
            if($installment->Installment->coin == 'Bsf'){
                $total = $amount * 1;
            }
            if($installment->Installment->coin == 'USD'){
                $total = $amount * $change_usd;
            }
            if($installment->Installment->coin == 'COP'){
                $total = $amount * $change_cop;
            }
        if(!is_null($installment->Installment->expire_at)){
            $expire = $installment->Installment->expire_at->format('d-M-Y');
        }else{
            $expire = 'No aplica';
        }
                $this->fpdf->setX($setx_logo);
                $this->fpdf->Cell($w[0],$table_h,$installment->Installment->description,1);
                $this->fpdf->Cell($w[1],$table_h,$expire,1);
                $this->fpdf->Cell($w[2],$table_h,number_format($total). " Bsf",'1',0,'R');
                $this->fpdf->Cell($w[3],$table_h,number_format(1),'1',0,'R');
                $this->fpdf->Cell($w[4],$table_h,number_format($total). " Bsf",'1',0,'R');

                $this->fpdf->Ln();
               // $total+=$row[3]*$row[2];
            }
            
            $this->fpdf->setX($setx_logo);
                $this->fpdf->Cell($w[0],$table_h,"Abono en la cuenta de: ". $data_user->first_name_1." ". $data_user->last_name_1,1);
                $this->fpdf->Cell($w[1],$table_h,"No aplica",1);
                $this->fpdf->Cell($w[2],$table_h,number_format($abono). " Bsf",'1',0,'R');
                $this->fpdf->Cell($w[3],$table_h,number_format(1),'1',0,'R');
                $this->fpdf->Cell($w[4],$table_h,number_format($abono). " Bsf",'1',0,'R');
                $this->fpdf->Ln();
                $this->fpdf->setX($setx_logo);
                if(!is_null($payment->observation)){
                    $this->fpdf->Cell($w[0] + $w[1] + $w[2] + $w[3],$table_h," Observacion: ". $payment->observation,1);
                }
//dd(count($installments));
        /////////////////////////////
//// Apartir de aqui esta la tabla con los subtotales y totales
$yposdinamic = $ypd + ((count($installments)+1)*$yp1);
//dd($yposdinamic);
        $this->fpdf->setY($yposdinamic);
        $this->fpdf->setX(235);
            $this->fpdf->Ln();
/////////////////////////////

    $monto_total = $payment->amount_bsf;
    $iva = $monto_total*0.16;
    $sub = $monto_total - $iva;

$header = array("", "");
if($invoiceSetting->iva == true){
$data2 = array(
    array("IVA 16%",$iva),
    array("Sub-total",$sub),
    array("Total", $monto_total),
);
}else{
$data2 = array(
    array("Total", $monto_total),
);
}
    // Column widths
    $w2 = array($w12, $w22);
    // Header

            $this->fpdf->Ln();
    // Data
    foreach($data2 as $row)
    {
        $this->fpdf->setX($data2x);
                $this->fpdf->Cell($w2[0],$table_h,$row[0],1);
                $this->fpdf->Cell($w2[1],$table_h,number_format($row[1], 2, ".",",")." Bsf",'1',0,'R');

                $this->fpdf->Ln();
    }

    $yposdinamic += (count($data2)*$yp2);
        $this->fpdf->SetFont('Helvetica','B',$para_font);    

        $this->fpdf->setY($yposdinamic);
        $this->fpdf->setX($setx_logo);
        $this->fpdf->MultiCell($para_w,$para_h,"Datos del alumno:", 0, 'J');
        $this->fpdf->SetFont('Helvetica','',$para_font);    
        $this->fpdf->setX($setx_logo);
        $this->fpdf->MultiCell($para_w,$para_h,utf8_decode("RIF o C.I: ".$data_user->document_type."-".$data_user->document_number), 0, 'J');
        $this->fpdf->setX($setx_logo);
        $this->fpdf->MultiCell($para_w,$para_h,utf8_decode("Nombre: ".$data_user->first_name_1." ".$data_user->first_name_2." ".$data_user->last_name_1." ".$data_user->last_name_2), 0, 'J');
        $this->fpdf->setX($setx_logo);
        $this->fpdf->MultiCell($para_w,$para_h,utf8_decode("Grado: ".$payment->Student->grade), 0, 'J');   

/////////////////////////////

       // $this->fpdf->output('Factura_'.$payment->number_invoice."_".$d."_".$data_user->first_name_1.'_'.$data_user->last_name_1.'.pdf',"D");
         $this->fpdf->output('Factura_'.$payment->number_invoice."_".$d."_".$data_user->first_name_1.'_'.$data_user->last_name_1.'.pdf',"D");
        exit;
    }
}
