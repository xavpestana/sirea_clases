<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\InvoiceSetting;

/**
 * @class SettingController
 * @brief Gestiona información de configuración de pago
 *
 * Clase que gestiona información de configuración de pago
 *
 * @author William Páez <paez.william8@gmail.com>
 * @license <a href='​http://www.gnu.org/licenses/gpl-2.0.html'>GNU Public License versión 2 (GPLv2)</a>
 */
class PaymentSettingController extends Controller
{
    /**
     * Define la configuración de la clase
     *
     * @author William Páez <paez.william8@gmail.com>
     */
    public function __construct()
    {
        //$this->middleware('permission:payment.settings.list', ['only' => 'index']);
    }

    /**
     * Muestra la página de configuración de pago
     *
     * @author William Páez <paez.william8@gmail.com>
     * @return \Illuminate\View\View    Muestra la página principal
     */
    public function index()
    {
        if(!auth()->user()->userSchool->payments) return redirect('/')->with('error', 'No tiene permisos para módulo de pagos');

        $invoiceSetting = InvoiceSetting::where('school_id', auth()->user()->userSchool->school_id)->first();
        return view('payments.settings', compact('invoiceSetting'));
    }

    /**
     * control de iva.
     *
     * @return \Illuminate\Http\Response
     */
    public function iva()
    {
        $invoiceSetting = InvoiceSetting::where('school_id', auth()->user()->userSchool->school_id)
                                        ->first();
      if ($invoiceSetting->iva == true) {
        $invoiceSetting = InvoiceSetting::where('school_id', auth()->user()->userSchool->school_id)
                                        ->update(['iva' => false]);
      }else{
        $invoiceSetting = InvoiceSetting::where('school_id', auth()->user()->userSchool->school_id)
                                        ->update(['iva' => true]);
      }
      return 0;
    }

    /**
     * control de iva.
     *
     * @return \Illuminate\Http\Response
     */
    public function headers()
    {
        $invoiceSetting = InvoiceSetting::where('school_id', auth()->user()->userSchool->school_id)
                                        ->first();
      if ($invoiceSetting->headers == true) {
        $invoiceSetting = InvoiceSetting::where('school_id', auth()->user()->userSchool->school_id)
                                        ->update(['headers' => false]);
      }else{
        $invoiceSetting = InvoiceSetting::where('school_id', auth()->user()->userSchool->school_id)
                                        ->update(['headers' => true]);
      }
      return 0;
    }

    /**
     * control simple.
     *
     * @return \Illuminate\Http\Response
     */
    public function simple()
    {
        $invoiceSetting = InvoiceSetting::where('school_id', auth()->user()->userSchool->school_id)
                                        ->first();
      if ($invoiceSetting->simple == true) {
        $invoiceSetting = InvoiceSetting::where('school_id', auth()->user()->userSchool->school_id)
                                        ->update(['simple' => false]);
      }else{
        $invoiceSetting = InvoiceSetting::where('school_id', auth()->user()->userSchool->school_id)
                                        ->update(['simple' => true]);
      }
      return 0;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
