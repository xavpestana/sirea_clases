<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Receipt;
use App\paymentRegister;

class ReceiptController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $paymentRegister = paymentRegister::where('school_id', auth()->user()->userSchool->school_id)
                                        ->where('status', 0)
                                        ->get();
        return view('payments.table_register',compact('paymentRegister'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        /*Guardando el pago*/
        $payment = new paymentRegister;
        $payment->student_id = $request->student;
        $payment->school_id = auth()->user()->family->school_id;
        $payment->family_id = auth()->user()->family->id;
        $payment->identity = $request->document;
        $payment->name = $request->name;
        $payment->address = $request->address;
        $payment->phone = $request->phone;
        $payment->school_bank_account = $request->bank;
        $payment->payment_method_id = $request->method;
        $payment->amount = $request->amount;
        $payment->coin = $request->coin;
        $payment->transfer_code = $request->reference;
        $payment->observation = $request->observation;
        $payment->status = 0;
        $payment->save();

        return 0;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $paymentRegister = paymentRegister::where('family_id', $id)->get();

        return view('family.payments.register', compact('paymentRegister'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $paymentRegister = paymentRegister::where('id', $id)->delete();

        return $paymentRegister;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function reports()
    {
        $paymentRegister = paymentRegister::where('school_id', auth()->user()->userSchool->school_id)
                                            ->where('status', '<>', 0)
                                            ->get();

        return view('payments.defaulters.table_register', compact('paymentRegister'));
    }
}
