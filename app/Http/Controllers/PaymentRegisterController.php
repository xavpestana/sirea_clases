<?php

namespace App\Http\Controllers;

use App\paymentRegister;
use Illuminate\Http\Request;

class PaymentRegisterController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\paymentRegister  $paymentRegister
     * @return \Illuminate\Http\Response
     */
    public function show(paymentRegister $paymentRegister)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\paymentRegister  $paymentRegister
     * @return \Illuminate\Http\Response
     */
    public function edit(paymentRegister $paymentRegister)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\paymentRegister  $paymentRegister
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, paymentRegister $paymentRegister)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\paymentRegister  $paymentRegister
     * @return \Illuminate\Http\Response
     */
    public function destroy(paymentRegister $paymentRegister)
    {
        //
    }
}
