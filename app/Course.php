<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Course extends Model
{
	protected $fillable = ['grade_id', 'name'];

    public function grade()
    {
        return $this->belongsTo(Grade::class);
    }
    public function Courses_sections_user()
    {
        return $this->hasMany(Courses_sections_user::class);
    }

    public function tasks()
    {
        return $this->hasMany(Task::class);
    }

}
