<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class teacherNotify extends Model
{
    protected $fillable = [
		'user_id',
		'notifies',
        'user_id_r'
	];

	public function user()
	{
		return $this->belongsTo(User::class);
	}
}
