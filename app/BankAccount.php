<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * @class BankAccount
 * @brief Datos de cuentas bancarias
 *
 * Gestiona el modelo de datos para las cuentas bancarias
 *
 * @author William Páez <paez.william8@gmail.com>
 * @license <a href='​http://www.gnu.org/licenses/gpl-2.0.html'>GNU Public License versión 2 (GPLv2)</a>
 */
class BankAccount extends Model
{
    /**
     * Lista de atributos que pueden ser asignados masivamente
     *
     * @var array $fillable
     */
    protected $fillable = ['account_number', 'bank_id'];

    /**
     * Relación morfológica de cuenta bancaria
     *
     * @author William Páez <paez.william8@gmail.com>
     * @return \Illuminate\Database\Eloquent\Relations\MorphTo
     */
    public function bankAccountable()
    {
        return $this->morphTo();
    }

    /**
     * Método que obtiene la cuentabancaria asociada a un Banco
     *
     * @author  William Páez <paez.william8@gmail.com>
     * @return object Objeto con los registros relacionados al modelo Banco
     */
    public function bank()
    {
        return $this->belongsTo(Bank::class);
    }
}
