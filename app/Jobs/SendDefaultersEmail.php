<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class SendDefaultersEmail implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;
    protected $defaulters;
    protected $school;
    protected $student;
    protected $appName;
    protected $appUrl;
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($defaulters, $school, $student)
    {
        $this->defaulters = $defaulters;
        $this->school = $school;
        $this->student = $student;
        $this->appName = config('app.name');
        $this->appUrl = config('app.url');
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $email = new DefaultersEmail($this->defaulters, $this->school, $this->student);
    }
}
