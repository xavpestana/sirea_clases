<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class SendMassEmail implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $user;
    protected $subject;
    protected $body;
    protected $school;
    protected $appName;
    protected $appUrl;


    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(User $user, $subject, $body, $school)
    {
        $this->user = $user;
        $this->subject = $subject;
        $this->body = $body;
        $this->school = $school;
        $this->appName = config('app.name');
        $this->appUrl = config('app.url');
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $email = new MassEmail($this->user, $this->subject, $this->body, $this->school)
    }
}
