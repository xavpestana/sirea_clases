<?php

namespace App\Exports;

use App\Enrollment;
use App\Grade;
use Maatwebsite\Excel\Concerns\FromQuery;
use Maatwebsite\Excel\Concerns\WithTitle;
use Maatwebsite\Excel\Concerns\WithMapping;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;

class EnrollmentsPerSectionSheet implements FromQuery, WithTitle, WithHeadings, WithMapping, ShouldAutoSize
{
    private $grade_name;
    private $level_name;

    public function __construct(string $grade_name, string $level_name)
    {
        $this->grade_name = $grade_name;
        $this->level_name = $level_name;
    }

    /**
     * @return Builder
     */
    public function query()
    {
        return Enrollment::query();
        //if($enrollments->count() > 0 ) return $enrollments;
    }

    /**
     * @return string
     */
    public function title(): string
    {
        return $this->grade_name;
    }

    /**
    * @var Enrollment $enrollment
    */
    public function map($enrollment): array
    {
        
        $student = $enrollment->student;
        $profile = $student->profile;
        $user_student = $profile->user;
        $family = $student->family;
        $representatives = $student->family->representatives;
        $representatives_name ="";
        $representatives_phone ="";
        $representatives_parents = "";
        $representatives_emails = "";
        if(!is_null($representatives)){
          foreach ($representatives as $representative) {
                $representatives_name .= $representative->first_name_1." ".$representative->last_name_1." / ";
                $representatives_phone .= $representative->phone." / ";
                $representatives_parents .= $representative->relationship->name." / ";
                $representatives_emails .= $representative->correo." / ";
            }  
        }

        return [
            $profile->first_name_1.' '.$profile->first_name_2,
            $profile->last_name_1.' '.$profile->last_name_2,
            
            $student->shirt_size,
            $student->pant_size,
            $student->shoe_size,
            $student->waist_size,
            $student->chest_size,
            $student->arm_size,
            $student->weight,
            $student->height,
            $student->blood_type,
            $student->motor_deficiency,
            $student->intellectual_deficiency,
            $student->hearing_impairment,
            $student->visual_deficiency,
            $student->respiratory_deficiency,
            $student->health_problem,
            $student->glasses,
            $student->medical_report,
            $student->surgical_intervention,
            $student->allergic,
            $student->medical_treatment,
            $student->medicine,
            $student->health_insurance,
            $student->medical_report_special,
            $student->practice_sports,
            $student->diabetes,
            $student->epilepsy,
           
        ];
    }

    public function headings(): array
    {
        return [
            'Nombres',
            'Apellidos',
            'Documento',
            'Correo Estudiante',
            'Fecha de nacimiento',
            'Teléfono',
            'País de nacimiento',
            'Género',
            'Medida de cintura',
            'Medida de pecho',
            'Medida de brazos',
            'Talla camisa',
            'Talla pantalón',
            'Talla zapato',
            'Peso',
            'Altura',
            'Sangre',
            'Compromiso motor',
            'Compromiso cognitivo',
            'Compromiso auditivo',
            'Compromiso visual',
            'Problema respiratorio',
            'Otros problemas de salud',
            '¿Usa lentes?',
            'Informe medico',
            'Intervenido Quirurgicamente',
            '¿Alguna alergia?',
            'Tratamiento médico',
            'Medicamento que toma',
            'health_insurance',
            'Informe medico de compromiso',
            'Practica de Eduicación Física',
            'Diabetes',
            'Epilepsia',
            'Apellidos de la familia',
            'Télefono',
            'Ingreso mensual',
            'Dirección',
            'Parroquia',
            'Ciudad',
            'Municipio',
            'Estado',
            'Nombres de Representantes',
            'Telefonos',
            'Parentescos',
            'Correo Representantes'

        ];
    }
}
