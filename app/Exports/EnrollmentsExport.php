<?php

namespace App\Exports;

use App\Grade;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;

class EnrollmentsExport implements FromView
{
    public function view(): View
    {

        $school = auth()->user()->userSchool->school;

        $actual_season = $school->seasons->where('status')->first();
        
        return view('exports.enrollments', [
            'grades' => Grade::all(),
            'actual_season' => $actual_season,
        ]);
    }
}