<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Section extends Model
{
    protected $fillable = ['grade_id', 'name', 'season_id'];

    public function grade()
    {
        return $this->belongsTo(Grade::class);
    }

    public function Courses_sections_user()
    {
        return $this->hasMany(Courses_sections_user::class);
    }

    public function season()
    {
        return $this->belongsTo(Season::class);
    }

     public function evaluationPlan()
    {
        return $this->hasOne(evaluationPlan::class);
    }
    /**
     * Método que obtiene las inscripciones asociadas a una sección
     *
     * @author  William Páez <paez.william8@gmail.com>
     * @return object Objeto con los registros relacionados al modelo Inscripción
     */
    public function enrollments()
    {
        return $this->hasMany(Enrollment::class);
    }
}
