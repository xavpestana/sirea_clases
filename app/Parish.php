<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * @class Parish
 * @brief Datos de parroquias
 *
 * Gestiona el modelo de datos para las Parroquias
 *
 * @author William Páez <paez.william8@gmail.com>
 * @license <a href='​http://www.gnu.org/licenses/gpl-2.0.html'>GNU Public License versión 2 (GPLv2)</a>
 */
class Parish extends Model
{
    /**
     * Lista de atributos que pueden ser asignados masivamente
     *
     * @var array $fillable
     */
    protected $fillable = ['name', 'municipality_id', 'code'];

    /**
     * Método que obtiene el Municipio asociado a una Parroquia
     *
     * @author  William Páez <paez.william8@gmail.com>
     * @return object Objeto con los registros relacionados al modelo Municipality
     */
    public function municipality()
    {
        return $this->belongsTo(Municipality::class);
    }

    public function schools()
    {
        return $this->hasMany(School::class);
    }

    public function families()
    {
        return $this->hasMany(Family::class);
    }

    public function teachers()
    {
        return $this->hasMany(Teacher::class);
    }
}
