<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class InvoiceReceived extends Mailable
{
    use Queueable, SerializesModels;
    public $installments;
    public $payment;
    public $school;
    public $user;
    public $appName;
    public $appUrl;
    public $invoiceSetting;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($installments, $payment, $school, $user, $invoiceSetting)
    {
        $this->installments = $installments;
        $this->payment = $payment;
        $this->school = $school;
        $this->user = $user;
        $this->invoiceSetting = $invoiceSetting;
        $this->appName = config('app.name');
        $this->appUrl = config('app.url');
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('emails.invoice_email')
                    ->subject(config('app.name') . " - Recibo de Pago");
    }
}
