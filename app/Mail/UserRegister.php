<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use App\User;

/**
 * @class UserRegister
 * @brief Envío de correo al registrarse un usuario
 *
 * Clase que gestiona el envío de correos a los usuarios registrados
 *
 * @author William Páez <paez.william8@gmail.com>
 * @license <a href='​http://www.gnu.org/licenses/gpl-2.0.html'>GNU Public License versión 2 (GPLv2)</a>
 */
class UserRegister extends Mailable
{
    use Queueable, SerializesModels;

    public $user;
    public $password;
    public $appName;
    public $appUrl;

    /**
     * Define la configuración de la clase
     *
     * @author William Páez <paez.william8@gmail.com>
     * @return void
     */
    public function __construct(User $user, $password)
    {
        $this->user = $user;
        $this->password = $password;
        $this->appName = config('app.name');
        $this->appUrl = config('app.url');
    }

    /**
     * Construye la vista que contiene el cuerpo del correo
     *
     * @author William Páez <paez.william8@gmail.com>
     * @return $this
     */
    public function build()
    {
        return $this->subject(config('app.name') . " - Registro")->view('emails.user-register');
    }
}
