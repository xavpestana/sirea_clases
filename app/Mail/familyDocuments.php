<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use App\User;

class familyDocuments extends Mailable
{
    use Queueable, SerializesModels;
    public $user;
    public $password;
    public $appName;
    public $appUrl;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(User $user, $password)
    {
        $this->user = $user;
        $this->password = $password;
        $this->appName = config('app.name');
        $this->appUrl = config('app.url');
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->subject(config('app.name') . " - Codigo de Etica")
                    ->view('emails.code')
                    ->attach(public_path('/documents').'/Codigo_de_etica.pdf', [
                    'as' => 'Codigo_de_etica.pdf',
                    'mime' => 'application/pdf',
                ]);
    }
}
