<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class DefaultersEmail extends Mailable implements ShouldQueue
{
    use Queueable, SerializesModels;
    public $defaulters;
    public $school;
    public $student;
    public $appName;
    public $appUrl;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($defaulters, $school, $student)
    {
        $this->defaulters = $defaulters;
        $this->school = $school;
        $this->student = $student;
        $this->appName = config('app.name');
        $this->appUrl = config('app.url');
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('emails.defaulters_email')
                    ->subject(config('app.name') . " - Notificacion de cobro");
    }
}
