<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * @class Concept
 * @brief Datos de conceptos
 *
 * Gestiona el modelo de datos para los conceptos
 *
 * @author William Páez <paez.william8@gmail.com>
 * @license <a href='​http://www.gnu.org/licenses/gpl-2.0.html'>GNU Public License versión 2 (GPLv2)</a>
 */
class Concept extends Model
{
    /**
     * Lista de atributos que pueden ser asignados masivamente
     *
     * @var array $fillable
     */
    protected $fillable = ['name', 'amount', 'school_id'];

    /**
     * Método que obtiene los conceptos asociados a muchas inscripciones
     *
     * @author  William Páez <paez.william8@gmail.com>
     * @return object Objeto con los registros relacionados al modelo Inscripción
     */
    public function enrollments()
    {
        return $this->belongsToMany(Enrollment::class)->withTimestamps();
    }

    /**
     * Método que obtiene el colegio asociado a un concepto de inscripción
     *
     * @author  William Páez <paez.william8@gmail.com>
     * @return object Objeto con los registros relacionados al modelo School
     */
    public function school()
    {
        return $this->belongsTo(School::class);
    }
}
