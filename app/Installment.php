<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * @class Installment
 * @brief Datos de cuotas
 *
 * Gestiona el modelo de datos para las cuotas
 *
 * @author William Páez <paez.william8@gmail.com>
 * @license <a href='​http://www.gnu.org/licenses/gpl-2.0.html'>GNU Public License versión 2 (GPLv2)</a>
 */
class Installment extends Model
{
    /**
     * Lista de atributos que pueden ser asignados masivamente
     *
     * @var array $fillable
     */
    protected $fillable = ['description', 'amount', 'plan_id', 'expire_at'];
    protected $dates = ['expire_at'];

    /**
     * Método que obtiene el plan de pago asociados a una cuota
     *
     * @author  William Páez <paez.william8@gmail.com>
     * @return object Objeto con los registros relacionados al modelo Plan
     */
    public function plan()
    {
        return $this->belongsTo(Plan::class);
    }
    public function installmentPayment()
    {
        return $this->hasMany(installmentPayment::class);
    }
}
