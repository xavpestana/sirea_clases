<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * @class Bank
 * @brief Datos de bancos
 *
 * Gestiona el modelo de datos para los bancos
 *
 * @author William Páez <paez.william8@gmail.com>
 * @license <a href='​http://www.gnu.org/licenses/gpl-2.0.html'>GNU Public License versión 2 (GPLv2)</a>
 */
class Bank extends Model
{
    /**
     * Lista de atributos que pueden ser asignados masivamente
     *
     * @var array $fillable
     */
    protected $fillable = ['code', 'name', 'short_name'];

    /**
     * Método que obtiene las cuentas bancarias asociadas a un banco
     *
     * @author  William Páez <paez.william8@gmail.com>
     * @return object Objeto con los registros relacionados al modelo BankAccount
     */
    public function bankAccounts()
    {
        return $this->hasMany(BankAccount::class);
    }
}
