<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class responseActivity extends Model
{
	protected $fillable = ['task_id', 'user_id', 'file_responses_id'];

    public function fileResponse()
    {
        return $this->hasOne(fileResponse::class);
    }
    public function user()
	{
		return $this->belongsTo(User::class);
	}
}
