<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
/**
 * @class Family
 * @brief Datos de la Familia
 *
 * Gestiona el modelo de datos para Familia
 *
 * @author Paúl Rojas <paul.rojase@gmail.com>
 * @license <a href='​http://www.gnu.org/licenses/gpl-2.0.html'>GNU Public License versión 2 (GPLv2)</a>
 */
class Poll extends Model
{
    /**
     * Lista de atributos que pueden ser asignados masivamente
     *
     * @var array $fillable
     */	
	protected $fillable = [
		'family_id',
        'companion',
        'arrival',
        'departure',
        'marital_status',
        'religion',
        'property_type',
        'home_type',
        'emergency_phone',
        'policy_description'
	];

    public function family()
    {
    	return $this->belongsTo(family::class);
    }
}
