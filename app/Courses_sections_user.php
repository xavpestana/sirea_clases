<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Courses_sections_user extends Model
{
    protected $fillable = [
		'course_id',
		'section_id',
		'user_id',
	];
	
	public function Course()
    {
        return $this->belongsTo(Course::class);
    }
    public function Section()
    {
        return $this->belongsTo(Section::class);
    }
    public function User()
    {
        return $this->belongsTo(User::class);
    }

}
