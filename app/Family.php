<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * @class Family
 * @brief Datos de la Familia
 *
 * Gestiona el modelo de datos para Familia
 *
 * @author Paúl Rojas <paul.rojase@gmail.com>
 * @license <a href='​http://www.gnu.org/licenses/gpl-2.0.html'>GNU Public License versión 2 (GPLv2)</a>
 */
class Family extends Model
{
    /**
     * Lista de atributos que pueden ser asignados masivamente
     *
     * @var array $fillable
     */
	protected $fillable = [
		'user_id',
        'school_id',
		'last_name_1',
		'last_name_2',
		'phone',
		'monthly_income',
		'dependent_income',
		'contributors',
		'parish_id',
        'city_id',
		'address'
	];
    /**
     * Método que obtiene el parroquia a la que pertenece la familia
     *
     * @author  Paúl Rojas <paul.rojase@gmail.com>
     * @return object Objeto con los registros relacionados al modelo Family
     */
    public function parish()
    {
        return $this->belongsTo(Parish::class);
    }
    /**
     * Método que obtiene el colegio asociado con la familia
     *
     * @author  Paúl Rojas <paul.rojase@gmail.com>
     * @return object Objeto con los registros relacionados al modelo Family
     */
    public function school()
    {
        return $this->belongsTo(School::class);
    }
    /**
     * Método que obtiene el usuario del sistema asociado a la cuenta familia
     *
     * @author  Paúl Rojas <paul.rojase@gmail.com>
     * @return object Objeto con los registros relacionados al modelo Family
     */
    public function user()
    {
        return $this->belongsTo(User::class);
    }
    /**
     * Método que obtiene los represententanetes asociados a la familia
     *
     * @author  Paúl Rojas <paul.rojase@gmail.com>
     * @return object Objeto con los registros relacionados al modelo Family
     */
    public function representatives()
    {
    	return $this->hasMany(Representative::class);
    }
    /**
     * Método que obtiene los estudiantes asociados a la familia
     *
     * @author  Paúl Rojas <paul.rojase@gmail.com>
     * @return object Objeto con los registros relacionados al modelo Family
     */
    public function students()
    {
        return $this->hasMany(Student::class);
    }

    /**
     * Método que obtiene la Ciudad asociada a una Familia
     *
     * @author  Paúl Rojas <paul.rojase@gmail.com>
     * @return object Objeto con los registros relacionados al modelo City
     */
    public function city()
    {
        return $this->belongsTo(City::class);
    }

    /**
     * Método que obtiene la encuesta asociada a la familia
     *
     * @author  Paúl Rojas <paul.rojase@gmail.com>
     * @return object Objeto con los registros relacionados al modelo Encuesta
     */
    public function poll()
    {
        return $this->hasOne(Poll::class);
    }

    /**
     * Obtiene todas las cuentas bancarias asociadas a la familia
     *
     * @author  William Páez <paez.william8@gmail.com>
     * @return object Objeto con los registros relacionados al modelo BankAccount
     */
    public function bankAccounts()
    {
        return $this->morphMany(BankAccount::class, 'bank_accountable');
    }
}
