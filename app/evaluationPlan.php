<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class evaluationPlan extends Model
{
    protected $fillable = ['period_id', 'section_id', 'percents', 'description', 'fecha'];

    public function evaluation()
    {
        return $this->belongsTo(evaluation::class);
    }
    public function section()
    {
        return $this->hasMany(Section::class);
    }
}
