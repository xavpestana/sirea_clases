<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * @class Enrollment
 * @brief Datos de inscripciones
 *
 * Gestiona el modelo de datos para las inscripciones de estudiantes
 *
 * @author William Páez <paez.william8@gmail.com>
 * @license <a href='​http://www.gnu.org/licenses/gpl-2.0.html'>GNU Public License versión 2 (GPLv2)</a>
 */
class Enrollment extends Model
{
    /**
     * Lista de atributos que pueden ser asignados masivamente
     *
     * @var array $fillable
     */
    protected $fillable = ['student_id', 'section_id', 'plan_id'];

    /**
     * Método que obtiene las inscripciones asociadas a muchos conceptos
     *
     * @author  William Páez <paez.william8@gmail.com>
     * @return object Objeto con los registros relacionados al modelo Concepto
     */
    public function concepts()
    {
        return $this->belongsToMany(Concept::class)->withTimestamps();
    }

    /**
     * Método que obtiene al estudiante asociado a una inscripción
     *
     * @author  William Páez <paez.william8@gmail.com>
     * @return object Objeto con los registros relacionados al modelo Estudiante
     */
    public function student()
    {
        return $this->belongsTo(Student::class);
    }

    /**
     * Método que obtiene la sección asociada a una inscripción
     *
     * @author  William Páez <paez.william8@gmail.com>
     * @return object Objeto con los registros relacionados al modelo Sección
     */
    public function section()
    {
        return $this->belongsTo(Section::class);
    }

    /**
     * Método que obtiene el plan de pago asociado a una inscripción
     *
     * @author  William Páez <paez.william8@gmail.com>
     * @return object Objeto con los registros relacionados al modelo Plan
     */
    public function plan()
    {
        return $this->belongsTo(Plan::class);
    }

    /**
     * Método que obtiene los recibos de pago asociadas a una inscripción
     *
     * @author  William Páez <paez.william8@gmail.com>
     * @return object Objeto con los registros relacionados al modelo Receipt
     */
    public function receipts()
    {
        return $this->hasMany(Receipt::class);
    }
}
