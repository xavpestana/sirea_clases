<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class fileResponse extends Model
{
	protected $fillable = ['task_id', 'url', 'extension'];

    public function responseActivity()
    {
        return $this->belongsTo(responseActivity::class);
    }
}
