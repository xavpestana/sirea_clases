<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Relationship extends Model
{
	protected $fillable = [
        'name'
    ];


	public function representative()
	{
		return $this->hasOne(Representantive::class);
	}
}
