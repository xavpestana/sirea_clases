<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Director extends Model
{
	protected $fillable = [
        'name',
        'document_type',
        'document_number',
        'gender_id',
        'school_id',
    ];

    public function school()
    {
        return $this->belongsTo(School::class);
    }

    public function gender()
    {
        return $this->belongsTo(Gender::class);
    }
}
