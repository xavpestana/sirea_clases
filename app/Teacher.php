<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
/**
 * @class Teacher
 * @brief Datos del docente
 *
 * Gestiona el modelo de datos para Docente
 *
 * @author Paúl Rojas <paul.rojase@gmail.com>
 * @license <a href='​http://www.gnu.org/licenses/gpl-2.0.html'>GNU Public License versión 2 (GPLv2)</a>
 */
class Teacher extends Model
{
	protected $fillable = [
		'profile_id',
		'school_id',
        'city_id',
        'parish_id'
	];
    /**
     * Método que obtiene el perfil asociado con el docente
     *
     * @author  Paúl Rojas <paul.rojase@gmail.com>
     * @return object Objeto con los registros relacionados al modelo Teacher
     */
	public function profile()
	{
		return $this->belongsTo(Profile::class);
	}
    /**
     * Método que obtiene el colegio asociado con el docente
     *
     * @author  Paúl Rojas <paul.rojase@gmail.com>
     * @return object Objeto con los registros relacionados al modelo Teacher
     */
	public function school()
	{
		return $this->belongsTo(School::class);
	}
    /**
     * Método que obtiene la parroquia asociada con el docente
     *
     * @author  Paúl Rojas <paul.rojase@gmail.com>
     * @return object Objeto con los registros relacionados al modelo Teacher
     */
    public function parish()
    {
        return $this->belongsTo(Parish::class);
    }


    /**
     * Método que obtiene la Ciudad asociada a un Docente
     *
     * @author  Paúl Rojas <paul.rojase@gmail.com>
     * @return object Objeto con los registros relacionados al modelo City
     */
    public function city()
    {
        return $this->belongsTo(City::class);
    }
}
