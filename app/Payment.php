<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * @class Payment
 * @brief Datos de pagos
 *
 * Gestiona el modelo de datos para los pagos
 *
 * @author William Páez <paez.william8@gmail.com>
 * @license <a href='​http://www.gnu.org/licenses/gpl-2.0.html'>GNU Public License versión 2 (GPLv2)</a>
 */
class Payment extends Model
{
    /**
     * Lista de atributos que pueden ser asignados masivamente
     *
     * @var array $fillable
     */
    protected $fillable = [
        'student_id',
        'school_id', 
        'number_invoice', 
        'identity', 
        'name', 
        'address',
        'phone', 
        'school_bank_account', 
        'payment_method_id', 
        'amount', 
        'coin', 
        'amount_bsf', 
        'change_amount', 
        'verified', 
        'annulled', 
        'transfer_code'
    ];

    /**
     * Método que obtiene el Pago asociado a un recibo
     *
     * @author  William Páez <paez.william8@gmail.com>
     * @return object Objeto con los registros relacionados al modelo Receipt
     */
    public function receipt()
    {
        return $this->belongsTo(Receipt::class);
    }

    /**
     * Método que obtiene el Pago asociado a un método de pago
     *
     * @author  William Páez <paez.william8@gmail.com>
     * @return object Objeto con los registros relacionados al modelo PaymentMethod
     */
    public function paymentMethod()
    {
        return $this->belongsTo(PaymentMethod::class);
    }
    public function installmentPayment()
    {
        return $this->hasMany(installmentPayment::class);
    }
    public function School()
    {
        return $this->belongsTo(School::class);
    }
    public function Student()
    {
        return $this->belongsTo(Student::class);
    }
}
