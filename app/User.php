<?php

namespace App;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

use Spatie\Permission\Traits\HasRoles;

class User extends Authenticatable
{
    use Notifiable, HasRoles;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password', 'disable'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function family()
    {
        return $this->hasOne(Family::class);
    }

    public function profile()
    {
        return $this->hasOne(Profile::class);
    }

    public function Courses_sections_user()
    {
        return $this->hasMany(Courses_sections_user::class);
    }
    public function comments()
    {
        return $this->hasMany(Comments::class);
    }
    public function teacherNotify()
    {
        return $this->hasMany(teacherNotify::class);
    }
    public function fileResponse()
    {
        return $this->hasOne(fileResponse::class);
    }

    public function sections()
    {
        return $this->belongsToMany('\App\Section', 'section_user')
            ->withPivot('user_id', 'section_id');
    }

    /**
     * Método que obtiene el usuario escolar asociado a un usuario
     *
     * @author  William Páez <paez.william8@gmail.com>
     * @return object Objeto con los registros relacionados al modelo UserSchool
     */
    public function userSchool()
    {
        return $this->hasOne(UserSchool::class);
    }
}
