<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
/**
 * @class Profile
 * @brief Datos del Perfil
 *
 * Gestiona el modelo de datos para Perfil
 *
 * @author Paúl Rojas <paul.rojase@gmail.com>
 * @license <a href='​http://www.gnu.org/licenses/gpl-2.0.html'>GNU Public License versión 2 (GPLv2)</a>
 */

class Profile extends Model
{
    /**
     * Lista de atributos que pueden ser asignados masivamente
     *
     * @var array $fillable
     */    
	protected $fillable = [
		'user_id',
		'document_type',
		'document_number',
		'first_name_1',
		'first_name_2',
		'last_name_1',
		'last_name_2',
		'birthdate',
		'phone',
		'country_id',
		'gender_id',
		'address',
		'avatar'
	];
    /**
     * Método que obtiene el usuario asociado al perfil
     *
     * @author  Paúl Rojas <paul.rojase@gmail.com>
     * @return object Objeto con los registros relacionados al modelo Profile
     */
	public function user()
	{
		return $this->belongsTo(User::class);
	}
    /**
     * Método que obtiene el estudiante asociado con el perfil
     *
     * @author  Paúl Rojas <paul.rojase@gmail.com>
     * @return object Objeto con los registros relacionados al modelo Profile
     */
	public function student()
	{
		return $this->hasOne(Student::class);
	}
    /**
     * Método que obtiene el docente asociado con el perfil
     *
     * @author  Paúl Rojas <paul.rojase@gmail.com>
     * @return object Objeto con los registros relacionados al modelo Profile
     */
	public function teacher()
	{
		return $this->hasOne(Teacher::class);
	}
    /**
     * Método que obtiene el pais asociado con el perfil
     *
     * @author  Paúl Rojas <paul.rojase@gmail.com>
     * @return object Objeto con los registros relacionados al modelo Profile
     */
	public function country()
	{
		return $this->belongsTo(Country::class);
	}
    /**
     * Método que obtiene el genero asociado con el perfil
     *
     * @author  Paúl Rojas <paul.rojase@gmail.com>
     * @return object Objeto con los registros relacionados al modelo Profile
     */
    public function gender()
    {
        return $this->belongsTo(Gender::class);
    }
}
