<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * @class Receipt
 * @brief Datos del recibo de pago
 *
 * Gestiona el modelo de datos para el recibo de pago
 *
 * @author William Páez <paez.william8@gmail.com>
 * @license <a href='​http://www.gnu.org/licenses/gpl-2.0.html'>GNU Public License versión 2 (GPLv2)</a>
 */
class Receipt extends Model
{
    /**
     * Lista de atributos que pueden ser asignados masivamente
     *
     * @var array $fillable
     */
    protected $fillable = ['name', 'amount', 'paid', 'enrollment_id'];

    /**
     * Método que obtiene la inscripción asociado a un recibo de pago
     *
     * @author  William Páez <paez.william8@gmail.com>
     * @return object Objeto con los registros relacionados al modelo Enrollment
     */
    public function enrollment()
    {
        return $this->belongsTo(Enrollment::class);
    }

    /**
     * Método que obtiene los pagos asociados a un Recibo
     *
     * @author  William Páez <paez.william8@gmail.com>
     * @return object Objeto con los registros relacionados al modelo Payment
     */
    public function payments()
    {
        return $this->hasMany(Payment::class);
    }
}
