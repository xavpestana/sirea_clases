<?php

namespace App\Services;

class BloodTypes
{
    public function get()
    {

		$bloodTypes = collect(['AB+', 'AB-', 'A+',  'A-', 'B+',  'B-', 'O+', 'O-']);

		$i=1;
        $bloodTypesArray[''] = 'Selecciona';
        foreach ($bloodTypes as $bloodType) {
            $bloodTypesArray[$i] = $bloodType;
            $i++;
        }
        return $bloodTypesArray;
    }
}
