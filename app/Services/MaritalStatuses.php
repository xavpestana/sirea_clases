<?php

namespace App\Services;

class MaritalStatuses
{
    public function get()
    {

		$maritalStatuses = collect(['Matrimonio civil', 'Matrimonio por la Iglesia', 'Divorciados',  'Concubinato', 'Madre soltera',  'Padre soltero', 'Viuda o viudo']);

		$i=1;
        $maritalStatusesArray[''] = 'Selecciona';
        foreach ($maritalStatuses as $maritalStatus) {
            $maritalStatusesArray[$i] = $maritalStatus;
            $i++;
        }
        return $maritalStatusesArray;
    }
}