<?php

namespace App\Services;

class Transportations
{
    public function get()
    {
		$transportations = collect([
            'Carro propio',
            'A pie',
            'Transporte escolar',
            'Transporte público'
        ]);

		$i=1;
        $transportationsArray[''] = 'Selecciona';
        foreach ($transportations as $transportation) {
            $transportationsArray[$i] = $transportation;
            $i++;
        }
        return $transportationsArray;
    }
}