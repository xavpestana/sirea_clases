<?php

namespace App\Services;

class HomeTypes
{
    public function get()
    {
		$homeTypes = collect([
            'Apartamento',
            'Quinta',
            'Casa familiar',
            'Casa de bloques',
            'Pieza o habitación',
            'Otra'
        ]);

		$i=1;
        $homeTypesArray[''] = 'Selecciona';
        foreach ($homeTypes as $homeTypes) {
            $homeTypesArray[$i] = $homeTypes;
            $i++;
        }
        return $homeTypesArray;
    }
}