<?php

namespace App\Services;

use App\Country;

class Countries
{
    public function get()
    {
        $countries = Country::get();
        $countriesArray[''] = 'Selecciona';
        foreach ($countries as $country) {
            $countriesArray[$country->id] = $country->name;
        }
        return $countriesArray;
    }
}
