<?php

namespace App\Services;

use App\Relationship;

class Relationships
{
    public function get()
    {
        $relationships = Relationship::get();
        $relationshipsArray[''] = 'Selecciona';
        foreach ($relationships as $relationship) {
            $relationshipsArray[$relationship->id] = $relationship->name;
        }
        return $relationshipsArray;
    }
}
