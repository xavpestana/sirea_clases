<?php

namespace App\Services;

class Religions
{
    public function get()
    {
		$religions = collect([
            'Catolica',
            'Protestante',
            'Otra'
        ]);

		$i=1;
        $maritalStatusesArray[''] = 'Selecciona';
        foreach ($religions as $religion) {
            $religionsArray[$i] = $religion;
            $i++;
        }
        return $religionsArray;
    }
}