<?php

namespace App\Services;

class PropertyTypes
{
    public function get()
    {
		$propertyTypes = collect([
            'Propia y pagada',
            'Propia y en proceso de pago',
            'Alquilada',
            'Compartida',
            'Otra'
        ]);

		$i=1;
        $propertyTypesArray[''] = 'Selecciona';
        foreach ($propertyTypes as $propertyTypes) {
            $propertyTypesArray[$i] = $propertyTypes;
            $i++;
        }
        return $propertyTypesArray;
    }
}