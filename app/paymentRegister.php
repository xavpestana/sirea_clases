<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class paymentRegister extends Model
{
    protected $fillable = [
        'student_id',
        'school_id', 
        'family_id', 
        'identity', 
        'name', 
        'address',
        'phone', 
        'school_bank_account', 
        'payment_method_id', 
        'amount', 
        'coin', 
        'transfer_code',
        'observation',
        'status'
    ];


    public function paymentMethod()
    {
        return $this->belongsTo(PaymentMethod::class);
    }
    public function family()
    {
        return $this->belongsTo(Family::class);
    }
    public function School()
    {
        return $this->belongsTo(School::class);
    }
    public function Student()
    {
        return $this->belongsTo(Student::class);
    }
}
