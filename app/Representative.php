<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
/**
 * @class Representative
 * @brief Datos del Representante
 *
 * Gestiona el modelo de datos para Representante
 *
 * @author Paúl Rojas <paul.rojase@gmail.com>
 * @license <a href='​http://www.gnu.org/licenses/gpl-2.0.html'>GNU Public License versión 2 (GPLv2)</a>
 */
class Representative extends Model
{
    /**
     * Lista de atributos que pueden ser asignados masivamente
     *
     * @var array $fillable
     */	
	protected $fillable = [
		'family_id',
		'document_type',
		'document_number',
		'first_name_1',
		'first_name_2',
		'last_name_1',
		'last_name_2',
		'birthdate',
		'phone',
		'country_id',
		'gender_id',
		'avatar',
		'principal',
		'relationship_id',
        'correo'
	];
    /**
     * Método que obtiene la familia asociada con el representante
     *
     * @author  Paúl Rojas <paul.rojase@gmail.com>
     * @return object Objeto con los registros relacionados al modelo Representative
     */
    public function family()
    {
        return $this->belongsTo(Family::class);
    }
    /**
     * Método que obtiene el parentesco asociado con el representante
     *
     * @author  Paúl Rojas <paul.rojase@gmail.com>
     * @return object Objeto con los registros relacionados al modelo Representative
     */
    public function relationship()
	{
		return $this->belongsTo(Relationship::class);
	}
    /**
     * Método que obtiene el genero asociada con el representante
     *
     * @author  Paúl Rojas <paul.rojase@gmail.com>
     * @return object Objeto con los registros relacionados al modelo Representative
     */
	public function gender()
	{
		return $this->belongsTo(Gender::class);
	}

    /**
     * Método que obtiene el pais asociado con el representante
     *
     * @author  Paúl Rojas <paul.rojase@gmail.com>
     * @return object Objeto con los registros relacionados al modelo Representative
     */
    public function country()
    {
        return $this->belongsTo(Country::class);
    }

}
