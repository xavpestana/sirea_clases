<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Gender extends Model
{
	protected $fillable = ['name'];

	public function profiles()
	{
		return $this->hasMany(Profile::class);
	}

	public function directors()
	{
		return $this->hasMany(Director::class);
	}

	public function representative()
	{
		return $this->hasMany(Representative::class);
	}

}
