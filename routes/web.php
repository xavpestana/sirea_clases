<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('principal', function () {
    return view('principal');
});

/**
 * -----------------------------------------------------------------------
 * Ruta para acceder a la página inicial del sistema
 * -----------------------------------------------------------------------
 *
 * @author William Páez <paez.william8@gmail.com>
 * @license <a href='​http://www.gnu.org/licenses/gpl-2.0.html'>GNU Public License versión 2 (GPLv2)</a>
 */
Route::get('/', 'HomeController@index')->name('index');

Auth::routes(['register' => 'false']);

/**
 * -----------------------------------------------------------------------
 * Rutas para desplegar los selects dinamicos de estados, ciudades, municipios y parroquias
 * -----------------------------------------------------------------------
 *
 * @author Paúl Rojas <paul.rojase@gmail.com>
 * @license <a href='​http://www.gnu.org/licenses/gpl-2.0.html'>GNU Public License versión 2 (GPLv2)</a>
 */
Route::get('/cities', 'CityController@getCities');
Route::get('/municipalities', 'MunicipalityController@getMunicipalities');
Route::get('/parishes', 'ParishController@getParishes');

Route::get('/list-states', 'StateController@listStates')->name('list-states');
Route::get('/list-municipalities/{state_id}', 'MunicipalityController@listMunicipalities')->name('list-municipalities');
Route::get('/list-parishes/{municipality_id}', 'ParishController@listParishes')->name('list-parishes');
Route::get('/list-schools/{parish_id}', 'SchoolController@listSchools')->name('list-schools');

Route::resource(
    'pagos/bancos',
    'BankController',
    [
        'names' => [
            'create' => 'banks.create',
            'store' => 'banks.store',
            'show' => 'banks.show',
            'edit' => 'banks.edit',
            'update' => 'banks.update',
            'destroy' => 'banks.destroy'
        ],
        'except' => ['index','create','store','edit','update','destroy'],
    ],
);
Route::get('pagos/bancos/show/vue-list', 'BankController@vueList')->name('banks.vue-list');
Route::get('pagos/list-banks', 'BankController@listBanks')->name('banks.list-banks');

Route::resource(
    'pagos/representantes',
    'PaymentRepresentativeController',
    [
        'names' => [
            'index' => 'paidrepresentative.index',
            'create' => 'paidrepresentative.create',
            'store' => 'paidrepresentative.store',
            'show' => 'paidrepresentative.show',
            'edit' => 'paidrepresentative.edit',
            'update' => 'paidrepresentative.update',
            'destroy' => 'paidrepresentative.destroy'
        ],
    ],
);
Route::get('reporte/pagos/online/', 'ReceiptController@reports')->name('reports.online');
Route::resource(
    'paid/representantes',
    'ReceiptController',
    [
        'names' => [
            'index' => 'paidrepre.index',
            'create' => 'paidrepre.create',
            'store' => 'paidrepre.store',
            'show' => 'paidrepre.show',
            'edit' => 'paidrepre.edit',
            'update' => 'paidrepre.update',
            'destroy' => 'paidrepre.destroy'
        ],
    ],
);

Route::get('list-receipts', 'ReceiptController@listReceipts')->name('Receipts.list-receitps');

Route::get('Biblioteca/estudiantes', 'LibraryController@student')->name('library.student');

Route::get('Biblioteca/docente', 'LibraryController@teacher')->name('library.teacher');

Route::resource(
    'Biblioteca/',
    'LibraryController',
    [
        'names' => [
            'index' => 'library.index',
            'create' => 'library.create',
            'store' => 'library.store',
            'show' => 'library.show',
            'edit' => 'library.edit',
            'update' => 'library.update',
            'destroy' => 'library.destroy'
        ],
    ],
);

Route::resource(
    'pagos/metodo-pagos',
    'PaymentMethodController',
    [
        'names' => [
            'create' => 'payment-methods.create',
            'store' => 'payment-methods.store',
            'show' => 'payment-methods.show',
            'edit' => 'payment-methods.edit',
            'update' => 'payment-methods.update',
            'destroy' => 'payment-methods.destroy'
        ],
        'except' => ['index', 'edit', 'update', 'destroy'],
    ],
);
Route::get('pagos/metodo-pagos/show/vue-list', 'PaymentMethodController@vueList')->name('payment-methods.vue-list');
Route::get(
    'pagos/list-payment-methods',
    'PaymentMethodController@listPaymentMethods'
)->name('payment-methods.list-payment-methods');

Route::get('altenate/list-payments', 'AlternatePaymentController@list')->name('ap.list');
Route::get('alternate/send', 'AlternatePaymentController@send')->name('ap.send');

Route::resource(
    'pagos/recibo',
    'AlternatePaymentController',
    [
        'names' => [
            'index' => 'ap.index',
            'create' => 'ap.create',
            'store' => 'ap.store',
            'show' => 'ap.show',
            'edit' => 'ap.edit',
            'update' => 'ap.update',
            'destroy' => 'ap.destroy'
        ],
        
    ],
);

Route::get('list-bank-accounts', 'BankAccountController@listBankAccounts')->name('bank-accounts.list-bank-accounts');

Route::get('enviar/facturas', 'InvoiceController@send')->name('invoice.send');
Route::get('print/factura/{dimension}/{id}', 'InvoiceController@print')->name('invoice.print');

Route::resource(
    'facturas',
    'InvoiceController',
    [
        'names' => [
            'create' => 'invoice.create',
            'store' => 'invoice.store',
            'show' => 'invoice.show',
            'edit' => 'invoice.edit',
            'update' => 'invoice.update',
            'destroy' => 'invoice.destroy'
        ],
    ],
);
Route::post('defaulters/send', 'DefaultersController@send')->name('defaulters.send');
Route::get('defaulters/list', 'DefaultersController@list')->name('defaulters.list');
Route::resource(
    'defaulters',
    'DefaultersController',
    [
        'names' => [
            'index' => 'defaulter.index',
            'create' => 'defaulter.create',
            'store' => 'defaulter.store',
            'show' => 'defaulter.show',
            'edit' => 'defaulter.edit',
            'update' => 'defaulter.update',
            'destroy' => 'defaulter.destroy'
        ],
    ],
);
Route::get('Reportes', 'ReportsController@index')->name('reports.index');
Route::get('Reportes/table', 'ReportsController@table')->name('reports.table');
Route::get('Anulados', 'ReportsController@nullable')->name('reports.nullable');
Route::get('Reporte/pagos', 'ReportsController@report_paid')->name('reports.report_paid');
Route::get('conteo/pagos/{installments}', 'ReportsController@conteo')->name('reports.conteo');
/**
 * -----------------------------------------------------------------------
 * Grupo de rutas establecidas en el namespace Auth
 * -----------------------------------------------------------------------
 *
 * @author William Páez <paez.william8@gmail.com>
 * @license <a href='​http://www.gnu.org/licenses/gpl-2.0.html'>GNU Public License versión 2 (GPLv2)</a>
 */
Route::group(['middleware' => ['auth', 'verified'], 'namespace' => 'Auth'], function () {
    Route::resource(
        'usuarios',
        'UserController',
        [
            'names' => [
                'index' => 'users.index',
                'create' => 'users.create',
                'store' => 'users.store',
                'show' => 'users.show',
                'edit' => 'users.edit',
                'update' => 'users.update',
                'destroy' => 'users.destroy'
            ]
        ],
    );
    Route::get('usuarios/show/vue-list', 'UserController@vueList')->name('users.vue-list');
    Route::get('get-json-roles', 'UserController@getJsonRoles')->name('users.get-json-roles');
    Route::get('get-roles', 'UserController@getRoles')->name('users.get-roles');
});

/**
 * -----------------------------------------------------------------------
 * Ruta para acceder a la página inicial del sistema
 * -----------------------------------------------------------------------
 *
 * @author William Páez <paez.william8@gmail.com>
 * @license <a href='​http://www.gnu.org/licenses/gpl-2.0.html'>GNU Public License versión 2 (GPLv2)</a>
 */
Route::get('/home', 'HomeController@index')->name('home');

/**
 * -----------------------------------------------------------------------
 * Grupo de rutas de acceso exclusivo para el usuario administrador
 * -----------------------------------------------------------------------
 *
 * @author William Páez <paez.william8@gmail.com>
 * @license <a href='​http://www.gnu.org/licenses/gpl-2.0.html'>GNU Public License versión 2 (GPLv2)</a>
 */
Route::group(['middleware' => ['web', 'auth', 'role:admin'], 'prefix' => 'admin'], function () {
    Route::resource(
        'configuraciones',
        'Admin\SettingController',
        [
            'names' => ['index' => 'settings.index'],
            'except' => ['create','store','edit','update','show','destroy'],
        ],
    );

    Route::resource(
        'colegios',
        'SchoolController',
        ['names' => [
            'index' => 'schools.index',
            'edit' =>'schools.edit',
            'create' => 'schools.create',
            'store' => 'schools.store',
            'update' => 'schools.update'
            ]
        ],
        ['except' => ['show','destroy']]
    );

    Route::get('usuarios', 'Auth\UserController@getAllUsers')->name('admin.all-users');
    Route::get('usuarios/operadores', 'Auth\UserController@getOperatorUsers')->name('admin.operator-users');
    Route::get('usuarios/escolares', 'Auth\UserController@getSchoolUsers')->name('admin.school-users');
    Route::get('usuarios/familias', 'Auth\UserController@getFamilyUsers')->name('admin.family-users');
    Route::get('usuarios/estudiantes', 'Auth\UserController@getStudentUsers')->name('admin.student-users');
    Route::get('usuarios/docentes', 'Auth\UserController@getTeacherUsers')->name('admin.teacher-users');

    Route::get('agregar-familias-masivamente', 'FamilyController@addFamiliesMass')->name('add-families-mass');
    Route::post('crear-familias-masivamente', 'FamilyController@createFamiliesMass')->name('create-families-mass');
});

/**
 * -----------------------------------------------------------------------
 * Grupo de rutas de acceso exclusivo para el usuario colegio
 * -----------------------------------------------------------------------
 *
 * @author Paúl Rojas <paul.rojase@gmail.com>
 * @license <a href='​http://www.gnu.org/licenses/gpl-2.0.html'>GNU Public License versión 2 (GPLv2)</a>
 */
Route::group(['middleware' => ['web', 'auth', 'role:school'], 'prefix' => 'colegio'], function () {
    Route::resource(
        'familias',
        'FamilyController',
        ['names' => [
            'index' => 'families.index',
            'store' => 'families.store',
            'edit' => 'school.families.edit',
            'update' => 'school.families.update',
            'destroy' => 'families.destroy'
            ]
        ],
        ['except' => ['create','show']]
    );
    Route::resource(
        'estudiantes',
        'StudentController',
        ['names' => [
            'index' => 'school.students.index',
            'edit' =>   'school.students.edit',
            'update' => 'school.students.update'
            ]
        ],
        ['except' => ['create','store','show','destroy']]
    );
    Route::get('list-students', 'StudentController@listStudents')->name('students.list-students');

    Route::resource(
        'representantes',
        'RepresentativeController',
        ['names' => [
            'index' => 'school.representatives.index',
            'edit' => 'school.representatives.edit',
            'update' => 'school.representatives.update'
            ]
        ],
        ['except' => ['create','store','show','destroy']]
    );
    Route::resource(
        'usuarios',
        'UserSchoolController',
        [
            'names' => [
                'index' => 'user-schools.index',
                'create' => 'user-schools.create',
                'store' => 'user-schools.store',
                'show' => 'user-schools.show',
            ],
            'except' => ['edit','update','destroy'],
        ],
    );
    Route::get('usuarios/show/vue-list', 'UserSchoolController@vueList')->name('user-schools.vue-list');

    Route::resource(
        'colegio',
        'SchoolController',
        [
            'names' => [
                'edit' => 'school.edit',
                'update' => 'school.update',
            ],
            'except' => ['index','create','store','show','destroy'],
        ],
    );

    Route::resource(
        'director',
        'DirectorController',
        [
            'names' => [
                'create' => 'director.create',
                'store' => 'director.store',
                'edit' => 'director.edit',
                'update' => 'director.update',
                'destroy' => 'director.destroy'
            ],
            'except' => ['index'],
        ],
    );

    Route::resource(
        'ano-escolar',
        'SeasonController',
        [
            'names' => [
                'create' => 'season.create',
                'store' => 'season.store',
                'edit' => 'season.edit',
                'update' => 'season.update',
            ],
            'except' => ['index','destroy'],
        ],
    );
    Route::get('list-seasons', 'SeasonController@listSeasons')->name('seasons.list-seasons');

    Route::resource(
        'periodo',
        'PeriodController',
        [
            'names' => [
                'update' => 'period.update'
            ],
            'except' => ['index','create', 'store', 'edit','destroy'],
        ],
    );
    Route::resource(
        'seccion',
        'SectionController',
        [
            'names' => [
                'store' => 'section.store'
            ],
            'except' => ['index','create','edit','update','destroy'],
        ],
    );
    Route::get('list-sections/{grade_id}', 'SectionController@listSections')->name('sections.list-sections');

    Route::get('configuraciones', 'UserSchoolController@settings')->name('school.settings');

    Route::get('list-levels', 'LevelController@listLevels')->name('levels.list-levels');

    Route::get('list-grades/{level_id}', 'GradeController@listGrades')->name('grades.list-levels');

    Route::get('familias/change-password/{id}','FamilyController@changePassword');    

    Route::get('no-inscritos', 'EnrollmentController@noEnrollments')->name('no-enrollments');

    Route::post('inscribir', 'EnrollmentController@inscribir')->name('inscribir');

    Route::get('nuevo-mensaje', 'MessageController@newMessage')->name('new-message');

    Route::post('enviar-mensaje', 'MessageController@sendMessage')->name('send-message');

    Route::get('/excel', 'EnrollmentController@enrollmentsExcel')->name('enrollments-excel');

    Route::post('change-school', 'SchoolController@changeSchool')->name('change-school');

/*****Rutas para bloquear usuarios****/

Route::resource(
        'lock',
        'LockController',
        ['names' =>
        ['index' => 'lock.index']],
        ['except' => ['create','store','edit','update','show','destroy']]
    );
/****Rutas para facturacion****/
Route::resource(
        'cuentas',
        'BankAccountController',
        [
            'names' => [
                'create' => 'accountsBank.create',
                'store' => 'accountsBank.store',
                'show' => 'accountsBank.show',
                'edit' => 'accountsBank.edit',
                'update' => 'accountsBank.update',
                'destroy' => 'accountsBank.destroy',
            ],
            'except' => ['index'],
        ],
    );
});

//Rutas para planillas
Route::get('planilla/{student_id}', 'PDFController@planilla')->name('pdf.planilla');
Route::get('student-carnet/{student_id}', 'PDFController@studentCarnet')->name('pdf.student-carnet');
Route::get('student-constancia/{enrollment_id}', 'PDFController@constancia')->name('pdf.student-constancia');
Route::get('bc', 'PDFController@bc')->name('bc');

/**
 * -----------------------------------------------------------------------
 * Grupo de rutas de acceso exclusivo para el usuario familia
 * -----------------------------------------------------------------------
 *
 * @author Paúl Rojas <paul.rojase@gmail.com>
 * @license <a href='​http://www.gnu.org/licenses/gpl-2.0.html'>GNU Public License versión 2 (GPLv2)</a>
 */
Route::group(['middleware' => ['web', 'auth', 'role:family'], 'prefix' => 'familia'], function () {
    Route::resource(
        'familias',
        'FamilyController',
        ['names' => [
            'edit' => 'families.edit',
            'update' => 'families.update'
            ]
        ],
        ['except' => ['index', 'create','store','show','destroy']]
    );
    Route::resource(
        'estudiantes',
        'StudentController',
        ['names' => [
            'index' => 'students.index',
            'create' => 'students.create',
            'store' => 'students.store',
            'edit' => 'students.edit',
            'update' => 'students.update',
            'destroy' => 'students.destroy'
            ]
        ],
        ['except' => ['show']]
    );

    Route::resource(
        'representantes',
        'RepresentativeController',
        ['names' => [
            'index' => 'representatives.index',
            'create' => 'representatives.create',
            'store' => 'representatives.store',
            'edit' => 'representatives.edit',
            'update' => 'representatives.update',
            'destroy' => 'representatives.destroy',
            ]
        ],
        ['except' => ['show']]
    );

    /*Route::resource(
        'cuentas-bancarias',
        'FamilyBankAccountController',
        [
            'names' => [
                'index' => 'family-bank-accounts.index',
                'create' => 'family-bank-accounts.create',
                'store' => 'family-bank-accounts.store',
                'show' => 'family-bank-accounts.show',
                'edit' => 'family-bank-accounts.edit',
                'update' => 'family-bank-accounts.update',
                'destroy' => 'family-bank-accounts.destroy',
            ],
        ],
    );*/
    Route::get(
        'cuentas-bancarias/show/vue-list',
        'FamilyBankAccountController@vueList'
    )->name('family-bank-accounts.vue-list');
    Route::get(
        'list-family-bank-accounts',
        'FamilyBankAccountController@listFamilyBankAccounts'
    )->name('family-bank-accounts.list-family-bank-accounts');

    /*Route::resource(
        'pagos',
        'FamilyPaymentController',
        [
            'names' => [
                'index' => 'family-payments.index',
                'create' => 'family-payments.create',
                'store' => 'family-payments.store',
                'show' => 'family-payments.show',
                'edit' => 'family-payments.edit',
                'update' => 'family-payments.update',
                'destroy' => 'family-payments.destroy',
            ],
            'except' => ['edit', 'update', 'destroy'],
        ],
    );*/
    Route::get(
        'pagos/show/vue-list',
        'FamilyPaymentController@vueList'
    )->name('family-payment.vue-list');



    //Route::get('docentes', 'UserController@usersTeachers')->name('teachers.index');
});

/**
 * -----------------------------------------------------------------------
 * Grupo de rutas de acceso exclusivo para el usuario colegio - docentes - materias
 * -----------------------------------------------------------------------
 *
 * @author Xavier Pestana <xpestana4@gmail.com>
 * @license <a href='​http://www.gnu.org/licenses/gpl-2.0.html'>GNU Public License versión 2 (GPLv2)</a>
 */
Route::group(['middleware' => ['web', 'auth', 'role:school'], 'prefix' => 'colegio'], function () {
    Route::get('show/teachers', 'TeacherController@table')->name('teachers.table');
    Route::post('edit/teachers/psw', 'TeacherController@psw')->name('teachers.psw');
    Route::resource(
        'teachers',
        'TeacherController',
        ['names' =>
        ['index' => 'teachers.index']],
        ['except' => ['create','store','edit','update','show','destroy']]
    );
    Route::get('show/courses', 'CourseController@table')->name('courses.table');
    Route::get('show/sections', 'CourseController@sections')->name('courses.sections');
    Route::resource(
        'courses',
        'CourseController',
        ['names' =>
        ['index' => 'courses.index']],
        ['except' => ['create','store','edit','update','show','destroy']]
    );
});

/**
 * -----------------------------------------------------------------------
 * Grupo de rutas de acceso exclusivo para el usuario docente
 * -----------------------------------------------------------------------
 *
 * @author Xavier Pestana <xpestana4@gmail.com>
 * @license <a href='​http://www.gnu.org/licenses/gpl-2.0.html'>GNU Public License versión 2 (GPLv2)</a>
 */


Route::group(['middleware' => ['web', 'auth', 'role:teacher|student'], 'prefix' => 'docente'], function () {
    Route::get('profile', 'TaskController@profile')->name('materias.profile');
    Route::post('save', 'TaskController@save')->name('tasks.save');
    Route::get('showcomment', 'TaskController@showcomment')->name('showcomment');
    Route::delete('delete_comment', 'TaskController@delete_comment')->name('delete.comment');
    Route::post('comment', 'TaskController@comment')->name('comment');
    Route::post('image/avatar', 'TeacherController@avatar');
    Route::post('update', 'TeacherController@profile')->name('profile');
    Route::get('students/{id}', 'TaskController@students')->name('tasks.students');
    Route::get('messages/{id}', 'TaskController@messages')->name('tasks.messages');
    Route::get('showresponse', 'TaskController@showresponse')->name('showresponse');
    Route::get('destroy', 'StudentActivitiesController@destroy')->name('studentActivity.delete');
    Route::resource(
        'tasks',
        'TaskController',
        ['names' =>  ['index' => 'tasks.index']],
        ['except' => ['create','store','edit','update','show','destroy']]
    );
    Route::get('search', 'ChatController@search')->name('chat.search');
    Route::get('show/students', 'ChatController@students')->name('chat.students');
    Route::get('show/side', 'ChatController@sidebar_message')->name('chat.sidebar');
    Route::get('show/count', 'ChatController@count_message')->name('chat.count');
    Route::resource(
        'chat',
        'ChatController',
        ['names' =>  ['index' => 'chat.index']],
        ['except' => ['create','store','edit','update','show','destroy']]
    );
});
Route::group(['middleware' => ['web', 'auth', 'role:teacher'], 'prefix' => 'docente'], function () {

    Route::resource(
        'evaluations',
        'evaluationController',
        ['names' =>  ['index' => 'evaluations.index']],
        ['except' => ['create','store','edit','update','show','destroy']]
    );
    Route::resource(
        'evaluationPlan',
        'evaluationPlanController',
        ['names' =>  ['index' => 'evaluationPlan.index']],
        ['except' => ['create','store','edit','update','show','destroy']]
    );
    Route::resource(
        'evaluacion',
        'evPrimaryController',
        ['names' =>  ['index' => 'evaluacion.index']],
        ['except' => ['create','store','edit','update','show','destroy']]
    );
    Route::resource(
        'evaluacionPlan',
        'evPLanPrimaryController',
        ['names' =>  ['index' => 'evaluacionPlan.index']],
        ['except' => ['create','store','edit','update','show','destroy']]
    );
    });
/**
 * -----------------------------------------------------------------------
 * Grupo de rutas de acceso exclusivo para el usuario estudiante
 * -----------------------------------------------------------------------
 *
 * @author Xavier Pestana <xpestana4@gmail.com>
 * @license <a href='​http://www.gnu.org/licenses/gpl-2.0.html'>GNU Public License versión 2 (GPLv2)</a>
 */


Route::group(['middleware' => ['web', 'auth', 'role:student'], 'prefix' => 'activities'], function () {

   Route::get('response', 'StudentActivitiesController@response')->name('studentActivity.response');
    Route::get('published', 'StudentActivitiesController@published')->name('studentActivity.published');
   Route::get('task', 'StudentActivitiesController@activities')->name('studentActivity.activities');

   Route::resource(
        'student',
        'StudentActivitiesController',
        ['names' =>  [
            'index' => 'studentActivity.index',
            'show' => 'studentActivity.show',
        ]],
        ['except' => ['create','store','edit','update','show','destroy']]
    );

});
/**
 * -----------------------------------------------------------------------
 * Grupo de rutas de acceso exclusivo para el desarrollo de APISRESTfull
 * -----------------------------------------------------------------------
 *
 * @author Xavier Pestana <xpestana4@gmail.com>
 * @license <a href='​http://www.gnu.org/licenses/gpl-2.0.html'>GNU Public License versión 2 (GPLv2)</a>
 */

Route::get('get/families/{school}', 'ApiController@families');
Route::get('get/representatives/{school}', 'ApiController@representatives');
Route::get('get/students/{school}', 'ApiController@students');
Route::get('get/sesion/{school}/{document}', 'ApiController@sesion');



/**
 * -----------------------------------------------------------------------
 * Grupo de rutas de acceso para el usuario escolar
 * -----------------------------------------------------------------------
 *
 * @author William Páez <paez.william8@gmail.com>
 * @license <a href='​http://www.gnu.org/licenses/gpl-2.0.html'>GNU Public License versión 2 (GPLv2)</a>
 */
Route::group(['middleware' => ['web', 'auth', 'role:school'], 'prefix' => 'pagos'], function () {
       
    Route::post('settings/iva', 'PaymentSettingController@iva')->name('payment-settings.iva');
    Route::post('settings/headers', 'PaymentSettingController@headers')->name('payment-settings.headers');
    Route::post('settings/simple', 'PaymentSettingController@simple')->name('payment-settings.simple');
    
    Route::resource(
        'configuraciones',
        'PaymentSettingController',
        [
            'names' => ['index' => 'payment-settings.index'],
            'except' => ['create','store','edit','update','show','destroy'],
        ],
    );

    Route::resource(
        'conceptos',
        'ConceptController',
        [
            'names' => [
                'index' => 'concepts.index',
                'create' => 'concepts.create',
                'store' => 'concepts.store',
                'show' => 'concepts.show',
                'edit' => 'concepts.edit',
                'update' => 'concepts.update',
                'destroy' => 'concepts.destroy'
            ],
            
        ],
    );

    Route::post('cookies', 'CookieController@save')->name('coin.save');
    Route::resource(
        'planes',
        'PlanController',
        [
            'names' => [
                'index' => 'plans.index',
                'create' => 'plans.create',
                'store' => 'plans.store',
                'show' => 'plans.show',
                'edit' => 'plans.edit',
                'update' => 'plans.update',
                'destroy' => 'plans.destroy'
            ],
        ],
    );


    Route::resource(
        'installments',
        'InstallmentController',
        [
            'names' => [
                'index' => 'installments.index',
                'create' => 'installments.create',
                'store' => 'installments.store',
                'show' => 'installments.show',
                'edit' => 'installments.edit',
                'update' => 'installments.update',
                'destroy' => 'installments.destroy'
            ],
        ],
    );

    Route::resource(
        'inscripciones',
        'EnrollmentController',
        [
            'names' => [
                'index' => 'enrollments.index',
                'create' => 'enrollments.create',
                'store' => 'enrollments.store',
                'show' => 'enrollments.show',
                'edit' => 'enrollments.edit',
                'update' => 'enrollments.update',
                'destroy' => 'enrollments.destroy'
            ],
        ],
    );
    Route::get('inscripciones/show/vue-list', 'EnrollmentController@vueList')->name('enrollments.vue-list');
    Route::get('list-enrollments', 'EnrollmentController@listEnrollments')->name('enrollments.list-enrollments');

Route::get('list/installments', 'PaymentController@installments')->name('payments.installments');
Route::get('student', 'PaymentController@liststudents')->name('payments.list_students');

    Route::resource(
        'facturar',
        'PaymentController',
        [
            'names' => [
                'index' => 'payments.index',
                'create' => 'payments.create',
                'store' => 'payments.store',
                'show' => 'payments.show',
                'edit' => 'payments.edit',
                'update' => 'payments.update',
                'destroy' => 'payments.destroy'
            ],
        ],
    );
    Route::get('show/vue-list', 'PaymentController@vueList')->name('payments.vue-list');
    Route::get('facturar/verificar/{payment_id}', 'PaymentController@verifyPayment')->name('payments.verify-payment');
    Route::get('facturar/anular/{payment_id}', 'PaymentController@annulledPayment')->name('payments.annulled-payment');

    Route::resource(
        'facturar/configuraciones',
        'InvoiceSettingController',
        [
            'names' => ['store' => 'invoice-settings.store'],
            'except' => ['index','create','show','edit','update','destroy'],
        ],
    );
});

//ruta temporal para ver el form
Route::get('form-familia', function () {
    return view('family.edit');
})->name('form-familia');
/*Route::group(['prefix' => 'familia', 'middleware' => ['role:family']], function () {
    //rutas accesibles solo para usuarios familia
    Route::get('/', function(){
        return view('family.index');
    })->name('familia');
});


Route::group(['prefix' => 'estudiante', 'middleware' => ['role:student']], function () {
    //rutas accesibles solo para usuarios estudiantes
    Route::get('/', function(){
        return view('student.index');
    })->name('estudiante');
});

Route::group(['prefix' => 'docente', 'middleware' => ['role:teacher']], function () {
    //rutas accesibles solo para usuarios docentes
    Route::get('/', function(){
        return view('teacher.index');
    })->name('docente');
});

*/

/*    Route::get('familias', 'FamiliaController@index')->name('familias');
    Route::get('representantes', 'Representanteontroller@index')->name('representantes');
    Route::get('estudiantes', 'EstudianteController@index')->name('estudiantes');
    Route::get('docentes', 'DocenteController@index')->name('docentes');

    Route::get('matriculados', 'EstudianteController@matriculados')->name('matriculados');
    Route::get('no-matriculados', 'EstudianteController@pendientes')->name('no-matriculados');

    Route::get('areas', 'AreaController@index');
    Route::get('asignaciones', 'AsignacionController@index');
    Route::get('informes-descriptivos', 'NotaController@informesDescriptivos');

    Route::get('pagos', 'PagoController@index');
    Route::get('registrar-pago', 'PagoController@nuevoPago');
    Route::get('usuarios-bloqueados', 'UserController@bloqueados');

    Route::get('coutas', 'CoutaController@index');
    Route::get('periodo-actual', 'PeriodoController@actual');
    Route::get('docentes-inhabilitados', 'DocenteController@inhabilitados');
    Route::get('usuarios', 'UserController@index');*/
/*});*/

/*Route::group(['middleware' => ['role:familia']], function () {
    //rutas accesibles solo para familias
    Route::get('datos-basicos', 'FamiliaController@formulario');
    Route::get('representantes', 'RepresentanteController@familiaRepresentantes');
    Route::get('nuevo-representante', 'RepresentanteController@formulario');
    Route::get('estudiantes', 'RepresentanteController@familiaEstudiantes');
    Route::get('nuevo-estudiante', 'EstudianteController@formulario');

    Route::get('reporte/plantilla', 'RepresentanteController@crearPlanilla');
    Route::get('reporte/carnet', 'RepresentanteController@familiaRepresentantes');
    Route::get('reporte/constancia', 'RepresentanteController@familiaRepresentantes');

});*/
